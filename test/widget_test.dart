// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:otcpay/bloc/auth/authenticate_bloc.dart';
import 'package:otcpay/bloc/general/general_bloc.dart';
import 'package:otcpay/bloc/hot/hot.dart';
import 'package:otcpay/bloc/otc/otc.dart';
import 'package:otcpay/bloc/proof/proof_bloc.dart';
import 'package:otcpay/component/Input.dart';
import 'package:otcpay/screen/LoginPage.dart';
import 'package:otcpay/screen/NotificationPage.dart';
import 'package:otcpay/screen/OnboardPage.dart';

const double swipeOffset = 300;

Widget createWidgetForTesting({Widget child}) {
  return MultiBlocProvider(providers: [
    BlocProvider<AuthenticateBloc>(create: (context) => AuthenticateBloc()),
    BlocProvider<ProofBloc>(create: (context) => ProofBloc()),
    BlocProvider<GeneralBloc>(create: (context) => GeneralBloc()),
    BlocProvider<OtcBloc>(create: (context) => OtcBloc()),
    BlocProvider<HotBloc>(create: (context) => HotBloc()),
  ], child: MaterialApp(home: child));
}

void main() {
  testWidgets('Onboard page smoke test', (WidgetTester tester) async {
    await tester.pumpWidget(createWidgetForTesting(child: OnBoardingPage()));

    expect(find.text('Cost Saving'), findsOneWidget);
    expect(find.text('save 2.68% per global remittance'), findsOneWidget);

    TestGesture gesture = await tester.startGesture(
        Offset(swipeOffset, swipeOffset)); //Position of the scrollview
    await gesture.moveBy(Offset(-swipeOffset, 0)); //How much to scroll by
    await tester.pumpAndSettle();

    expect(find.text('Fastest Settlement'), findsOneWidget);
    expect(find.text('Delivery international payments within the same day.'),
        findsOneWidget);

    gesture = await tester.startGesture(
        Offset(swipeOffset, swipeOffset)); //Position of the scrollview
    await gesture.moveBy(Offset(-swipeOffset * 2, 0)); //How much to scroll by
    await tester.pumpAndSettle();

    expect(find.text('Secured Daily Turnover'), findsOneWidget);
    expect(find.text('Reached daily international transaction of HKD 25mio+'),
        findsOneWidget);
  });

  testWidgets('Login page smoke test', (WidgetTester tester) async {
    await tester.pumpWidget(createWidgetForTesting(child: LoginPage()));

    expect(find.text('OTCPAY'), findsOneWidget);
    expect(find.byType(InputTitle), findsNWidgets(2));
    expect(find.byType(InputBox), findsNWidgets(2));
    expect(find.byIcon(Icons.check_circle_outline_outlined), findsOneWidget);
    expect(find.byType(ConfirmButton), findsOneWidget);
    expect(tester.widget<ConfirmButton>(find.byType(ConfirmButton)).validator(),
        false);
    expect(find.text('loginPage.forgotPassword?'), findsOneWidget);
  });

  testWidgets('Notification page smoke test', (WidgetTester tester) async {
    await tester.pumpWidget(createWidgetForTesting(child: NotificationPage()));

    expect(find.byType(TextButton), findsOneWidget);
  });
}
