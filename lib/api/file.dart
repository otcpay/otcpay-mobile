import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:otcpay/model/FIleResponse.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'constant.dart';

Future<String> uploadFile(String path, String type) async {
  if (path == null || path == '') return '';
  if (path.contains('https://www.googleapis.com/storage/v1/b/otcpay'))
    return path;

  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
  };

  var uri = Uri.parse('https://$server/api/v1/file');
  var request = http.MultipartRequest('POST', uri)
    ..headers.addAll(headers)
    ..fields.addEntries([new MapEntry('type', type)])
    ..files.add(await http.MultipartFile.fromPath('file', path));

  var response = await request.send();
  var responseData = await response.stream.toBytes();
  String responseString = String.fromCharCodes(responseData);
  Map<String, dynamic> json = jsonDecode(responseString);
  if (response.statusCode == 200) {
    FileResponse response = FileResponse.fromJson(json);
    return response.selfLink;
  }

  print(responseString);
  throw (json['message']);
}
