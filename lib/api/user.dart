import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:otcpay/model/AuthenticationDetail.dart';
import 'package:otcpay/model/Profile.dart';
import 'package:otcpay/model/User.dart';
import 'package:otcpay/model/Notification.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'constant.dart';
import 'file.dart';

Future<AuthenticationDetail> authenticate(
    String username, String password) async {
  const headers = {
    'Authorization': 'Bearer token',
    'Content-Type': 'application/json'
  };
  Map<String, String> body = {'username': username, 'password': password};
  var response = await http.post(Uri.https(server, '/api/authenticate'),
      headers: headers, body: jsonEncode(body));
  Map<String, dynamic> json = jsonDecode(response.body);
  if (response.statusCode == 200) {
    return AuthenticationDetail.fromJson(json);
  } else if (response.statusCode == 401) {
    throw ('Wrong Password, please enter again');
  } else if (response.statusCode == 404) {
    throw ('User not found');
  }

  throw (json['message']);
}

Future<bool> checkAccount(String username) async {
  const headers = {
    'Authorization': 'Bearer token',
  };
  var response = await http.get(
      Uri.https(server, '/api/v1/user/account', {'username': username}),
      headers: headers);
  Map<String, dynamic> json = jsonDecode(response.body);
  return json['result'];
}

Future<AuthenticationDetail> register(User user) async {
  const headers = {
    'Authorization': 'Bearer token',
    'Content-Type': 'application/json'
  };
  var response = await http.post(Uri.https(server, '/api/v1/users/register'),
      headers: headers, body: jsonEncode(user));
  Map<String, dynamic> json = jsonDecode(response.body);
  if (response.statusCode == 200) {
    return AuthenticationDetail.fromJson(json);
  } else if (response.statusCode == 409) {
    throw ('Account has been registered, login now?');
  }

  throw (json['message']);
}

Future<ValidateDetail> validateToken(String token) async {
  const headers = {
    'Authorization': 'Bearer token',
    'Content-Type': 'application/json',
  };
  Map<String, String> body = {
    'jwttoken': token,
  };

  var response = await http.post(Uri.https(server, '/api/validate'),
      headers: headers, body: jsonEncode(body));
  Map<String, dynamic> json = jsonDecode(response.body);
  if (response.statusCode == 200) {
    return ValidateDetail.fromJson(json);
  }

  throw ('Session expired, please login again');
}

Future<bool> resetPassword(String username, String password) async {
  const headers = {
    'Authorization': 'Bearer token',
    'Content-Type': 'application/json',
  };
  Map<String, String> body = {
    'account': username,
    'password': password,
  };

  var response = await http.put(
      Uri.https(server, '/api/v1/users/resetPassword'),
      headers: headers,
      body: jsonEncode(body));
  Map<String, dynamic> json = jsonDecode(response.body);
  if (response.statusCode == 200) {
    return json['result'];
  }

  throw ('Failed to reset password, please try again later');
}

Future<bool> updateUser(String userId, Profile profile) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  String newProfilePath = await uploadFile(profile.profilePic, 'profile');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json',
  };
  Map<String, String> body = {
    'username': profile.username,
    'region': profile.region,
    'whatsappId': profile.whatsappId,
    'wechatId': profile.wechatId,
    'telegramId': profile.telegramId,
    'profilePic': newProfilePath,
  };

  var response = await http.put(Uri.https(server, '/api/v1/users/' + userId),
      headers: headers, body: jsonEncode(body));
  Map<String, dynamic> json = jsonDecode(response.body);
  if (response.statusCode == 200) {
    return json['result'];
  }

  throw ('Failed to update profile, please try again later');
}

Future<bool> updateToken(String userId, String token) async {
  if (userId == null || userId == '') return false;

  try {
    const headers = {
      'Authorization': 'Bearer token',
      'Content-Type': 'application/json',
    };
    Map<String, String> body = {'notificationToken': token};

    var response = await http.put(Uri.https(server, '/api/v1/users/' + userId),
        headers: headers, body: jsonEncode(body));
    Map<String, dynamic> json = jsonDecode(response.body);
    if (response.statusCode == 200) {
      return json['result'];
    }
  } catch (e) {
    print('Failed to update notification token');
    print(e);
  }

  return false;
}

Future<bool> readAllNotification(String userId) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json',
  };

  var response = await http.post(
      Uri.https(server, '/api/v1/notifications/read/' + userId),
      headers: headers);
  return response.statusCode == 200;
}

Future<List<Notification>> fetchUserNotification(String userId) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json',
  };

  var response = await http.get(
      Uri.https(server, '/api/v1/notifications/' + userId),
      headers: headers);
  List<dynamic> json = jsonDecode(response.body);
  List<Notification> result = [];
  for (Map<String, dynamic> object in json) {
    result.add(Notification.fromJson(object));
  }
  return result;
}
