import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:otcpay/model/Profile.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'constant.dart';

Future<Profile> getProfile(String userId) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  var response = await http.get(Uri.https(server, '/api/v1/users/$userId'),
      headers: headers);
  Map<String, dynamic> json = jsonDecode(utf8.decode(response.bodyBytes));
  return Profile.fromJson(json);
}

Future<bool> updateProfile(String userId, Profile profile) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  Map<String, String> idProof = {
    'image': profile.idProof != null ? profile.idProof.image : '',
    'detail': profile.idProof != null ? profile.idProof.detail : ''
  };
  Map<String, String> addressProof = {
    'image': profile.addressProof != null ? profile.addressProof.image : '',
    'detail': profile.addressProof != null ? profile.addressProof.detail : ''
  };
  var body = {
    'idProof': idProof,
    'addressProof': addressProof,
  };
  var response = await http.put(Uri.https(server, '/api/v1/users/$userId'),
      headers: headers, body: jsonEncode(body));
  Map<String, dynamic> json = jsonDecode(response.body);
  if (response.statusCode == 200) {
    return json['result'];
  }

  print(response.body);
  throw ('Failed to update profile, please try again later');
}

Future<bool> updateWallets(String userId, List<Wallet> wallets) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  var body = {
    'wallets': wallets,
  };
  var response = await http.put(Uri.https(server, '/api/v1/users/$userId'),
      headers: headers, body: jsonEncode(body));
  Map<String, dynamic> json = jsonDecode(response.body);
  if (response.statusCode == 200) {
    return json['result'];
  }

  print(response.body);
  throw ('Failed to update wallets, please try again later');
}

Future<bool> updateBanks(String userId, List<BankProof> banks) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  var body = {
    'banks': banks,
  };
  var response = await http.put(Uri.https(server, '/api/v1/users/$userId'),
      headers: headers, body: jsonEncode(body));
  Map<String, dynamic> json = jsonDecode(response.body);
  if (response.statusCode == 200) {
    return json['result'];
  }

  print(response.body);
  throw ('Failed to update banks recipients, please try again later');
}

Future<bool> updateDefaultBanks(String userId, int index) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  Map<String, int> body = {
    'defaultBanks': index,
  };
  var response = await http.put(Uri.https(server, '/api/v1/users/$userId'),
      headers: headers, body: jsonEncode(body));
  Map<String, dynamic> json = jsonDecode(response.body);
  if (response.statusCode == 200) {
    return json['result'];
  }

  print(response.body);
  throw ('Failed to update default banks recipients, please try again later');
}

Future<bool> updateDefaultWallets(String userId, int index) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  Map<String, int> body = {
    'defaultWallets': index,
  };
  var response = await http.put(Uri.https(server, '/api/v1/users/$userId'),
      headers: headers, body: jsonEncode(body));
  Map<String, dynamic> json = jsonDecode(response.body);
  if (response.statusCode == 200) {
    return json['result'];
  }

  print(response.body);
  throw ('Failed to update default wallets, please try again later');
}
