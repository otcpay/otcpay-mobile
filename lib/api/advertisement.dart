import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:otcpay/model/Advertisement.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'constant.dart';

Future<List<Advertisement>> getAdvertisement() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  var response = await http.get(
      Uri.https(server, '/api/v1/latest/advertisements'),
      headers: headers);
  List<dynamic> json = jsonDecode(utf8.decode(response.bodyBytes));

  List<Advertisement> result = [];
  for (Map<String, dynamic> object in json) {
    result.add(Advertisement.fromJson(object));
  }
  return result;
}

Future<List<Advertisement>> getPaginatedAdvertisement(String cursor) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  var response = await http.get(
      Uri.https(server, '/api/v1/latest/advertisements', {
        'cursor': cursor,
      }),
      headers: headers);
  List<dynamic> json = jsonDecode(utf8.decode(response.bodyBytes));

  List<Advertisement> result = [];
  for (Map<String, dynamic> object in json) {
    result.add(Advertisement.fromJson(object));
  }
  return result;
}

Future<Advertisement> postAd(Advertisement advertisement) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  var response = await http.post(
      Uri.https(
        server,
        '/api/v1/advertisements',
      ),
      headers: headers,
      body: jsonEncode(advertisement));
  Map<String, dynamic> json = jsonDecode(response.body);
  if (response.statusCode == 200) {
    return Advertisement.fromJson(json);
  }

  print(response.body);
  throw (json['message']);
}

Future<bool> updateAd(String id, Map<String, dynamic> data) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  var response = await http.put(
      Uri.https(server, '/api/v1/advertisements/' + id),
      body: jsonEncode(data),
      headers: headers);
  Map<String, dynamic> json = jsonDecode(response.body);
  if (response.statusCode == 200) {
    return json['result'];
  }

  print(response.body);
  throw ('Failed to delete Ad, please try again later');
}

Future<bool> deleteAd(String id) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  var response = await http.delete(
      Uri.https(server, '/api/v1/advertisements/' + id),
      headers: headers);
  Map<String, dynamic> json = jsonDecode(response.body);
  if (response.statusCode == 200) {
    return json['result'];
  }

  print(response.body);
  throw ('Failed to delete Ad, please try again later');
}

Future<List<Advertisement>> getUserAd(String userId, int page) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  const List<String> fields = [
    'crytocurrency',
    'name',
    'id',
    'type',
    'upperLimit',
    'lowerLimit',
    'settleCurrency',
    'profilePic',
    'offer',
    'updatedDate',
    'status',
    'quantity'
  ];
  var response = await http.get(
      Uri.https(server, '/api/v1/advertisements', {
        'fields': fields.join(','),
        'page': page.toString(),
        'refresh': 'false',
        'size': '100',
        'keywords': userId
      }),
      headers: headers);
  Map<String, dynamic> json = jsonDecode(utf8.decode(response.bodyBytes));

  List<Advertisement> result = [];
  for (Map<String, dynamic> object in json['data']) {
    try {
      result.add(Advertisement.fromJson(object));
    } catch (e, s) {
      if (e is TypeError) {
        print('Failed to parse advertisement');
        print(object);
      } else {
        print('Unknown error during advertisement parse');
        print(s);
      }
    }
  }
  return result;
}
