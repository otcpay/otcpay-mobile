import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:otcpay/model/AdRequest.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'constant.dart';

Future<bool> createRequest(AdRequest request) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  var response = await http.post(
      Uri.https(
        server,
        '/api/v1/buySellRecords',
      ),
      headers: headers,
      body: jsonEncode(request));
  if (response.statusCode == 200) {
    return true;
  }

  print(response.body);
  throw ('Failed to create Ad Request, please try again later');
}

Future<List<AdRequest>> getUserRequest(String userId, int page) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  const List<String> fields = [
    'crytocurrency',
    'name',
    'id',
    'type',
    'amount',
    'offerRate',
    'totalCost',
    'settleCurrency',
    'updatedDate',
    'status'
  ];
  var response = await http.get(
      Uri.https(server, '/api/v1/buySellRecords', {
        'fields': fields.join(','),
        'page': page.toString(),
        'refresh': 'false',
        'size': '100',
        'keywords': userId
      }),
      headers: headers);
  Map<String, dynamic> json = jsonDecode(utf8.decode(response.bodyBytes));

  List<AdRequest> result = [];
  for (Map<String, dynamic> object in json['data']) {
    try {
      result.add(AdRequest.fromJson(object));
    } catch (e, s) {
      if (e is TypeError) {
        print('Failed to parse ad request');
        print(object);
      } else {
        print('Unknown error during ad request parse');
        print(s);
      }
    }
  }
  return result;
}
