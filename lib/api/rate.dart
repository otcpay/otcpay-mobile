import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:otcpay/model/ComparisonResult.dart';
import 'package:otcpay/model/rate/CryptoRate.dart';
import 'package:otcpay/model/rate/OtcRate.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'constant.dart';

Future<ComparisonResult> getDailyRate(
    {String fromCurrency, String toCurrency}) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  var response = await http.get(
      Uri.https(server, '/api/v1/fiat-rates', {
        'fromCurrency': fromCurrency,
        'toCurrency': toCurrency,
        'type': 'SELL',
      }),
      headers: headers);
  Map<String, dynamic> json = jsonDecode(response.body);
  return ComparisonResult.fromJson(json);
}

Future<CryptoRate> getCryptoRate(String fromCurrency, String toCurrency) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  var response = await http.get(
      Uri.https(server, '/api/v1/crypto-rates',
          {'fromCurrency': fromCurrency, 'toCurrency': toCurrency}),
      headers: headers);
  Map<String, dynamic> json = jsonDecode(response.body);
  return CryptoRate.fromJson(json);
}

Future<List<ComparisonResult>> getDefaultDailyRate(
    {String fromCurrency, String toCurrency}) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  var response = await http.get(Uri.https(server, '/api/v1/fiat-rates/default'),
      headers: headers);
  List<dynamic> json = jsonDecode(response.body);

  List<ComparisonResult> result = [];
  for (Map<String, dynamic> object in json) {
    ComparisonResult cresult = ComparisonResult.fromJson(object);
    result.add(cresult);
  }
  return result;
}

Future<List<CryptoRate>> getDefaultCryptoRate(
    {String fromCurrency, String toCurrency}) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  var response = await http
      .get(Uri.https(server, '/api/v1/crypto-rates/default'), headers: headers);
  List<dynamic> json = jsonDecode(response.body);

  List<CryptoRate> result = [];
  for (Map<String, dynamic> object in json) {
    result.add(CryptoRate.fromJson(object));
  }
  return result;
}

Future<List<OtcRate>> getOtcRate(
    {String fromCurrency, String toCurrency}) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  var response =
      await http.get(Uri.https(server, '/api/v1/otc-rates'), headers: headers);
  List<dynamic> json = jsonDecode(response.body);

  List<OtcRate> result = [];
  for (Map<String, dynamic> object in json) {
    result.add(OtcRate.fromJson(object));
  }
  return result;
}
