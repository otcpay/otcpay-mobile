import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:otcpay/model/InvoicePayment.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'constant.dart';

Future<List<InvoicePayment>> getInvoicePayment() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  var response = await http.get(
      Uri.https(server, '/api/v1/latest/invoicePayments'),
      headers: headers);
  List<dynamic> json = jsonDecode(utf8.decode(response.bodyBytes));

  List<InvoicePayment> result = [];
  for (Map<String, dynamic> object in json) {
    result.add(InvoicePayment.fromJson(object));
  }
  return result;
}

Future<List<InvoicePayment>> getUserInvoicePayment(
    String userId, int page) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  const List<String> fields = [
    'referenceRate',
    'sendCurrency',
    'receivedAmount',
    'name',
    'updatedDate',
    'id',
    'receivedCurrency',
    'amountSaved',
    'settlementMethod',
    'status'
  ];
  var response = await http.get(
      Uri.https(server, '/api/v1/invoicePayments', {
        'fields': fields.join(','),
        'page': page.toString(),
        'refresh': 'false',
        'size': '100',
        'keywords': userId
      }),
      headers: headers);
  Map<String, dynamic> json = jsonDecode(utf8.decode(response.bodyBytes));

  List<InvoicePayment> result = [];
  for (Map<String, dynamic> object in json['data']) {
    try {
      result.add(InvoicePayment.fromJson(object));
    } catch (e, s) {
      if (e is TypeError) {
        print('Failed to parse invoice payment');
        print(object);
      } else {
        print('Unknown error during invoice payment parse');
        print(s);
      }
    }
  }
  return result;
}

Future<InvoicePayment> createInvoicePayment(InvoicePayment payment) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  var response = await http.post(Uri.https(server, '/api/v1/invoicePayments'),
      headers: headers, body: jsonEncode(payment));
  Map<String, dynamic> json = jsonDecode(response.body);
  if (response.statusCode == 200) {
    return InvoicePayment.fromJson(json);
  }

  print(response.body);
  throw (json['message']);
}
