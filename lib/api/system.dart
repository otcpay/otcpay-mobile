import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'constant.dart';

Future<Map<String, dynamic>> getMapping() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  var response = await http.get(Uri.https(server, '/api/system/mapping'),
      headers: headers);
  Map<String, dynamic> json = jsonDecode(response.body);
  return json;
}

Future<String> getStatisticTime() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String token = prefs.getString('token');

  Map<String, String> headers = {
    'Authorization': 'Bearer $token',
    'Content-Type': 'application/json'
  };
  var response = await http.get(Uri.https(server, '/api/system/config/time'),
      headers: headers);
  Map<String, dynamic> json = jsonDecode(response.body);
  return json['lastUpdatedTime'];
}
