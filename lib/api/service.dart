import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server.dart';
import 'package:otcpay/api/constant.dart';

class TwilioPhoneVerify {
  final String baseUrl = 'verify.twilio.com';

  Future sendSmsCode(String phone) async {
    var authn = 'Basic ' + base64Encode(utf8.encode('$accountId:$authToken'));
    var response = await http.post(
        Uri.https(baseUrl, '/v2/Services/$serviceId/Verifications'),
        body: {'To': '+$phone', 'Channel': 'sms'},
        headers: {'Authorization': authn});

    if (response.statusCode == 200 || response.statusCode == 201) {
      return {
        'statusCode': response.statusCode.toString(),
        'message': 'success'
      };
    } else {
      return {
        'statusCode': response.statusCode.toString(),
        'message': '${jsonDecode(response.body)['message']}'
      };
    }
  }

  Future verifySmsCode(String phone, String code) async {
    var authn = 'Basic ' + base64Encode(utf8.encode('$accountId:$authToken'));
    var response = await http.post(
        Uri.https(baseUrl, '/v2/Services/$serviceId/VerificationCheck'),
        body: {'To': '+$phone', 'Code': code},
        headers: {'Authorization': authn});
    var js = jsonDecode(response.body);
    if (js['status'] == 'approved') {
      return {
        'statusCode': response.statusCode.toString(),
        'message': 'approved'
      };
    } else {
      return {
        'statusCode': response.statusCode.toString(),
        'message': '${js['message']}'
      };
    }
  }
}

enum EmailType { RESET_PW, VERIFICATION }

class EmailService {
  final smtpServer = SmtpServer(
    'smtpout.secureserver.net',
    port: 465,
    ssl: true,
    username: username,
    password: password,
  );

  Future<void> sendCode(String account, String code, EmailType type) async {
    String html = template;
    if (type == EmailType.VERIFICATION) {
      html = html
          .replaceAll('{{title}}', 'Welcome to OTCPAY')
          .replaceAll('{{contentTitle}}', 'Verify your email address')
          .replaceAll('{{content}}',
              'Thanks for signing up! We\'re excited to have on board. Please input the following verification code:')
          .replaceAll('{{code}}', code);
    } else {
      html = html
          .replaceAll('{{title}}', 'Reset Password Request')
          .replaceAll('{{contentTitle}}', 'Hi,')
          .replaceAll('{{content}}',
              'Someone (hopefully you) has requested a password reset for your OTCPAY account. Please input the following verification code:')
          .replaceAll('{{code}}', code);
    }

    final message = Message()
      ..from = Address(username, 'OTC Pay Team')
      ..recipients.add(account)
      ..subject = type == EmailType.VERIFICATION
          ? 'Verify your email'
          : 'Request to reset your OTCPAY account password'
      ..html = html;
    try {
      final sendReport = await send(message, smtpServer);
      print('Message sent: ' + sendReport.toString());
    } on MailerException catch (e) {
      print('Message not sent.');
      for (var p in e.problems) {
        print('Problem: ${p.code}: ${p.msg}');
      }

      throw e;
    }
  }
}
