import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:otcpay/bloc/Observer.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/screen/LoginPage.dart';
import 'package:otcpay/screen/OnboardPage.dart';
import 'package:otcpay/screen/home/HomePage.dart';
import 'package:otcpay/screen/home/mine/about/PrivacyPage.dart';
import 'package:otcpay/screen/home/mine/about/TermsPage.dart';
import 'package:otcpay/screen/home/mine/bank/ManageBankPage.dart';
import 'package:otcpay/screen/home/mine/order/AdOrderPage.dart';
import 'package:otcpay/screen/home/mine/order/OTCOrderPage.dart';
import 'package:otcpay/screen/home/mine/order/PaymentOrderPage.dart';
import 'package:otcpay/screen/home/mine/wallet/ManageWalletPage.dart';
import 'package:otcpay/screen/home/payment/SettlementPage.dart';
import 'package:otcpay/screen/signup/SignUpPage.dart';
import 'package:otcpay/screen/verify/VerifyPage.dart';
import 'package:otcpay/style/Color.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:uni_links/uni_links.dart';

import 'api/user.dart';
import 'bloc/auth/authenticate_bloc.dart';
import 'bloc/general/general_bloc.dart';
import 'bloc/hot/hot.dart';
import 'bloc/otc/otc.dart';
import 'bloc/proof/proof_bloc.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

const AndroidNotificationChannel channel = AndroidNotificationChannel(
  'high_importance_channel', // id
  'High Importance Notifications', // title
  'This channel is used for important notifications.', // description
  importance: Importance.max,
);

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print("Handling a background message: ${message.messageId}");
}

Future<String> initUniLinks() async {
  String initialLink;
  try {
    initialLink = await getInitialLink();
  } on PlatformException {
    print('Failed to open deep link');
  }

  return initialLink;
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  await Firebase.initializeApp();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  Bloc.observer = SimpleBlocObserver();

  SharedPreferences.getInstance().then((prefs) {
    String userId = prefs.getString('userId') ?? '';
    FirebaseMessaging.instance.onTokenRefresh.listen((token) {
      print('refreshed notification token:' + token);
      updateToken(userId, token);
    });
  });

  const AndroidInitializationSettings initializationSettingsAndroid =
      AndroidInitializationSettings('app_icon');
  final IOSInitializationSettings initializationSettingsIOS =
      IOSInitializationSettings(
          requestSoundPermission: true,
          requestBadgePermission: true,
          requestAlertPermission: true);
  final InitializationSettings initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid, iOS: initializationSettingsIOS);

  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: (String payload) async {
    if (payload != null) {
      print('notification payload: $payload');
    }
  });

  final String link = await initUniLinks();

  runApp(MultiBlocProvider(
      providers: [
        BlocProvider<AuthenticateBloc>(create: (context) => AuthenticateBloc()),
        BlocProvider<ProofBloc>(create: (context) => ProofBloc()),
        BlocProvider<GeneralBloc>(create: (context) => GeneralBloc()),
        BlocProvider<OtcBloc>(create: (context) => OtcBloc()),
        BlocProvider<HotBloc>(create: (context) => HotBloc()),
      ],
      child: EasyLocalization(
          supportedLocales: [
            Locale('en', 'US'),
            Locale('zh', 'HK'),
            Locale('zh', 'CN'),
          ],
          useFallbackTranslations: true,
          fallbackLocale: Locale('en', 'US'),
          path: 'translations',
          child: MyMaterialApp(link))));
}

class MyMaterialApp extends StatelessWidget {
  final String link;

  const MyMaterialApp(this.link);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      localizationsDelegates: context.localizationDelegates,
      initialRoute: link,
      routes: {
        '/': (context) => MyApp(link),
        '/login': (context) => LoginPage(),
        '/signup': (context) => SignUpPage(),
        '/home': (context) => HomePage(),
        '/verify': (context) => VerifyPage(),
        '/settlement': (context) => SettlementPage(),
        '/wallet': (context) => ManageWalletPage(),
        '/bank': (context) => ManageBankPage(),
        '/payment-order': (context) => PaymentOrderPage(),
        '/otc-order': (context) => OTCOrderPage(),
        '/ad-order': (context) => AdOrderPage(),
        '/terms': (context) => TermsPage(),
        '/privacy': (context) => PrivacyPage(),
      },
      theme: ThemeData(
          primaryColor: inputBoxBackgroundColor,
          splashColor: Colors.white24,
          canvasColor: globalBackgroundColor,
          cardTheme: CardTheme(
            shape: RoundedRectangleBorder(
              borderRadius: const BorderRadius.all(
                Radius.circular(8.0),
              ),
            ),
          ),
          appBarTheme: AppBarTheme(
            color: globalBackgroundColor,
          ),
          textButtonTheme: TextButtonThemeData(
              style: ButtonStyle(
            overlayColor:
                MaterialStateColor.resolveWith((states) => Colors.white24),
          )),
          colorScheme: ColorScheme.fromSwatch().copyWith(
              primary: globalPrimaryColor, secondary: Colors.white24)),
    );
  }
}

class MyApp extends StatefulWidget {
  final String link;

  const MyApp(this.link);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;

  int stage = 0;

  void _firebaseCloudMessagingListeners() async {
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true, // Required to display a heads up notification
      badge: true,
      sound: true,
    );

    NotificationSettings settings = await _firebaseMessaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: true,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');
    } else if (settings.authorizationStatus ==
        AuthorizationStatus.provisional) {
      print('User granted provisional permission');
    } else {
      print('User declined or has not accepted permission');
    }

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      AndroidNotification android = message.notification?.android;

      // If `onMessage` is triggered with a notification, construct our own
      // local notification to show to users using the created channel.
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                channel.description,
                icon: android?.smallIcon,
              ),
            ));
      }
    });
  }

  @override
  void initState() {
    super.initState();

    _firebaseCloudMessagingListeners();

    SharedPreferences.getInstance().then((prefs) {
      setState(() {
        stage = prefs.getInt('stage') ?? 0;
        print('Stage: $stage.');
      });
    });

    BlocProvider.of<HotBloc>(context).add(InitialQuoteFetch(refresh: false));
    BlocProvider.of<OtcBloc>(context).add(InitialFetch(false));
    BlocProvider.of<GeneralBloc>(context).add(GetMapping());
  }

  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 3,
      navigateAfterSeconds: Scaffold(
          body: stage == 0
              ? OnBoardingPage()
              : widget.link == null
                  ? LoginPage(isFirstPage: true)
                  : HomePage()),
      photoSize: 88.0,
      image: Image.asset('icons/launch_icon.png'),
      imageBackground: AssetImage('icons/Boarding_Background.png'),
      loaderColor: globalPrimaryColor,
    );
  }
}
