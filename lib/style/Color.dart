import 'package:flutter/material.dart';

// global color
const globalGradientBackgroundColor = Color(0xFF43444D);
const globalBackgroundColor = Color(0xFF2B2D39);
const globalPrimaryColor = Color(0xFFCBAF78);
const globalSecondaryColor = Color(0xFFC7B07F);
const globalSecondaryGradientColor = Color(0xFF645840);
const globalDisabledPrimaryColor = Color(0xFF7F7462);
const globalDisabledSecondaryColor = Color(0x78C7B07F);

// input color
const inputBoxBackgroundColor = Color(0xFF323440);
const loadingIndicatorBgColor = Color(0xCC323440);
const headerColor = Color(0xFF707070);
const subTitleColor = Color(0xFF9A9A9A);
const cancelColor = Color(0xFFD1D1D1);
const dropdownBgColor = Color(0xFF43444C);

// indicator color
const upColor = Color(0xFF50B091);
const upBackgroundColor = Color(0x5550B091);
const downColor = Color(0xFFFB6D65);
const downBackgroundColor = Color(0x55FB6D65);
const labelColor = Color(0xFF71727A);
const dotBorderColor = Color(0xFF707070);
const dropdownColor = Color(0xFF2A2C38);
const expireColor = Color(0xFF43444D);
const dividerColor = Color(0xFF545560);
