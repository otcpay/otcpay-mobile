import 'package:flutter/material.dart';

import 'Color.dart';

// pixel setting
const double preAuthMargin = 25;
const double globalElevation = 2;
const double quoteItemTopRowHeight = 14;
const double quoteItemHeight = 85;
const double boardItemHeight = 55;
const double bannerHeight = 135;
const double otcItemHeight = 126;
const double otcItemDetailHeight = 80;
const double otcItemAvatarRadius = 10;

// gradient setting
const LinearGradient quoteItemGradient = LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: [globalGradientBackgroundColor, globalBackgroundColor]);
const LinearGradient boardItemGradient = LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: [globalGradientBackgroundColor, inputBoxBackgroundColor]);

// edge insets setting
const quoteItemInnerPadding = EdgeInsets.only(top: 10.0, bottom: 5.0);
const quoteItemPadding = EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0);
const quoteItemMargin = EdgeInsets.only(bottom: 10);
const boardItemPadding =
    EdgeInsets.only(left: 15.0, right: 15.0, top: 12.0, bottom: 12.0);
const otcItemPadding = EdgeInsets.only(top: 15, left: 15, right: 15);
const otcItemMargin = EdgeInsets.only(bottom: 2);
const otcItemTitleMargin = EdgeInsets.only(left: 10.0, right: 5.0);
const otcItemDetailMargin = EdgeInsets.only(top: 5.0);
