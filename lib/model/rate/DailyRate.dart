import 'package:otcpay/component/ListItem.dart';

class DailyRate {
  final String fromCurrency;
  final String toCurrency;
  final String rate;
  final RATE_MODE mode;

  const DailyRate({this.fromCurrency, this.toCurrency, this.rate, this.mode});
}
