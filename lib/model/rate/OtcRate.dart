class OtcRate {
  double rate;
  double otcBoardRate;
  String sourceCurrency;
  String targetCurrency;
  bool defaultRate;
  String type;

  OtcRate(
      {this.rate,
      this.otcBoardRate,
      this.sourceCurrency,
      this.targetCurrency,
      this.type,
      this.defaultRate});

  OtcRate.fromJson(Map<String, dynamic> json) {
    rate = json['rate'];
    otcBoardRate = json['otcBoardRate'];
    sourceCurrency = json['sourceCurrency'];
    targetCurrency = json['targetCurrency'];
    defaultRate = json['defaultRate'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['rate'] = this.rate;
    data['sourceCurrency'] = this.sourceCurrency;
    data['targetCurrency'] = this.targetCurrency;
    data['defaultRate'] = this.defaultRate;
    data['otcBoardRate'] = this.otcBoardRate;
    data['type'] = this.type;
    return data;
  }
}
