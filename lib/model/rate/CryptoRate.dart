class CryptoRate {
  double amount;
  double open;
  double close;
  double high;
  int id;
  int count;
  double low;
  int version;
  double vol;

  CryptoRate(
      {this.amount,
      this.open,
      this.close,
      this.high,
      this.id,
      this.count,
      this.low,
      this.version,
      this.vol});

  CryptoRate.fromJson(Map<String, dynamic> json) {
    amount = json['amount'];
    open = json['open'];
    close = json['close'];
    high = json['high'];
    id = json['id'];
    count = json['count'];
    low = json['low'];
    version = json['version'];
    vol = json['vol'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amount'] = this.amount;
    data['open'] = this.open;
    data['close'] = this.close;
    data['high'] = this.high;
    data['id'] = this.id;
    data['count'] = this.count;
    data['low'] = this.low;
    data['version'] = this.version;
    data['vol'] = this.vol;
    return data;
  }
}
