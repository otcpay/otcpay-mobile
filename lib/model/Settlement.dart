class Settlement {
  String costRate;
  String createdDate;
  String finalAmount;
  String finalRate;
  String id;
  String externalId;
  bool settled;
  String settledTime;
  String type;
  String updatedDate;
  String userId;

  Settlement(
      {this.costRate,
      this.createdDate,
      this.finalAmount,
      this.finalRate,
      this.id,
      this.externalId,
      this.settled,
      this.settledTime,
      this.type,
      this.updatedDate,
      this.userId});

  Settlement.fromJson(Map<String, dynamic> json) {
    costRate = json['costRate'];
    createdDate = json['createdDate'];
    finalAmount = json['finalAmount'];
    finalRate = json['finalRate'];
    id = json['id'];
    externalId = json['externalId'];
    settled = json['settled'];
    settledTime = json['settledTime'];
    type = json['type'];
    updatedDate = json['updatedDate'];
    userId = json['userId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['costRate'] = this.costRate;
    data['createdDate'] = this.createdDate;
    data['finalAmount'] = this.finalAmount;
    data['finalRate'] = this.finalRate;
    data['id'] = this.id;
    data['externalId'] = this.externalId;
    data['settled'] = this.settled;
    data['settledTime'] = this.settledTime;
    data['type'] = this.type;
    data['updatedDate'] = this.updatedDate;
    data['userId'] = this.userId;
    return data;
  }
}
