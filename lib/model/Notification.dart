class Notification {
  String body;
  String createdAt;
  bool read;
  String title;

  Notification({this.body, this.createdAt, this.read, this.title});

  Notification.fromJson(Map<String, dynamic> json) {
    body = json['body'];
    createdAt = json['createdAt'];
    read = json['read'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['body'] = this.body;
    data['createdAt'] = this.createdAt;
    data['read'] = this.read;
    data['title'] = this.title;
    return data;
  }
}
