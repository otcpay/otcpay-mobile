class BannerObject {
  String url;
  String hyperlink;
  int order;

  BannerObject({this.url, this.hyperlink, this.order});

  BannerObject.fromJson(Map<String, dynamic> json) {
    url = json['url'];
    hyperlink = json['hyperlink'];
    order = json['order'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['url'] = this.url;
    data['hyperlink'] = this.hyperlink;
    data['order'] = this.order;
    return data;
  }
}
