class AuthenticationDetail {
  String jwttoken;
  String role;
  String userId;

  AuthenticationDetail({this.userId, this.role, this.jwttoken});

  AuthenticationDetail.fromJson(Map<String, dynamic> json)
      : jwttoken = json['jwttoken'],
        role = json['role'],
        userId = json['userId'];
}

class ValidateDetail {
  String username;
  String role;
  String userId;

  ValidateDetail.fromJson(Map<String, dynamic> json)
      : username = json['username'],
        role = json['role'],
        userId = json['userId'];
}
