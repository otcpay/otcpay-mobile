class FileResponse {
  String selfLink;
  String mediaLink;
  String name;
  int size;
  int createdTime;

  FileResponse.fromJson(Map<String, dynamic> json)
      : selfLink = json['selfLink'],
        size = json['size'],
        mediaLink = json['mediaLink'],
        name = json['name'],
        createdTime = json['createdTime'];
}
