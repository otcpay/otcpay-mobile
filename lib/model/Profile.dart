class Profile {
  String account;
  String profilePic;
  String username;
  String region;
  String whatsappId;
  String wechatId;
  String telegramId;
  String googleId;
  bool verified;
  Proof idProof;
  Proof addressProof;

  List<BankProof> banks;
  List<Wallet> wallets;

  int defaultWallets;
  int defaultBanks;

  Profile(
      {this.account,
      this.username,
      this.region,
      this.whatsappId,
      this.wechatId,
      this.telegramId,
      this.profilePic,
      this.googleId,
      this.verified,
      this.idProof,
      this.addressProof,
      this.banks,
      this.wallets,
      this.defaultBanks,
      this.defaultWallets});

  Profile.fromJson(Map<String, dynamic> json)
      : account = json['account'],
        username = json['username'],
        region = json['region'],
        whatsappId = json['whatsappId'],
        wechatId = json['wechatId'],
        telegramId = json['telegramId'],
        profilePic = json['profilePic'],
        googleId = json['googleId'],
        verified = json['verified'],
        idProof = Proof.fromJson(json['idProof']),
        addressProof = Proof.fromJson(json['addressProof']),
        defaultBanks = json['defaultBanks'] ?? -1,
        defaultWallets = json['defaultWallets'] ?? -1,
        banks = json['banks'] == null
            ? []
            : (json['banks'] as List)
                .map((e) => BankProof.fromJson(e))
                .toList(),
        wallets = json['wallets'] == null
            ? []
            : (json['wallets'] as List).map((e) => Wallet.fromJson(e)).toList();
}

class Proof {
  String image;
  String detail;

  Proof({this.image, this.detail});

  Proof.fromJson(Map<String, dynamic> json)
      : image = json != null ? json['image'] : '',
        detail = json != null ? json['detail'] : '';
}

class BankProof extends Proof {
  String holderName;
  String bankCode;
  String bankName;

  BankProof({this.bankName, this.bankCode, this.holderName, image, detail})
      : super(detail: detail, image: image);

  BankProof.fromJson(Map<String, dynamic> json)
      : bankCode = json != null ? json['bankCode'] : '',
        bankName = json != null ? json['bankName'] : '',
        holderName = json != null ? json['holderName'] : '',
        super.fromJson(json);

  Map toJson() =>
      {
        'bankCode': bankCode,
        'bankName': bankName,
        'holderName': holderName,
        'image': image,
        'detail': detail,
      };
}

class Wallet {
  String type;
  String subType;
  String name;
  String address;

  Wallet({this.type, this.name, this.address, this.subType});

  Wallet.fromJson(Map<String, dynamic> json)
      : type = json != null ? json['type'] : '',
        name = json != null ? json['name'] : '',
        address = json != null ? json['address'] : '',
        subType = json != null ? json['subType'] : '';

  Map toJson() =>
      {
        'name': name,
        'type': type,
        'address': address,
        'subType': subType
      };
}
