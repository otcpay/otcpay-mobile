class InvoicePayment {
  String amountNeeded;
  String amountSaved;
  String contact;
  String contactMethod;
  String settlementMethod;
  String createdDate;
  String id;
  String invoiceImage;
  String name;
  String receiveBankAddress;
  String receiveBank;
  String receiveBankNumber;
  String receiveName;
  String receivedAmount;
  String receivedCurrency;
  String referenceRate;
  String sendCurrency;
  String bankImage;
  String sendFrom;
  String sendTo;
  String type;
  String updatedDate;
  String userId;
  String status;

  // no required field as detail will be filled in in different steps
  InvoicePayment(
      {this.amountNeeded,
      this.amountSaved,
      this.contact,
      this.contactMethod,
      this.settlementMethod,
      this.createdDate,
      this.id,
      this.invoiceImage,
      this.name,
      this.bankImage,
      this.receiveBank,
      this.receiveBankNumber,
      this.receiveName,
      this.receivedAmount,
      this.receivedCurrency,
      this.receiveBankAddress,
      this.referenceRate,
      this.sendCurrency,
      this.sendFrom,
      this.sendTo,
      this.type,
      this.updatedDate,
      this.status,
      this.userId});

  InvoicePayment.fromJson(Map<String, dynamic> json) {
    amountNeeded = json['amountNeeded'];
    amountSaved = json['amountSaved'];
    contact = json['contact'];
    contactMethod = json['contactMethod'];
    settlementMethod = json['settlementMethod'];
    createdDate = json['createdDate'];
    id = json['id'];
    invoiceImage = json['invoiceImage'];
    bankImage = json['bankImage'];
    name = json['name'];
    receiveBank = json['receiveBank'];
    receiveBankAddress = json['receiveBankAddress'];
    receiveBankNumber = json['receiveBankNumber'];
    receiveName = json['receiveName'];
    receivedAmount = json['receivedAmount'];
    receivedCurrency = json['receivedCurrency'];
    referenceRate = json['referenceRate'];
    sendCurrency = json['sendCurrency'];
    sendFrom = json['sendFrom'];
    sendTo = json['sendTo'];
    type = json['type'];
    updatedDate = json['updatedDate'];
    userId = json['userId'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amountNeeded'] = this.amountNeeded;
    data['amountSaved'] = this.amountSaved;
    data['contact'] = this.contact;
    data['contactMethod'] = this.contactMethod;
    data['settlementMethod'] = this.settlementMethod;
    data['createdDate'] = this.createdDate;
    data['id'] = this.id;
    data['invoiceImage'] = this.invoiceImage;
    data['bankImage'] = this.bankImage;
    data['name'] = this.name;
    data['receiveBank'] = this.receiveBank;
    data['receiveBankAddress'] = this.receiveBankAddress;
    data['receiveBankNumber'] = this.receiveBankNumber;
    data['receiveName'] = this.receiveName;
    data['receivedAmount'] = this.receivedAmount;
    data['receivedCurrency'] = this.receivedCurrency;
    data['referenceRate'] = this.referenceRate;
    data['sendCurrency'] = this.sendCurrency;
    data['sendFrom'] = this.sendFrom;
    data['sendTo'] = this.sendTo;
    data['type'] = this.type;
    data['updatedDate'] = this.updatedDate;
    data['userId'] = this.userId;
    data['status'] = this.status;
    return data;
  }
}
