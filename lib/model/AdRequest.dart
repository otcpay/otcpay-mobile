import 'package:meta/meta.dart';

class AdRequest {
  String id;
  String createdDate;
  String updatedDate;
  bool settled;
  String amount;
  String offerRate;
  String userId;
  String adId;
  String settleCurrency;
  String type;
  String crytocurrency;
  String name;
  String contact;
  String settleRate;
  String totalCost;
  String status;

  AdRequest(
      {this.id,
      this.settled,
      this.settleRate,
      this.createdDate,
      this.updatedDate,
      @required this.userId,
      @required this.amount,
      @required this.offerRate,
      @required this.type,
      @required this.crytocurrency,
      @required this.settleCurrency,
      @required this.totalCost,
      this.adId,
      this.status,
      this.name,
      this.contact});

  AdRequest.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    settled = json['settled'];
    settleRate = json['settleRate'];
    createdDate = json['createdDate'];
    updatedDate = json['updatedDate'];
    userId = json['userId'];
    amount = json['amount'];
    offerRate = json['offerRate'];
    name = json['name'];
    contact = json['contact'];
    type = json['type'];
    crytocurrency = json['crytocurrency'];
    settleCurrency = json['settleCurrency'];
    totalCost = json['totalCost'];
    adId = json['adId'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['settled'] = this.settled;
    data['settleRate'] = this.settleRate;
    data['createdDate'] = this.createdDate;
    data['updatedDate'] = this.updatedDate;
    data['userId'] = this.userId;
    data['amount'] = this.amount;
    data['offerRate'] = this.offerRate;
    data['name'] = this.name;
    data['contact'] = this.contact;
    data['type'] = this.type;
    data['crytocurrency'] = this.crytocurrency;
    data['settleCurrency'] = this.settleCurrency;
    data['totalCost'] = this.totalCost;
    data['adId'] = this.adId;
    data['status'] = this.status;
    return data;
  }
}
