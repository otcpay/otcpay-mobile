class ComparisonResult {
  String fromCurrency;
  String toCurrency;
  double otcBoardRate;
  List<Rates> rates;
  bool defaultRate;

  ComparisonResult(
      {this.fromCurrency,
      this.toCurrency,
      this.otcBoardRate,
      this.rates,
      this.defaultRate});

  ComparisonResult.fromJson(Map<String, dynamic> json) {
    fromCurrency = json['fromCurrency'];
    toCurrency = json['toCurrency'];
    otcBoardRate = json['otcBoardRate'];
    defaultRate = json['defaultRate'] ?? true;
    if (json['rates'] != null) {
      rates = [];
      json['rates'].forEach((v) {
        rates.add(new Rates.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['fromCurrency'] = this.fromCurrency;
    data['toCurrency'] = this.toCurrency;
    data['otcBoardRate'] = this.otcBoardRate;
    data['defaultRate'] = this.defaultRate;
    if (this.rates != null) {
      data['rates'] = this.rates.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Rates {
  String name;
  String time;
  double rate;
  int order;

  Rates({this.name, this.time, this.rate, this.order});

  Rates.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    time = json['time'];
    rate = json['rate'];
    order = json['order'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['time'] = this.time;
    data['rate'] = this.rate;
    data['order'] = this.order;
    return data;
  }
}
