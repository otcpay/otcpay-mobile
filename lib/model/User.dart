class User {
  final String account;
  final String profilePic;
  final String password;
  final String username;
  final String region;
  final String whatsappId;
  final String wechatId;
  final String telegramId;

  const User(
      {this.account,
      this.password,
      this.username,
      this.region,
      this.whatsappId,
      this.wechatId,
      this.telegramId,
      this.profilePic});

  User.fromJson(Map<String, dynamic> json)
      : account = json['account'],
        password = json['password'],
        username = json['username'],
        region = json['region'],
        whatsappId = json['whatsappId'],
        wechatId = json['wechatId'],
        telegramId = json['telegramId'],
        profilePic = json['profilePic'];

  Map<String, dynamic> toJson() => {
        'account': account,
        'password': password,
        'username': username,
        'region': region,
        'whatsappId': whatsappId,
        'wechatId': wechatId,
        'telegramId': telegramId,
        'profilePic': profilePic,
      };
}
