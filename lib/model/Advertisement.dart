import 'package:meta/meta.dart';

class Advertisement {
  String contact;
  String createdDate;
  String crytocurrency;
  String id;
  String lowerLimit;
  String name;
  String offer;
  String region;
  String settleCurrency;
  String type;
  String updatedDate;
  String upperLimit;
  String userId;
  String profilePic;
  String status;
  String quantity;
  bool verified;

  Advertisement(
      {this.contact,
      this.createdDate,
      @required this.crytocurrency,
      this.id,
      @required this.lowerLimit,
      this.name,
      @required this.offer,
      this.region,
      @required this.settleCurrency,
      this.type,
      this.updatedDate,
      @required this.upperLimit,
      @required this.userId,
      @required this.quantity,
      this.profilePic,
      this.status,
      this.verified});

  Advertisement.fromJson(Map<String, dynamic> json) {
    contact = json['contact'];
    createdDate = json['createdDate'];
    crytocurrency = json['crytocurrency'];
    id = json['id'];
    lowerLimit = json['lowerLimit'];
    name = json['name'];
    offer = json['offer'];
    region = json['region'];
    settleCurrency = json['settleCurrency'];
    type = json['type'];
    updatedDate = json['updatedDate'];
    upperLimit = json['upperLimit'];
    userId = json['userId'];
    profilePic = json['profilePic'];
    status = json['status'];
    quantity = json['quantity'];
    verified = json['verified'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['contact'] = this.contact;
    data['createdDate'] = this.createdDate;
    data['crytocurrency'] = this.crytocurrency;
    data['id'] = this.id;
    data['lowerLimit'] = this.lowerLimit;
    data['name'] = this.name;
    data['offer'] = this.offer;
    data['region'] = this.region;
    data['settleCurrency'] = this.settleCurrency;
    data['type'] = this.type;
    data['updatedDate'] = this.updatedDate;
    data['upperLimit'] = this.upperLimit;
    data['userId'] = this.userId;
    data['profilePic'] = this.profilePic;
    data['status'] = status;
    data['quantity'] = quantity;
    data['verified'] = verified;
    return data;
  }
}
