import 'package:flutter/material.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/screen/home/payment/RecipientDetailPage.dart';
import 'package:otcpay/screen/home/payment/SettlementPage.dart';
import 'package:otcpay/screen/verify/VerifyPage.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/style/Spec.dart';

class InputBox extends StatefulWidget {
  final TextEditingController controller;
  final Widget suffixIcon;
  final Widget leadingIcon;
  final String hintText;
  final double height;
  final double fontSize;
  final int maxLine;
  final EdgeInsets margin;
  final bool obscureText;
  final bool enableSuggestions;
  final bool autocorrect;
  final bool showSuffixIcon;
  final bool readOnly;
  final bool overrideSuffixIcon;
  final bool overridePadding;
  final bool Function() callback;
  final TextInputType keyboardType;

  const InputBox(
      {@required this.controller,
      this.hintText,
      this.suffixIcon,
      this.leadingIcon,
      this.height,
      this.fontSize = 14,
      this.margin,
      this.maxLine = 1,
      this.enableSuggestions = true,
      this.autocorrect = true,
      this.obscureText = false,
      this.showSuffixIcon = false,
      this.overrideSuffixIcon = false,
      this.overridePadding = false,
      this.readOnly = false,
      this.keyboardType,
      this.callback});

  @override
  _InputBoxState createState() => _InputBoxState();
}

class _InputBoxState extends State<InputBox> {
  bool showIcon = false;
  bool dirty = false;

  void _onChange(String text) {
    setState(() {
      showIcon = text.length > 0;
      dirty = true;
    });
  }

  Widget _suffixIcon() {
    if (showIcon && widget.showSuffixIcon && !widget.overrideSuffixIcon) {
      return IconButton(
          splashRadius: 15.0,
          icon: Icon(Icons.clear),
          iconSize: 20.0,
          onPressed: widget.controller.clear,
          color: Color(0xFF71727A));
    }

    if (widget.showSuffixIcon && widget.suffixIcon != null) {
      return widget.suffixIcon;
    }

    if (dirty && widget.callback != null && widget.callback()) {
      return Icon(Icons.check, color: Colors.green);
    } else if (dirty && widget.callback != null && !widget.callback()) {
      return Icon(Icons.clear,
          color: showIcon ? Colors.red : inputBoxBackgroundColor);
    }

    return null;
  }

  double _padding() {
    if (widget.overridePadding) {
      return 15;
    }

    if ((showIcon && widget.showSuffixIcon) ||
        (dirty && widget.callback != null && widget.callback()) ||
        (dirty && widget.callback != null && !widget.callback()) ||
        widget.suffixIcon != null) {
      return 16;
    }

    return 0;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      margin: widget.margin,
      color: inputBoxBackgroundColor,
      child: Container(
          height: widget.height,
          child: TextField(
            style: TextStyle(color: Colors.white, fontSize: widget.fontSize),
            maxLines: widget.maxLine,
            onChanged: _onChange,
            controller: widget.controller,
            cursorColor: Colors.white,
            obscureText: widget.obscureText,
            autocorrect: widget.autocorrect,
            enableSuggestions: widget.enableSuggestions,
            readOnly: widget.readOnly,
            keyboardType: widget.keyboardType,
            decoration: InputDecoration(
                icon: widget.leadingIcon,
                suffixIcon: _suffixIcon(),
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                contentPadding: EdgeInsets.only(
                    left: widget.leadingIcon == null ? 20 : 0, top: _padding()),
                hintText: widget.hintText,
                hintStyle: TextStyle(color: subTitleColor, fontSize: 14)),
          )),
    );
  }
}

class InputTitle extends StatelessWidget {
  final String title;
  final double fontSize;

  const InputTitle({
    @required this.title,
    this.fontSize,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      margin: EdgeInsets.only(left: preAuthMargin),
      child: Row(
        children: [
          BoldTitle(title: title, fontSize: fontSize),
        ],
      ),
    );
  }
}

class ConfirmButton extends StatefulWidget {
  static bool defaultValidator() {
    return true;
  }

  final void Function() callback;
  final bool Function() validator;
  final String title;
  final double height;
  final double fontSize;
  final EdgeInsets margin;
  final GlobalKey key;
  final Color color;
  final Color disabledColor;

  const ConfirmButton({
    @required this.callback,
    @required this.title,
    @required this.height,
    this.fontSize = 16,
    this.validator = defaultValidator,
    this.color = globalPrimaryColor,
    this.disabledColor = globalDisabledPrimaryColor,
    this.margin,
    this.key,
  }) : super(key: key);

  @override
  ConfirmButtonState createState() => ConfirmButtonState();
}

class ConfirmButtonState extends State<ConfirmButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: widget.margin,
        height: widget.height,
        child: SizedBox.expand(
            child: FlatButton(
          disabledColor: widget.disabledColor,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
          color: widget.color,
          onPressed: widget.validator == null
              ? null
              : widget.validator()
                  ? widget.callback
                  : null,
          child: Text(widget.title,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: widget.fontSize,
                  fontWeight: FontWeight.w900)),
        )));
  }
}

abstract class CommonRadio extends StatelessWidget {
  final Set<MaterialState> _interactiveStates = const <MaterialState>{
    MaterialState.pressed,
    MaterialState.hovered,
    MaterialState.focused,
    MaterialState.selected,
  };

  const CommonRadio();

  Color getColor(states) {
    if (states.any(_interactiveStates.contains)) {
      return globalPrimaryColor;
    }

    return Color(0xFFD1D1D1);
  }
}

class ProofRadioButton extends CommonRadio {
  final void Function(ProofChoice) proofCallback;
  final ProofChoice proofValue;
  final ProofChoice proofGroupValue;

  const ProofRadioButton(
      {this.proofCallback, this.proofValue, this.proofGroupValue});

  @override
  Widget build(BuildContext context) {
    return Radio(
      fillColor: MaterialStateProperty.resolveWith(getColor),
      activeColor: globalPrimaryColor,
      value: proofValue,
      groupValue: proofGroupValue,
      onChanged: proofCallback,
    );
  }
}

class CurrencyRadioButton extends CommonRadio {
  final void Function(CurrencyChoice) currencyCallback;
  final CurrencyChoice currencyValue;
  final CurrencyChoice currencyGroupValue;

  const CurrencyRadioButton(
      {this.currencyCallback, this.currencyValue, this.currencyGroupValue});

  @override
  Widget build(BuildContext context) {
    return Radio(
      fillColor: MaterialStateProperty.resolveWith(getColor),
      activeColor: globalPrimaryColor,
      value: currencyValue,
      groupValue: currencyGroupValue,
      onChanged: currencyCallback,
    );
  }
}

class PaymentRadioButton extends CommonRadio {
  final void Function(PaymentMethod) paymentCallback;
  final PaymentMethod paymentValue;
  final PaymentMethod paymentGroupValue;

  const PaymentRadioButton(
      {this.paymentCallback, this.paymentValue, this.paymentGroupValue});

  @override
  Widget build(BuildContext context) {
    return Radio(
      fillColor: MaterialStateProperty.resolveWith(getColor),
      activeColor: globalPrimaryColor,
      value: paymentValue,
      groupValue: paymentGroupValue,
      onChanged: paymentCallback,
    );
  }
}

class ContactRadioButton extends CommonRadio {
  final void Function(ContactMethod) contactCallback;
  final ContactMethod contactValue;
  final ContactMethod contactGroupValue;

  const ContactRadioButton(
      {this.contactCallback, this.contactValue, this.contactGroupValue});

  @override
  Widget build(BuildContext context) {
    return Radio(
      fillColor: MaterialStateProperty.resolveWith(getColor),
      activeColor: globalPrimaryColor,
      value: contactValue,
      groupValue: contactGroupValue,
      onChanged: contactCallback,
    );
  }
}
