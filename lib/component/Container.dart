import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/style/Spec.dart';

class ShadowContainer extends StatelessWidget {
  final Widget child;
  final double height;
  final EdgeInsets margin;

  const ShadowContainer(
      {@required this.child, @required this.height, this.margin});

  @override
  Widget build(BuildContext context) {
    return Container(
        height: height,
        margin: margin,
        child: Material(
          elevation: globalElevation,
          color: inputBoxBackgroundColor,
          borderRadius: BorderRadius.circular(8.0),
          child: child,
        ));
  }
}

class DotLineContainer extends StatelessWidget {
  final Widget image;
  final double height;
  final String title;
  final String subTitle;
  final EdgeInsets margin;
  final void Function() callback;
  final bool showMask;

  const DotLineContainer(
      {@required this.image,
      @required this.height,
      @required this.title,
      @required this.subTitle,
      @required this.callback,
      this.margin,
      this.showMask = false});

  Widget _buildWidget() {
    if (showMask) {
      return Container(
          margin: margin,
          height: height,
          child: Stack(
            children: [
              DottedBorder(
                  color: dotBorderColor,
                  borderType: BorderType.RRect,
                  radius: Radius.circular(8),
                  padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                  strokeWidth: 2,
                  dashPattern: [10, 10, 10, 10],
                  child: Row(
                    children: [
                      Expanded(
                          child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 5, bottom: 8),
                            child: NormalTitle(title: title, fontSize: 14),
                          ),
                          SubTitle(title: subTitle, color: subTitleColor),
                        ],
                      )),
                      image
                    ],
                  )),
              InkWell(
                borderRadius: BorderRadius.circular(8),
                onTap: callback,
                child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: Color(0x66C7B07F),
                    ),
                    child: Center(
                        child: Icon(Icons.check_circle_outline,
                            size: 60, color: globalPrimaryColor))),
              )
            ],
          ));
    }

    return Container(
      margin: margin,
      height: height,
      child: InkWell(
        borderRadius: BorderRadius.circular(8),
        onTap: callback,
        child: DottedBorder(
            color: dotBorderColor,
            borderType: BorderType.RRect,
            radius: Radius.circular(8),
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            strokeWidth: 2,
            dashPattern: [10, 10, 10, 10],
            child: Row(
              children: [
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 5, bottom: 8),
                      child: NormalTitle(title: title, fontSize: 14),
                    ),
                    SubTitle(title: subTitle, color: subTitleColor),
                  ],
                )),
                image
              ],
            )),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildWidget();
  }
}

class RadioContainer extends StatelessWidget {
  final double width;
  final Widget title;
  final Widget leading;

  const RadioContainer({this.width = 110, this.leading, this.title});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(left: 5),
        width: width,
        child: ListTile(
          dense: true,
          contentPadding: EdgeInsets.zero,
          leading: leading,
          title: title,
        ));
  }
}

class MainTitleContainer extends StatelessWidget {
  final String text;

  const MainTitleContainer(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20, top: 30),
      child: BoldTitle(fontSize: 14, title: text),
    );
  }
}

class SecondaryContainer extends StatelessWidget {
  final String text;

  const SecondaryContainer(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20, top: 20),
      child: BoldTitle(fontSize: 14, title: text),
    );
  }
}

class ParagraphContainer extends StatelessWidget {
  final String text;

  const ParagraphContainer(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20, top: 10),
      child: SubTitle(fontSize: 12, title: text),
    );
  }
}

class BulletParagraphContainer extends StatelessWidget {
  final String text;

  const BulletParagraphContainer(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 40, right: 20, top: 10),
      child: SubTitle(fontSize: 12, title: text),
    );
  }
}
