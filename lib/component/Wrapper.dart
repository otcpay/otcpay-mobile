import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:otcpay/bloc/auth/authenticate.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/bloc/hot/hot.dart';
import 'package:otcpay/bloc/otc/otc.dart';
import 'package:otcpay/bloc/proof/proof.dart';
import 'package:otcpay/generated/locale_keys.g.dart';

import 'Indicator.dart';
import 'Title.dart';

class LoadingWrapper extends StatelessWidget {
  final Widget child;
  final Type isLoadingState;
  final bool needBackground;

  final AuthenticateState state;
  final ProofState proofState;
  final GeneralState generalState;
  final HotState hotState;
  final OtcState otcState;

  const LoadingWrapper({
    @required this.child,
    this.needBackground = false,
    this.state,
    this.hotState,
    this.proofState,
    this.generalState,
    this.otcState,
    this.isLoadingState = Authenticating,
  });

  bool isLoading() {
    if (state != null) {
      return state.runtimeType == isLoadingState;
    }

    if (generalState != null) {
      return generalState.runtimeType == isLoadingState;
    }

    if (hotState != null) {
      return hotState.runtimeType == isLoadingState;
    }

    if (otcState != null) {
      return otcState.runtimeType == isLoadingState;
    }

    return proofState.runtimeType == isLoadingState;
  }

  Widget _loadingIndicator() {
    if (needBackground) {
      return Material(
        borderRadius: BorderRadius.circular(8),
        color: Color(0xDF2B2D39),
        child: Container(
          height: 155,
          width: 155,
          padding: EdgeInsets.all(15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(height: 50, width: 50, child: LoadingIndicator()),
              Padding(
                padding: EdgeInsets.only(top: 10),
                child: BoldTitle(title: tr(LocaleKeys.other_waiting)),
              )
            ],
          ),
        ),
      );
    }

    return LoadingIndicator();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      child,
      isLoading() && needBackground
          ? Positioned.fill(child: Container(color: Color(0x73000000)))
          : Container(),
      isLoading()
          ? Align(
              alignment: Alignment.center,
              child: _loadingIndicator(),
            )
          : Container()
    ]);
  }
}

class FixedBottomButtonWrapper extends StatelessWidget {
  final List<Widget> children;
  final Widget button;

  const FixedBottomButtonWrapper(
      {@required this.children, @required this.button});

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Expanded(child: ListView(padding: EdgeInsets.zero, children: children)),
      button
    ]);
  }
}
