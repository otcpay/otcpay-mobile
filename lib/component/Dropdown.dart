import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/util/StringUtil.dart';

class DropDownField extends FormField<String> {
  final dynamic value;
  final Widget icon;
  final String hintText;
  final TextStyle hintStyle;
  final String labelText;
  final TextStyle labelStyle;
  final TextStyle textStyle;
  final bool required;
  final bool enabled;
  final List<dynamic> items;
  final List<TextInputFormatter> inputFormatters;
  final FormFieldSetter<dynamic> setter;
  final ValueChanged<dynamic> onValueChanged;
  final bool strict;
  final bool showIcon;
  final bool overrideLabelColor;
  final int itemsVisibleInDropdown;

  /// Controls the text being edited.
  ///
  /// If null, this widget will create its own [TextEditingController] and
  /// initialize its [TextEditingController.text] with [initialValue].
  final TextEditingController controller;

  DropDownField(
      {Key key,
      this.controller,
      this.value,
      this.required: false,
      this.showIcon: false,
      this.overrideLabelColor: true,
      this.icon,
      this.hintText,
      this.hintStyle: const TextStyle(
          fontWeight: FontWeight.normal, color: Colors.grey, fontSize: 18.0),
      this.labelText,
      this.labelStyle: const TextStyle(
          fontWeight: FontWeight.normal, color: Colors.grey, fontSize: 18.0),
      this.inputFormatters,
      this.items,
      this.textStyle: const TextStyle(
          fontWeight: FontWeight.bold, color: Colors.black, fontSize: 14.0),
      this.setter,
      this.onValueChanged,
      this.itemsVisibleInDropdown: 3,
      this.enabled: true,
      this.strict: true})
      : super(
          key: key,
          initialValue: controller != null ? controller.text : (value ?? ''),
          onSaved: setter,
          builder: (FormFieldState<String> field) {
            final DropDownFieldState state = field;
            final ScrollController _scrollController = ScrollController();
            final InputDecoration effectiveDecoration = InputDecoration(
                filled: true,
                icon: icon,
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                suffixIcon: Container(
                  decoration: BoxDecoration(
                      color: showIcon && overrideLabelColor
                          ? labelColor
                          : inputBoxBackgroundColor,
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(8),
                          bottomRight: Radius.circular(8))),
                  child: IconButton(
                      color: Colors.white,
                      icon: Icon(Icons.arrow_drop_down, size: 30.0),
                      onPressed: () {
                        SystemChannels.textInput.invokeMethod('TextInput.hide');
                        state.setState(() {
                          state._showdropdown = !state._showdropdown;
                        });
                      }),
                ),
                contentPadding: EdgeInsets.only(top: 16, left: 20),
                prefixIcon: value == '' || !showIcon
                    ? null
                    : Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        child: Image.asset(
                            'icons/' + getIconName(value) + '.png',
                            width: 20,
                            height: 20),
                      ),
                prefixIconConstraints: BoxConstraints(
                  minHeight: 20,
                  minWidth: 20,
                ),
                hintStyle: hintStyle,
                labelStyle: labelStyle,
                hintText: hintText,
                labelText: labelText);

            var selectedItems = state._getChildren(state._items);
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                        child: Container(
                      decoration: BoxDecoration(
                          color: inputBoxBackgroundColor,
                          borderRadius: BorderRadius.circular(8.0)),
                      child: TextFormField(
                        controller: state.effectiveController,
                        decoration: effectiveDecoration.copyWith(
                            errorText: field.errorText),
                        style: textStyle,
                        textAlign: TextAlign.start,
                        autofocus: false,
                        obscureText: false,
                        maxLines: 1,
                        onSaved: setter,
                        enabled: enabled,
                        inputFormatters: inputFormatters,
                        showCursor: false,
                      ),
                    )),
                  ],
                ),
                !state._showdropdown
                    ? Container()
                    : Container(
                        alignment: Alignment.topCenter,
                        height: selectedItems.length < itemsVisibleInDropdown
                            ? selectedItems.length * 48.0
                            : itemsVisibleInDropdown * 48.0,
                        //limit to default 3 items in dropdownlist view and then remaining scrolls
                        width: MediaQuery.of(field.context).size.width,
                        child: ListView(
                          padding: EdgeInsets.zero,
                          cacheExtent: 0.0,
                          scrollDirection: Axis.vertical,
                          controller: _scrollController,
                          children: items.isNotEmpty
                              ? ListTile.divideTiles(
                                      context: field.context,
                                      tiles: selectedItems)
                                  .toList()
                              : [],
                        ),
                      ),
              ],
            );
          },
        );

  @override
  DropDownFieldState createState() => DropDownFieldState();
}

class DropDownFieldState extends FormFieldState<String> {
  TextEditingController _controller;
  bool _showdropdown = false;
  String _searchText = "";

  @override
  DropDownField get widget => super.widget;

  TextEditingController get effectiveController =>
      widget.controller ?? _controller;

  List<String> get _items => widget.items;

  @override
  void didUpdateWidget(DropDownField oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.controller != oldWidget.controller) {
      oldWidget.controller?.removeListener(_handleControllerChanged);
      widget.controller?.addListener(_handleControllerChanged);

      if (oldWidget.controller != null && widget.controller == null)
        _controller =
            TextEditingController.fromValue(oldWidget.controller.value);
      if (widget.controller != null) {
        setValue(widget.controller.text);
        if (oldWidget.controller == null) _controller = null;
      }
    }
  }

  @override
  void dispose() {
    widget.controller?.removeListener(_handleControllerChanged);
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    if (widget.controller == null) {
      _controller = TextEditingController(text: widget.initialValue);
    }

    effectiveController.addListener(_handleControllerChanged);

    _searchText = effectiveController.text;
  }

  @override
  void reset() {
    super.reset();
    setState(() {
      effectiveController.text = widget.initialValue;
    });
  }

  List<ListTile> _getChildren(List<String> items) {
    List<ListTile> childItems = [];
    for (var item in items) {
      if (_searchText.isNotEmpty) {
        if (item.toUpperCase().contains(_searchText.toUpperCase()))
          childItems.add(_getListTile(item));
      } else {
        childItems.add(_getListTile(item));
      }
    }
    return childItems;
  }

  ListTile _getListTile(String text) {
    return ListTile(
      tileColor: dropdownBgColor,
      dense: true,
      leading: Image.asset('icons/' + getIconName(text) + '.png',
          width: 20, height: 20),
      title: Text(
        text,
        style: TextStyle(color: Colors.white),
      ),
      onTap: () {
        setState(() {
          effectiveController.text = text;
          _handleControllerChanged();
          _showdropdown = false;
          if (widget.onValueChanged != null) widget.onValueChanged(text);
        });

        FocusScope.of(context).unfocus();
      },
    );
  }

  void _handleControllerChanged() {
    // Suppress changes that originated from within this class.
    //
    // In the case where a controller has been passed in to this widget, we
    // register this change listener. In these cases, we'll also receive change
    // notifications for changes originating from within this class -- for
    // example, the reset() method. In such cases, the FormField value will
    // already have been set.
    if (effectiveController.text != value) didChange(effectiveController.text);

    setState(() {
      if (effectiveController.text.isEmpty) {
        _searchText = "";
      } else {
        _searchText = effectiveController.text;
      }
    });
  }
}

class ConfigurableExpansionTile extends StatefulWidget {
  final ValueChanged<bool> onExpansionChanged;
  final List<Widget> children;
  final Color headerBackgroundColorStart;
  final Color headerBackgroundColorEnd;
  final Color expandedBackgroundColor;
  final bool initiallyExpanded;
  final Widget header;
  final Widget headerExpanded;
  final Widget animatedWidgetFollowingHeader;
  final Widget animatedWidgetPrecedingHeader;
  final Duration kExpand;
  final Color borderColorStart;
  final Color borderColorEnd;
  final bool topBorderOn;
  final bool bottomBorderOn;
  final Animatable<double> headerAnimationTween;
  final Animatable<double> borderAnimationTween;
  final Animatable<double> animatedWidgetTurnTween;
  final Animatable<double> animatedWidgetTween;

  const ConfigurableExpansionTile(
      {Key key,
      this.headerBackgroundColorStart = Colors.transparent,
      this.onExpansionChanged,
      this.children = const <Widget>[],
      this.initiallyExpanded = false,
      @required this.header,
      this.animatedWidgetFollowingHeader,
      this.animatedWidgetPrecedingHeader,
      this.expandedBackgroundColor,
      this.borderColorStart = Colors.transparent,
      this.borderColorEnd = Colors.transparent,
      this.topBorderOn = true,
      this.bottomBorderOn = true,
      this.kExpand = const Duration(milliseconds: 200),
      this.headerBackgroundColorEnd,
      this.headerExpanded,
      this.headerAnimationTween,
      this.borderAnimationTween,
      this.animatedWidgetTurnTween,
      this.animatedWidgetTween})
      : assert(initiallyExpanded != null),
        super(key: key);

  static final Animatable<double> _easeInTween =
      CurveTween(curve: Curves.easeIn);
  static final Animatable<double> _halfTween =
      Tween<double>(begin: 0.0, end: 0.5);

  static final Animatable<double> _easeOutTween =
      CurveTween(curve: Curves.easeOut);

  @override
  ConfigurableExpansionTileState createState() =>
      ConfigurableExpansionTileState();
}

class ConfigurableExpansionTileState extends State<ConfigurableExpansionTile>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> _iconTurns;
  Animation<double> _heightFactor;

  Animation<Color> _borderColor;
  Animation<Color> _headerColor;

  final ColorTween _borderColorTween = ColorTween();
  final ColorTween _headerColorTween = ColorTween();

  bool _isExpanded = false;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(duration: widget.kExpand, vsync: this);
    _heightFactor = _controller.drive(ConfigurableExpansionTile._easeInTween);
    _iconTurns = _controller.drive(
        (widget.animatedWidgetTurnTween ?? ConfigurableExpansionTile._halfTween)
            .chain(widget.animatedWidgetTween ??
                ConfigurableExpansionTile._easeInTween));

    _borderColor = _controller.drive(_borderColorTween.chain(
        widget.borderAnimationTween ??
            ConfigurableExpansionTile._easeOutTween));
    _borderColorTween.end = widget.borderColorEnd;

    _headerColor = _controller.drive(_headerColorTween.chain(
        widget.headerAnimationTween ?? ConfigurableExpansionTile._easeInTween));
    _headerColorTween.end =
        widget.headerBackgroundColorEnd ?? widget.headerBackgroundColorStart;
    _isExpanded =
        PageStorage.of(context)?.readState(context) ?? widget.initiallyExpanded;
    if (_isExpanded) _controller.value = 1.0;
    _controller.addListener(_listener);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _listener() {
    if (_controller.value == 0 && widget.onExpansionChanged != null)
      widget.onExpansionChanged(_isExpanded);
  }

  void handleTap() {
    setState(() {
      _isExpanded = !_isExpanded;
      if (_isExpanded) {
        _controller.forward();
      } else {
        _controller.reverse().then<void>((void value) {
          if (!mounted) return;
          setState(() {
            // Rebuild without widget.children.
          });
        });
      }
      PageStorage.of(context)?.writeState(context, _isExpanded);
    });
    if (widget.onExpansionChanged != null && _isExpanded)
      widget.onExpansionChanged(_isExpanded);
  }

  Widget _buildChildren(BuildContext context, Widget child) {
    final Color borderSideColor = _borderColor.value ?? widget.borderColorStart;
    final Color headerColor =
        _headerColor?.value ?? widget.headerBackgroundColorStart;
    return Container(
      decoration: BoxDecoration(
          border: Border(
        top: BorderSide(
            color: widget.topBorderOn ? borderSideColor : Colors.transparent),
        bottom: BorderSide(
            color:
                widget.bottomBorderOn ? borderSideColor : Colors.transparent),
      )),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          GestureDetector(
              onTap: handleTap,
              child: Container(
                  color: headerColor,
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      RotationTransition(
                        turns: _iconTurns,
                        child:
                            widget.animatedWidgetPrecedingHeader ?? Container(),
                      ),
                      _getHeader(),
                      RotationTransition(
                        turns: _iconTurns,
                        child:
                            widget.animatedWidgetFollowingHeader ?? Container(),
                      )
                    ],
                  ))),
          ClipRect(
            child: Align(
              heightFactor: _heightFactor.value,
              child: child,
            ),
          ),
        ],
      ),
    );
  }

  Widget _getHeader() {
    if (!_isExpanded) {
      return widget.header;
    } else {
      return widget.headerExpanded ?? widget.header;
    }
  }

  @override
  Widget build(BuildContext context) {
    final bool closed = !_isExpanded && _controller.isDismissed;
    return AnimatedBuilder(
      animation: _controller.view,
      builder: _buildChildren,
      child: closed
          ? null
          : Container(
              color: widget.expandedBackgroundColor ?? Colors.transparent,
              child: Column(children: widget.children)),
    );
  }
}
