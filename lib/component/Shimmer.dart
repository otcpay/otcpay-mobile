import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/style/Spec.dart';
import 'package:shimmer/shimmer.dart';

// ignore_for_file: non_constant_identifier_names

Widget OTCItemListShimmer() {
  return ListView.builder(
      itemBuilder: (_, __) => Container(
          height: otcItemHeight,
          padding: otcItemPadding,
          margin: otcItemMargin,
          decoration: BoxDecoration(gradient: quoteItemGradient),
          child: Column(children: [
            Row(children: [
              Container(
                width: otcItemAvatarRadius * 2,
                height: otcItemAvatarRadius * 2,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(otcItemAvatarRadius),
                    color: Colors.white),
              ),
              Container(
                margin: otcItemTitleMargin,
                width: 40,
                height: 15,
                color: Colors.white,
              ),
            ]),
            Container(
                height: otcItemDetailHeight,
                margin: otcItemDetailMargin,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: 60,
                            height: 15,
                            color: Colors.white,
                          ),
                          Container(
                            width: 150,
                            height: 15,
                            color: Colors.white,
                          ),
                          Container(
                            width: 100,
                            height: 10,
                            color: Colors.white,
                          ),
                        ]),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Container(
                          width: 50,
                          height: 20,
                          color: Colors.white,
                        ),
                        Container(
                          width: 90,
                          height: 35,
                          color: Colors.white,
                        ),
                      ],
                    )
                  ],
                ))
          ])));
}

Widget QuoteItemListShimmer() {
  return ListView.builder(
    itemBuilder: (_, __) => Material(
      elevation: globalElevation,
      child: Container(
        decoration: BoxDecoration(
          gradient: quoteItemGradient,
          borderRadius: BorderRadius.circular(8.0),
        ),
        margin: quoteItemMargin,
        padding: quoteItemPadding,
        height: quoteItemHeight,
        child: Shimmer.fromColors(
          baseColor: Colors.grey[700],
          highlightColor: Colors.grey[400],
          enabled: true,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: quoteItemTopRowHeight,
                    width: 100,
                    color: Colors.white,
                  ),
                  Container(
                    height: quoteItemTopRowHeight,
                    width: 150,
                    color: Colors.white,
                  ),
                ],
              ),
              const Padding(padding: quoteItemInnerPadding),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: quoteItemTopRowHeight,
                    width: 20,
                    color: Colors.white,
                  ),
                  Container(
                    height: quoteItemTopRowHeight,
                    width: 20,
                    color: Colors.white,
                  ),
                  Container(
                    height: quoteItemTopRowHeight,
                    width: 20,
                    color: Colors.white,
                  ),
                ],
              ),
              const Padding(
                padding: EdgeInsets.only(top: 2.0),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: quoteItemTopRowHeight,
                    width: 60,
                    color: Colors.white,
                  ),
                  Container(
                    height: quoteItemTopRowHeight,
                    width: 60,
                    color: Colors.white,
                  ),
                  Container(
                    height: quoteItemTopRowHeight,
                    width: 60,
                    color: Colors.white,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    ),
    itemCount: 6,
  );
}

Widget BannerShimmer() {
  return Shimmer.fromColors(
    baseColor: Colors.grey[700],
    highlightColor: Colors.grey[400],
    enabled: true,
    child: Container(
      width: double.infinity,
      height: bannerHeight,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8.0),
      ),
    ),
  );
}

Widget BoardShimmer() {
  return ListView.builder(
    itemBuilder: (_, index) => Container(
        height: boardItemHeight,
        padding: boardItemPadding,
        decoration: BoxDecoration(
            color: inputBoxBackgroundColor,
            gradient: index % 2 == 0 ? boardItemGradient : null),
        child: Shimmer.fromColors(
          baseColor: Colors.grey[700],
          highlightColor: Colors.grey[400],
          enabled: true,
          child:
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            Container(
              width: 110.0,
              height: 30.0,
              color: Colors.white,
            ),
            Container(
              width: 50.0,
              height: 30.0,
              color: Colors.white,
            ),
            Container(
              width: 55.0,
              height: 30.0,
              color: Colors.white,
            ),
          ]),
        )),
    itemCount: 6,
  );
}
