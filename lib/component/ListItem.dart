import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/auth/authenticate.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/screen/LoginPage.dart';
import 'package:otcpay/screen/home/otc/NewAdPage.dart';
import 'package:otcpay/screen/home/otc/header/MainFilter.dart';
import 'package:otcpay/screen/home/payment/RequestOrderPage.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/style/Spec.dart';
import 'package:otcpay/util/StringUtil.dart';
import 'package:otcpay/util/TimeUtil.dart';

import 'Indicator.dart';
import 'Input.dart';

enum RATE_MODE { BUY, SELL, REMIT }

class DailyRateItem extends StatelessWidget {
  final String fromCurrency;
  final String toCurrency;
  final String rate;
  final RATE_MODE mode;
  final bool isGradient;
  final void Function() callback;

  const DailyRateItem({
    @required this.fromCurrency,
    @required this.toCurrency,
    @required this.rate,
    @required this.mode,
    @required this.callback,
    this.isGradient = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: boardItemHeight,
      padding: boardItemPadding,
      decoration: BoxDecoration(
          color: inputBoxBackgroundColor,
          gradient: isGradient ? boardItemGradient : null),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            margin: EdgeInsets.only(right: 5.0),
            child: BoldTitle(title: '$fromCurrency > $toCurrency'),
          ),
          BoldTitle(title: rate, color: globalSecondaryColor),
          TextButton(
              onPressed: callback,
              child: BoldTitle(
                  title: getBuySellRemitButtonTitle(mode),
                  fontSize: 12,
                  color: mode == RATE_MODE.REMIT
                      ? globalBackgroundColor
                      : Colors.white),
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(mode == RATE_MODE.BUY
                          ? upColor
                          : mode == RATE_MODE.REMIT
                              ? globalSecondaryColor
                              : downColor)))
        ],
      ),
    );
  }
}

class QuoteListItem extends StatelessWidget {
  final String fromCurrency;
  final String toCurrency;
  final String person;
  final String time;
  final String amount;
  final String rate;
  final String saved;
  final String id;
  final String type;
  final String status;

  final bool showQuoteBy;
  final bool showOrderId;
  final bool overrideGradient;

  const QuoteListItem(
      {@required this.fromCurrency,
      @required this.toCurrency,
      @required this.person,
      @required this.time,
      @required this.amount,
      @required this.rate,
      @required this.saved,
      @required this.status,
      this.id,
      this.type,
      this.showOrderId = false,
      this.showQuoteBy = true,
      this.overrideGradient = false});

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: globalElevation,
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            color: overrideGradient ? inputBoxBackgroundColor : null,
            gradient: overrideGradient ? null : quoteItemGradient),
        height: showOrderId ? 116 : quoteItemHeight,
        padding: quoteItemPadding,
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(children: [
                  Image.asset('icons/$fromCurrency.png',
                      width: quoteItemTopRowHeight,
                      height: quoteItemTopRowHeight),
                  Container(
                    margin: EdgeInsets.only(left: 8.0),
                    child: BoldTitle(title: '$fromCurrency  →  '),
                  ),
                  Image.asset('icons/$toCurrency.png',
                      width: quoteItemTopRowHeight,
                      height: quoteItemTopRowHeight),
                  Container(
                    margin: EdgeInsets.only(left: 8.0),
                    child: BoldTitle(title: '$toCurrency'),
                  )
                ]),
                SubTitle(
                  title: showQuoteBy
                      ? tr(LocaleKeys.other_quotedBy) + person + ', ' + time
                      : tr(LocaleKeys.other_placed) + time,
                  color: subTitleColor,
                )
              ],
            ),
            Container(
              margin: quoteItemInnerPadding,
              child: Row(
                children: [
                  Expanded(
                    child: SubTitle(
                      title: tr(LocaleKeys.common_amount),
                      color: subTitleColor,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 20.0),
                    child: SubTitle(
                      title: tr(LocaleKeys.common_rate),
                      color: subTitleColor,
                    ),
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        SubTitle(
                          title: tr(LocaleKeys.common_saved),
                          color: subTitleColor,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(child: BoldTitle(title: toCurrency + amount)),
                Container(
                  margin: EdgeInsets.only(right: 30.0),
                  child: BoldTitle(title: rate),
                ),
                Expanded(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    BoldTitle(
                        title: fromCurrency + saved,
                        color: status != null && status == 'Expired'
                            ? subTitleColor
                            : globalPrimaryColor),
                  ],
                )),
              ],
            ),
            showOrderId
                ? OrderItemFooter(id: id, type: type, status: status)
                : Container(),
          ],
        ),
      ),
    );
  }
}

class SettingListItem extends StatelessWidget {
  final String item;
  final String icon;
  final void Function() callback;
  final bool needExtraMargin;
  final bool needExtraHeight;
  final bool showLeading;
  final bool showTrailing;
  final EdgeInsets overrideMargin;

  const SettingListItem(
      {@required this.item,
      @required this.icon,
      @required this.callback,
      this.overrideMargin,
      this.needExtraMargin = false,
      this.needExtraHeight = false,
      this.showLeading = true,
      this.showTrailing = true});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: needExtraMargin
            ? EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0)
            : overrideMargin == null
                ? EdgeInsets.only(top: 2.0, left: 20.0, right: 20.0)
                : overrideMargin,
        child: Material(
          elevation: globalElevation,
          color: inputBoxBackgroundColor,
          borderRadius: BorderRadius.circular(8.0),
          child: ListTile(
            contentPadding: needExtraHeight
                ? EdgeInsets.fromLTRB(10, 15, 10, 15)
                : EdgeInsets.only(left: 10, right: 10),
            horizontalTitleGap: 0,
            onTap: callback,
            leading: showLeading
                ? Image.asset('icons/' + getIconName(icon) + '.png',
                    width: 34, height: 34)
                : null,
            title: NormalTitle(title: item),
            trailing: showTrailing
                ? Icon(Icons.arrow_forward_ios, color: subTitleColor)
                : null,
          ),
        ));
  }
}

class OTCListItem extends StatefulWidget {
  final String id;
  final String name;
  final String rate;
  final String quantity;
  final String upperLimit;
  final String lowerLimit;
  final String fromCurrency;
  final String toCurrency;
  final String profilePic;
  final String time;
  final String status;
  final bool verified;
  final RATE_MODE mode;

  final bool showQuickAction;
  final bool showGradient;
  final bool showOrderId;
  final bool showOption;

  const OTCListItem(
      {@required this.id,
      @required this.name,
      @required this.rate,
      @required this.quantity,
      @required this.upperLimit,
      @required this.lowerLimit,
      @required this.fromCurrency,
      @required this.toCurrency,
      @required this.time,
      @required this.mode,
      @required this.profilePic,
      this.verified,
      this.status,
      this.showQuickAction = true,
      this.showGradient = true,
      this.showOrderId = false,
      this.showOption = false});

  @override
  _OTCListItemState createState() => _OTCListItemState();
}

class _OTCListItemState extends State<OTCListItem> {
  void _navigate() {
    AuthenticateState state = BlocProvider.of<AuthenticateBloc>(context).state;

    if (state is Authenticated) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (innerContext) => RequestOrderPage(
                    id: widget.id,
                    mode: widget.mode,
                    rate: widget.rate,
                    fromCurrency: widget.mode == RATE_MODE.BUY
                        ? widget.fromCurrency
                        : widget.toCurrency,
                    toCurrency: widget.mode == RATE_MODE.BUY
                        ? widget.toCurrency
                        : widget.fromCurrency,
                    lowerLimit: widget.lowerLimit,
                    upperLimit: widget.upperLimit,
                  )));
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (_) => LoginPage(isFirstPage: true)),
          (route) => false);
    }
  }

  void _edit() {
    Navigator.of(context).pop();
    Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => NewAdPage(
              currency: _getCurrency(),
              mode: widget.mode,
              isEdit: true,
              id: widget.id,
              settleCurrency: widget.mode == RATE_MODE.BUY
                  ? widget.toCurrency
                  : widget.fromCurrency,
              max: widget.upperLimit,
              min: widget.lowerLimit,
              quantity: widget.quantity,
              rate: widget.rate,
            )));
  }

  void _copyId() {
    Clipboard.setData(ClipboardData(text: widget.id));

    final snackBar = SnackBar(content: Text(tr(LocaleKeys.common_coped)));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void _dismiss() {
    Navigator.of(context).pop();
  }

  void _deleteAd() {
    _dismiss();

    _showAlert();
  }

  void _confirm() {
    _dismiss();

    BlocProvider.of<GeneralBloc>(context).add(DeleteAd(widget.id));
  }

  void _showAlert() {
    showDialog(
      context: context,
      builder: (BuildContext context) => Theme(
        data: ThemeData.dark(),
        child: CupertinoAlertDialog(
          title: NormalTitle(title: tr(LocaleKeys.common_confirmDelete)),
          actions: [
            CupertinoDialogAction(
                isDefaultAction: false,
                child: NormalTitle(title: tr(LocaleKeys.common_cancel)),
                onPressed: _dismiss),
            CupertinoDialogAction(
                isDefaultAction: false,
                child: NormalTitle(
                    title: tr(LocaleKeys.common_yes),
                    color: globalPrimaryColor),
                onPressed: _confirm)
          ],
        ),
      ),
    );
  }

  void _action(BuildContext context) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        backgroundColor: inputBoxBackgroundColor,
        context: context,
        builder: (context) {
          return Container(
            height: 175,
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: labelColor,
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(8),
                          topLeft: Radius.circular(8))),
                  child: ListTile(
                    onTap: _edit,
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        BoldTitle(
                            title: tr(LocaleKeys.common_edit), fontSize: 16),
                      ],
                    ),
                  ),
                ),
                Container(
                  color: labelColor,
                  margin: EdgeInsets.only(bottom: 5),
                  child: ListTile(
                    onTap: _deleteAd,
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        BoldTitle(
                            title: tr(LocaleKeys.other_removeAd), fontSize: 16),
                      ],
                    ),
                  ),
                ),
                Container(
                  color: labelColor,
                  child: ListTile(
                    onTap: _dismiss,
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        BoldTitle(
                            title: tr(LocaleKeys.common_cancel),
                            fontSize: 16,
                            color: cancelColor),
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  bool isExpired() {
    return widget.status == null || widget.status == 'Expired';
  }

  int _getCurrency() {
    String crypto =
        widget.mode == RATE_MODE.BUY ? widget.fromCurrency : widget.toCurrency;
    return options.indexOf(crypto);
  }

  String _profilePic() {
    if (widget.profilePic == null || widget.profilePic.isEmpty)
      return 'https://via.placeholder.com/150';

    return widget.profilePic + '?alt=media';
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: otcItemHeight,
      padding: otcItemPadding,
      margin: otcItemMargin,
      decoration: BoxDecoration(
          color: widget.showGradient ? null : inputBoxBackgroundColor,
          gradient: widget.showGradient ? quoteItemGradient : null),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  CircleAvatar(
                    radius: otcItemAvatarRadius,
                    backgroundImage: NetworkImage(_profilePic()),
                  ),
                  Container(
                    margin: otcItemTitleMargin,
                    child: BoldTitle(title: widget.name),
                  ),
                  widget.verified != null && widget.verified
                      ? Image.asset('icons/verified_gold.png',
                          width: 14, height: 14)
                      : Container()
                ],
              ),
              widget.showOption
                  ? Material(
                      child: InkWell(
                        splashColor: Colors.white,
                        onTap: () => _action(context),
                        child: Icon(Icons.menu, color: Colors.white, size: 14),
                      ),
                    )
                  : Container(),
            ],
          ),
          Container(
            height: otcItemDetailHeight,
            margin: otcItemDetailMargin,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        child: NormalTitle(
                            title: tr(LocaleKeys.common_quantity) +
                                ': ' +
                                widget.quantity +
                                '' +
                                widget.toCurrency)),
                    Container(
                      child: NormalTitle(
                          title: tr(LocaleKeys.common_limit) +
                              ': ' +
                              widget.fromCurrency +
                              widget.lowerLimit +
                              ' - ' +
                              widget.fromCurrency +
                              widget.upperLimit),
                    ),
                    widget.showOrderId
                        ? Container(
                            height: 24,
                            child: Row(
                              children: [
                                Container(
                                    margin: EdgeInsets.only(top: 8, right: 5),
                                    child: SubTitle(
                                        title: tr(LocaleKeys.other_orderId) +
                                            trimHypen(widget.id),
                                        color: subTitleColor)),
                                Material(
                                  child: InkWell(
                                    splashColor: Colors.white,
                                    onTap: _copyId,
                                    child: Icon(Icons.copy,
                                        color: Colors.white, size: 14),
                                  ),
                                ),
                              ],
                            ),
                          )
                        : SubTitle(
                            title: tr(LocaleKeys.other_updated) + widget.time,
                            color: subTitleColor),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                        child: RichText(
                      text: TextSpan(
                          style: TextStyle(
                              color: isExpired()
                                  ? subTitleColor
                                  : globalPrimaryColor,
                              fontWeight: FontWeight.bold),
                          children: [
                            TextSpan(text: widget.fromCurrency),
                            TextSpan(
                              text: widget.rate,
                              style: TextStyle(fontSize: 20.0),
                            ),
                          ]),
                    )),
                    widget.showQuickAction
                        ? TextButton(
                            onPressed: isExpired() ? null : _navigate,
                            child: BoldTitle(
                                title: getBuySellRemitButtonTitle(widget.mode),
                                fontSize: 16,
                                color: Colors.white),
                            style: ButtonStyle(
                                padding: MaterialStateProperty.all(
                                    EdgeInsets.only(
                                        left: 30,
                                        right: 30,
                                        top: 5,
                                        bottom: 5)),
                                backgroundColor:
                                    MaterialStateProperty.all(isExpired()
                                        ? subTitleColor
                                        : widget.mode == RATE_MODE.BUY
                                            ? upColor
                                            : downColor)))
                        : Container(
                            height: 13,
                            child: ModeIndicator(
                                color: isExpired()
                                    ? subTitleColor
                                    : globalPrimaryColor,
                                mode: widget.status,
                                needMargin: false,
                                fontSize: 10)),
                    widget.showOrderId
                        ? SubTitle(
                            title: tr(LocaleKeys.other_updated) + widget.time,
                            color: subTitleColor)
                        : Container()
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class ProfileListItem extends StatelessWidget {
  final String title;
  final String value;

  final Color color;

  final bool showTrailingIcon;
  final bool showValue;
  final void Function() callback;

  const ProfileListItem(
      {@required this.title,
      this.value,
      this.color = inputBoxBackgroundColor,
      this.showTrailingIcon = false,
      this.showValue = true,
      this.callback});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: callback,
      child: Container(
        height: 45,
        decoration:
            BoxDecoration(color: color, borderRadius: BorderRadius.circular(8)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            InputTitle(title: title),
            showTrailingIcon
                ? Icon(showTrailingIcon ? Icons.check : Icons.arrow_forward_ios,
                    color: subTitleColor)
                : showValue
                    ? NormalTitle(title: value)
                    : Container()
          ],
        ),
      ),
    );
  }
}

class AdRequestListItem extends StatelessWidget {
  final RATE_MODE mode;
  final String id;
  final String offerRate;
  final String amount;
  final String updatedDate;
  final String settleCurrency;
  final String crytocurrency;
  final String totalCost;

  const AdRequestListItem(
      {@required this.id,
      @required this.amount,
      @required this.updatedDate,
      @required this.offerRate,
      @required this.mode,
      @required this.settleCurrency,
      @required this.crytocurrency,
      @required this.totalCost});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 135.0,
      decoration: BoxDecoration(
          color: inputBoxBackgroundColor,
          borderRadius: BorderRadius.circular(8)),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  BuySellIndicator(fontSize: 14, mode: mode),
                  Container(
                      margin: EdgeInsets.only(bottom: 1),
                      child: BoldTitle(title: crytocurrency)),
                ],
              ),
              Container(
                  margin: EdgeInsets.only(right: 20),
                  child: SubTitle(
                      title: tr(LocaleKeys.other_placed) + updatedDate,
                      color: subTitleColor))
            ],
          ),
          Container(
            height: 65.0,
            margin: EdgeInsets.only(top: 5.0),
            child: Row(
              children: [
                Container(
                  margin: EdgeInsets.only(left: 20),
                  width: 80,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SubTitle(title: tr(LocaleKeys.common_price)),
                      SubTitle(title: tr(LocaleKeys.common_cost)),
                      SubTitle(title: tr(LocaleKeys.common_quantity)),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      BoldTitle(title: offerRate, fontSize: 10),
                      BoldTitle(
                          title: '$totalCost $settleCurrency', fontSize: 10),
                      BoldTitle(title: '$amount $crytocurrency', fontSize: 10),
                    ],
                  ),
                )
              ],
            ),
          ),
          Container(
              margin: EdgeInsets.only(left: 20), child: OrderItemFooter(id: id))
        ],
      ),
    );
  }
}

class OrderItemFooter extends StatelessWidget {
  final String id;
  final String type;
  final String status;

  const OrderItemFooter({@required this.id, this.type, this.status});

  void _copyId(BuildContext context) {
    Clipboard.setData(ClipboardData(text: id));

    final snackBar = SnackBar(content: Text(tr(LocaleKeys.common_coped)));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Expanded(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                  margin: EdgeInsets.only(top: 8),
                  child: SubTitle(
                      title: tr(LocaleKeys.other_orderId) + trimHypen(id),
                      color: subTitleColor)),
              SizedBox(
                height: 24,
                width: 24,
                child: IconButton(
                    padding: EdgeInsets.zero,
                    color: Colors.white,
                    icon: Icon(Icons.copy),
                    onPressed: () => _copyId(context),
                    iconSize: 14),
              )
            ],
          ),
        ),
        type == null
            ? Container()
            : ModeIndicator(
                mode: type,
                color: status != null && status == 'Expired'
                    ? subTitleColor
                    : globalPrimaryColor)
      ],
    );
  }
}

class ManageItem extends StatelessWidget {
  final Widget title;
  final Widget subTitle;

  final void Function() onEdit;
  final void Function() onDelete;
  final void Function() onSetDefault;

  final String mode;
  final String subType;

  final bool isDefault;
  final bool showRedirect;
  final bool showIcon;
  final bool showIndicator;
  final bool useGradient;

  const ManageItem({
    @required this.title,
    @required this.subTitle,
    this.onEdit,
    this.onDelete,
    this.onSetDefault,
    this.mode,
    this.subType,
    this.showRedirect = false,
    this.isDefault = false,
    this.showIcon = false,
    this.showIndicator = false,
    this.useGradient = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 145,
        child: Card(
          color: inputBoxBackgroundColor,
          elevation: 2,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      showIcon
                          ? Padding(
                              padding: EdgeInsets.only(left: 25, top: 15),
                              child: Image.asset('icons/bitcoin.png',
                                  width: 26, height: 26),
                            )
                          : Container(),
                      Padding(
                        padding:
                            EdgeInsets.only(left: showIcon ? 10 : 25, top: 15),
                        child: title,
                      ),
                    ],
                  ),
                  showIndicator
                      ? Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                margin: EdgeInsets.only(
                                    top: 15, right: subType == null ? 10 : 0),
                                child: ModeIndicator(
                                    mode: mode,
                                    needMargin: false,
                                    fontSize: 10)),
                            subType == null
                                ? Container()
                                : Container(
                                    margin: EdgeInsets.only(top: 15, right: 10),
                                    child: ModeIndicator(
                                        mode: subType,
                                        needMargin: false,
                                        shrinkMargin: true,
                                        fontSize: 10))
                          ],
                        )
                      : Container()
                ],
              ),
              Padding(
                  padding: EdgeInsets.only(left: showIcon ? 25 : 15, top: 15),
                  child: subTitle),
              Padding(
                  padding: EdgeInsets.all(10),
                  child: Divider(color: subTitleColor)),
              Container(
                height: 25,
                child: showRedirect
                    ? Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 20),
                            child: GestureDetector(
                              onTap: () =>
                                  Navigator.of(context).pushNamed('/bank'),
                              child: Text(
                                tr(LocaleKeys.bankPage_recordTitle),
                                style: TextStyle(
                                  fontSize: 16,
                                  color: subTitleColor,
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            ),
                          )
                        ],
                      )
                    : Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                padding: EdgeInsets.only(left: 12, right: 10),
                                child: InkWell(
                                  onTap: onSetDefault,
                                  child: Icon(Icons.check_circle_outline,
                                      size: 25,
                                      color:
                                          isDefault ? upColor : subTitleColor),
                                ),
                              ),
                              NormalTitle(
                                  title: tr(LocaleKeys.common_default),
                                  color: subTitleColor)
                            ],
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Container(
                                padding: EdgeInsets.only(right: 5),
                                decoration: BoxDecoration(
                                    border: Border(
                                        right:
                                            BorderSide(color: subTitleColor))),
                                child: TextButton(
                                    style: TextButton.styleFrom(
                                        minimumSize: Size(25, 15),
                                        padding:
                                            EdgeInsets.fromLTRB(5, 2, 5, 2)),
                                    onPressed: onEdit,
                                    child: NormalTitle(
                                        title: tr(LocaleKeys.common_edit),
                                        color: subTitleColor)),
                              ),
                              Container(
                                padding: EdgeInsets.only(left: 10, right: 5),
                                child: TextButton(
                                    style: TextButton.styleFrom(
                                        minimumSize: Size(25, 15),
                                        padding:
                                            EdgeInsets.fromLTRB(5, 2, 5, 2)),
                                    onPressed: onDelete,
                                    child: NormalTitle(
                                        title: tr(LocaleKeys.common_delete),
                                        color: subTitleColor)),
                              )
                            ],
                          )
                        ],
                      ),
              ),
            ],
          ),
        ));
  }
}

class NotificationItem extends StatelessWidget {
  final String title;
  final String body;
  final String time;
  final bool read;

  const NotificationItem(
      {@required this.title,
      @required this.time,
      @required this.read,
      @required this.body});

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 136,
        child: Card(
            color: inputBoxBackgroundColor,
            elevation: 2,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 10, top: 5),
                        child: Icon(Icons.brightness_1,
                            size: 10.0,
                            color: !read ? Colors.red : Colors.transparent),
                      )
                    ],
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 10, right: 80),
                      child: NormalTitle(title: title)),
                  Container(
                      margin: EdgeInsets.only(top: 10, left: 10, right: 80),
                      child: SubTitle(
                          title: body, color: subTitleColor, fontSize: 12)),
                  Padding(
                      padding: EdgeInsets.all(10),
                      child: Divider(color: subTitleColor)),
                  Container(
                      margin: EdgeInsets.only(left: 10),
                      child: NormalTitle(
                          title: getRelativeTime(time),
                          fontSize: 12,
                          color: subTitleColor))
                ])));
  }
}
