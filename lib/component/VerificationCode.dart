import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/api/service.dart';
import 'package:otcpay/bloc/auth/authenticate.dart';
import 'package:otcpay/bloc/auth/authenticate_bloc.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/style/Spec.dart';
import 'package:otcpay/util/ParamUtil.dart';

import 'Input.dart';
import 'Title.dart';
import 'Wrapper.dart';

class VerificationCode extends StatefulWidget {
  final void Function() callback;
  final double titleFontSize;
  final bool needFullWidth;
  final bool showClose;
  final String account;
  final EmailType type;

  const VerificationCode(
      {@required this.callback,
      @required this.account,
      @required this.type,
      this.showClose = true,
      this.needFullWidth = false,
      this.titleFontSize = 24.0});

  @override
  _VerificationCodeState createState() => _VerificationCodeState();
}

class _VerificationCodeState extends State<VerificationCode> {
  final Duration oneSec = const Duration(seconds: 1);

  AuthenticateBloc bloc;

  int _start = 59;
  Timer _timer;

  void _startTimer() {
    _sendCode();

    _timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          timer.cancel();
          setState(() {
            _start = 60;
          });
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
  }

  bool _validate() {
    return _start == 0 || _start == 60;
  }

  void _dismissAlert() {
    Navigator.pop(context);
  }

  void _showAlert(String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) => Theme(
        data: ThemeData.dark(),
        child: CupertinoAlertDialog(
          title: NormalTitle(title: message),
          actions: [
            CupertinoDialogAction(
                isDefaultAction: true,
                child: NormalTitle(title: tr(LocaleKeys.common_ok)),
                onPressed: _dismissAlert)
          ],
        ),
      ),
    );
  }

  void _complete(String code) {
    FocusScope.of(context).unfocus();

    if ((_start == 0 || _start == 60)) {}
    bloc.add(VerifyCode(code: code, account: widget.account));
  }

  void _sendCode() {
    bloc.add(SendCode(account: widget.account, type: widget.type));
  }

  void _dismiss() {
    Navigator.of(context).pop();
  }

  List<Widget> _renderChildren() {
    return [
      widget.showClose
          ? Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  TextButton(
                      onPressed: _dismiss,
                      child: NormalTitle(
                        title: tr(LocaleKeys.common_close),
                        color: subTitleColor,
                      ))
                ],
              ),
            )
          : Container(),
      Container(
          margin: EdgeInsets.only(
              top: widget.showClose ? 0 : 30,
              left: preAuthMargin,
              right: preAuthMargin,
              bottom: 5),
          child: Row(
            children: [
              BoldTitle(
                  title: tr(LocaleKeys.verificationPage_enterDigit),
                  fontSize: widget.titleFontSize),
            ],
          )),
      Container(
        margin: EdgeInsets.only(
            left: preAuthMargin, right: preAuthMargin, bottom: 40),
        child: Row(
          children: [
            RichText(
                text:
                    TextSpan(style: TextStyle(color: subTitleColor), children: [
              TextSpan(text: tr(LocaleKeys.verificationPage_verificationSent)),
              TextSpan(
                text: isValidPhoneNum(widget.account)
                    ? '+' + widget.account
                    : widget.account,
                style: TextStyle(fontSize: 12.0, color: Colors.white),
              ),
            ]))
          ],
        ),
      ),
      Container(
          margin: EdgeInsets.only(bottom: 25),
          padding: EdgeInsets.only(left: preAuthMargin, right: preAuthMargin),
          child: _VerificationCodeInput(
            keyboardType: TextInputType.number,
            length: 4,
            autofocus: true,
            itemDecoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(8.0)),
            onCompleted: _complete,
          )),
      ConfirmButton(
          validator: _validate,
          title: (_start == 0 || _start == 60)
              ? tr(LocaleKeys.verificationPage_sendAgain)
              : tr(LocaleKeys.verificationPage_sendAgain) + ' ($_start\s)',
          height: 50,
          margin: EdgeInsets.only(
              top: 40, left: preAuthMargin, right: preAuthMargin),
          callback: _startTimer),
    ];
  }

  @override
  void initState() {
    super.initState();

    bloc = BlocProvider.of<AuthenticateBloc>(context);

    _startTimer();
  }

  @override
  void dispose() {
    _timer.cancel();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthenticateBloc, AuthenticateState>(
        listener: (BuildContext context, AuthenticateState state) {
      if (state is VerifyError) _showAlert(state.message);
      if (state is Verified) widget.callback();
    }, builder: (BuildContext context, AuthenticateState state) {
      if (widget.needFullWidth) {
        return LoadingWrapper(
            isLoadingState: Verifying,
            state: state,
            child: ListView(children: _renderChildren()));
      }

      return Container(
          height: 3000,
          child: LoadingWrapper(
            state: state,
            isLoadingState: Verifying,
            child: Column(
                mainAxisSize: MainAxisSize.min, children: _renderChildren()),
          ));
    });
  }
}

class _VerificationCodeInput extends StatefulWidget {
  final ValueChanged<String> onCompleted;
  final TextInputType keyboardType;
  final int length;
  final double itemSize;
  final BoxDecoration itemDecoration;
  final TextStyle textStyle;
  final bool autofocus;

  _VerificationCodeInput(
      {Key key,
      this.onCompleted,
      this.keyboardType = TextInputType.number,
      this.length = 4,
      this.itemDecoration,
      this.itemSize = 50,
      this.textStyle = const TextStyle(fontSize: 25.0, color: Colors.black),
      this.autofocus = true})
      : assert(length > 0),
        assert(itemSize > 0),
        super(key: key);

  @override
  _VerificationCodeInputState createState() =>
      new _VerificationCodeInputState();
}

class _VerificationCodeInputState extends State<_VerificationCodeInput> {
  final List<FocusNode> _listFocusNode = <FocusNode>[];
  final List<TextEditingController> _listControllerText =
      <TextEditingController>[];
  List<String> _code = [];
  int _currentIdex = 0;

  @override
  void initState() {
    if (_listFocusNode.isEmpty) {
      for (var i = 0; i < widget.length; i++) {
        _listFocusNode.add(new FocusNode());
        _listControllerText.add(new TextEditingController());
        _code.add(' ');
      }
    }
    super.initState();
  }

  String _getInputVerify() {
    String verifycode = '';
    for (var i = 0; i < widget.length; i++) {
      for (var index = 0; index < _listControllerText[i].text.length; index++) {
        if (_listControllerText[i].text[index] != ' ') {
          verifycode += _listControllerText[i].text[index];
        }
      }
    }
    return verifycode;
  }

  Widget _buildInputItem(int index) {
    bool border = (widget.itemDecoration == null);
    return TextField(
      keyboardType: widget.keyboardType,
      maxLines: 1,
      maxLength: 2,
      focusNode: _listFocusNode[index],
      decoration: InputDecoration(
          border: (border ? null : InputBorder.none),
          enabled: _currentIdex == index,
          counterText: '',
          errorMaxLines: 1,
          fillColor: Colors.black),
      onChanged: (String value) {
        if (value.length > 1 && index < widget.length ||
            index == 0 && value.isNotEmpty) {
          if (index == widget.length - 1) {
            widget.onCompleted(_getInputVerify());
            return;
          }
          if (_listControllerText[index + 1].value.text.isEmpty) {
            _listControllerText[index + 1].value =
                new TextEditingValue(text: ' ');
          }
          if (value.length == 2) {
            if (value[0] != _code[index]) {
              _code[index] = value[0];
            } else if (value[1] != _code[index]) {
              _code[index] = value[1];
            }
            if (value[0] == ' ') {
              _code[index] = value[1];
            }
            _listControllerText[index].text = _code[index];
          }
          _next(index);

          return;
        }
        if (value.isEmpty && index >= 0) {
          if (_listControllerText[index - 1].value.text.isEmpty) {
            _listControllerText[index - 1].value =
                new TextEditingValue(text: ' ');
          }
          _prev(index);
        }
      },
      controller: _listControllerText[index],
      maxLengthEnforcement: MaxLengthEnforcement.enforced,
      autocorrect: false,
      textAlign: TextAlign.center,
      autofocus: widget.autofocus,
      style: widget.textStyle,
    );
  }

  void _next(int index) {
    if (index != widget.length) {
      setState(() {
        _currentIdex = index + 1;
      });
      SchedulerBinding.instance.addPostFrameCallback((_) {
        FocusScope.of(context).requestFocus(_listFocusNode[index + 1]);
      });
    }
  }

  void _prev(int index) {
    if (index > 0) {
      setState(() {
        if (_listControllerText[index].text.isEmpty) {
          _listControllerText[index - 1].text = ' ';
        }
        _currentIdex = index - 1;
      });
      SchedulerBinding.instance.addPostFrameCallback((_) {
        FocusScope.of(context).requestFocus(_listFocusNode[index - 1]);
      });
    }
  }

  List<Widget> _buildListWidget() {
    List<Widget> listWidget = [];
    for (int index = 0; index < widget.length; index++) {
      double left = (index == 0) ? 0.0 : (widget.itemSize / 10);
      listWidget.add(Container(
          height: widget.itemSize * 1.1,
          width: widget.itemSize,
          margin: EdgeInsets.only(left: left),
          decoration: widget.itemDecoration,
          child: _buildInputItem(index)));
    }
    return listWidget;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: _buildListWidget(),
    );
  }
}
