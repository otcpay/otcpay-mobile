import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:otcpay/generated/locale_keys.g.dart';

import 'package:otcpay/style/Color.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/util/StringUtil.dart';

import 'ListItem.dart';

class LoadingIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation<Color>(globalPrimaryColor),
    );
  }
}

class CurrencyIndicator extends StatelessWidget {
  final String currency;
  final String rate;
  final String percentage;
  final bool isUp;

  const CurrencyIndicator(
      {@required this.currency,
      @required this.rate,
      @required this.percentage,
      @required this.isUp});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 12.0),
          height: 20.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                padding: EdgeInsets.only(right: 4.0),
                child: Image.asset('icons/$currency.png'),
              ),
              BoldTitle(title: currency),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 8.0, bottom: 5.0),
          child: BoldTitle(title: rate, color: isUp ? upColor : downColor),
        ),
        Container(
          color: isUp ? upBackgroundColor : downBackgroundColor,
          padding: EdgeInsets.fromLTRB(5.0, 2.0, 5.0, 2.0),
          child: SubTitle(title: percentage, color: isUp ? upColor : downColor),
        )
      ],
    );
  }
}

class HighlightIndicator extends StatelessWidget {
  final String label;

  const HighlightIndicator({this.label});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: labelColor, borderRadius: BorderRadius.circular(8.0)),
      padding: EdgeInsets.fromLTRB(8.0, 2.0, 8.0, 2.0),
      child: SubTitle(title: label),
    );
  }
}

class StepIndicator extends StatelessWidget {
  final Widget icon;
  final String title;
  final bool isActive;
  final EdgeInsets padding;

  const StepIndicator(
      {@required this.icon,
      @required this.title,
      this.isActive = false,
      this.padding = const EdgeInsets.all(5.0)});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 60.0,
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                  child: Container(
                      color: isActive ? globalPrimaryColor : labelColor,
                      height: 1)),
              Container(
                decoration: BoxDecoration(
                    color: isActive ? globalPrimaryColor : labelColor,
                    borderRadius: BorderRadius.circular(20.0)),
                padding: padding,
                child: Center(
                  child: icon,
                ),
              ),
              Expanded(
                  child: Container(
                      color: isActive ? globalPrimaryColor : labelColor,
                      height: 1)),
            ],
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            child: Text(title,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: isActive ? globalPrimaryColor : labelColor,
                    fontSize: 10.0)),
          )
        ],
      ),
    );
  }
}

class ModeIndicator extends StatelessWidget {
  final String mode;
  final Color color;
  final double fontSize;
  final bool needMargin;
  final bool shrinkMargin;

  const ModeIndicator(
      {@required this.mode,
      this.color = globalPrimaryColor,
      this.fontSize = 16,
      this.needMargin = true,
      this.shrinkMargin = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
          left: shrinkMargin ? 10 : 20, top: needMargin ? 10 : 0),
      padding: EdgeInsets.only(left: 5, right: 5),
      decoration: BoxDecoration(
          border: Border.all(color: color),
          borderRadius: BorderRadius.circular(5)),
      child: BoldTitle(title: mode, fontSize: fontSize, color: color),
    );
  }
}

class BuySellIndicator extends StatelessWidget {
  final RATE_MODE mode;
  final double fontSize;

  const BuySellIndicator(
      {@required this.mode, this.fontSize = 16});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 20, top: 10, right: 10),
      padding: EdgeInsets.only(left: 5, right: 5),
      decoration: BoxDecoration(
          border:
              Border.all(color: mode == RATE_MODE.BUY ? upColor : downColor),
          borderRadius: BorderRadius.circular(5)),
      child: BoldTitle(
          title: getBuySellRemitButtonTitle(mode),
          fontSize: fontSize,
          color: mode == RATE_MODE.BUY ? upColor : downColor),
    );
  }
}

class SettlementIndicator extends StatelessWidget {
  final bool showLabel;
  final bool showAmountSaved;
  final bool showSettlementDate;
  final String totalAmount;
  final String title;
  final String amountSaved;

  const SettlementIndicator(
      {this.showLabel = true,
      @required this.totalAmount,
      @required this.title,
      this.showAmountSaved = true,
      this.amountSaved,
      this.showSettlementDate = true});

  List<Widget> _optionalElement() {
    List<Widget> widgets = [];

    if (showAmountSaved) {
      widgets.addAll([
        Container(
            margin: EdgeInsets.only(left: 5, right: 5, bottom: 5),
            child: Divider(color: dividerColor, thickness: 1)),
        Container(
          margin: EdgeInsets.only(left: 5, right: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              NormalTitle(title: tr(LocaleKeys.other_totalAmountSaved)),
              BoldTitle(title: amountSaved),
            ],
          ),
        )
      ]);
    }

    if (showSettlementDate) {
      widgets.add(Container(
        margin: EdgeInsets.only(left: 5, right: 5, top: 10),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          NormalTitle(title: tr(LocaleKeys.other_settlementDate)),
          BoldTitle(title: 'T+0', color: globalSecondaryColor)
        ]),
      ));
    }

    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(20),
      color: inputBoxBackgroundColor,
      child: Container(
        padding: EdgeInsets.all(10),
        height: showSettlementDate
            ? 192
            : showAmountSaved
                ? 162
                : 121,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              children: [
                Container(
                  height: 20,
                  padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                  margin: EdgeInsets.only(left: 5, bottom: 10),
                  decoration: BoxDecoration(
                    color: showLabel
                        ? globalSecondaryColor
                        : inputBoxBackgroundColor,
                    borderRadius: BorderRadius.circular(4),
                  ),
                  child: showLabel
                      ? BoldTitle(
                          title: 'OTCPAY',
                          color: globalBackgroundColor,
                          fontSize: 12)
                      : null,
                )
              ].where((element) => element != null).toList(),
            ),
            NormalTitle(title: title),
            Container(
              margin: EdgeInsets.only(top: 10, bottom: 15),
              child: BoldTitle(
                  title: totalAmount,
                  color: globalSecondaryColor,
                  fontSize: 22),
            ),
            ..._optionalElement()
          ],
        ),
      ),
    );
  }
}

class SeparateIndicator extends StatelessWidget {
  final String title;
  final bool extraMargin;

  const SeparateIndicator({@required this.title, this.extraMargin = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
          left: 25, right: 25, top: 5, bottom: extraMargin ? 10 : 0),
      height: 14,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
              child: Container(
                  margin: EdgeInsets.only(right: 5),
                  height: 1,
                  color: expireColor)),
          SubTitle(title: title, color: expireColor),
          Expanded(
              child: Container(
                  margin: EdgeInsets.only(left: 5),
                  height: 1,
                  color: expireColor)),
        ],
      ),
    );
  }
}

class ExpireIndicator extends StatelessWidget {
  final String title;

  const ExpireIndicator({@required this.title});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 35, top: 15, bottom: 15),
      color: expireColor,
      child: SubTitle(
          title: tr(LocaleKeys.orderPage_expirePrefix) +
              title +
              tr(LocaleKeys.orderPage_expirePostfix)),
    );
  }
}

class OptionalIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: expireColor,
      margin: EdgeInsets.only(left: 15),
      padding: EdgeInsets.only(left: 5, right: 5, top: 2, bottom: 2),
      child: NormalTitle(
        fontSize: 10,
        title: tr(LocaleKeys.common_optional),
      ),
    );
  }
}
