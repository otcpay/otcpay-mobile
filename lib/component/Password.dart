import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/auth/authenticate.dart';
import 'package:otcpay/bloc/proof/proof_bloc.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/style/Spec.dart';
import 'package:otcpay/util/ParamUtil.dart';

import 'Dropdown.dart';
import 'Input.dart';
import 'Title.dart';
import 'Wrapper.dart';

class ForgetPassword extends StatefulWidget {
  final void Function() callback;
  final Widget title;
  final bool showClose;
  final TextEditingController controller;
  final TextEditingController codeController;

  ForgetPassword(
      {@required this.callback,
      @required this.controller,
      @required this.codeController,
      @required this.title,
      this.showClose = true});

  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  final GlobalKey<ConfigurableExpansionTileState> dropDownKey = GlobalKey();
  final GlobalKey<ConfirmButtonState> key = GlobalKey();

  List<String> codes = ['852', '86', '1'];
  int code = 0;
  double height = 380;

  bool isPhone = true;

  void _updateButtonState() {
    if (key.currentState != null) {
      key.currentState.setState(() {});
    }
  }

  void _setCode(int key) {
    setState(() {
      code = key;
      widget.codeController.text = codes[code];
    });

    dropDownKey.currentState.handleTap();
  }

  void toggleMode() {
    setState(() {
      isPhone = !isPhone;
      if (!isPhone) height = 380;
    });
  }

  void _dismiss() {
    Navigator.of(context).pop();
  }

  void _onDropdownToggle(bool isOpen) {
    setState(() {
      height = 380;
      if (isOpen) height += codes.length * 28;
    });
  }

  bool _validateInput() {
    return widget.controller.text.length > 0;
  }

  List<Widget> _codeFilter() {
    return codes
        .asMap()
        .map((key, value) => MapEntry(
            key,
            GestureDetector(
                onTap: () => _setCode(key),
                child: Container(
                    color: code == key ? labelColor : inputBoxBackgroundColor,
                    height: 28,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[NormalTitle(title: '+' + value)],
                    )))))
        .values
        .toList();
  }

  @override
  void initState() {
    super.initState();

    widget.controller.addListener(_updateButtonState);
    widget.codeController.text = codes[code];

    AuthenticateState state = BlocProvider.of<AuthenticateBloc>(context).state;

    if (state is Authenticated) {
      String account = BlocProvider.of<ProofBloc>(context).repo.account;
      widget.controller.text = account;
      isPhone = isValidPhoneNum(account);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthenticateBloc, AuthenticateState>(
        builder: (BuildContext context, AuthenticateState state) {
      return Container(
        height: height,
        child: LoadingWrapper(
          state: state,
          isLoadingState: Validating,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              widget.showClose
                  ? Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          TextButton(
                              onPressed: _dismiss,
                              child: NormalTitle(
                                title: tr(LocaleKeys.common_close),
                                color: subTitleColor,
                              ))
                        ],
                      ),
                    )
                  : Container(),
              Container(
                  margin: EdgeInsets.only(
                      top: widget.showClose ? 0 : 30,
                      left: preAuthMargin,
                      right: preAuthMargin,
                      bottom: 20),
                  child: Row(
                    children: [widget.title],
                  )),
              Container(
                margin: EdgeInsets.only(
                    left: preAuthMargin, right: preAuthMargin, bottom: 10),
                child: SubTitle(
                    title:
                        tr(LocaleKeys.resetPasswordPage_resetPasswordSubTitle),
                    fontSize: 12.0,
                    color: subTitleColor),
              ),
              InputTitle(
                  title: isPhone
                      ? tr(LocaleKeys.common_mobile)
                      : tr(LocaleKeys.common_email)),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  isPhone
                      ? Container(
                          width: 90,
                          child: Card(
                            color: inputBoxBackgroundColor,
                            margin: EdgeInsets.only(left: preAuthMargin),
                            child: Container(
                              padding: EdgeInsets.only(top: 10, bottom: 10),
                              child: ConfigurableExpansionTile(
                                  onExpansionChanged: _onDropdownToggle,
                                  key: dropDownKey,
                                  animatedWidgetFollowingHeader: const Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.white,
                                  ),
                                  header: Flexible(
                                      child: NormalTitle(
                                          title: '+' + codes[code],
                                          color: Colors.white)),
                                  headerExpanded: NormalTitle(
                                      title: '+' + codes[code],
                                      color: Colors.white),
                                  headerBackgroundColorStart:
                                      Colors.transparent,
                                  expandedBackgroundColor: Colors.transparent,
                                  headerBackgroundColorEnd: Colors.transparent,
                                  children: _codeFilter()),
                            ),
                          ),
                        )
                      : Container(),
                  Expanded(
                      flex: 1,
                      child: InputBox(
                          controller: widget.controller,
                          hintText: isPhone
                              ? tr(LocaleKeys.common_mobileInputHint)
                              : tr(LocaleKeys.common_emailInputHint),
                          margin: EdgeInsets.only(
                              left: isPhone ? 9 : preAuthMargin,
                              right: preAuthMargin)))
                ],
              ),
              widget.showClose
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.only(right: preAuthMargin),
                          child: TextButton(
                              style: TextButton.styleFrom(
                                padding: EdgeInsets.zero,
                              ),
                              onPressed: toggleMode,
                              child: NormalTitle(
                                  title: isPhone
                                      ? tr(LocaleKeys.common_email)
                                      : tr(LocaleKeys.resetPasswordPage_phone),
                                  color: globalPrimaryColor)),
                        )
                      ],
                    )
                  : Container(),
              ConfirmButton(
                  key: key,
                  validator: _validateInput,
                  title: tr(LocaleKeys.common_sendVerification),
                  height: 50,
                  margin: EdgeInsets.only(
                      left: preAuthMargin, right: preAuthMargin, top: 30),
                  callback: widget.callback),
            ],
          ),
        ),
      );
    });
  }
}

class ResetPassword extends StatefulWidget {
  final String account;
  final Widget title;

  const ResetPassword({@required this.account, @required this.title});

  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  final GlobalKey<ConfirmButtonState> key = GlobalKey();
  final TextEditingController pwUniqueController = TextEditingController();
  final TextEditingController resetPwUniqueController = TextEditingController();
  final validCharacters = RegExp(r'^[a-zA-Z0-9_\-=@,\.;!\?]+$');

  AuthenticateBloc bloc;

  String _checkReason(String input) {
    if (input.length > 0 && input.length < 6)
      return tr(LocaleKeys.resetPasswordPage_tooShort);
    if (input.length > 16) return tr(LocaleKeys.resetPasswordPage_tooLong);
    if (input.length > 0 && !isAsciiChar(validCharacters, input))
      return tr(LocaleKeys.resetPasswordPage_invalidChar);

    return '';
  }

  bool _verifyPw() {
    return pwUniqueController.text.length > 5 &&
        pwUniqueController.text.length < 17 &&
        isAsciiChar(validCharacters, pwUniqueController.text);
  }

  bool _verifyConfirmPw() {
    return pwUniqueController.text == resetPwUniqueController.text;
  }

  bool _validateInput() {
    return pwUniqueController.text.length > 0 &&
        resetPwUniqueController.text.length > 0 &&
        _verifyConfirmPw();
  }

  void _dismiss() {
    Navigator.of(context).pop();
  }

  void _updateButtonState() {
    setState(() {});
  }

  void _resetPw() {
    bloc.add(
        ResetPw(username: widget.account, password: pwUniqueController.text));
  }

  @override
  void initState() {
    super.initState();

    pwUniqueController.addListener(_updateButtonState);
    resetPwUniqueController.addListener(_updateButtonState);

    bloc = BlocProvider.of<AuthenticateBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AuthenticateBloc, AuthenticateState>(
        builder: (BuildContext context, AuthenticateState state) {
      return Container(
        height: 440.0,
        child: LoadingWrapper(
          state: state,
          isLoadingState: Resetting,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                        onPressed: _dismiss,
                        child: NormalTitle(
                          title: tr(LocaleKeys.common_close),
                          color: subTitleColor,
                        ))
                  ],
                ),
              ),
              Container(
                  margin: EdgeInsets.only(
                      left: preAuthMargin, right: preAuthMargin, bottom: 20),
                  child: Row(
                    children: [widget.title],
                  )),
              Container(
                child: ResetPasswordContent(
                  buttonKey: key,
                  pwUniqueController: pwUniqueController,
                  resetPwUniqueController: resetPwUniqueController,
                  buttonTitle: tr(LocaleKeys.resetPasswordPage_resetPassword),
                  buttonCallback: _resetPw,
                  checkReason: _checkReason(pwUniqueController.text),
                  verifyPw: _verifyPw,
                  verifyConfirmPw: _verifyConfirmPw,
                  validatePassword: _validateInput,
                  policyMargin: EdgeInsets.only(
                      left: preAuthMargin, right: preAuthMargin, bottom: 10),
                ),
              ),
            ],
          ),
        ),
      );
    });
  }
}

class ResetPasswordContent extends StatefulWidget {
  final GlobalKey<ConfirmButtonState> buttonKey;
  final TextEditingController pwUniqueController;
  final TextEditingController resetPwUniqueController;
  final String checkReason;
  final String buttonTitle;
  final bool Function() verifyPw;
  final bool Function() verifyConfirmPw;
  final bool Function() validatePassword;
  final void Function() buttonCallback;
  final EdgeInsets policyMargin;

  const ResetPasswordContent(
      {@required this.buttonKey,
      @required this.pwUniqueController,
      @required this.resetPwUniqueController,
      @required this.buttonTitle,
      @required this.buttonCallback,
      @required this.checkReason,
      @required this.validatePassword,
      @required this.verifyConfirmPw,
      @required this.verifyPw,
      @required this.policyMargin});

  @override
  _ResetPasswordContentState createState() => _ResetPasswordContentState();
}

class _ResetPasswordContentState extends State<ResetPasswordContent> {
  bool showPw = true;
  bool showResetPw = true;

  void _toggleShowPw() {
    setState(() {
      showPw = !showPw;
    });
  }

  void _toggleShowResetPw() {
    setState(() {
      showResetPw = !showResetPw;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: widget.policyMargin,
          child: SubTitle(
              title: tr(LocaleKeys.resetPasswordPage_passwordPolicy),
              fontSize: 12.0,
              color: subTitleColor),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Expanded(
                child: Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(right: 10),
                  child: InputTitle(
                      title: tr(LocaleKeys.common_password), fontSize: 16.0),
                ),
                InkWell(
                    borderRadius: BorderRadius.circular(10),
                    onTap: _toggleShowPw,
                    child: Image.asset(
                      showPw ? 'icons/eye-slash.png' : 'icons/eye.png',
                      width: 20,
                      height: 20,
                    ))
              ],
            )),
            Padding(
              padding: EdgeInsets.only(right: preAuthMargin, bottom: 5),
              child: SubTitle(title: widget.checkReason, color: Colors.red),
            ),
          ],
        ),
        InputBox(
            margin: EdgeInsets.only(left: preAuthMargin, right: preAuthMargin),
            obscureText: showPw,
            enableSuggestions: false,
            autocorrect: false,
            controller: widget.pwUniqueController,
            callback: widget.verifyPw,
            hintText: tr(LocaleKeys.common_passwordInputHint)),
        Row(
          children: [
            Padding(
              padding: EdgeInsets.only(right: 10),
              child: InputTitle(
                  title: tr(LocaleKeys.resetPasswordPage_confirmPassword),
                  fontSize: 16.0),
            ),
            InkWell(
                borderRadius: BorderRadius.circular(10),
                onTap: _toggleShowResetPw,
                child: Image.asset(
                  showResetPw ? 'icons/eye-slash.png' : 'icons/eye.png',
                  width: 20,
                  height: 20,
                ))
          ],
        ),
        InputBox(
            margin: EdgeInsets.only(
                left: preAuthMargin, right: preAuthMargin, bottom: 30),
            obscureText: showResetPw,
            enableSuggestions: false,
            autocorrect: false,
            controller: widget.resetPwUniqueController,
            callback: widget.verifyConfirmPw,
            hintText: tr(LocaleKeys.resetPasswordPage_passwordAgainHint)),
        ConfirmButton(
            key: widget.buttonKey,
            validator: widget.validatePassword,
            title: widget.buttonTitle,
            height: 50,
            margin: EdgeInsets.only(left: preAuthMargin, right: preAuthMargin),
            callback: widget.buttonCallback),
      ],
    );
  }
}
