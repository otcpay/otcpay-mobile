import 'package:flutter/material.dart';

class NormalTitle extends StatelessWidget {
  final String title;
  final double fontSize;
  final Color color;

  const NormalTitle(
      {@required this.title, this.color = Colors.white, this.fontSize = 14.0});

  @override
  Widget build(BuildContext context) {
    return Text(title, style: TextStyle(color: color, fontSize: fontSize));
  }
}

class BoldTitle extends StatelessWidget {
  final String title;
  final double fontSize;
  final Color color;

  const BoldTitle(
      {@required this.title, this.color = Colors.white, this.fontSize = 14.0});

  @override
  Widget build(BuildContext context) {
    return Text(title,
        style: TextStyle(
            color: color, fontSize: fontSize, fontWeight: FontWeight.bold));
  }
}

class SubTitle extends StatelessWidget {
  final String title;
  final double fontSize;
  final Color color;
  final TextAlign align;

  const SubTitle(
      {@required this.title,
      this.color = Colors.white,
      this.fontSize = 10.0,
      this.align = TextAlign.start});

  @override
  Widget build(BuildContext context) {
    return Text(title,
        style: TextStyle(color: color, fontSize: fontSize), textAlign: align);
  }
}
