import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/auth/authenticate.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/screen/LoginPage.dart';
import 'package:otcpay/screen/NotificationPage.dart';
import 'package:otcpay/style/Color.dart';

class NotificationButton extends StatelessWidget {
  final bool inverseColor;

  const NotificationButton({this.inverseColor = false});

  bool _hasUnreadNotification(BuildContext context) {
    return BlocProvider.of<GeneralBloc>(context)
            .notifications
            .where((n) => !n.read)
            .length >
        0;
  }

  void _navigateToNotification(BuildContext context) {
    AuthenticateState state = BlocProvider.of<AuthenticateBloc>(context).state;

    if (state is Authenticated) {
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (_) => NotificationPage()));
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (_) => LoginPage(isFirstPage: true)),
          (route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GeneralBloc, GeneralState>(
        builder: (BuildContext context, GeneralState state) {
      return Stack(children: <Widget>[
        IconButton(
            icon: Icon(Icons.notifications_none_outlined, size: 30.0),
            color: inverseColor ? Colors.black : globalPrimaryColor,
            onPressed: () => _navigateToNotification(context)),
        Positioned(
          // draw a red marble
          top: 10.0,
          right: 12.0,
          child: Icon(Icons.brightness_1,
              size: 10.0,
              color: _hasUnreadNotification(context)
                  ? Colors.red
                  : Colors.transparent),
        )
      ]);
    });
  }
}
