import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/api/service.dart';
import 'package:otcpay/api/user.dart';
import 'package:otcpay/bloc/auth/authenticate.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/bloc/proof/proof.dart';
import 'package:otcpay/component/Input.dart';
import 'package:otcpay/component/Password.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/component/VerificationCode.dart';
import 'package:otcpay/component/Wrapper.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/style/Spec.dart';
import 'package:otcpay/util/ParamUtil.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  final bool isFirstPage;
  final bool fromLogoutPage;

  const LoginPage({this.isFirstPage = false, this.fromLogoutPage = false});

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<ConfirmButtonState> key = GlobalKey();

  final TextEditingController codeController = TextEditingController();
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController forgetAccountController = TextEditingController();

  int step = 0;

  bool dirty = false;
  bool checkedToC = true;
  bool isOnBoardFirstPage = false;

  AuthenticateBloc bloc;

  void _clickSignUp() {
    Navigator.pushNamed(context, '/signup');
  }

  void _gotToTerms() {
    Navigator.pushNamed(context, '/terms');
  }

  void _clickLogin() {
    FocusScope.of(context).unfocus();

    bloc.add(Authenticate(usernameController.text, passwordController.text));
  }

  void _validate() {
    if (bloc.state is NotAuthenticated) {
      bloc.add(Validate());
    }
  }

  void _authenticated() {
    BlocProvider.of<ProofBloc>(context).add(GetProfile(bloc.repo.userId));
    BlocProvider.of<GeneralBloc>(context)
        .add(FetchNotification(bloc.repo.userId));

    final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
    _firebaseMessaging.getToken().then((token) {
      updateToken(bloc.repo.userId, token);
    });

    _next();
  }

  void _next() {
    Navigator.pushNamedAndRemoveUntil(context, '/home', (route) => false);
  }

  void _previous() {
    try {
      // clear snackbar in best effort
      ScaffoldMessenger.of(context).clearSnackBars();
    } catch (e) {
      // do nothing
    }

    if (widget.isFirstPage || isOnBoardFirstPage) {
      _next();
    } else {
      Navigator.pop(context);
    }
  }

  void _dismissAlert() {
    Navigator.pop(context);
  }

  void _dismissAndNavigate() {
    _dismissAlert();

    Navigator.pushNamed(context, '/signup', arguments: usernameController.text);
  }

  void _showSnackbar(String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void _showSignUpAlert() {
    showDialog(
      context: context,
      builder: (BuildContext context) => Theme(
        data: ThemeData.dark(),
        child: CupertinoAlertDialog(
          title: NormalTitle(title: tr(LocaleKeys.loginPage_notRegister)),
          actions: [
            CupertinoDialogAction(
                isDefaultAction: false,
                child: NormalTitle(title: tr(LocaleKeys.common_cancel)),
                onPressed: _dismissAlert),
            CupertinoDialogAction(
                isDefaultAction: false,
                child: NormalTitle(
                    title: tr(LocaleKeys.loginPage_signup),
                    color: globalPrimaryColor),
                onPressed: _dismissAndNavigate)
          ],
        ),
      ),
    );
  }

  void _showAlert(String message) {
    if (message == 'User not found') {
      _showSignUpAlert();
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) => Theme(
          data: ThemeData.dark(),
          child: CupertinoAlertDialog(
            title: NormalTitle(title: message),
            actions: [
              CupertinoDialogAction(
                  isDefaultAction: true,
                  child: NormalTitle(title: tr(LocaleKeys.common_ok)),
                  onPressed: _dismissAlert)
            ],
          ),
        ),
      );
    }
  }

  void toggleToC() {
    setState(() {
      checkedToC = !checkedToC;
    });
  }

  void _showResetSuccess() {
    _dismissAlert();

    Future.delayed(const Duration(milliseconds: 1000), () {
      showDialog(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) => AlertDialog(
          contentPadding: EdgeInsets.only(left: 40, right: 40, bottom: 30),
          titlePadding: EdgeInsets.only(top: 40, bottom: 10),
          backgroundColor: inputBoxBackgroundColor,
          title: Text(tr(LocaleKeys.loginPage_passwordChangedTitle),
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white, fontSize: 22)),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                SubTitle(
                  align: TextAlign.center,
                  color: subTitleColor,
                  fontSize: 12,
                  title: tr(LocaleKeys.loginPage_passwordChangedSubTitle),
                ),
                Image.asset('icons/Check.png'),
                ConfirmButton(
                    callback: _dismissAlert,
                    title: tr(LocaleKeys.loginPage_loginCap),
                    height: 50),
              ],
            ),
          ),
        ),
      );
    });
  }

  void _showForgetPassword() {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        backgroundColor: globalBackgroundColor,
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Container(
            padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).viewInsets.bottom,
            ),
            child: StatefulBuilder(builder: (innerContext, setModalState) {
              void callback() {
                if (step == 0) {
                  if (isValidAccount(forgetAccountController.text)) {
                    bloc.add(ResetCheckAccount(forgetAccountController.text));
                  }
                } else {
                  setModalState(() => ++step);
                }
              }

              return BlocConsumer<AuthenticateBloc, AuthenticateState>(listener:
                  (BuildContext blocContext, AuthenticateState state) {
                if (state is Validated) {
                  setModalState(() => ++step);
                }
              }, builder: (BuildContext blocContext, AuthenticateState state) {
                return _renderSteps(callback);
              });
            }),
          );
        }).whenComplete(() => setState(() {
          step = 0;
        }));
  }

  void _updateButtonState() {
    if (key.currentState != null) {
      key.currentState.setState(() {});
    }
  }

  void _getArgument() {
    if (!dirty) {
      String message = ModalRoute.of(context).settings.arguments;
      isOnBoardFirstPage = message == 'onFirstPage';
      dirty = true;
    }
  }

  void _setControllerText(TextEditingController controller, String value) {
    controller.value = TextEditingValue(
      text: value,
      selection: TextSelection.fromPosition(
        TextPosition(offset: value.length),
      ),
    );
  }

  void _getPersistedCredential() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String username = prefs.getString('username') ?? '';
    String password = prefs.getString('password') ?? '';
    _setControllerText(usernameController, username);
    _setControllerText(passwordController, password);
  }

  bool _validState(AuthenticateState state) {
    return !(state is Authenticating || state is Validating);
  }

  bool _validateInput() {
    return usernameController.text.length > 0 &&
        passwordController.text.length > 0 &&
        checkedToC;
  }

  Widget _renderSteps(callback) {
    if (step == 0) {
      return ForgetPassword(
          title: BoldTitle(
              title: tr(LocaleKeys.loginPage_forgotPassword), fontSize: 16.0),
          codeController: codeController,
          callback: callback,
          controller: forgetAccountController);
    }

    if (step == 1) {
      String username = forgetAccountController.text;
      if (codeController.text != null &&
          codeController.text.isNotEmpty &&
          isValidPhoneNum(username)) {
        username = codeController.text + username;
      }
      return VerificationCode(
          callback: callback, account: username, type: EmailType.RESET_PW);
    }

    return ResetPassword(
        title: BoldTitle(
            title: tr(LocaleKeys.resetPasswordPage_resetPasswordTitle),
            fontSize: 16.0),
        account: forgetAccountController.text);
  }

  @override
  void initState() {
    super.initState();

    bloc = BlocProvider.of<AuthenticateBloc>(context);

    if (!widget.fromLogoutPage) _validate();

    usernameController.addListener(_updateButtonState);
    passwordController.addListener(_updateButtonState);

    WidgetsBinding.instance
        .addPostFrameCallback((_) => _getPersistedCredential());
  }

  @override
  void dispose() {
    usernameController.dispose();
    passwordController.dispose();
    forgetAccountController.dispose();
    codeController.dispose();

    try {
      // clear snackbar in best effort
      ScaffoldMessenger.of(context).clearSnackBars();
    } catch (e) {
      // do nothing
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _getArgument();
    return BlocConsumer<AuthenticateBloc, AuthenticateState>(
        listener: (BuildContext context, AuthenticateState state) {
      if (state is ValidateError) _showSnackbar(state.message);
      if (state is AuthenticateError) _showAlert(state.message);
      if (state is Authenticated) _authenticated();
      if (state is Reset) _showResetSuccess();
    }, builder: (BuildContext context, AuthenticateState state) {
      return LoadingWrapper(
        needBackground: true,
        state: state,
        child: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                onPressed: _validState(state) ? _previous : null,
                icon: Icon(Icons.arrow_back, color: Colors.white),
              ),
              title: Text(tr(LocaleKeys.loginPage_loginTitle)),
              centerTitle: true,
              elevation: 1.0,
              actions: [
                TextButton(
                    onPressed: _clickSignUp,
                    child: Text(
                      tr(LocaleKeys.loginPage_signup),
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12.0,
                      ),
                    )),
              ],
            ),
            body: ListView(
              children: [
                Container(
                  height: 140,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        'icons/launch_icon.png',
                        width: 37.0,
                        height: 37.0,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 8.0),
                        child: Text(
                          'OTCPAY',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontFamily: 'Krungthep'),
                        ),
                      )
                    ],
                  ),
                ),
                InputTitle(title: tr(LocaleKeys.loginPage_accountInputTitle)),
                InputBox(
                    height: 50.0,
                    controller: usernameController,
                    hintText: tr(LocaleKeys.loginPage_accountInputHint),
                    showSuffixIcon: true,
                    margin: EdgeInsets.only(
                        left: preAuthMargin, right: preAuthMargin, bottom: 5)),
                InputTitle(title: tr(LocaleKeys.common_password)),
                InputBox(
                    obscureText: true,
                    enableSuggestions: false,
                    autocorrect: false,
                    controller: passwordController,
                    hintText: tr(LocaleKeys.common_passwordInputHint),
                    margin: EdgeInsets.only(
                        left: preAuthMargin, right: preAuthMargin, bottom: 5)),
                Container(
                  height: 30.0,
                  margin: EdgeInsets.only(
                      left: 10, right: preAuthMargin, bottom: 30),
                  child: Row(
                    children: [
                      IconButton(
                          padding: EdgeInsets.zero,
                          iconSize: 25.0,
                          splashRadius: 15.0,
                          icon: Icon(Icons.check_circle_outline_outlined,
                              color: checkedToC ? Colors.green : Colors.white),
                          onPressed: toggleToC),
                      Container(
                        child: RichText(
                          text: TextSpan(children: [
                            TextSpan(
                              text: tr(LocaleKeys.loginPage_agree),
                              style: TextStyle(
                                  color: Colors.white, fontSize: 12.0),
                            ),
                            TextSpan(
                              text: tr(LocaleKeys.loginPage_toc),
                              recognizer: TapGestureRecognizer()
                                ..onTap = _gotToTerms,
                              style: TextStyle(
                                  color: Colors.blueAccent,
                                  fontSize: 12.0,
                                  decoration: TextDecoration.underline),
                            )
                          ]),
                        ),
                      )
                    ],
                  ),
                ),
                ConfirmButton(
                    key: key,
                    validator: _validState(state) ? _validateInput : null,
                    title: tr(LocaleKeys.loginPage_loginCap),
                    height: 50,
                    margin: EdgeInsets.only(
                        left: preAuthMargin, right: preAuthMargin),
                    callback: _clickLogin),
                Container(
                    margin: EdgeInsets.only(
                        left: preAuthMargin, right: preAuthMargin),
                    height: 50,
                    child: TextButton(
                        onPressed: _showForgetPassword,
                        child: Text(
                          tr(LocaleKeys.loginPage_forgotPassword) + '?',
                          style: TextStyle(
                            color: subTitleColor,
                            fontSize: 14.0,
                          ),
                        ))),
              ],
            )),
      );
    });
  }
}
