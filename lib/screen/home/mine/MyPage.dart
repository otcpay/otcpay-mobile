import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/auth/authenticate.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/bloc/proof/proof.dart';
import 'package:otcpay/component/ListItem.dart';
import 'package:otcpay/component/Notification.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/screen/home/mine/HelpPage.dart';
import 'package:otcpay/screen/home/mine/about/AboutPage.dart';
import 'package:otcpay/screen/home/mine/profile/ViewProfilePage.dart';
import 'package:otcpay/screen/home/mine/setting/SettingPage.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/util/StringUtil.dart';
import 'package:package_info_plus/package_info_plus.dart';

import '../../../generated/locale_keys.g.dart';

import '../../LoginPage.dart';

class CurvedClipper extends CustomClipper<Path> {
  final bool needExtraHeight;

  const CurvedClipper({this.needExtraHeight});

  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, needExtraHeight ? size.height - 78 : size.height - 120);
    path.quadraticBezierTo(size.width / 2, size.height, size.width,
        needExtraHeight ? size.height - 78 : size.height - 120);
    path.lineTo(size.width, 0);

    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

class MyPage extends StatefulWidget {
  @override
  _MyPageState createState() => _MyPageState();
}

class _MyPageState extends State<MyPage> {
  AuthenticateBloc authBloc; // ignore: close_sinks
  GeneralBloc generalBloc; // ignore: close_sinks
  ProofBloc proofBloc; // ignore: close_sinks

  PackageInfo info;

  String userId;

  bool _isAuthenticated() {
    AuthenticateState state = BlocProvider.of<AuthenticateBloc>(context).state;
    return (state is Authenticated);
  }

  void _goToLogin() {
    generalBloc.add(NavigateTab(index: 0));
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (_) => LoginPage(isFirstPage: true)),
        (route) => false);
  }

  void navigateToViewProfile() {
    if (!_isAuthenticated()) {
      _goToLogin();
    } else {
      Navigator.of(context).push(
          MaterialPageRoute(builder: (_) => ViewProfilePage(proofBloc.repo)));
    }
  }

  void navigateToPaymentOrder() {
    if (!_isAuthenticated()) {
      _goToLogin();
    } else {
      Navigator.of(context).pushNamed('/payment-order');
    }
  }

  void navigateToOTCOrder() {
    if (!_isAuthenticated()) {
      _goToLogin();
    } else {
      Navigator.of(context).pushNamed('/otc-order');
    }
  }

  void navigateToWallet() {
    if (!_isAuthenticated()) {
      _goToLogin();
    } else {
      Navigator.of(context).pushNamed('/wallet');
    }
  }

  void navigateToBank() {
    if (!_isAuthenticated()) {
      _goToLogin();
    } else {
      Navigator.of(context).pushNamed('/bank');
    }
  }

  void navigateToSetting() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => SettingPage()));
  }

  void navigateToHelpCenter() {
    Navigator.of(context).push(MaterialPageRoute(builder: (_) => HelpPage()));
  }

  void navigateToAbout() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => AboutPage(info)));
  }

  void navigateToVerify() {
    if (!_isAuthenticated()) {
      _goToLogin();
    } else {
      Navigator.of(context).pushNamed('/verify');
    }
  }

  bool showVerified() {
    if (authBloc.state is Authenticated) {
      return proofBloc.repo.verified != null && proofBloc.repo.verified;
    }

    return false;
  }

  bool notifyVerify() {
    if (authBloc.state is Authenticated) {
      return proofBloc.repo.verified != null && proofBloc.repo.verified;
    }

    return false;
  }

  Widget _profileIcon() {
    return authBloc.state is Authenticated &&
            proofBloc.repo.profilePic != null &&
            proofBloc.repo.profilePic != ''
        ? ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: Image.network(
              proofBloc.repo.profilePic + '?alt=media',
              fit: BoxFit.fill,
            ))
        : Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(8.0)));
  }

  @override
  void initState() {
    super.initState();

    authBloc = BlocProvider.of<AuthenticateBloc>(context);
    proofBloc = BlocProvider.of<ProofBloc>(context);
    generalBloc = BlocProvider.of<GeneralBloc>(context);

    userId = authBloc.repo.userId ?? '';

    PackageInfo.fromPlatform()
        .then((PackageInfo packageInfo) => info = packageInfo);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: PreferredSize(
              preferredSize: Size.fromHeight(notifyVerify() ? 180 : 220),
              child: Stack(
                clipBehavior: Clip.none,
                fit: StackFit.expand,
                children: [
                  ClipPath(
                      clipper: CurvedClipper(needExtraHeight: notifyVerify()),
                      child: Container(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                          colors: [
                            globalSecondaryColor,
                            globalSecondaryGradientColor
                          ],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                        )),
                      )),
                  Positioned(
                      right: 1.0,
                      child: NotificationButton(inverseColor: true)),
                  Positioned(
                      bottom: notifyVerify() ? 15 : 50,
                      left: 15.0,
                      right: 15.0,
                      child: BlocBuilder<ProofBloc, ProofState>(
                          builder: (context, state) {
                        String region = proofBloc.repo.region ?? 'Hong Kong';
                        String username = proofBloc.repo.username ?? 'Guest';
                        return GestureDetector(
                          onTap: navigateToViewProfile,
                          child: Container(
                            height: 125,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                  image: AssetImage("icons/cardbg.png"),
                                  fit: BoxFit.cover,
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: inputBoxBackgroundColor
                                        .withOpacity(0.5),
                                    spreadRadius: 0.7,
                                    blurRadius: 2,
                                    offset: Offset(
                                        -1, 2), // changes position of shadow
                                  )
                                ],
                                borderRadius: BorderRadius.circular(8.0)),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(left: 10.0),
                                  height: 47.0,
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        margin: EdgeInsets.only(right: 8),
                                        width: 47.0,
                                        child: Stack(
                                          clipBehavior: Clip.none,
                                          fit: StackFit.expand,
                                          children: [
                                            _profileIcon(),
                                            Positioned(
                                                bottom: -5.0,
                                                right: -5.0,
                                                child: Image.asset(
                                                    'icons/edit.png',
                                                    width: 12,
                                                    height: 12))
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            BoldTitle(
                                                title: username, fontSize: 16),
                                            Container(
                                              margin: EdgeInsets.only(top: 2),
                                              child: SubTitle(
                                                  title: 'UID: $userId',
                                                  color: subTitleColor),
                                            ),
                                            Row(
                                              children: [
                                                Image.asset(
                                                    'icons/' +
                                                        getIconName(region) +
                                                        '.png',
                                                    width: 14,
                                                    height: 14),
                                                Container(
                                                  margin:
                                                      EdgeInsets.only(left: 5),
                                                  child: SubTitle(
                                                      title: region,
                                                      color: subTitleColor),
                                                )
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                      showVerified()
                                          ? Container(
                                              width: 100.0,
                                              height: 50.0,
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.only(
                                                          topLeft:
                                                              Radius.circular(
                                                                  25.0),
                                                          bottomLeft:
                                                              Radius.circular(
                                                                  25.0)),
                                                  gradient: LinearGradient(
                                                      begin:
                                                          Alignment.centerLeft,
                                                      end: Alignment(2.5, 0),
                                                      colors: [
                                                        globalSecondaryColor,
                                                        globalSecondaryGradientColor,
                                                      ])),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                children: [
                                                  Image.asset(
                                                      'icons/verified_black.png',
                                                      width: 20,
                                                      height: 20),
                                                  Padding(
                                                      padding: EdgeInsets.only(
                                                          left: 5.0,
                                                          right: 5.0),
                                                      child: Text(
                                                          tr(LocaleKeys
                                                              .myPage_verified),
                                                          style: TextStyle(
                                                              color:
                                                                  inputBoxBackgroundColor,
                                                              fontSize: 16.0,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold)))
                                                ],
                                              ),
                                            )
                                          : Container(
                                              width: 100.0, height: 50.0)
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        );
                      })),
                  notifyVerify()
                      ? Container()
                      : Positioned(
                          left: 20,
                          right: 20,
                          bottom: -10,
                          child: Container(
                            height: 55,
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              color: globalDisabledPrimaryColor,
                              borderRadius: BorderRadius.circular(25),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                    child: Row(
                                  children: [
                                    Container(
                                        margin: EdgeInsets.only(
                                            left: 10, right: 20),
                                        child: Image.asset(
                                            'icons/verified_grey.png')),
                                    BoldTitle(
                                        title:
                                            tr(LocaleKeys.myPage_verifyTitle),
                                        color: globalBackgroundColor)
                                  ],
                                )),
                                TextButton(
                                    onPressed: navigateToVerify,
                                    style: ButtonStyle(
                                      shape: MaterialStateProperty.all(
                                          RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20))),
                                      backgroundColor:
                                          MaterialStateProperty.all(
                                              globalSecondaryColor),
                                    ),
                                    child: BoldTitle(
                                        title:
                                            tr(LocaleKeys.myPage_verifyButton),
                                        color: globalBackgroundColor))
                              ],
                            ),
                          ))
                ],
              )),
          body: ListView(
              padding: EdgeInsets.only(top: notifyVerify() ? 0 : 20),
              children: [
                Container(
                  height: 85,
                  child: Row(
                    children: [
                      Expanded(
                        child: SettingListItem(
                            needExtraHeight: true,
                            item: tr(LocaleKeys.myPage_paymentOrders),
                            icon: 'Payment Orders',
                            showTrailing: false,
                            callback: navigateToPaymentOrder,
                            overrideMargin: EdgeInsets.only(
                                top: 2.0, left: 20, right: 5.0)),
                      ),
                      Expanded(
                        child: SettingListItem(
                            needExtraHeight: true,
                            item: tr(LocaleKeys.myPage_otcOrders),
                            icon: 'OTC Orders',
                            showTrailing: false,
                            callback: navigateToOTCOrder,
                            overrideMargin: EdgeInsets.only(
                                top: 2.0, left: 5.0, right: 20.0)),
                      ),
                    ],
                  ),
                ),
                SettingListItem(
                  item: tr(LocaleKeys.myPage_manageWallet),
                  icon: 'Manage Wallet Address',
                  needExtraMargin: true,
                  callback: navigateToWallet,
                ),
                SettingListItem(
                  item: tr(LocaleKeys.myPage_manageRecipient),
                  icon: 'Manage Recipient Banks',
                  callback: navigateToBank,
                ),
                SettingListItem(
                  item: tr(LocaleKeys.myPage_setting),
                  icon: 'Setting',
                  needExtraMargin: true,
                  callback: navigateToSetting,
                ),
                SettingListItem(
                  item: tr(LocaleKeys.myPage_help),
                  icon: 'Help Center',
                  callback: navigateToHelpCenter,
                ),
                SettingListItem(
                  item: tr(LocaleKeys.myPage_about),
                  icon: 'About OTCPAY',
                  callback: navigateToAbout,
                )
              ])),
    );
  }
}
