import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:otcpay/bloc/auth/authenticate.dart';
import 'package:otcpay/bloc/proof/proof.dart';
import 'package:otcpay/component/Indicator.dart';
import 'package:otcpay/component/Input.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/component/Wrapper.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/model/Profile.dart';
import 'package:otcpay/style/Color.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:qr_code_tools/qr_code_tools.dart';
import 'package:qr_flutter/qr_flutter.dart';

class EditWalletPage extends StatefulWidget {
  final Wallet data;
  final int index;

  const EditWalletPage({this.data, this.index});

  @override
  _EditWalletPageState createState() => _EditWalletPageState();
}

class _EditWalletPageState extends State<EditWalletPage> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  final GlobalKey<ConfirmButtonState> key = GlobalKey();
  final TextEditingController walletController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final picker = ImagePicker();

  bool scanning = false;

  List<String> currencies = ['USDT', 'USDC', 'BUSD', 'BTC', 'ETH'];
  List<String> subTypes = ['ERC20', 'TRC20', 'Omni'];

  String crypto = '';
  String subType = 'NONE';

  QRViewController controller;

  void _onQRViewCreated(QRViewController controller) {
    setState(() {
      this.controller = controller;
    });
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        walletController.text = scanData.code;
        scanning = false;
      });
    });
  }

  void _toggleView() {
    _dismiss();

    setState(() {
      scanning = true;
    });
  }

  void _update() {
    setState(() {});
  }

  void _toggleCurrency(String c) {
    setState(() {
      crypto = c;
      if (crypto != 'USDT') {
        subType = 'NONE';
      } else {
        subType = 'ERC20';
      }
    });
  }

  void _toggleSubType(String s) {
    setState(() {
      subType = s;
    });
  }

  void _save() {
    if (widget.data == null && widget.index == null) {
      BlocProvider.of<ProofBloc>(context).add(AddWallet(
          userId: BlocProvider.of<AuthenticateBloc>(context).repo.userId,
          callback: _uploadCallback,
          data: Wallet(
              type: crypto,
              subType: subType,
              name: nameController.text,
              address: walletController.text)));
    } else {
      BlocProvider.of<ProofBloc>(context).add(UpdateWallets(
          callback: _uploadCallback,
          userId: BlocProvider.of<AuthenticateBloc>(context).repo.userId,
          index: widget.index,
          data: Wallet(
              type: crypto,
              subType: subType,
              name: nameController.text,
              address: walletController.text)));
    }
  }

  void _showSnackbar(String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void _uploadCallback() {
    _showSnackbar(tr(LocaleKeys.common_updateSuccess));
  }

  void _dismiss() {
    Navigator.of(context).pop();
  }

  void _action() {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        backgroundColor: inputBoxBackgroundColor,
        context: context,
        builder: (context) {
          return Container(
            height: 175,
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: labelColor,
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(8),
                          topLeft: Radius.circular(8))),
                  child: ListTile(
                    onTap: _toggleView,
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        BoldTitle(
                            title: tr(LocaleKeys.common_camera), fontSize: 16),
                      ],
                    ),
                  ),
                ),
                Container(
                  color: labelColor,
                  margin: EdgeInsets.only(bottom: 5),
                  child: ListTile(
                    onTap: _getImage,
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        BoldTitle(
                            title: tr(LocaleKeys.common_gallery), fontSize: 16),
                      ],
                    ),
                  ),
                ),
                Container(
                  color: labelColor,
                  child: ListTile(
                    onTap: _dismiss,
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        BoldTitle(
                            title: tr(LocaleKeys.common_cancel),
                            fontSize: 16,
                            color: cancelColor),
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  bool _codeValid() {
    return walletController.text.length > 0;
  }

  bool _validateInput() {
    return _codeValid() && nameController.text.length > 0;
  }

  List<Widget> _currenciesIndicator() {
    return currencies
        .map((e) => GestureDetector(
              onTap: () => _toggleCurrency(e),
              child: ModeIndicator(
                  mode: e,
                  fontSize: 12,
                  needMargin: false,
                  color: crypto == e ? globalPrimaryColor : labelColor),
            ))
        .toList();
  }

  List<Widget> _subTypesIndicator() {
    return subTypes
        .map((e) => GestureDetector(
              onTap: () => _toggleSubType(e),
              child: ModeIndicator(
                  mode: e,
                  fontSize: 12,
                  needMargin: false,
                  color: subType == e ? globalPrimaryColor : labelColor),
            ))
        .toList();
  }

  Future _getImage() async {
    _dismiss();

    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      String data = await QrCodeToolsPlugin.decodeFrom(pickedFile.path);
      setState(() {
        walletController.text = data;
      });
    } else {
      print('No image selected.');
    }
  }

  @override
  void initState() {
    super.initState();

    if (widget.data != null) {
      walletController.text = widget.data.address;
      nameController.text = widget.data.name;
      crypto = widget.data.type;
    }

    walletController.addListener(_update);
    nameController.addListener(_update);
  }

  @override
  void dispose() {
    controller?.dispose();
    walletController.dispose();
    nameController.dispose();

    super.dispose();
  }

  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller?.pauseCamera();
    }
    controller?.resumeCamera();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ProofBloc, ProofState>(
        listener: (BuildContext context, ProofState state) {
      if (state is UpdateError) _showSnackbar(state.message);
    }, builder: (BuildContext context, ProofState state) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Edit Wallet Address'),
          centerTitle: true,
          elevation: 1.0,
        ),
        body: LoadingWrapper(
          proofState: state,
          isLoadingState: Updating,
          child: ListView(
            children: [
              InputTitle(title: tr(LocaleKeys.walletPage_walletNameInputTitle)),
              InputBox(
                  margin: EdgeInsets.only(left: 15, right: 15),
                  controller: nameController,
                  hintText: tr(LocaleKeys.walletPage_walletNameInputTitle)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InputTitle(title: tr(LocaleKeys.walletPage_cryptoInputTitle)),
                  Container(
                    margin: EdgeInsets.only(right: 20),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: _currenciesIndicator(),
                    ),
                  )
                ],
              ),
              subType == 'NONE'
                  ? Container()
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Material(
                          elevation: 2,
                          child: Container(
                            color: inputBoxBackgroundColor,
                            padding:
                                EdgeInsets.only(right: 20, top: 10, bottom: 10),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: _subTypesIndicator(),
                            ),
                          ),
                        )
                      ],
                    ),
              InputTitle(
                  title: tr(LocaleKeys.walletPage_walletAddressInputTitle)),
              InputBox(
                  margin: EdgeInsets.only(left: 15, right: 15),
                  controller: walletController,
                  hintText: tr(LocaleKeys.walletPage_walletAddressInputHint)),
              Container(
                height: MediaQuery.of(context).size.width - 80,
                color: Colors.white,
                margin:
                    EdgeInsets.only(left: 40, right: 40, top: 40, bottom: 5),
                child: scanning
                    ? QRView(key: qrKey, onQRViewCreated: _onQRViewCreated)
                    : Stack(
                        fit: StackFit.expand,
                        children: [
                          QrImage(
                            data: walletController.text,
                            version: QrVersions.auto,
                          ),
                          !_codeValid()
                              ? Positioned.fill(
                                  child: Container(
                                  color: Color(0x8871727A),
                                  child: IconButton(
                                      iconSize: 79,
                                      color: Colors.white,
                                      icon: Icon(Icons.upload_rounded),
                                      onPressed: _action),
                                ))
                              : null
                        ].where((element) => element != null).toList(),
                      ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextButton(
                      child: Text(tr(LocaleKeys.walletPage_replaceQrCode),
                          style: TextStyle(
                            color: subTitleColor,
                            fontSize: 12,
                            decoration: TextDecoration.underline,
                          ),
                          textAlign: TextAlign.start),
                      onPressed: _action)
                ],
              ),
              ConfirmButton(
                  key: key,
                  validator: _validateInput,
                  title: tr(LocaleKeys.common_save),
                  height: 50,
                  margin:
                      EdgeInsets.only(left: 20, right: 20, top: 30, bottom: 20),
                  callback: _save),
            ],
          ),
        ),
      );
    });
  }
}
