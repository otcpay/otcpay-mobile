import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/auth/authenticate.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/component/Indicator.dart';
import 'package:otcpay/component/ListItem.dart';
import 'package:otcpay/component/Wrapper.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/util/TimeUtil.dart';

class PaymentOrderPage extends StatefulWidget {
  @override
  _PaymentOrderPageState createState() => _PaymentOrderPageState();
}

class _PaymentOrderPageState extends State<PaymentOrderPage>
    with SingleTickerProviderStateMixin {
  static const states = ['Processing', 'Sent', 'Expired'];
  final List<String> localeStates = [
    LocaleKeys.orderPage_processing,
    LocaleKeys.orderPage_sent,
    LocaleKeys.orderPage_expired
  ];

  int selectedStatus = 0;
  int page = 1;

  Completer<void> _refreshCompleter;

  TabController _tabController;

  GeneralBloc bloc;

  void _updateFilter(int i) {
    setState(() {
      selectedStatus = i;
    });
  }

  Future<void> _refreshData() async {
    bloc.add(FetchPaymentOrder(
        userId: BlocProvider.of<AuthenticateBloc>(context).repo.userId,
        refresh: true));

    return _refreshCompleter.future;
  }

  List<Widget> _orderList() {
    return bloc.payments
        .where((e) => e.status == states[selectedStatus])
        .map((e) => Container(
              margin: EdgeInsets.only(bottom: 5, left: 5, right: 5, top: 5),
              child: QuoteListItem(
                  fromCurrency: e.sendCurrency,
                  toCurrency: e.receivedCurrency,
                  person: e.name,
                  time: getRelativeTime(e.updatedDate),
                  amount: e.receivedAmount,
                  rate: e.referenceRate,
                  saved: e.amountSaved,
                  id: e.id,
                  type: e.settlementMethod,
                  status: e.status,
                  showOrderId: true,
                  showQuoteBy: false,
                  overrideGradient: true),
            ))
        .toList();
  }

  @override
  void initState() {
    super.initState();

    _refreshCompleter = Completer<void>();

    bloc = BlocProvider.of<GeneralBloc>(context);
    bloc.add(FetchPaymentOrder(
        userId: BlocProvider.of<AuthenticateBloc>(context).repo.userId,
        refresh: false,
        isLoadMore: false));

    _tabController = TabController(length: states.length, vsync: this);
  }

  @override
  void dispose() {
    _tabController?.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(tr(LocaleKeys.orderPage_myPaymentOrderTitle)),
          elevation: 1,
          titleSpacing: 5,
          bottom: TabBar(
            controller: _tabController,
            onTap: _updateFilter,
            indicatorColor: globalPrimaryColor,
            tabs: localeStates.map((e) => Tab(text: tr(e))).toList(),
          ),
        ),
        body: BlocConsumer<GeneralBloc, GeneralState>(
            listener: (BuildContext context, GeneralState state) {
          if (state is FetchedPayment) {
            _refreshCompleter?.complete();
            _refreshCompleter = Completer();
          }
        }, builder: (BuildContext context, GeneralState state) {
          return LoadingWrapper(
            generalState: state,
            isLoadingState: FetchingPayment,
            child: RefreshIndicator(
              onRefresh: _refreshData,
              child: ListView(
                children: [
                  ExpireIndicator(title: tr(LocaleKeys.orderPage_paymentOrder)),
                  ..._orderList(),
                  SeparateIndicator(title: tr(LocaleKeys.other_noMoreRecords))
                ],
              ),
            ),
          );
        }));
  }
}
