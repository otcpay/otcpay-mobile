import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/auth/authenticate_bloc.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/component/Indicator.dart';
import 'package:otcpay/component/ListItem.dart';
import 'package:otcpay/component/Wrapper.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/util/TimeUtil.dart';

class OTCOrderPage extends StatefulWidget {
  @override
  _OTCOrderPageState createState() => _OTCOrderPageState();
}

class _OTCOrderPageState extends State<OTCOrderPage>
    with SingleTickerProviderStateMixin {
  static const states = ['Processing', 'Matched', 'Cancelled'];
  final List<String> localeStates = [
    LocaleKeys.orderPage_processing,
    LocaleKeys.orderPage_matched,
    LocaleKeys.orderPage_cancelled
  ];

  int selectedStatus = 0;
  int page = 1;

  Completer<void> _refreshCompleter;

  TabController _controller;

  GeneralBloc bloc;

  void _navigateToAdOrder() {
    Navigator.of(context).pushNamed('/ad-order');
  }

  void _updateFilter(int i) {
    setState(() {
      selectedStatus = i;
    });
  }

  Future<void> _refreshData() async {
    bloc.add(FetchAdOrder(
        userId: BlocProvider.of<AuthenticateBloc>(context).repo.userId,
        refresh: true,
        isLoadMore: false));

    return _refreshCompleter.future;
  }

  List<Widget> _orderList() {
    return bloc.requests
        .where((e) => e.status == states[selectedStatus])
        .map((e) => Container(
            margin: EdgeInsets.only(bottom: 10, left: 5, right: 5),
            child: AdRequestListItem(
              id: e.id,
              amount: e.amount,
              updatedDate: getRelativeTime(e.updatedDate),
              offerRate: e.offerRate,
              mode: EnumToString.fromString(RATE_MODE.values, e.type),
              crytocurrency: e.crytocurrency,
              settleCurrency: e.settleCurrency,
              totalCost: e.totalCost,
            )))
        .toList();
  }

  @override
  void initState() {
    super.initState();

    _refreshCompleter = Completer<void>();
    bloc = BlocProvider.of<GeneralBloc>(context);
    bloc.add(FetchAdOrder(
        userId: BlocProvider.of<AuthenticateBloc>(context).repo.userId,
        refresh: false,
        isLoadMore: false));

    _controller = TabController(length: states.length, vsync: this);
  }

  @override
  void dispose() {
    _controller?.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(tr(LocaleKeys.orderPage_myOtcOrderTitle)),
          elevation: 1,
          titleSpacing: 5,
          bottom: TabBar(
            controller: _controller,
            onTap: _updateFilter,
            indicatorColor: globalPrimaryColor,
            tabs: localeStates.map((e) => Tab(text: tr(e))).toList(),
          ),
          actions: [
            IconButton(
                icon: Icon(Icons.description), onPressed: _navigateToAdOrder)
          ],
        ),
        body: BlocConsumer<GeneralBloc, GeneralState>(
            listener: (BuildContext context, GeneralState state) {
          if (state is FetchedPayment) {
            _refreshCompleter?.complete();
            _refreshCompleter = Completer();
          }
        }, builder: (BuildContext context, GeneralState state) {
          return LoadingWrapper(
            generalState: state,
            isLoadingState: FetchingPayment,
            child: RefreshIndicator(
              onRefresh: _refreshData,
              child: ListView(children: [
                ExpireIndicator(title: tr(LocaleKeys.orderPage_otcOrder)),
                ..._orderList(),
                SeparateIndicator(title: tr(LocaleKeys.other_noMoreRecords))
              ]),
            ),
          );
        }));
  }
}
