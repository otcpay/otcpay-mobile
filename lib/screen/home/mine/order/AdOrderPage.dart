import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/auth/authenticate_bloc.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/component/Indicator.dart';
import 'package:otcpay/component/ListItem.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/component/Wrapper.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/util/TimeUtil.dart';

class AdOrderPage extends StatefulWidget {
  @override
  _AdOrderPageState createState() => _AdOrderPageState();
}

class _AdOrderPageState extends State<AdOrderPage>
    with SingleTickerProviderStateMixin {
  Completer<void> _refreshCompleter;

  GeneralBloc bloc;

  String type = 'BUY';

  Future<void> _refreshData() async {
    bloc.add(FetchAd(
        userId: BlocProvider.of<AuthenticateBloc>(context).repo.userId,
        refresh: true,
        isLoadMore: false));

    return _refreshCompleter.future;
  }

  List<Widget> _orderList() {
    return bloc.ads
        .where((e) => e.type == type)
        .map((e) => Container(
            margin: EdgeInsets.only(bottom: 10),
            child: OTCListItem(
                id: e.id,
                name: e.name,
                quantity: e.quantity,
                upperLimit: e.upperLimit,
                lowerLimit: e.lowerLimit,
                fromCurrency: e.settleCurrency,
                profilePic: e.profilePic,
                toCurrency: e.crytocurrency,
                rate: e.offer,
                status: e.status,
                time: getRelativeTime(e.updatedDate),
                mode: EnumToString.fromString(RATE_MODE.values, e.type),
                showQuickAction: false,
                showGradient: false,
                showOrderId: true,
                showOption: true)))
        .toList();
  }

  void _filterBuy() {
    setState(() {
      type = 'BUY';
    });
  }

  void _filterSell() {
    setState(() {
      type = 'SELL';
    });
  }

  @override
  void initState() {
    super.initState();

    _refreshCompleter = Completer<void>();
    bloc = BlocProvider.of<GeneralBloc>(context);
    bloc.add(FetchAd(
        userId: BlocProvider.of<AuthenticateBloc>(context).repo.userId,
        refresh: false,
        isLoadMore: false));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            centerTitle: true,
            title: Text(tr(LocaleKeys.orderPage_myAdOrderTitle)),
            elevation: 1,
            titleSpacing: 5,
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(48),
              child: Row(
                children: [
                  Expanded(
                      child: TextButton(
                          onPressed: _filterBuy,
                          style: ButtonStyle(
                            shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.zero)),
                            backgroundColor: MaterialStateProperty.all(
                                type == 'BUY' ? upColor : labelColor),
                          ),
                          child: Container(
                              margin: EdgeInsets.only(top: 10, bottom: 10),
                              child: BoldTitle(
                                  title: tr(LocaleKeys.common_buy))))),
                  Expanded(
                      child: TextButton(
                          onPressed: _filterSell,
                          style: ButtonStyle(
                            shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.zero)),
                            backgroundColor: MaterialStateProperty.all(
                                type == 'SELL' ? downColor : labelColor),
                          ),
                          child: Container(
                              margin: EdgeInsets.only(top: 10, bottom: 10),
                              child: BoldTitle(
                                  title: tr(LocaleKeys.common_sell))))),
                ],
              ),
            )),
        body: BlocConsumer<GeneralBloc, GeneralState>(
            listener: (BuildContext context, GeneralState state) {
          if (state is FetchedPayment) {
            _refreshCompleter?.complete();
            _refreshCompleter = Completer();
          }
        }, builder: (BuildContext context, GeneralState state) {
          return LoadingWrapper(
            generalState: state,
            isLoadingState: FetchingPayment,
            child: RefreshIndicator(
              onRefresh: _refreshData,
              child: ListView(
                children: [
                  ExpireIndicator(title: tr(LocaleKeys.orderPage_otcAd)),
                  ..._orderList(),
                  SeparateIndicator(title: tr(LocaleKeys.other_noMoreRecords))
                ],
              ),
            ),
          );
        }));
  }
}
