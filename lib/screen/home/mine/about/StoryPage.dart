import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:otcpay/component/Container.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/style/Color.dart';

class StoryPage extends StatefulWidget {
  const StoryPage();

  @override
  _StoryPageState createState() => _StoryPageState();
}

class _StoryPageState extends State<StoryPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(tr(LocaleKeys.aboutPage_about) + ' OTCPAY'),
          elevation: 1,
        ),
        body: ListView(
          padding: EdgeInsets.only(left: 20, right: 20),
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Container(
                  width: 150,
                  height: 150,
                  child: Stack(
                    clipBehavior: Clip.none,
                    children: [
                      Positioned(
                          left: -30,
                          top: -10,
                          child: Image.asset('icons/AppIcon.png')),
                    ],
                  ),
                ),
                Flexible(
                  child: Container(
                    margin: EdgeInsets.only(bottom: 20),
                    child: BoldTitle(
                      title: tr(LocaleKeys.aboutPage_storyTitle),
                      fontSize: 16,
                      color: globalPrimaryColor,
                    ),
                  ),
                ),
              ],
            ),
            MainTitleContainer(
                'A cheaper, faster, disruptive way to transfer money abroad '),
            ParagraphContainer(
                'OTCPAY offers a disruptive and innovative way to send out the money from one place to another destination via our blockchain settlement network. OTCPAY is HK’s first remittance startup using blockchain technology offering global remittances services of FIAT-FIAT, CRYPTO-FIAT, FIAT-CRYPTO currency pairs in recent digital and e-commerce development. With strong blockchain settlement network and money exchange partnerships in Asian countries, i.e. Taiwan, Korea, Japan, Macau, Vietnam, Philippines, Thailand and GBA, OTCPAY performs as an alternative channel to provide reliable, secured, faster(T+0), confidential and reasonable remittances for our clients when comparing with conventional telegraphic transfer from local banks. The similar service providers, like Transferwise, Instarem, TransferGO, Azimo, Xoom & OFX also provide international remittance services for online users via its automatic matching system. Let’s join over 10 million people who get the real exchange rate with us. We’re up to 10x cheaper than banks.'),
            MainTitleContainer('OTCPAY, we offers: '),
            ParagraphContainer(
                '1.   Global Remittance or Convert USDT into Local FIAT Currency with T+0 Settlement '),
            ParagraphContainer(
                '2.   Instant Compare Different Banks/Money Exchanges\' rates via AI Go Compare'),
            ParagraphContainer(
                '3.   Connect with Different Over-the-Counter C2C Markets '),
            MainTitleContainer('OTCPAY, we truly help:'),
            ParagraphContainer(
                'Transparent Cost Saving - with fully utilization of the blockchain technology and encrypted distributed ledgers, OTCPAY provides trusted real-time verification of transactions without the need for intermediaries such as correspondent banks and clearing houses. In average, we can save up to 2.68% cost per global remittance transaction.'),
            ParagraphContainer(
                'Fastest Settlement(T+0) - we speed up the international payment processing services and converts the sender’s local fiat currency into crypto-currency, then converts the crypto into the receiver’s local currency, usually delivering international payments within the same day.'),
            ParagraphContainer(
                'Most Secured Daily Turnover - The number of cryptocurrency transactions processed on a single day reached its highest value at the beginning of 2021. Stable coin, Tether (USDT) dominates with transaction volume of over \$600 billion. OTCPAY reaches daily international transaction of HK\$25mio+. '),
            MainTitleContainer('OTCPAY AI-Go-Compare'),
            ParagraphContainer(
                'Step 1: Simply tell us the destination and how much you’re transferring, we will make the estimation comparison in terms of time and cost to you though our instant AI Go Comparison. After verifying your identity documents of ID card/passport and address proof, we will proceed your request accordingly.'),
            ParagraphContainer(
                'Step 2: Send us your funds. We accept stable coin, USDT, bank transfers from your account or bank notes in your local countries. We\'ll notify you once we receive your funds. Input the recipient information with account holder’s name, account number and other account details along with the invoice.'),
            ParagraphContainer(
                'Step 3: We track your transfer online or with our mobile app. Once the fund is settled, you will get notified from our professional dealer. International transfer to most countries takes 0-1 business days.'),
            MainTitleContainer('OTCPAY Over-the-Counter(OTC) Trading, P2P'),
            ParagraphContainer(
                'OTCPAY offers openly crypto-currency arbitrage business opportunities for our users. Via the mobile application network, ORCPAY offers an interactive peer-to-peer community of users in which users can access or match available options for the purchase or sale of cryptocurrency advertisements across a variety of cities/countries. Among various trading exchanges, OTCPAY enables users to obtain the most favourable transactional terms and identify arbitrage opportunities. OTCPAY facilitates and includes users’ ability to become verified members, browse advertisements, send private messages and contribute to the OTCPAY online & offline community.'),
            Container(margin: EdgeInsets.only(bottom: 50))
          ],
        ));
  }
}
