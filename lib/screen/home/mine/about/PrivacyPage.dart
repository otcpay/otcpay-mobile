import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:otcpay/component/Container.dart';
import 'package:otcpay/component/Input.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/style/Color.dart';

class PrivacyPage extends StatelessWidget {
  void _accept(BuildContext context) {
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RawScrollbar(
        thumbColor: globalPrimaryColor,
        radius: Radius.circular(20),
        thickness: 5,
        child: NotificationListener<OverscrollIndicatorNotification>(
          onNotification: (OverscrollIndicatorNotification overscroll) {
            overscroll.disallowGlow();
            return false;
          },
          child: Stack(
            children: [
              ListView(
                children: [
                  Container(
                    margin: EdgeInsets.fromLTRB(20, 20, 20, 10),
                    child: BoldTitle(
                        title: tr(LocaleKeys.aboutPage_privacyTitle),
                        color: globalPrimaryColor,
                        fontSize: 24),
                  ),
                  Container(
                      margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
                      child: BoldTitle(
                          title: tr(LocaleKeys.aboutPage_lastUpdated) +
                              'June 2021',
                          color: dividerColor)),
                  Divider(color: dividerColor),
                  ParagraphContainer(
                      'This Privacy Policy is an overview of how we collect, use, and process your personal data when you use our website https://www.otcpay.asia(hereinafter: “Website”), our web application (hereinafter: “Web App”) and our mobile application (hereinafter: “App”; jointly called: “Services”). The Services are owned and operated by OTCPAY LIMITED (“the Company”, “we” or “us”). We respect personal data and are committed to full implementation and compliance with the data protection principles and all relevant provisions of the Personal Data (Privacy) Ordinance, Cap. 486 (“the Ordinance”). This Policy is applicable to the users of the Services as well as any other persons that their personal data is collected, by whatever means, used and held by us.'),
                  ParagraphContainer(
                      'Please read this Policy carefully, as it becomes legally binding when you use our Services. We take privacy and protection of your data very seriously and are committed to handling the personal information of those we engage with, whether customers, suppliers or colleagues responsibly and in a way that meets the legal requirements of the countries in which we operate. '),
                  MainTitleContainer('1. Responsible Authority'),
                  ParagraphContainer(
                      'The responsible authority for the collection, processing and use of personal data is:'),
                  ParagraphContainer(
                      'OTCPAY is the trading name of OTCPAY LIMITED, (“OTCPAY”, “us”, “we”, “our'
                      ') an international business with headquarters on the A20, 10/F, Lofter Legend, Kwun Tong, Hong Kong. Our business registration number is 72552576-000-01-21-3. If you have any questions about how we protect or use your data, please kindly email us at info@otcpay.asia.'),
                  MainTitleContainer(
                      '2. Kinds of personal data collected, used and held'),
                  ParagraphContainer(
                      'We will collect and process the following data about you:'),
                  SecondaryContainer('2.1 Information you give us.'),
                  ParagraphContainer(
                      'You may give us information about you when you sign up to use our service, e.g. when you provide us with personal details like your name and email address. This also includes information you provide through your continued use of our Services, your participation in discussion boards or other social media functions on our Website or App, through entering a competition, promotion or survey, and by reporting problems with our Services. The information you give us may include your name, address, email address, phone number, financial information (including credit card, debit card, or bank account information), payment reason, geographical location, social security number, personal description and photograph.'),
                  ParagraphContainer(
                      'In some cases, such as when you send or receive high value or high volume transactions, or where we need to comply with anti-money laundering regulations, we may also need more commercial or identification information from you.'),
                  ParagraphContainer(
                      'In providing the personal data of any individual (other than yourself) that receive payments from you during your use of our Services, you promise that you have obtained consent from such individual to disclose his/her personal data to us, as well his/her consent to our collection, use and disclosure of such personal data, for the purposes set out in this Privacy Policy.'),
                  SecondaryContainer('2.2 Information we collect about you.'),
                  ParagraphContainer(
                      'With regard to your use of our Services, we may automatically collect the following information:'),
                  ParagraphContainer(
                      'details of the transactions you carry out when using our Services, including geographic location from which the transaction originates;'),
                  ParagraphContainer(
                      'technical information, including the Internet protocol (IP) address used to connect your computer to the internet, your login information, browser type and version, time zone setting, browser plug-in types and versions, operating system and platform;'),
                  ParagraphContainer(
                      'information about your visit, including the full Uniform Resource Locators (URL) clickstream to, through and from our Website or App (including date and time); products you viewed or searched for; page response times, download errors, length of visits to certain pages, page interaction information (such as scrolling, clicks, and mouse-overs), and methods used to browse away from the page and any phone number used to call our customer support number.'),
                  SecondaryContainer(
                      '2.3 Information we receive from other sources.'),
                  ParagraphContainer(
                      'We may receive information about you if you use any of the other websites we operate or the other services we provide. We are also working closely with third parties and may receive information about you from them.'),
                  ParagraphContainer('For example:'),
                  ParagraphContainer(
                      'the banks you use to transfer money to us will provide us with your basic personal information, such as your name and address, as well as your financial information such as your bank account details;'),
                  BulletParagraphContainer(
                      '•	business partners may provide us with your name and address, as well as financial information, such as card payment information;'),
                  BulletParagraphContainer(
                      '•	advertising networks, analytics providers and search information providers may provide us with pseudonymised information about you, such as confirming how you found our website;'),
                  BulletParagraphContainer(
                      '•	credit reference agencies do not provide us with any personal information about yourself, but we may use them to corroborate the information you have provided to us.'),
                  SecondaryContainer(
                      '2.4 Information from social media networks.'),
                  ParagraphContainer(
                      'If you log in to our Services using your social media account (for example, Facebook or Google) we will receive relevant information that is necessary to enable our Services and authenticate you. The social media network will provide us with access to certain information that you have provided to them, including your name, profile image and email address. We use such information, together with any other information you directly provide to us when registering or using our Services, to create your account and to communicate with you about the information, products and services that you request from us. You may also be able to specifically request that we have access to the contacts in your social media account so that you can send a referral link to your family and friends. We will use, disclose and store all of this information in accordance with this privacy policy.'),
                  SecondaryContainer('2.5 Sensitive data'),
                  ParagraphContainer(
                      'We process a limited amount of sensitive data when we carry out verification of identity documents that contain biometric data. Where we are relying on the substantial public interest condition.'),
                  SecondaryContainer('2.6 Children’s data'),
                  ParagraphContainer(
                      'Our products and services are directed at adults aged 18 years and over, and not intended for children. We do not knowingly collect data from this age group. Our verification process prevents OTCPAY collecting this data. If any data is collected from a child without verification of parental consent, it will be deleted.'),
                  MainTitleContainer(
                      '3. How we protect your personal information'),
                  SecondaryContainer(
                      '3.1 We take the safeguarding of your information very seriously, and take a number of steps to ensure it stays secure:'),
                  BulletParagraphContainer(
                      '•	Communication over the Internet between you and OTCPAY servers are encrypted using strong asymmetric encryption. This makes it unreadable to anyone who might be listening in.'),
                  BulletParagraphContainer(
                      '•	We update and patch our servers in a timely manner'),
                  BulletParagraphContainer(
                      '•	Our technical security team proactively monitors for abnormal and malicious activity in our servers and services'),
                  BulletParagraphContainer(
                      '•	When information you’ve given us is not in active use, it is encrypted at rest. This means it’s unreadable from server hard-drives without the decryption secret.'),
                  SecondaryContainer(
                      '3.2 We do regular audits. As part of these audits, our security is validated by external auditors.'),
                  SecondaryContainer(
                      '3.3 We restrict access to your personal information to those employees of OTCPAY who have a business reason for knowing such information. We continuously educate and train our employees about the importance of confidentiality and privacy of customer personal information. We maintain physical, electronic and procedural safeguards that comply with the relevant laws and regulations to protect your personal information from unauthorised access.'),
                  MainTitleContainer('4. Ways we use your information'),
                  SecondaryContainer('4.1 We use your information:'),
                  BulletParagraphContainer(
                      '•	to carry out our obligations relating to your contract with us and to provide you with the information, products and services;'),
                  BulletParagraphContainer(
                      '•	to comply with any applicable legal and/or regulatory requirements;'),
                  BulletParagraphContainer(
                      '•	to notify you about changes to our Services;'),
                  BulletParagraphContainer(
                      '•	as part of our efforts to keep our Services safe and secure;'),
                  BulletParagraphContainer(
                      '•	to administer our Services and for internal operations, including troubleshooting, data analysis, testing, research, statistical and survey purposes;'),
                  BulletParagraphContainer(
                      '•	to improve our Services and to ensure that they are presented in the most effective manner;'),
                  BulletParagraphContainer(
                      '•	to allow other OTCPAY customers to request or send money to you through our services when providing matching information for your phone number or email;'),
                  BulletParagraphContainer(
                      '•	to measure or understand the effectiveness of advertising we serve and to deliver relevant advertising to you;'),
                  BulletParagraphContainer(
                      '•	to allow you to participate in interactive features of our Services, when you choose to do so;'),
                  BulletParagraphContainer(
                      '•	to provide you with information about other similar goods and services we offer;'),
                  BulletParagraphContainer(
                      '•	to combine information we receive from other sources with the information you give to us and information we collect about you. We may use this information and the combined information for the purposes set out above (depending on the types of information we receive).'),
                  MainTitleContainer('5. Retention of personal data'),
                  SecondaryContainer(
                      '5.1 The Company will retain all your personal data and the transaction records so long as is necessary for the fulfilment of (i) the purpose(s) for which they were collected and (ii) of the legal requirements. All your personal data and the transaction records will be destroyed thereafter.'),
                  SecondaryContainer(
                      '5.2 Our information and data are stored and maintained in our electronic database, facilities, servers and back-up servers which may be located outside your country, where the legal protection and standard of security requirements may not be equivalent to the same provided by and legally required by the applicable laws and regulations of your country. You hereby expressly consent on our transferring, storing, maintain and retaining all your information and data in our electronic databases, facilitates, servers and back-up servers located outside your country for the purposes referred to the above.'),
                  SecondaryContainer(
                      '5.3 As a regulated financial institution, OTCPAY is required by law to store some of your personal and transactional data beyond the closure of your account with us. We only access your data internally on a need to know basis, and we’ll only access or process it if absolutely necessary.'),
                  SecondaryContainer(
                      '5.4 We will always delete data that is no longer required by a relevant law or jurisdiction in which we operate.'),
                  MainTitleContainer('6. Accuracy and change of personal data'),
                  SecondaryContainer(
                      '6.1 In order to ensure the correctness of the members’ personal data, particularly contact details, retained by the Company, you may receive our request of confirmation or verification. Please feel free to contact us for correction or change of personal data or opt-out from receiving future direct promotional and marketing information.'),
                  MainTitleContainer(
                      '7. Personal data handling and security measures'),
                  SecondaryContainer(
                      '7.1 The personal data we collect about you will not be disclosed to any other party without your prior approval.'),
                  SecondaryContainer(
                      '7.2 We have established in place security measures and monitoring procedures to prevent unauthorized or accidental access, processing, erasure, loss or use of the data. All personnel that may access to the data will be ensured complying with the requirements of data protection'),
                  SecondaryContainer(
                      '7.3 Our security measures are listed below:'),
                  BulletParagraphContainer(
                      '•	controls on password complexity, re-tries or resets are implemented to prevent passwords from being compromised;'),
                  BulletParagraphContainer(
                      '•	Advanced encryption technology is used to protect the safety of personal data (including names, email addresses, telephone numbers, credit card information and purchase records) during transmission. Our SecureSocketLayer (SSL) system will prevent your data being transmitted from unauthorized interception or access by third parties;'),
                  BulletParagraphContainer(
                      '•	Only the personnel that have been trained with the strict privacy guidelines and procedures are authorized to access or handle your personal data. Such personnel are accountable for non-compliance with the related obligations;'),
                  BulletParagraphContainer(
                      '•	The security system will be reviewed regularly. Apart from statutory requirements, the Company will not transfer or disclose any of your personal data to any other parties.'),
                  MainTitleContainer('8. Limitation of Liability'),
                  SecondaryContainer(
                      '8.1 The Company has implemented all reasonably practicable procedures, measures and steps to safeguard the security of your personal data. However, no method of transmission over the Internet, or method of electronic storage, is totally secure. We cannot guarantee its absolute security. Unless there is proved negligence, we shall not accept any liabilities therefore.'),
                  MainTitleContainer(
                      '9. Legal basis for processing in Asia & Europe'),
                  SecondaryContainer(
                      '9.1 The lawful basis we use for collecting and processing your information in Asia & Europe (as required by current legislation) are as follows:'),
                  BulletParagraphContainer(
                      '•	Where it is necessary for entering into or performing a contract with you;'),
                  BulletParagraphContainer(
                      '•	Where we have a legitimate interest to do so, provided your rights do not override those interests;'),
                  BulletParagraphContainer(
                      '•	Where you have consented to its uses;'),
                  BulletParagraphContainer(
                      '•	Where our colleagues believe it is in your vital interests to share your personal details;'),
                  BulletParagraphContainer(
                      '•	Where required to comply with our legal obligations.'),
                  MainTitleContainer('10. Disclosure of your information'),
                  SecondaryContainer(
                      '10.1 We may share your information with selected third parties including:'),
                  BulletParagraphContainer(
                      '•	affiliates, business partners, suppliers and subcontractors for the performance and execution of any contract we enter into with them or you;'),
                  BulletParagraphContainer(
                      '•	advertisers and advertising networks solely to select and serve relevant adverts to you and others with your consent;'),
                  BulletParagraphContainer(
                      '•	analytics and search engine providers that assist us in the improvement and optimisation of our site; and'),
                  BulletParagraphContainer(
                      '•	our group entities or subsidiaries'),
                  SecondaryContainer(
                      '10.2 We may disclose your personal information to third parties:'),
                  BulletParagraphContainer(
                      '•	such as affiliates, business partners, suppliers and subcontractors for the performance and execution of any contract we enter into with them or you;'),
                  BulletParagraphContainer(
                      '•	in the event that we sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets;'),
                  BulletParagraphContainer(
                      '•	if we are under a duty to disclose or share your personal data in order to comply with any legal obligation, or in order to enforce or apply our Customer Agreement and other applicable agreements; or to protect the rights, property, or safety of OTCPAY, our customers, or others. This includes exchanging information with other companies and organisations for the purposes of fraud protection and credit risk reduction;'),
                  BulletParagraphContainer(
                      '•	to assist us in conducting or co-operating in investigations of fraud or other illegal activity where we believe it is reasonable and appropriate to do so;'),
                  BulletParagraphContainer(
                      '•	to prevent and detect fraud or crime;'),
                  BulletParagraphContainer(
                      '•	in response to a subpoena, warrant, court order, or as otherwise required by law;'),
                  BulletParagraphContainer(
                      '•	to assess financial and insurance risks;'),
                  BulletParagraphContainer(
                      '•	to recover debt or in relation to your insolvency; and'),
                  BulletParagraphContainer(
                      '•	to develop customer relationships, services and systems.'),
                  SecondaryContainer(
                      '10.3 We do not have a list of all third parties we share your data with, as this would be dependent on your specific use of our Services. However, if you would like further information about who we have shared your data with, or to be provided with a list specific to you, you can request this by writing to info@octpay.aisa.'),
                  MainTitleContainer(
                      '11. Sharing and storing your personal data'),
                  SecondaryContainer(
                      '11.1 We may transfer and store your data at a destination outside the European Economic Area ("EEA"). It may also be processed by staff operating outside the EEA who work for us or for one of our suppliers. Such staff may be engaged in, among other things, the fulfilment of your payment order, the processing of your payment details and the provision of support services. By submitting your personal data, you agree to this transfer, storing and processing. We will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy.'),
                  SecondaryContainer(
                      '11.2 In order to provide our Services to you, it is sometimes necessary for us to transfer your data to the third parties outlined in section 6.1 that are based outside of the European Economic Area. In these cases, we ensure that both ourselves and our partners take adequate and appropriate technical, physical and organisational security measures to protect your data. We also ensure we have appropriate contractual protections (e.g. BCR, Standard Contractual Clauses) in place with these parties receiving the data outside the EEA.'),
                  MainTitleContainer(
                      '12. Profiling and Automated Decision Making'),
                  SecondaryContainer(
                      '12.1 We may use some elements of your data to customise our Services and the information we provide to you, and to address your needs — such as your country of residence and transaction history. For example, if you frequently send funds from one particular currency to another, we may use this information to inform you of new product updates or features that may be useful for you. When we do this, we take all necessary measures to ensure that your privacy and security are protected — and we only use pseudonymised data wherever possible. This activity has no legal effect on you.'),
                  SecondaryContainer(
                      '12.2 We may use Automated Decision Making (ADM) to improve your experience, or to help fight financial crime. For example, so that we can provide you with a fast and efficient service, we may use ADM to verify your identity documents, or to confirm the accuracy of the information you have provided to us. None of our ADM processes have a legal effect on you.'),
                  MainTitleContainer('13. Cookies'),
                  SecondaryContainer(
                      '13.1 We use small files (known as cookies) to distinguish you from other users, see how you use our site and products while providing you with the best experience. They also enable us to improve our services. For detailed information on cookies and other technologies we use and the purposes for which we use them.'),
                  MainTitleContainer('14. Your rights'),
                  SecondaryContainer(
                      '14.1 Subject to applicable laws, you may have the right to access information we hold about you. Your right of access can be exercised in accordance with the relevant data protection legislation. If you have any questions in relation to our use of your personal information, contact us. Under certain conditions, you may have the right to require us to:'),
                  BulletParagraphContainer(
                      '•	provide you with further details on the use we make of your information;'),
                  BulletParagraphContainer(
                      '•	provide you with a copy of the information that you have provided to us;'),
                  BulletParagraphContainer(
                      '•	update any inaccurate, incorrect, or out of date personal information we hold;'),
                  BulletParagraphContainer(
                      '•	delete any personal information that is no longer necessary, or no longer subject to a legal obligation to which OTCPAY is subject to. OTCPAY has legal obligations so it may not be possible to delete your data at the time of request. Once the required time has passed then we will be able to comply with your request;'),
                  BulletParagraphContainer(
                      '•	where processing is based on consent, to withdraw your consent so that we stop that particular processing;'),
                  BulletParagraphContainer(
                      '•	cease direct marketing to you, by contacting us or adjusting your notification preferences in the settings section of your account;'),
                  BulletParagraphContainer(
                      '•	where we undertake wholly automated decision making which results in the creation of a legal obligation or a similar significant impact, you may request that we provide information about the decision-making methodology and ask us to verify that the automated decision has been made correctly. We may reject the request, as permitted by applicable law, including when providing the information would result in a disclosure of a trade secret or would interfere with the prevention or detection of fraud or other crime. However, generally in these circumstances we will verify that the algorithm and source data are functioning as anticipated without error or bias or if required by law to adjust the processing.'),
                  BulletParagraphContainer(
                      '•	object to any processing based on the legitimate interests ground unless our reasons for undertaking that processing outweigh any prejudice to your data protection rights'),
                  BulletParagraphContainer(
                      '•	restrict how we use your information whilst a complaint is being investigated'),
                  SecondaryContainer(
                      '14.2 Your exercise of these rights is subject to certain exemptions to safeguard the public interest (e.g. the prevention or detection of crime) and our interests (e.g. the maintenance of legal privilege). If you exercise any of these rights we will check your entitlement and respond in most cases within a month.'),
                  MainTitleContainer('15. Third-party links'),
                  SecondaryContainer(
                      '15.1 Our Services may, from time to time, contain links to and from the websites of our partner networks, advertisers and affiliates. If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility for them. Please check these policies before you submit any personal data to these websites.'),
                  MainTitleContainer('16. Changes to our privacy policy'),
                  SecondaryContainer(
                      '16.1 To keep up with changing legislation, best practice and changes in how we process personal information, we may revise this Privacy Policy at any time without notice by posting a revised version on this website. To stay up to date on any changes, check back periodically.'),
                  MainTitleContainer('17. Contact Us'),
                  SecondaryContainer(
                      '17.1 Please send any requests(including Opt-out), enquires or comments regarding our Privacy Policy, please kindly email us at info@otcpay.aisa or contact out Customer Services Officer at 24682222. In general circumstances, your requests or enquiries will be handled within 14 working days after receipt of the same and verification of your identity. You can also write to us at the following address – OTCPAY (OYCPAY LIMITED), A20, 10/F, Lofter Legend, Kwun Tong, Hong Kong.'),
                  MainTitleContainer('18. Effective Date'),
                  SecondaryContainer(
                      '18.1 This Privacy Policy becomes effective from May 1, 2021.'),
                  Container(margin: EdgeInsets.only(bottom: 50))
                ],
              ),
              Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    color: globalBackgroundColor,
                    height: 100,
                    child: Row(
                      children: [
                        Expanded(
                            child: ConfirmButton(
                                callback: () => _accept(context),
                                margin: EdgeInsets.only(left: 20, right: 20),
                                title: tr(LocaleKeys.aboutPage_close),
                                height: 50)),
                      ],
                    ),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
