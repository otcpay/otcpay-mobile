import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:otcpay/component/ListItem.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/screen/home/mine/about/PrivacyPage.dart';
import 'package:otcpay/screen/home/mine/about/TermsPage.dart';
import 'package:package_info_plus/package_info_plus.dart';

import 'StoryPage.dart';

class AboutPage extends StatelessWidget {
  final PackageInfo packageInfo;

  const AboutPage(this.packageInfo);

  static const List<String> itemList = [
    'Our Story',
    'Terms And Conditions',
    'Privacy'
  ];

  void _navigateToStory(context) {
    Navigator.of(context).push(MaterialPageRoute(builder: (_) => StoryPage()));
  }

  void _navigateToTerms(context) {
    Navigator.of(context).push(
        MaterialPageRoute(builder: (_) => TermsPage(needChecking: false)));
  }

  void _navigateToPrivacy(context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => PrivacyPage()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(tr(LocaleKeys.aboutPage_about)),
        centerTitle: true,
        elevation: 1.0,
      ),
      body: ListView(
        children: [
          SettingListItem(
            item: tr(LocaleKeys.aboutPage_ourStory),
            icon: itemList[0],
            showLeading: false,
            needExtraMargin: true,
            callback: () => _navigateToStory(context),
          ),
          SettingListItem(
            item: tr(LocaleKeys.aboutPage_toc),
            icon: itemList[1],
            showLeading: false,
            callback: () => _navigateToTerms(context),
          ),
          SettingListItem(
            item: tr(LocaleKeys.aboutPage_privacy),
            icon: itemList[2],
            showLeading: false,
            callback: () => _navigateToPrivacy(context),
          ),
          Container(
            margin: EdgeInsets.only(top: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                BoldTitle(title: packageInfo.appName),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SubTitle(
                    title: tr(LocaleKeys.aboutPage_version) +
                        packageInfo.version +
                        '(' +
                        packageInfo.buildNumber +
                        ')'),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SubTitle(title: 'Copyright © 2021 OTCPAY LIMITED'),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SubTitle(title: 'All rights reserved.'),
            ],
          )
        ],
      ),
    );
  }
}
