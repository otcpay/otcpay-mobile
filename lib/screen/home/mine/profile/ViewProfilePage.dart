import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:otcpay/component/Input.dart';
import 'package:otcpay/component/ListItem.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/model/Profile.dart';
import 'package:otcpay/style/Color.dart';

import 'EditProfilePage.dart';

class ViewProfilePage extends StatelessWidget {
  final Profile profile;

  const ViewProfilePage(this.profile);

  void _navigateToEdit(BuildContext context) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => EditProfilePage(
              account: profile.account,
              username: profile.username,
              region: profile.region,
              wechatId: profile.wechatId,
              whatsappId: profile.whatsappId,
              telegramId: profile.telegramId,
              profilePic: profile.profilePic,
            )));
  }

  bool showPlaceHolder() {
    return profile.profilePic != null && profile.profilePic != '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(tr(LocaleKeys.profilePage_profile)),
          centerTitle: true,
          elevation: 1.0,
        ),
        body: ListView(
          children: [
            Container(
                margin: EdgeInsets.only(left: 15, right: 15, top: 50),
                height: 410,
                decoration: BoxDecoration(
                    color: inputBoxBackgroundColor,
                    borderRadius: BorderRadius.circular(8)),
                child: Stack(
                  clipBehavior: Clip.none,
                  children: [
                    Positioned(
                        top: -35,
                        left: MediaQuery.of(context).size.width / 2 - 50,
                        right: MediaQuery.of(context).size.width / 2 - 50,
                        child: showPlaceHolder()
                            ? ClipRRect(
                                borderRadius: BorderRadius.circular(8.0),
                                child: Image.network(
                                  profile.profilePic + '?alt=media',
                                  height: 70,
                                  fit: BoxFit.cover,
                                ),
                              )
                            : Image.asset(
                                'icons/account_photo.png',
                                height: 70,
                                fit: BoxFit.cover,
                              )),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        margin: EdgeInsets.only(right: 20),
                        height: 360,
                        child: Column(
                          children: [
                            ProfileListItem(
                              title: tr(LocaleKeys.profilePage_account),
                              value: profile.account,
                            ),
                            Container(
                                margin: EdgeInsets.only(left: 20),
                                child: Divider(color: labelColor)),
                            ProfileListItem(
                              title: tr(LocaleKeys.profilePage_username),
                              value: profile.username,
                            ),
                            Container(
                                margin: EdgeInsets.only(left: 20),
                                child: Divider(color: labelColor)),
                            ProfileListItem(
                              title: tr(LocaleKeys.profilePage_region),
                              value: profile.region,
                            ),
                            Container(
                                margin: EdgeInsets.only(left: 20),
                                child: Divider(color: labelColor)),
                            ProfileListItem(
                              title: tr(LocaleKeys.profilePage_whatsapp),
                              value: profile.whatsappId,
                            ),
                            Container(
                                margin: EdgeInsets.only(left: 20),
                                child: Divider(color: labelColor)),
                            ProfileListItem(
                              title: tr(LocaleKeys.profilePage_wechat),
                              value: profile.wechatId,
                            ),
                            Container(
                                margin: EdgeInsets.only(left: 20),
                                child: Divider(color: labelColor)),
                            ProfileListItem(
                              title: tr(LocaleKeys.profilePage_telegram),
                              value: profile.telegramId,
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                )),
            ConfirmButton(
                title: tr(LocaleKeys.profilePage_edit),
                height: 45,
                margin:
                    EdgeInsets.only(left: 25, right: 25, bottom: 20, top: 50),
                callback: () => _navigateToEdit(context))
          ],
        ));
  }
}
