import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/auth/authenticate_bloc.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/bloc/proof/proof.dart';
import 'package:otcpay/component/Wrapper.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/screen/signup/ProfilePage.dart';

class EditProfilePage extends StatefulWidget {
  final String account;
  final String username;
  final String region;
  final String whatsappId;
  final String wechatId;
  final String telegramId;
  final String profilePic;

  const EditProfilePage(
      {this.account = '',
      this.username = '',
      this.region = '',
      this.whatsappId = '',
      this.wechatId = '',
      this.telegramId = '',
      this.profilePic});

  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  void _showSnackbar(String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void _success(String message) {
    _showSnackbar(message);

    BlocProvider.of<ProofBloc>(context).add(
        GetProfile(BlocProvider.of<AuthenticateBloc>(context).repo.userId));
  }

  @override
  void dispose() {
    try {
      // clear snackbar in best effort
      ScaffoldMessenger.of(context).clearSnackBars();
    } catch (e) {
      // do nothing
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(tr(LocaleKeys.profilePage_profile)),
        centerTitle: true,
        elevation: 1.0,
      ),
      body: BlocConsumer<GeneralBloc, GeneralState>(
          listener: (BuildContext context, GeneralState state) {
        if (state is Error) _showSnackbar(state.message);
        if (state is ProfileUpdated)
          _success(tr(LocaleKeys.common_updateSuccess));
      }, builder: (BuildContext context, GeneralState state) {
        return LoadingWrapper(
            generalState: state,
            isLoadingState: ProfileUpdating,
            child: ProfilePageContent(
              account: widget.account,
              username: widget.username,
              region: widget.region,
              wechatId: widget.wechatId,
              whatsappId: widget.whatsappId,
              telegramId: widget.telegramId,
              profilePic: widget.profilePic,
              buttonTitle: tr(LocaleKeys.common_save),
              isRegister: false,
            ));
      }),
    );
  }
}
