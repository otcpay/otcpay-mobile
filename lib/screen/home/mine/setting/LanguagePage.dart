import 'package:easy_localization/easy_localization.dart';
import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';
import 'package:otcpay/component/ListItem.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/style/Color.dart';

class LanguagePage extends StatefulWidget {
  @override
  _LanguagePageState createState() => _LanguagePageState();
}

class _LanguagePageState extends State<LanguagePage> {
  int language = 0;

  void setEn() async {
    await context.setLocale(Locale('en', 'US'));
  }

  void setZh() async {
    await context.setLocale(Locale('zh', 'HK'));
  }

  void setCN() async {
    await context.setLocale(Locale('zh', 'CN'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(tr(LocaleKeys.settingPage_language)),
          centerTitle: true,
          elevation: 1.0,
        ),
        body: ListView(
          padding: EdgeInsets.only(top: 10, right: 20),
          children: [
            ProfileListItem(
                color: Colors.transparent,
                title: 'English',
                showTrailingIcon: context.locale.countryCode == 'US',
                showValue: false,
                callback: setEn),
            Container(
                margin: EdgeInsets.only(left: 20),
                child: Divider(color: labelColor)),
            ProfileListItem(
                color: Colors.transparent,
                title: '繁體中文',
                showTrailingIcon: context.locale.countryCode == 'HK',
                showValue: false,
                callback: setZh),
            Container(
                margin: EdgeInsets.only(left: 20),
                child: Divider(color: labelColor)),
            ProfileListItem(
                color: Colors.transparent,
                title: '简体中文',
                showTrailingIcon: context.locale.countryCode == 'CN',
                showValue: false,
                callback: setCN),
          ],
        ));
  }
}
