import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/api/service.dart';
import 'package:otcpay/bloc/auth/authenticate.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/bloc/proof/proof.dart';
import 'package:otcpay/component/Input.dart';
import 'package:otcpay/component/Password.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/component/VerificationCode.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/util/ParamUtil.dart';

class ResetPasswordPage extends StatefulWidget {
  @override
  _ResetPasswordPageState createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {
  final TextEditingController accountController = TextEditingController();
  final TextEditingController codeController = TextEditingController();

  int step = 0;

  AuthenticateBloc bloc;

  void _nextStep() {
    setState(() {
      step++;
    });
  }

  void _dismiss() {
    Navigator.pop(context);

    Navigator.pushNamed(context, '/login');
  }

  void _showResetSuccess() {
    BlocProvider.of<AuthenticateBloc>(context).add(Logout());
    BlocProvider.of<ProofBloc>(context).add(ClearProfile());
    BlocProvider.of<GeneralBloc>(context).add(NavigateTab(index: 0));

    Future.delayed(const Duration(milliseconds: 1000), () {
      showDialog(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) => AlertDialog(
          contentPadding: EdgeInsets.only(left: 40, right: 40, bottom: 30),
          titlePadding: EdgeInsets.only(top: 40, bottom: 10),
          backgroundColor: inputBoxBackgroundColor,
          title: Text(tr(LocaleKeys.loginPage_passwordChangedTitle),
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white, fontSize: 22)),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                SubTitle(
                  align: TextAlign.center,
                  color: subTitleColor,
                  fontSize: 12,
                  title: tr(LocaleKeys.loginPage_passwordChangedSubTitle),
                ),
                Image.asset('icons/Check.png'),
                ConfirmButton(
                    callback: _dismiss,
                    title: tr(LocaleKeys.loginPage_loginCap),
                    height: 50),
              ],
            ),
          ),
        ),
      );
    });
  }

  void _showSnackbar(String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void _checkAccount() {
    if (isValidAccount(accountController.text)) {
      bloc.add(ResetCheckAccount(accountController.text));
    }
  }

  Widget _renderContentByStep() {
    if (step == 0) {
      return ListView(children: [
        ForgetPassword(
          title: BoldTitle(
              title: tr(LocaleKeys.resetPasswordPage_resetPasswordTitle),
              fontSize: 24),
          callback: _checkAccount,
          codeController: codeController,
          controller: accountController,
          showClose: false,
        ),
      ]);
    }

    if (step == 1) {
      String username = accountController.text;
      if (codeController.text != null && codeController.text.isNotEmpty) {
        username = codeController.text + username;
      }
      return VerificationCode(
        callback: _nextStep,
        titleFontSize: 24,
        needFullWidth: true,
        account: username,
        type: EmailType.RESET_PW,
      );
    }

    return SingleChildScrollView(
      child: ResetPassword(
        title: BoldTitle(
            title: tr(LocaleKeys.signupPage_createPassword), fontSize: 24),
        account: accountController.text,
      ),
    );
  }

  @override
  void initState() {
    super.initState();

    bloc = BlocProvider.of<AuthenticateBloc>(context);
  }

  @override
  void dispose() {
    accountController.dispose();
    codeController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(tr(LocaleKeys.resetPasswordPage_resetPasswordTitle)),
          centerTitle: true,
          elevation: 1.0,
        ),
        body: BlocConsumer<AuthenticateBloc, AuthenticateState>(
            listener: (BuildContext blocContext, AuthenticateState state) {
          if (state is ValidateError) _showSnackbar(state.message);
          if (state is Validated) _nextStep();
          if (state is Reset) _showResetSuccess();
        }, builder: (BuildContext blocContext, AuthenticateState state) {
          return _renderContentByStep();
        }));
  }
}
