import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/auth/authenticate.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/bloc/proof/proof.dart';
import 'package:otcpay/component/Input.dart';
import 'package:otcpay/component/ListItem.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/screen/home/mine/setting/ResetPasswordPage.dart';
import 'package:otcpay/style/Color.dart';

import '../../../LoginPage.dart';
import 'LanguagePage.dart';

class SettingPage extends StatelessWidget {
  static const List<String> itemList = [
    'Language',
    'Reset Password',
  ];

  void _logout(BuildContext context) {
    BlocProvider.of<AuthenticateBloc>(context).add(Logout());
    BlocProvider.of<ProofBloc>(context).add(ClearProfile());
    BlocProvider.of<GeneralBloc>(context).add(ClearOrderRecord());
  }

  void _navigateToLanguage(context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => LanguagePage()));
  }

  void _navigateToResetPw(context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => ResetPasswordPage()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(tr(LocaleKeys.settingPage_setting)),
        centerTitle: true,
        elevation: 1.0,
      ),
      body: BlocListener<AuthenticateBloc, AuthenticateState>(
        listener: (context, state) {
          if (state is NotAuthenticated) {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(
                    builder: (_) =>
                        LoginPage(isFirstPage: true, fromLogoutPage: true)),
                (route) => false);
          }
        },
        child: ListView(
          children: [
            SettingListItem(
              icon: itemList[0],
              item: tr(LocaleKeys.settingPage_language),
              showLeading: false,
              needExtraMargin: true,
              callback: () => _navigateToLanguage(context),
            ),
            SettingListItem(
              icon: itemList[1],
              item: tr(LocaleKeys.settingPage_resetPassword),
              showLeading: false,
              callback: () => _navigateToResetPw(context),
            ),
            ConfirmButton(
              margin: EdgeInsets.only(top: 50, left: 15, right: 15),
              callback: () => _logout(context),
              height: 50,
              color: inputBoxBackgroundColor,
              title: tr(LocaleKeys.settingPage_logout),
            )
          ],
        ),
      ),
    );
  }
}
