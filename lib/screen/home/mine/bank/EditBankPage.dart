import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:otcpay/bloc/auth/authenticate_bloc.dart';
import 'package:otcpay/bloc/proof/proof.dart';
import 'package:otcpay/component/Container.dart';
import 'package:otcpay/component/Indicator.dart';
import 'package:otcpay/component/Input.dart';
import 'package:otcpay/component/Wrapper.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/model/Profile.dart';
import 'package:otcpay/screen/home/payment/RecipientDetailPage.dart';

class EditBankPage extends StatefulWidget {
  final BankProof data;
  final int index;

  const EditBankPage({this.data, this.index});

  @override
  _EditBankPageState createState() => _EditBankPageState();
}

class _EditBankPageState extends State<EditBankPage> {
  final GlobalKey<ConfirmButtonState> key = GlobalKey();

  final TextEditingController nameController = TextEditingController();
  final TextEditingController accountNumController = TextEditingController();
  final TextEditingController bankNameController = TextEditingController();
  final TextEditingController bankAddressController = TextEditingController();

  final picker = ImagePicker();

  String path;

  void _updateButtonState() {
    if (key.currentState != null) {
      key.currentState.setState(() {});
    }
  }

  void _save() {
    if (widget.data == null && widget.index == null) {
      BlocProvider.of<ProofBloc>(context).add(AddBank(
          callback: _uploadCallback,
          userId: BlocProvider.of<AuthenticateBloc>(context).repo.userId,
          data: BankProof(
              image: path,
              detail: bankAddressController.text,
              bankCode: accountNumController.text,
              bankName: bankNameController.text,
              holderName: nameController.text)));
    } else {
      BlocProvider.of<ProofBloc>(context).add(UpdateBanks(
          callback: _uploadCallback,
          userId: BlocProvider.of<AuthenticateBloc>(context).repo.userId,
          index: widget.index,
          data: BankProof(
              image: path,
              detail: bankAddressController.text,
              bankCode: accountNumController.text,
              bankName: bankNameController.text,
              holderName: nameController.text)));
    }
  }

  void _uploadCallback() {
    _showSnackbar(tr(LocaleKeys.common_updateSuccess));
  }

  void _showSnackbar(String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  Future _getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      final f = File(pickedFile.path);
      int sizeInBytes = f.lengthSync();
      double sizeInMb = sizeInBytes / (1024 * 1024);
      if (sizeInMb > 5) {
        _showSnackbar(tr(LocaleKeys.common_fileTooLarge));
        return;
      }

      setState(() {
        path = pickedFile.path;
      });
    } else {
      print('No image selected.');
    }
  }

  bool _validate() {
    return nameController.text.length > 0 &&
        accountNumController.text.length > 0 &&
        bankNameController.text.length > 0;
  }

  @override
  void initState() {
    super.initState();

    if (widget.data != null) {
      nameController.text = widget.data.holderName;
      accountNumController.text = widget.data.bankCode;
      bankNameController.text = widget.data.bankName;
      bankAddressController.text = widget.data.detail;
      path = widget.data.image;
    }

    nameController.addListener(_updateButtonState);
    accountNumController.addListener(_updateButtonState);
    bankNameController.addListener(_updateButtonState);
  }

  @override
  void dispose() {
    nameController.dispose();
    accountNumController.dispose();
    bankNameController.dispose();
    bankAddressController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ProofBloc, ProofState>(
        listener: (BuildContext context, ProofState state) {
      if (state is UpdateError) _showSnackbar(state.message);
    }, builder: (BuildContext context, ProofState state) {
      return Scaffold(
          appBar: AppBar(
            title: Text(tr(LocaleKeys.bankPage_editBankTitle)),
            centerTitle: true,
            elevation: 1.0,
          ),
          body: LoadingWrapper(
              proofState: state,
              isLoadingState: Updating,
              child: ListView(
                children: [
                  ...recipientDetailContent(
                      nameController: nameController,
                      accountNumController: accountNumController,
                      bankNameController: bankNameController,
                      bankAddressController: bankAddressController),
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    child: Row(
                      children: [
                        InputTitle(
                            title:
                                tr(LocaleKeys.bankPage_bankDetailInputTitle)),
                        OptionalIndicator()
                      ],
                    ),
                  ),
                  DotLineContainer(
                      showMask: path != null && path.isNotEmpty,
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      image: Image.asset('icons/bank_proof.png'),
                      height: 90,
                      title: tr(LocaleKeys.bankPage_uploadButtonTitle),
                      subTitle: tr(LocaleKeys.bankPage_uploadButtonSubTitle),
                      callback: _getImage),
                  ConfirmButton(
                      margin: EdgeInsets.only(
                          top: 50.0, left: 20.0, right: 20.0, bottom: 20.0),
                      key: key,
                      validator: _validate,
                      title: tr(LocaleKeys.common_save),
                      height: 50,
                      callback: _save)
                ],
              )));
    });
  }
}
