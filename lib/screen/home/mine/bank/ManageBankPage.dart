import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/auth/authenticate_bloc.dart';
import 'package:otcpay/bloc/proof/proof.dart';
import 'package:otcpay/component/ListItem.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/component/Wrapper.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/model/Profile.dart';
import 'package:otcpay/style/Color.dart';

import 'EditBankPage.dart';

class ManageBankPage extends StatefulWidget {
  @override
  _ManageBankPageState createState() => _ManageBankPageState();
}

class _ManageBankPageState extends State<ManageBankPage> {
  // ignore: close_sinks
  ProofBloc bloc;

  void _edit(BankProof data, int index) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (_) => EditBankPage(data: data, index: index)));
  }

  void _add() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (_) => EditBankPage()));
  }

  void _dismissAlert() {
    Navigator.pop(context);
  }

  void _delete(int index) {
    _dismissAlert();

    bloc.add(DeleteBank(
        userId: BlocProvider.of<AuthenticateBloc>(context).repo.userId,
        index: index,
        callback: _uploadCallback));
  }

  void _uploadCallback() {
    _showSnackbar(tr(LocaleKeys.common_deleteSuccess));
  }

  void _setDefaultSuccess() {
    _showSnackbar(tr(LocaleKeys.common_updateSuccess));
  }

  void _showSnackbar(String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void _setDefault(int index) {
    bloc.add(UpdateDefaultBank(
        userId: BlocProvider.of<AuthenticateBloc>(context).repo.userId,
        index: index,
        callback: _setDefaultSuccess));
  }

  void _showAlert(int index) {
    showDialog(
      context: context,
      builder: (BuildContext context) => Theme(
        data: ThemeData.dark(),
        child: CupertinoAlertDialog(
          title: NormalTitle(title: tr(LocaleKeys.common_confirmDelete)),
          actions: [
            CupertinoDialogAction(
                isDefaultAction: false,
                child: NormalTitle(title: tr(LocaleKeys.common_cancel)),
                onPressed: _dismissAlert),
            CupertinoDialogAction(
                isDefaultAction: false,
                child: NormalTitle(
                    title: tr(LocaleKeys.common_yes),
                    color: globalPrimaryColor),
                onPressed: () => _delete(index))
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();

    bloc = BlocProvider.of<ProofBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ProofBloc, ProofState>(
        listener: (BuildContext context, ProofState state) {
      if (state is UpdateError) _showSnackbar(state.message);
    }, builder: (BuildContext context, ProofState state) {
      return LoadingWrapper(
        proofState: state,
        isLoadingState: Updating,
        child: Scaffold(
            appBar: AppBar(
              centerTitle: true,
              title: Text(tr(LocaleKeys.bankPage_manageBankTitle)),
              elevation: 1,
              titleSpacing: 5,
            ),
            body: ListView(
                padding: EdgeInsets.only(left: 10, right: 10, top: 5),
                children: BlocProvider.of<ProofBloc>(context)
                    .repo
                    .banks
                    .asMap()
                    .map((i, e) => MapEntry(
                        i,
                        ManageItem(
                          title: NormalTitle(title: e.holderName, fontSize: 16),
                          subTitle: NormalTitle(
                              title: e.bankName + '    ' + e.bankCode,
                              color: subTitleColor,
                              fontSize: 16),
                          isDefault: i == bloc.repo.defaultBanks,
                          onEdit: () => _edit(e, i),
                          onDelete: () => _showAlert(i),
                          onSetDefault: () => _setDefault(i),
                        )))
                    .values
                    .toList()),
            floatingActionButton: IconButton(
                splashRadius: 40,
                iconSize: 60,
                icon: Image.asset('icons/add.png'),
                onPressed: _add)),
      );
    });
  }
}
