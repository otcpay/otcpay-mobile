import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/style/Color.dart';
import 'package:url_launcher/url_launcher.dart';

class HelpPage extends StatefulWidget {
  const HelpPage();

  @override
  _HelpPageState createState() => _HelpPageState();
}

class _HelpPageState extends State<HelpPage> {
  void _launchWhatsApp() async {
    const url =
        'https://wa.me/85264219076?text=Hi OTCPAY! 如有任何疑問或查詢, 請立即與我們的客戶服務聯絡 (服務時間為每天早上10時至下午6時)';
    String encoded = Uri.encodeFull(url);
    await canLaunch(encoded)
        ? await launch(encoded)
        : _showSnackbar(tr(LocaleKeys.hotPage_cannotLaunchBrowser));
  }

  void _showSnackbar(String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  void dispose() {
    try {
      // clear snackbar in best effort
      ScaffoldMessenger.of(context).clearSnackBars();
    } catch (e) {
      // do nothing
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(tr(LocaleKeys.helpPage_helpCenter)),
          elevation: 1,
        ),
        body: ListView(
          padding: EdgeInsets.only(left: 50, right: 50),
          children: [
            Image.asset('icons/Whatsapp.png'),
            Center(
                child: NormalTitle(
                    title: tr(LocaleKeys.helpPage_contactWhatsapp),
                    fontSize: 22)),
            Container(
              margin: EdgeInsets.only(top: 10, bottom: 40),
              child: Center(
                child: NormalTitle(
                    title: tr(LocaleKeys.helpPage_time), color: subTitleColor),
              ),
            ),
            Container(
              margin: EdgeInsets.only(right: 80, left: 80),
              child: TextButton(
                  onPressed: _launchWhatsApp,
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(upColor),
                  ),
                  child: Container(
                      margin: EdgeInsets.only(top: 10, bottom: 10),
                      child: BoldTitle(
                        title: tr(LocaleKeys.helpPage_chat),
                        fontSize: 16,
                      ))),
            )
          ],
        ));
  }
}
