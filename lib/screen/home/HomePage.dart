import 'dart:async';

import 'package:bottom_bar_page_transition/bottom_bar_page_transition.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:otcpay/bloc/auth/authenticate.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/component/Input.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/screen/home/hot/HotPage.dart';
import 'package:otcpay/screen/home/mine/MyPage.dart';
import 'package:otcpay/screen/home/otc/OtcPage.dart';
import 'package:otcpay/screen/home/payment/PaymentPage.dart';
import 'package:otcpay/style/Color.dart';
import 'package:uni_links/uni_links.dart';

import '../../../generated/locale_keys.g.dart';

import '../LoginPage.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  static const List<String> _iconNames = [
    'Hot',
    'Payment',
    'OTC',
    'MINE',
  ];

  String location = '';

  bool dirty = false;

  GeneralBloc generalBloc;

  StreamSubscription _sub;

  StreamSubscription<ConnectivityResult> subscription;

  ConnectivityResult connectivityResult;

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    if (result == ConnectivityResult.none) {
      final snackBar =
          SnackBar(content: Text(tr(LocaleKeys.homePage_noConnection)));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    } else if (connectivityResult != null) {
      final snackBar =
          SnackBar(content: Text(tr(LocaleKeys.homePage_connectionResume)));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }

    connectivityResult = result;
  }

  Future<void> _initUniLinks() async {
    _sub = linkStream.listen((String link) {
      String route = link.replaceAll('otcpay://otcpay', '');
      if (!(BlocProvider.of<AuthenticateBloc>(context).state
          is Authenticated)) {
        Navigator.of(context).pushNamed(route);
      }
    }, onError: (err) {});
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return null;
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return null;
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return null;
    }

    return await Geolocator.getCurrentPosition();
  }

  void _initConnectivityCheck() {
    subscription =
        Connectivity().onConnectivityChanged.listen(_updateConnectionStatus);

    Future.delayed(Duration(seconds: 10)).then((_) {
      if (generalBloc.otcRates.isEmpty ||
          generalBloc.defaultFiatRates.isEmpty ||
          generalBloc.cryptoRates.isEmpty) {
        final snackBar =
            SnackBar(content: Text(tr(LocaleKeys.homePage_noRespond)));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      }
    });
  }

  void _onTap(index) {
    if (index == 1) {
      if (!(BlocProvider.of<AuthenticateBloc>(context).state
          is Authenticated)) {
        generalBloc.add(NavigateTab(index: 0));
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (_) => LoginPage(isFirstPage: true)),
            (route) => false);
        return;
      }
    }
    generalBloc.add(NavigateTab(index: index));
  }

  void _hideMessage() {
    Navigator.of(context).pop();
  }

  void _verifyPage() {
    Navigator.of(context).pushReplacementNamed('/verify');
  }

  void _goToOrder(String message) {
    if (message == 'otc_created') {
      Navigator.of(context).pushReplacementNamed('/otc-order');
    } else if (message == 'payment_created') {
      Navigator.of(context).pushReplacementNamed('/payment-order');
    }
  }

  void _showMessage() {
    if (!dirty) {
      String message = ModalRoute.of(context).settings.arguments;

      if (message != null && message.isNotEmpty && message == 'success') {
        Future.delayed(const Duration(milliseconds: 1000), () {
          showDialog(
            context: context,
            barrierDismissible: false, // user must tap button!
            builder: (BuildContext context) => AlertDialog(
              contentPadding: EdgeInsets.only(left: 40, right: 40, bottom: 30),
              titlePadding: EdgeInsets.only(top: 40, bottom: 10),
              backgroundColor: inputBoxBackgroundColor,
              title: Text(tr(LocaleKeys.homePage_congratsSignupTitle),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white, fontSize: 22)),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(bottom: 5),
                      child: Text(tr(LocaleKeys.myPage_verifyTitle),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: globalPrimaryColor,
                              fontSize: 16,
                              fontWeight: FontWeight.bold)),
                    ),
                    SubTitle(
                      align: TextAlign.center,
                      color: subTitleColor,
                      fontSize: 12,
                      title: tr(LocaleKeys.homePage_congratsSignupSubTitle),
                    ),
                    Image.asset('icons/identity_verification.png'),
                    ConfirmButton(
                        callback: _verifyPage,
                        title: tr(LocaleKeys.myPage_verifyButton),
                        height: 50),
                    Container(
                        margin: EdgeInsets.only(left: 20, right: 20),
                        height: 50,
                        child: TextButton(
                            onPressed: _hideMessage,
                            child: Text(
                              tr(LocaleKeys.homePage_notNow),
                              style: TextStyle(
                                color: subTitleColor,
                                fontSize: 12.0,
                              ),
                            ))),
                  ],
                ),
              ),
            ),
          );
        });
      } else if (message != null &&
          message.isNotEmpty &&
          message.contains('created')) {
        Future.delayed(const Duration(milliseconds: 1000), () {
          showDialog(
            context: context,
            barrierDismissible: false, // user must tap button!
            builder: (BuildContext context) => AlertDialog(
              contentPadding: EdgeInsets.only(left: 40, right: 40, bottom: 30),
              titlePadding: EdgeInsets.only(top: 40, bottom: 10),
              backgroundColor: inputBoxBackgroundColor,
              title: Text(tr(LocaleKeys.homePage_congratsOrderTitle),
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white, fontSize: 22)),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(bottom: 5),
                      child: Text(tr(LocaleKeys.homePage_thankYouOrder),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: globalPrimaryColor,
                              fontSize: 16,
                              fontWeight: FontWeight.bold)),
                    ),
                    SubTitle(
                      align: TextAlign.center,
                      color: subTitleColor,
                      fontSize: 12,
                      title: tr(LocaleKeys.homePage_congratsOrderSubTitle),
                    ),
                    Image.asset('icons/order_confirm.png'),
                    ConfirmButton(
                        callback: () => _goToOrder(message),
                        title: tr(LocaleKeys.homePage_viewOrders),
                        height: 50),
                    Container(
                        margin: EdgeInsets.only(left: 20, right: 20),
                        height: 50,
                        child: TextButton(
                            onPressed: _hideMessage,
                            child: Text(
                              tr(LocaleKeys.homePage_notNow),
                              style: TextStyle(
                                color: subTitleColor,
                                fontSize: 12.0,
                              ),
                            ))),
                  ],
                ),
              ),
            ),
          );
        });
      }

      dirty = true;
    }
  }

  String getIconPath(index) {
    String postFix = generalBloc.tabIndex == index ? 'active' : 'inactive';
    String prefix = _iconNames[index];
    return 'icons/' + prefix + '_' + postFix + '.png';
  }

  @override
  void initState() {
    super.initState();

    _initUniLinks();
    _initConnectivityCheck();

    generalBloc = BlocProvider.of<GeneralBloc>(context);

    _determinePosition()
        .then((value) =>
            placemarkFromCoordinates(value.latitude, value.longitude))
        .then((values) {
      if (values.length > 0) {
        location = values[0].country;
      }
    });
  }

  @override
  void dispose() {
    _sub?.cancel();

    subscription.cancel();

    try {
      // clear snackbar in best effort
      ScaffoldMessenger.of(context).clearSnackBars();
    } catch (e) {
      // do nothing
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<String> _names = [
      tr(LocaleKeys.homePage_hot),
      tr(LocaleKeys.homePage_payment),
      tr(LocaleKeys.homePage_otc),
      tr(LocaleKeys.homePage_mine),
    ];

    _showMessage();
    return BlocBuilder<GeneralBloc, GeneralState>(builder: (context, state) {
      return Scaffold(
        body: BottomBarPageTransition(
          builder: (_, index) => [
            HotPage(),
            PaymentPage(location: location),
            OtcPage(),
            MyPage(),
          ][index],
          currentIndex: generalBloc.tabIndex,
          totalLength: _names.length,
          transitionType: TransitionType.slide,
          transitionDuration: const Duration(milliseconds: 500),
          transitionCurve: Curves.ease,
        ),
        bottomNavigationBar: Container(
            decoration: BoxDecoration(
                boxShadow: [BoxShadow(color: Colors.black54, blurRadius: 10)]),
            child: Theme(
              data: ThemeData(splashColor: Colors.transparent),
              child: BottomNavigationBar(
                  currentIndex: generalBloc.tabIndex,
                  onTap: _onTap,
                  backgroundColor: inputBoxBackgroundColor,
                  type: BottomNavigationBarType.fixed,
                  selectedItemColor: globalPrimaryColor,
                  unselectedItemColor: Colors.grey,
                  selectedFontSize: 12,
                  unselectedFontSize: 12,
                  items: List.generate(
                      _names.length,
                      (index) => BottomNavigationBarItem(
                          icon: Image.asset(getIconPath(index),
                              width: 34, height: 34),
                          label: _names[index]))),
            )),
      );
    });
  }
}
