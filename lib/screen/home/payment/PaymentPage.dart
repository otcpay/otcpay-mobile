import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/component/Indicator.dart';
import 'package:otcpay/component/Input.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/model/InvoicePayment.dart';
import 'package:otcpay/screen/home/payment/ComparisonResultPage.dart';
import 'package:otcpay/screen/home/payment/RecipientDetailPage.dart';
import 'package:otcpay/screen/home/payment/TransferRequestPage.dart';
import 'package:otcpay/screen/verify/VerifyPage.dart';
import 'package:otcpay/style/Color.dart';

class PaymentPage extends StatefulWidget {
  final String location;

  const PaymentPage({this.location});

  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  final PageController controller = PageController();

  GeneralBloc generalBloc;

  String rate = '';
  String receiveCurrency = '';
  String sendCurrency = '';

  void _cancel() {
    generalBloc.add(ProceedToNext(step: 0, payment: InvoicePayment()));

    controller.animateToPage(0,
        duration: Duration(milliseconds: 500), curve: Curves.ease);
  }

  void _navigateToComparison() {
    controller.animateToPage(1,
        duration: Duration(milliseconds: 500), curve: Curves.ease);
  }

  void _navigateToVerification() {
    controller.animateToPage(2,
        duration: Duration(milliseconds: 500), curve: Curves.ease);
  }

  void _navigateToRecipient() {
    controller.animateToPage(3,
        duration: Duration(milliseconds: 500), curve: Curves.ease);
  }

  void _dismissAlert() {
    Navigator.pop(context);
  }

  void _exit() {
    SystemNavigator.pop();
  }

  Future<bool> _alertBeforeLeave() async {
    showDialog(
      context: context,
      builder: (BuildContext context) => Theme(
        data: ThemeData.dark(),
        child: CupertinoAlertDialog(
          title: NormalTitle(title: tr(LocaleKeys.paymentPage_confirmLeave)),
          actions: [
            CupertinoDialogAction(
                isDefaultAction: false,
                child: NormalTitle(title: tr(LocaleKeys.common_cancel)),
                onPressed: _dismissAlert),
            CupertinoDialogAction(
                isDefaultAction: false,
                child: NormalTitle(
                    title: tr(LocaleKeys.common_confirm),
                    color: globalPrimaryColor),
                onPressed: _exit)
          ],
        ),
      ),
    );

    return false;
  }

  @override
  void initState() {
    super.initState();

    generalBloc = BlocProvider.of<GeneralBloc>(context);
    GeneralState state = generalBloc.state;
    if (state is NavigatedTabBar) {
      Map payload = state.payload;
      if (payload != null) {
        sendCurrency = payload['fromCurrency'];
        receiveCurrency = payload['toCurrency'];
        rate = payload['rate'];
      } else if (widget.location != null && widget.location != '') {
        String currency = generalBloc.mapping['mapping'][widget.location];
        if (currency != null && currency != '')
          sendCurrency = currency;
        else
          sendCurrency = 'HKD';
      }
    }
  }

  @override
  void dispose() {
    controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _alertBeforeLeave,
      child: Scaffold(
          body: Column(
        children: [
          Container(
            color: inputBoxBackgroundColor,
            padding: EdgeInsets.only(bottom: 20.0, top: 48.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(tr(LocaleKeys.paymentPage_paymentTitle),
                    style: TextStyle(color: globalPrimaryColor, fontSize: 16.0))
              ],
            ),
          ),
          BlocConsumer<GeneralBloc, GeneralState>(
              listener: (context, state) {},
              builder: (context, state) {
                return Container(
                  color: inputBoxBackgroundColor,
                  height: 90.0,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                          child: Container(
                        margin: EdgeInsets.only(top: 16.5),
                        height: 1,
                        color: globalPrimaryColor,
                      )),
                      StepIndicator(
                        icon: Icon(Icons.save),
                        title: tr(LocaleKeys.paymentPage_transferRequest),
                        isActive: generalBloc.paymentStep >= 0,
                      ),
                      Expanded(
                          child: Container(
                        margin: EdgeInsets.only(top: 16.5),
                        height: 1,
                        color: generalBloc.paymentStep > 0
                            ? globalPrimaryColor
                            : labelColor,
                      )),
                      StepIndicator(
                        icon: Image.asset('icons/Comparison_Results.png'),
                        isActive: generalBloc.paymentStep > 0,
                        title: tr(LocaleKeys.paymentPage_comparisonResults),
                        padding: EdgeInsets.fromLTRB(9, 7, 9, 7),
                      ),
                      Expanded(
                          child: Container(
                        margin: EdgeInsets.only(top: 16.5),
                        height: 1,
                        color: generalBloc.paymentStep > 1
                            ? globalPrimaryColor
                            : labelColor,
                      )),
                      StepIndicator(
                        icon: Image.asset('icons/verified_black.png',
                            width: 16, height: 17),
                        title: tr(LocaleKeys.paymentPage_verificationCheck),
                        isActive: generalBloc.paymentStep > 1,
                        padding: EdgeInsets.fromLTRB(10, 9, 9, 8),
                      ),
                      Expanded(
                          child: Container(
                        margin: EdgeInsets.only(top: 16.5),
                        height: 1,
                        color: generalBloc.paymentStep > 2
                            ? globalPrimaryColor
                            : labelColor,
                      )),
                      StepIndicator(
                        icon: Image.asset('icons/Recipent_Detials.png'),
                        title: tr(LocaleKeys.paymentPage_recipientDetails),
                        isActive: generalBloc.paymentStep > 2,
                        padding: EdgeInsets.fromLTRB(9, 9, 9, 9),
                      ),
                      Expanded(
                          child: Container(
                        margin: EdgeInsets.only(top: 16.5),
                        height: 1,
                        color: generalBloc.paymentStep > 3
                            ? globalPrimaryColor
                            : labelColor,
                      )),
                    ],
                  ),
                );
              }),
          Expanded(
              child: PageView(
            physics: NeverScrollableScrollPhysics(),
            controller: controller,
            children: [
              TransferRequestPage(
                  callback: _navigateToComparison,
                  cancel: _cancel,
                  rate: rate,
                  receiveCurrency: receiveCurrency,
                  sendCurrency: sendCurrency),
              ComparisonResultPage(
                  callback: _navigateToVerification, cancel: _cancel),
              VerifyPageContent(
                overrideState: true,
                showOptions: false,
                buttonTitle: tr(LocaleKeys.common_next),
                callback: _navigateToRecipient,
                topDescription: [
                  InputTitle(title: tr(LocaleKeys.verifyPage_proofIdentity))
                ],
              ),
              RecipientDetailPage(
                cancel: _cancel,
              ),
            ],
          ))
        ],
      )),
    );
  }
}
