import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/bloc/general/general_event.dart';
import 'package:otcpay/bloc/proof/proof.dart';
import 'package:otcpay/component/Container.dart';
import 'package:otcpay/component/Input.dart';
import 'package:otcpay/component/ListItem.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/model/InvoicePayment.dart';
import 'package:otcpay/model/Profile.dart';
import 'package:otcpay/style/Color.dart';

List<Widget> recipientDetailContent(
    {@required TextEditingController nameController,
    @required TextEditingController accountNumController,
    @required TextEditingController bankNameController,
    @required TextEditingController bankAddressController}) {
  return [
    InputTitle(title: tr(LocaleKeys.paymentPage_recipientNameInputTitle)),
    InputBox(
      margin: EdgeInsets.only(left: 15, right: 15),
      controller: nameController,
      hintText: tr(LocaleKeys.paymentPage_recipientNameInputHint),
    ),
    InputTitle(title: tr(LocaleKeys.paymentPage_recipientBankDetailInputTitle)),
    InputBox(
      margin: EdgeInsets.only(left: 15, right: 15),
      controller: accountNumController,
      hintText: tr(LocaleKeys.loginPage_accountInputHint),
      keyboardType: TextInputType.numberWithOptions(decimal: true),
    ),
    InputBox(
      margin: EdgeInsets.only(left: 15, right: 15, top: 10),
      controller: bankNameController,
      hintText: tr(LocaleKeys.paymentPage_recipientBankNameInputHint),
    ),
    InputBox(
      maxLine: 4,
      height: 100,
      overridePadding: true,
      keyboardType: TextInputType.multiline,
      margin: EdgeInsets.only(left: 15, right: 15, top: 10),
      controller: bankAddressController,
      hintText: tr(LocaleKeys.paymentPage_recipientBankAddressInputHint),
    ),
  ];
}

enum ContactMethod { whatsapp, telegram, wechat }

class RecipientDetailPage extends StatefulWidget {
  final void Function() cancel;

  const RecipientDetailPage({@required this.cancel});

  @override
  _RecipientDetailPageState createState() => _RecipientDetailPageState();
}

class _RecipientDetailPageState extends State<RecipientDetailPage> {
  final GlobalKey<ConfirmButtonState> key = GlobalKey();

  final TextEditingController nameController = TextEditingController();
  final TextEditingController accountNumController = TextEditingController();
  final TextEditingController bankNameController = TextEditingController();
  final TextEditingController bankAddressController = TextEditingController();

  final picker = ImagePicker();

  int defaultBank = -1;

  ContactMethod _contactChoice = ContactMethod.whatsapp;

  String path;

  BankProof bank;

  GeneralBloc bloc;

  void _confirm() {
    FocusScope.of(context).unfocus();

    bloc.add(ProceedToNext(
        step: 4,
        payment: InvoicePayment(
          bankImage: path,
          receiveName: nameController.text,
          receiveBank: bankNameController.text,
          receiveBankNumber: accountNumController.text,
          receiveBankAddress: bankAddressController.text,
          contact: _getContact(),
          contactMethod: EnumToString.convertToString(_contactChoice),
        )));

    Navigator.of(context).pushNamed('/settlement');
  }

  void _updateButtonState() {
    if (key.currentState != null) {
      key.currentState.setState(() {});
    }
  }

  void _toggle(ContactMethod newContactChoice) {
    setState(() {
      _contactChoice = newContactChoice;
    });
  }

  void _showSnackbar(String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  String _getContact() {
    String contact = '';
    if (_contactChoice == ContactMethod.whatsapp)
      contact = BlocProvider.of<ProofBloc>(context).repo.whatsappId;

    if (_contactChoice == ContactMethod.wechat)
      contact = BlocProvider.of<ProofBloc>(context).repo.wechatId;

    if (_contactChoice == ContactMethod.telegram)
      contact = BlocProvider.of<ProofBloc>(context).repo.telegramId;

    if (contact == '') {
      contact = BlocProvider.of<ProofBloc>(context).repo.account;
    }

    return contact;
  }

  bool _validate() {
    return nameController.text.length > 0 &&
        accountNumController.text.length > 0 &&
        bankNameController.text.length > 0;
  }

  Future _getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      final f = File(pickedFile.path);
      int sizeInBytes = f.lengthSync();
      double sizeInMb = sizeInBytes / (1024 * 1024);
      if (sizeInMb > 5) {
        _showSnackbar(tr(LocaleKeys.common_fileTooLarge));
        return;
      }

      setState(() {
        path = pickedFile.path;
      });
    } else {
      print('No image selected.');
    }
  }

  List<Widget> _renderContent() {
    List<Widget> widgets = [];

    if (defaultBank == -1) {
      widgets.addAll([
        ...recipientDetailContent(
            nameController: nameController,
            accountNumController: accountNumController,
            bankNameController: bankNameController,
            bankAddressController: bankAddressController),
        Container(
          margin: EdgeInsets.only(top: 10),
          child: Row(
            children: [
              InputTitle(
                  title:
                      tr(LocaleKeys.paymentPage_recipientBankDetailInputTitle)),
              Container(
                color: expireColor,
                margin: EdgeInsets.only(left: 15),
                padding: EdgeInsets.only(left: 5, right: 5, top: 2, bottom: 2),
                child: NormalTitle(
                  fontSize: 10,
                  title: tr(LocaleKeys.common_optional),
                ),
              )
            ],
          ),
        ),
        DotLineContainer(
            showMask: path != null && path.isNotEmpty,
            margin: EdgeInsets.symmetric(horizontal: 20),
            image: Image.asset('icons/bank_proof.png'),
            height: 90,
            title: tr(LocaleKeys.bankPage_uploadButtonTitle),
            subTitle: tr(LocaleKeys.bankPage_uploadButtonSubTitle),
            callback: _getImage),
      ]);
    } else {
      widgets.add(Container(
        margin: EdgeInsets.only(right: 15, left: 15, top: 15),
        child: ManageItem(
          title: NormalTitle(title: bank.holderName, fontSize: 16),
          subTitle: NormalTitle(
              title: bank.bankName + '    ' + bank.bankCode,
              color: subTitleColor,
              fontSize: 16),
          showRedirect: true,
        ),
      ));
    }

    return widgets;
  }

  Widget _radioButton(ContactMethod choice) {
    return ContactRadioButton(
      contactValue: choice,
      contactGroupValue: _contactChoice,
      contactCallback: _toggle,
    );
  }

  @override
  void initState() {
    super.initState();

    defaultBank = BlocProvider.of<ProofBloc>(context).repo.defaultBanks;
    if (defaultBank != -1) {
      bank = BlocProvider.of<ProofBloc>(context).repo.banks[defaultBank];
      nameController.text = bank.holderName;
      accountNumController.text = bank.bankCode;
      bankNameController.text = bank.bankName;
      bankAddressController.text = bank.detail;
      path = bank.image;
    }

    nameController.addListener(_updateButtonState);
    accountNumController.addListener(_updateButtonState);
    bankNameController.addListener(_updateButtonState);

    bloc = BlocProvider.of<GeneralBloc>(context);
  }

  @override
  void dispose() {
    nameController.dispose();
    accountNumController.dispose();
    bankNameController.dispose();
    bankAddressController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.zero,
      children: [
        ..._renderContent(),
        Container(
            margin: EdgeInsets.only(left: 20, right: 20, bottom: 5, top: 20),
            child: Divider(color: dividerColor, thickness: 2)),
        InputTitle(title: tr(LocaleKeys.paymentPage_receivedAmount)),
        InputBox(
            margin: EdgeInsets.only(left: 15, right: 15),
            controller: null,
            readOnly: true,
            leadingIcon:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              Container(
                  margin: EdgeInsets.only(left: 20),
                  child: NormalTitle(
                      title: bloc.paymentDetail.receivedCurrency +
                          bloc.paymentDetail.receivedAmount)),
            ])),
        Container(
            margin: EdgeInsets.only(left: 20, right: 20, bottom: 5, top: 20),
            child: Divider(color: dividerColor, thickness: 2)),
        InputTitle(title: tr(LocaleKeys.paymentPage_contactMethod)),
        Row(
          children: [
            RadioContainer(
              width: 130,
              leading: _radioButton(ContactMethod.whatsapp),
              title: Align(
                child: NormalTitle(
                    title: 'WhatsApp',
                    fontSize: 14,
                    color: _contactChoice == ContactMethod.whatsapp
                        ? globalPrimaryColor
                        : Colors.white),
                alignment: Alignment(-50, 0),
              ),
            ),
            RadioContainer(
              width: 130,
              leading: _radioButton(ContactMethod.telegram),
              title: Align(
                child: NormalTitle(
                    title: 'Telegram',
                    fontSize: 14,
                    color: _contactChoice == ContactMethod.telegram
                        ? globalPrimaryColor
                        : Colors.white),
                alignment: Alignment(-8, 0),
              ),
            ),
            RadioContainer(
              width: 120,
              leading: _radioButton(ContactMethod.wechat),
              title: Align(
                child: NormalTitle(
                    title: tr(LocaleKeys.paymentPage_wechat),
                    fontSize: 14,
                    color: _contactChoice == ContactMethod.wechat
                        ? globalPrimaryColor
                        : Colors.white),
                alignment: Alignment(-7, 0),
              ),
            ),
          ],
        ),
        Container(
          margin: EdgeInsets.only(left: 20, right: 20),
          child: SubTitle(
            align: TextAlign.left,
            color: subTitleColor,
            fontSize: 12,
            title: tr(LocaleKeys.paymentPage_contactDetail),
          ),
        ),
        ConfirmButton(
            margin: EdgeInsets.only(
                top: 50.0, left: 20.0, right: 20.0, bottom: 5.0),
            key: key,
            validator: _validate,
            title: tr(LocaleKeys.paymentPage_finalToConfirm),
            height: 50,
            callback: _confirm),
        ConfirmButton(
            color: labelColor,
            title: tr(LocaleKeys.paymentPage_cancelCap),
            height: 50,
            margin: EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 5),
            callback: widget.cancel),
      ],
    );
  }
}
