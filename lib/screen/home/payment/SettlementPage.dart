import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/auth/authenticate_bloc.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/bloc/proof/proof_bloc.dart';
import 'package:otcpay/component/Container.dart';
import 'package:otcpay/component/Indicator.dart';
import 'package:otcpay/component/Input.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/component/Wrapper.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/model/rate/OtcRate.dart';
import 'package:otcpay/style/Color.dart';
import 'package:qr_flutter/qr_flutter.dart';

enum CurrencyChoice { FIAT, USDT }
enum PaymentMethod { ERC20, TRC20, Omni }

class SettlementPage extends StatefulWidget {
  @override
  _SettlementPageState createState() => _SettlementPageState();
}

class _SettlementPageState extends State<SettlementPage> {
  final TextEditingController addressController = TextEditingController();

  CurrencyChoice _currencyChoice = CurrencyChoice.FIAT;
  PaymentMethod _paymentMethod = PaymentMethod.ERC20;

  Map<PaymentMethod, String> addressMap = {
    PaymentMethod.ERC20: '0x0b05E089ce34E060C2D330f791bDd7b58a266673',
    PaymentMethod.TRC20: 'TWCvTkVkZk4vUCN3W3jc7ajE4BLJJPZGGg',
    PaymentMethod.Omni: '3GV4q7mz7Pa5tvfLCnHBnMo4apQZQuDJr9',
  };

  double usdtRate = 1;

  GeneralBloc bloc;

  void _copy() {
    Clipboard.setData(ClipboardData(text: addressController.text));

    _showSnackbar(tr(LocaleKeys.common_coped));
  }

  void _toggle(CurrencyChoice newCurrencyChoice) {
    setState(() {
      _currencyChoice = newCurrencyChoice;
    });
  }

  void _select(PaymentMethod newPaymentChoice) {
    setState(() {
      _paymentMethod = newPaymentChoice;
      addressController.text = addressMap[newPaymentChoice];
    });
  }

  void showQRCode() {
    showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        contentPadding: EdgeInsets.only(left: 40, right: 40, bottom: 30),
        backgroundColor: Colors.transparent,
        content: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Container(
                width: 250,
                height: 250,
                color: Colors.white,
                child: QrImage(
                  data: addressController.text,
                  version: QrVersions.auto,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _placeOrder() {
    bloc.add(SubmitPayment(
        userId: BlocProvider.of<AuthenticateBloc>(context).repo.userId,
        name: BlocProvider.of<ProofBloc>(context).repo.username,
        settlementMethod:
            _currencyChoice == CurrencyChoice.FIAT ? 'Cash' : 'USDT',
        referenceRate: usdtRate.toStringAsFixed(5),
        amountNeeded: _usdtEquivalent().toStringAsFixed(5)));
  }

  void _success() {
    Navigator.pushNamedAndRemoveUntil(context, '/home', (route) => false,
        arguments: 'payment_created');
  }

  void _showSnackbar(String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  double _usdtEquivalent() {
    double amountNeeded = double.parse(bloc.paymentDetail.receivedAmount);
    double rate = double.parse(bloc.paymentDetail.referenceRate);

    try {
      OtcRate rate = bloc.otcRates
          .where((element) =>
              element.sourceCurrency == bloc.paymentDetail.sendCurrency)
          .first;
      usdtRate = rate.rate;
    } catch (e) {
      print('failed to find equivalent usdt rate');
    }

    return amountNeeded / rate / usdtRate;
  }

  String _totalAmount() {
    try {
      double amountNeeded = double.parse(bloc.paymentDetail.receivedAmount);
      double rate = double.parse(bloc.paymentDetail.referenceRate);

      return bloc.paymentDetail.sendCurrency +
          ' ' +
          (amountNeeded / rate).toStringAsFixed(3);
    } catch (e) {
      return 'HKD 0';
    }
  }

  String _amountSaved() {
    try {
      return bloc.paymentDetail.sendCurrency +
          ' ' +
          bloc.paymentDetail.amountSaved;
    } catch (e) {
      return 'HKD 0';
    }
  }

  Widget _radioButton(CurrencyChoice choice) {
    return CurrencyRadioButton(
      currencyValue: choice,
      currencyGroupValue: _currencyChoice,
      currencyCallback: _toggle,
    );
  }

  Widget _paymentButton(PaymentMethod payment) {
    return PaymentRadioButton(
      paymentValue: payment,
      paymentGroupValue: _paymentMethod,
      paymentCallback: _select,
    );
  }

  Widget _footer() {
    if (_currencyChoice == CurrencyChoice.FIAT) {
      return Container(
        margin: EdgeInsets.only(left: 18, right: 15),
        child: SubTitle(
          color: subTitleColor,
          title: tr(LocaleKeys.paymentPage_contacDetailWithBreakLink),
        ),
      );
    }

    return Column(
      children: [
        InputTitle(title: tr(LocaleKeys.paymentPage_paymentMethod)),
        Row(
          children: [
            RadioContainer(
              leading: _paymentButton(PaymentMethod.ERC20),
              title: Align(
                child: NormalTitle(
                    title: 'ERC20',
                    fontSize: 14,
                    color: _currencyChoice == CurrencyChoice.FIAT
                        ? globalPrimaryColor
                        : Colors.white),
                alignment: Alignment(-15, 0),
              ),
            ),
            RadioContainer(
              leading: _paymentButton(PaymentMethod.TRC20),
              title: Align(
                child: NormalTitle(
                    title: 'TRC20',
                    fontSize: 14,
                    color: _currencyChoice == CurrencyChoice.FIAT
                        ? globalPrimaryColor
                        : Colors.white),
                alignment: Alignment(-15, 0),
              ),
            ),
            RadioContainer(
              leading: _paymentButton(PaymentMethod.Omni),
              title: Align(
                child: NormalTitle(
                    title: 'Omni',
                    fontSize: 14,
                    color: _currencyChoice == CurrencyChoice.FIAT
                        ? globalPrimaryColor
                        : Colors.white),
                alignment: Alignment(-5, 0),
              ),
            ),
          ],
        ),
        Container(
          margin: EdgeInsets.only(left: 15, right: 15),
          child: Row(
            children: [
              Expanded(
                  flex: 4,
                  child: InputBox(
                      controller: addressController,
                      fontSize: 11,
                      readOnly: true,
                      margin: EdgeInsets.only(right: 10))),
              TextButton(
                style: TextButton.styleFrom(
                    padding: EdgeInsets.zero,
                    minimumSize: Size(48, 48),
                    backgroundColor: globalPrimaryColor),
                onPressed: _copy,
                child: NormalTitle(title: tr(LocaleKeys.paymentPage_copy)),
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 130, right: 130, top: 20),
          child: ConfirmButton(
            callback: showQRCode,
            title: tr(LocaleKeys.paymentPage_showQrCode),
            height: 38,
            fontSize: 12,
            color: labelColor,
          ),
        )
      ],
    );
  }

  @override
  void initState() {
    super.initState();

    addressController.text = addressMap[PaymentMethod.ERC20];

    bloc = BlocProvider.of<GeneralBloc>(context);
  }

  @override
  void dispose() {
    addressController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(tr(LocaleKeys.otcPage_newPost)),
          centerTitle: true,
          elevation: 1.0,
        ),
        body:
            BlocConsumer<GeneralBloc, GeneralState>(listener: (context, state) {
          if (state is PaymentSuccess) _success();
          if (state is Error) _showSnackbar(state.message);
        }, builder: (context, state) {
          return LoadingWrapper(
            generalState: state,
            isLoadingState: SubmittingPayment,
            child: ListView(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 30, bottom: 5, left: 20),
                  child: BoldTitle(
                      title: tr(LocaleKeys.paymentPage_settlementCurrency),
                      fontSize: 24),
                ),
                Container(
                  margin: EdgeInsets.only(left: 20),
                  child: SubTitle(
                    title: tr(LocaleKeys.paymentPage_settlementDescription),
                    color: subTitleColor,
                    fontSize: 12,
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 15, left: 20),
                  child: BoldTitle(
                      title: tr(LocaleKeys.paymentPage_chooseSettlement),
                      fontSize: 14),
                ),
                Row(
                  children: [
                    RadioContainer(
                      leading: _radioButton(CurrencyChoice.FIAT),
                      title: Align(
                        child: NormalTitle(
                            title: 'FIAT',
                            fontSize: 14,
                            color: _currencyChoice == CurrencyChoice.FIAT
                                ? globalPrimaryColor
                                : Colors.white),
                        alignment: Alignment(-4, 0),
                      ),
                    ),
                    RadioContainer(
                        width: 130,
                        leading: _radioButton(CurrencyChoice.USDT),
                        title: Align(
                          child: NormalTitle(
                              title: 'USDT',
                              fontSize: 14,
                              color: _currencyChoice == CurrencyChoice.USDT
                                  ? globalPrimaryColor
                                  : Colors.white),
                          alignment: Alignment(-2.5, 0),
                        ))
                  ],
                ),
                SettlementIndicator(
                    showLabel: false,
                    showSettlementDate: false,
                    showAmountSaved: _currencyChoice == CurrencyChoice.FIAT,
                    title: _currencyChoice == CurrencyChoice.FIAT
                        ? tr(LocaleKeys.other_totalAmountNeeded)
                        : tr(LocaleKeys.common_quantity),
                    totalAmount: _currencyChoice == CurrencyChoice.FIAT
                        ? _totalAmount()
                        : _usdtEquivalent().toStringAsFixed(5) + ' USDT',
                    amountSaved: _amountSaved()),
                _footer(),
                ConfirmButton(
                    margin: EdgeInsets.only(
                        top: 50.0, left: 20.0, right: 20.0, bottom: 20.0),
                    title: tr(LocaleKeys.paymentPage_placeOrder),
                    height: 50,
                    callback: _placeOrder),
              ],
            ),
          );
        }));
  }
}
