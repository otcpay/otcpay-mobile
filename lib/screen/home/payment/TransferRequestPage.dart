import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/component/Dropdown.dart';
import 'package:otcpay/component/Input.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/model/InvoicePayment.dart';
import 'package:otcpay/style/Color.dart';

class TransferRequestPage extends StatefulWidget {
  final void Function() callback;
  final void Function() cancel;

  final String rate;
  final String receiveCurrency;
  final String sendCurrency;

  const TransferRequestPage(
      {@required this.callback,
      @required this.cancel,
      @required this.rate,
      @required this.receiveCurrency,
      @required this.sendCurrency});

  @override
  _TransferRequestPageState createState() => _TransferRequestPageState();
}

class _TransferRequestPageState extends State<TransferRequestPage> {
  final GlobalKey<ConfirmButtonState> buttonKey = GlobalKey();
  final GlobalKey<DropDownFieldState> destinationKey = GlobalKey();
  final GlobalKey<DropDownFieldState> fromKey = GlobalKey();
  final TextEditingController controller = TextEditingController();

  Map mapping;

  GeneralBloc bloc;

  String rate = '';
  String destination = '';
  String sendFrom = '';
  List<String> countries = ['Hong Kong', 'China', 'Taiwan', 'Macau', 'USA'];

  String receiveCurrency = '';
  String sendCurrency = '';
  List<String> destinationCurrencies = [];
  List<String> sendFromCurrencies = [];

  void _onDestinationChange(dynamic newValue) {
    setState(() {
      destination = newValue;

      receiveCurrency = mapping['mapping'][destination];
      destinationCurrencies = [receiveCurrency];
      if (receiveCurrency != 'USD') destinationCurrencies.add('USD');
      destinationKey.currentState.effectiveController.text = receiveCurrency;
    });
  }

  void _onSendFromChange(dynamic newValue) {
    setState(() {
      sendFrom = newValue;

      sendCurrency = mapping['mapping'][sendFrom];
      sendFromCurrencies = [sendCurrency];
      if (sendCurrency != 'USD') sendFromCurrencies.add('USD');
      fromKey.currentState.effectiveController.text = sendCurrency;
    });
  }

  void _onSendCurrencyChange(dynamic newValue) {
    setState(() {
      sendCurrency = newValue;
    });
  }

  void _onReceiveCurrencyChange(dynamic newValue) {
    setState(() {
      receiveCurrency = newValue;
    });
  }

  void _proceed() {
    FocusScope.of(context).unfocus();

    if (_transactionSupported()) {
      bloc.add(ProceedToNext(
          step: 1,
          payment: InvoicePayment(
            sendCurrency: sendCurrency,
            sendFrom: sendFrom,
            sendTo: destination,
            receivedCurrency: receiveCurrency,
            receivedAmount: controller.text,
            referenceRate:
                rate == null || rate.isEmpty ? getDefaultRate() : rate,
            type: 'B2C',
          )));

      bloc.add(GetComparisonResult(
          fromCurrency: sendCurrency, toCurrency: receiveCurrency));

      widget.callback();
    } else {
      final snackBar =
          SnackBar(content: Text(tr(LocaleKeys.paymentPage_pairNotSupported)));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  String getDefaultRate() {
    try {
      return bloc.defaultFiatRates
          .where((e) =>
              e.toCurrency == receiveCurrency && e.fromCurrency == sendCurrency)
          .first
          .otcBoardRate
          .toStringAsFixed(8);
    } catch (e) {
      return '1';
    }
  }

  bool _transactionSupported() {
    List validRate = bloc.defaultFiatRates
        .where((e) =>
            e.toCurrency == receiveCurrency && e.fromCurrency == sendCurrency)
        .toList();
    return validRate != null && validRate.isNotEmpty;
  }

  bool _validate() {
    return destination != null &&
        destination.length > 0 &&
        sendFrom != null &&
        sendFrom.length > 0 &&
        controller.text != null &&
        controller.text.length > 0 &&
        sendCurrency != null &&
        sendCurrency.length > 0 &&
        receiveCurrency != null &&
        receiveCurrency.length > 0;
  }

  String _getDefaultValue(String value) {
    if (value == null || value == 'null') return '';

    return value;
  }

  @override
  void initState() {
    super.initState();

    controller.addListener(() {
      buttonKey.currentState.setState(() {});
    });

    bloc = BlocProvider.of<GeneralBloc>(context);
    mapping = bloc.mapping;

    sendCurrency = widget.sendCurrency;
    receiveCurrency = widget.receiveCurrency;
    rate = widget.rate;

    sendFrom = mapping['reverseMapping'][sendCurrency] ?? '';
    destination = mapping['reverseMapping'][receiveCurrency] ?? '';
    countries = new Map<String, dynamic>.from(mapping['mapping']).keys.toList();

    if (mapping['mapping'][destination] != null &&
        mapping['mapping'][destination] != '') {
      destinationCurrencies.add(mapping['mapping'][destination]);
    }

    if (!sendFromCurrencies.contains(mapping['mapping'][sendFrom])) {
      sendFromCurrencies.add(mapping['mapping'][sendFrom]);
    }

    if (sendCurrency != 'USD' && !sendFromCurrencies.contains('USD')) {
      sendFromCurrencies.add('USD');
    }

    if (receiveCurrency != 'USD' && !destinationCurrencies.contains('USD')) {
      destinationCurrencies.add('USD');
    }
  }

  @override
  void dispose() {
    controller.dispose();

    try {
      // clear snackbar in best effort
      ScaffoldMessenger.of(context).clearSnackBars();
    } catch (e) {
      // do nothing
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          InputTitle(title: tr(LocaleKeys.paymentPage_destination)),
          Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            child: DropDownField(
                showIcon: true,
                value: _getDefaultValue(destination),
                textStyle: TextStyle(color: Colors.white, fontSize: 14),
                required: false,
                hintText: tr(LocaleKeys.profilePage_regionHint),
                hintStyle: TextStyle(color: subTitleColor, fontSize: 14.0),
                items: countries,
                onValueChanged: _onDestinationChange),
          ),
          InputTitle(title: tr(LocaleKeys.paymentPage_receivedCurrency)),
          Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            child: DropDownField(
                showIcon: true,
                overrideLabelColor: false,
                key: destinationKey,
                value: receiveCurrency,
                textStyle: TextStyle(color: Colors.white, fontSize: 14),
                required: false,
                hintText: tr(LocaleKeys.paymentPage_receivedCurrency),
                hintStyle: TextStyle(color: subTitleColor, fontSize: 14.0),
                items: destinationCurrencies,
                onValueChanged: _onReceiveCurrencyChange),
          ),
          InputTitle(title: tr(LocaleKeys.paymentPage_receivedAmount)),
          InputBox(
              controller: controller,
              hintText: tr(LocaleKeys.paymentPage_receivedAmount),
              keyboardType: TextInputType.numberWithOptions(decimal: true),
              leadingIcon: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                        margin: EdgeInsets.only(left: 20),
                        child: NormalTitle(title: '\$')),
                  ]),
              margin: EdgeInsets.only(left: 20.0, right: 20.0)),
          Container(
              margin: EdgeInsets.only(left: 20, right: 20, bottom: 5, top: 20),
              child: Divider(color: dividerColor, thickness: 2)),
          InputTitle(title: tr(LocaleKeys.paymentPage_sendFrom)),
          Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            child: DropDownField(
                showIcon: true,
                value: _getDefaultValue(sendFrom),
                textStyle: TextStyle(color: Colors.white, fontSize: 14),
                required: false,
                hintText: tr(LocaleKeys.paymentPage_sendFrom),
                hintStyle: TextStyle(color: subTitleColor, fontSize: 14.0),
                items: countries,
                onValueChanged: _onSendFromChange),
          ),
          InputTitle(title: tr(LocaleKeys.paymentPage_sendCurrency)),
          Container(
            margin: EdgeInsets.only(left: 20, right: 20),
            child: DropDownField(
                showIcon: true,
                overrideLabelColor: false,
                key: fromKey,
                value: sendCurrency,
                textStyle: TextStyle(color: Colors.white, fontSize: 14),
                required: false,
                hintText: tr(LocaleKeys.paymentPage_sendCurrency),
                hintStyle: TextStyle(color: subTitleColor, fontSize: 14.0),
                items: sendFromCurrencies,
                onValueChanged: _onSendCurrencyChange),
          ),
          ConfirmButton(
              margin: EdgeInsets.only(
                  top: 40.0, left: 20.0, right: 20.0, bottom: 20.0),
              key: buttonKey,
              validator: _validate,
              title: tr(LocaleKeys.paymentPage_goCompare),
              height: 50,
              callback: _proceed)
        ],
      ),
    );
  }
}
