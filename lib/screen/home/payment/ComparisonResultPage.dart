import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/component/Indicator.dart';
import 'package:otcpay/component/Input.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/component/Wrapper.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/model/ComparisonResult.dart';
import 'package:otcpay/model/InvoicePayment.dart';
import 'package:otcpay/model/rate/OtcRate.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/util/StringUtil.dart';
import 'package:otcpay/util/TimeUtil.dart';

class ComparisonResultPage extends StatefulWidget {
  final void Function() callback;
  final void Function() cancel;

  const ComparisonResultPage({@required this.callback, @required this.cancel});

  @override
  _ComparisonResultPageState createState() => _ComparisonResultPageState();
}

class _ComparisonResultPageState extends State<ComparisonResultPage> {
  bool expanded = false;
  int displayRow = 0;

  GeneralBloc bloc;

  void _proceed() {
    bloc.add(ProceedToNext(
        step: 2,
        payment: InvoicePayment(
            amountSaved: _saveAmount(), amountNeeded: _totalAmount())));

    widget.callback();
  }

  toggle() {
    setState(() {
      expanded = !expanded;
      displayRow = expanded
          ? bloc.result.rates.length
          : bloc.result.rates.where((e) => e.order < 7).toList().length;
    });
  }

  String _rate() {
    return bloc.paymentDetail.referenceRate;
  }

  double _totalAmountInDouble() {
    double amountNeeded = double.parse(bloc.paymentDetail.receivedAmount);
    double rate = double.parse(bloc.paymentDetail.referenceRate);
    return amountNeeded / rate;
  }

  String _totalAmount() {
    return _totalAmountInDouble().toStringAsFixed(3);
  }

  bool _strangeTTRate() {
    return (bloc.paymentDetail.sendCurrency == 'CNY' &&
            bloc.paymentDetail.receivedCurrency == 'HKD') ||
        (bloc.paymentDetail.sendCurrency == 'HKD' &&
            bloc.paymentDetail.receivedCurrency == 'CNY');
  }

  double _comparisonTotalAmount(Rates rate) {
    double amountNeeded = double.parse(bloc.paymentDetail.receivedAmount);

    if (_strangeTTRate() &&
        rate.name != 'Western Union' &&
        rate.name != 'TransferWise' &&
        rate.name != 'XE' &&
        rate.name != 'X-Rates') {
      return amountNeeded * rate.rate;
    }

    return amountNeeded / rate.rate;
  }

  String _saveAmount() {
    if (bloc.result.rates.length > 0) {
      try {
        return _getExternalRate();
      } catch (e) {
        print('No Comparison result found');
      }
    }

    return '0';
  }

  String _getExternalRate() {
    Rates hsbc = bloc.result.rates.first;
    double saved = _comparisonTotalAmount(hsbc) - _totalAmountInDouble();
    if (saved > 0) return saved.toStringAsFixed(3);

    return '0';
  }

  String _save() {
    return bloc.paymentDetail.sendCurrency + ' ' + _saveAmount();
  }

  double _height() {
    int defaultDisplayListSize =
        bloc.result.rates.where((e) => e.order < 7).toList().length;
    if (bloc.result.rates.length > defaultDisplayListSize) {
      int finalRowNum = expanded ? displayRow : defaultDisplayListSize;
      return (finalRowNum * 38 + 72).toDouble();
    }

    return (bloc.result.rates.length * 38 + 42).toDouble();
  }

  double _usdtEquivalent() {
    double usdtRate = 1;
    try {
      OtcRate rate = bloc.otcRates
          .where((element) => element.sourceCurrency == 'HKD')
          .first;
      usdtRate = rate.otcBoardRate;
    } catch (e) {
      print('failed to find equivalent usdt rate');
    }

    if (bloc.paymentDetail.sendCurrency == 'HKD') {
      return _totalAmountInDouble() / usdtRate;
    }

    return double.parse(bloc.paymentDetail.receivedAmount) / usdtRate;
  }

  List<Widget> _comparisonRow() {
    if (bloc.result.rates.length > 0) {
      if (displayRow == 0) {
        displayRow =
            bloc.result.rates.where((e) => e.order < 7).toList().length;
      }
      return bloc.result.rates
          .sublist(0, displayRow)
          .map((e) => Container(
                margin:
                    EdgeInsets.only(left: 15, top: 10, bottom: 12, right: 15),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                          flex: 6,
                          child: Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(right: 4),
                                child: Image.asset(
                                    'icons/' + getIconName(e.name + '.png'),
                                    width: 15,
                                    height: 15),
                              ),
                              NormalTitle(title: e.name),
                            ],
                          )),
                      Expanded(
                          flex: 6,
                          child: NormalTitle(
                              title: bloc.paymentDetail.sendCurrency +
                                  ' ' +
                                  _comparisonTotalAmount(e)
                                      .toStringAsFixed(3))),
                      Expanded(
                          flex: 3,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              NormalTitle(title: 'T+1'),
                            ],
                          ))
                    ]),
              ))
          .toList();
    }

    return [];
  }

  @override
  void initState() {
    super.initState();

    bloc = BlocProvider.of<GeneralBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GeneralBloc, GeneralState>(builder: (context, state) {
      return ListView(
        padding: EdgeInsets.zero,
        children: [
          Container(
            height: 120,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(top: 20, left: 20),
                  child: Row(
                    children: [
                      Container(
                        width: 8,
                        height: 8,
                        margin: EdgeInsets.only(right: 15),
                        decoration: BoxDecoration(
                            border: Border.all(color: globalPrimaryColor),
                            borderRadius: BorderRadius.circular(8)),
                      ),
                      SubTitle(
                          title: tr(LocaleKeys.paymentPage_bankFee) +
                              '0 ' +
                              bloc.paymentDetail.sendCurrency,
                          fontSize: 12),
                      Container(
                        padding: EdgeInsets.fromLTRB(10, 2, 10, 2),
                        margin: EdgeInsets.only(left: 5),
                        decoration: BoxDecoration(
                          color: globalSecondaryColor,
                          borderRadius: BorderRadius.circular(4),
                        ),
                        child: BoldTitle(
                            title: tr(LocaleKeys.paymentPage_free),
                            color: globalBackgroundColor,
                            fontSize: 12),
                      )
                    ],
                  ),
                ),
                Expanded(
                    child: Container(
                  margin: EdgeInsets.only(left: 23.5, bottom: 2),
                  width: 1,
                  color: globalSecondaryColor,
                )),
                Row(
                  children: [
                    Container(
                      width: 10,
                      height: 10,
                      margin: EdgeInsets.only(left: 19, right: 15),
                      decoration: BoxDecoration(
                          border: Border.all(color: globalPrimaryColor),
                          borderRadius: BorderRadius.circular(8)),
                    ),
                    SubTitle(
                        title: tr(LocaleKeys.paymentPage_rate) +
                            '1 ' +
                            bloc.paymentDetail.sendCurrency +
                            ' = ' +
                            _rate() +
                            ' ' +
                            bloc.paymentDetail.receivedCurrency,
                        fontSize: 12),
                    Container(
                      padding: EdgeInsets.only(left: 10, right: 10),
                      margin: EdgeInsets.only(left: 5),
                      decoration: BoxDecoration(
                        color: labelColor,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: SubTitle(
                          title: tr(LocaleKeys.other_updated) +
                              getRelativeTime(bloc.refTime),
                          fontSize: 12),
                    )
                  ],
                ),
                Expanded(
                    child: Container(
                  margin: EdgeInsets.only(left: 23.5, top: 2, bottom: 2),
                  width: 1,
                  color: globalSecondaryColor,
                )),
                Row(
                  children: [
                    Container(
                      width: 12,
                      height: 12,
                      margin: EdgeInsets.only(left: 18, right: 15),
                      decoration: BoxDecoration(
                          border: Border.all(color: globalPrimaryColor),
                          borderRadius: BorderRadius.circular(8)),
                    ),
                    RichText(
                        text:
                            TextSpan(style: TextStyle(fontSize: 14), children: [
                      TextSpan(
                          text: tr(LocaleKeys.paymentPage_equivalentTo),
                          style: TextStyle(color: Colors.white)),
                      TextSpan(
                          text: _usdtEquivalent().toStringAsFixed(5) + ' USDT',
                          style: TextStyle(color: globalSecondaryColor))
                    ]))
                  ],
                )
              ],
            ),
          ),
          SettlementIndicator(
            title: tr(LocaleKeys.other_totalAmountNeeded),
            totalAmount: bloc.paymentDetail.sendCurrency + ' ' + _totalAmount(),
            amountSaved: _save(),
          ),
          SeparateIndicator(
              title: tr(LocaleKeys.paymentPage_comparedWith),
              extraMargin: true),
          AnimatedContainer(
            duration: const Duration(milliseconds: 300),
            curve: Curves.fastOutSlowIn,
            margin: EdgeInsets.only(left: 15, right: 15, top: 5),
            height: _height(),
            child: Card(
              color: inputBoxBackgroundColor,
              child: LoadingWrapper(
                generalState: state,
                isLoadingState: Comparing,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: expireColor,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8),
                              topRight: Radius.circular(8))),
                      padding: EdgeInsets.only(
                          left: 15, top: 10, bottom: 10, right: 15),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                                flex: 6,
                                child: SubTitle(
                                    title: tr(LocaleKeys.paymentPage_source),
                                    color: subTitleColor)),
                            Expanded(
                              flex: 5,
                              child: SubTitle(
                                  title:
                                      tr(LocaleKeys.paymentPage_amountNeeded),
                                  color: subTitleColor),
                            ),
                            Expanded(
                              flex: 4,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  SubTitle(
                                      title:
                                          tr(LocaleKeys.other_settlementDate),
                                      color: subTitleColor),
                                ],
                              ),
                            ),
                          ]),
                    ),
                    ..._comparisonRow(),
                    bloc.result.rates.length >
                            bloc.result.rates
                                .where((e) => e.order < 7)
                                .toList()
                                .length
                        ? Center(
                            child: GestureDetector(
                                onTap: toggle,
                                child: Icon(
                                    expanded
                                        ? Icons.expand_less
                                        : Icons.expand_more,
                                    color: Colors.white)),
                          )
                        : Container()
                  ],
                ),
              ),
            ),
          ),
          ConfirmButton(
              title: tr(LocaleKeys.paymentPage_agreeToProceed),
              height: 50,
              margin: EdgeInsets.only(left: 20, right: 20, top: 30),
              callback: _proceed),
          ConfirmButton(
              color: labelColor,
              title: tr(LocaleKeys.paymentPage_cancelCap),
              height: 50,
              margin: EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
              callback: widget.cancel),
        ],
      );
    });
  }
}
