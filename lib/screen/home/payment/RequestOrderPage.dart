import 'package:easy_localization/easy_localization.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/auth/authenticate_bloc.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/bloc/proof/proof_bloc.dart';
import 'package:otcpay/component/Indicator.dart';
import 'package:otcpay/component/Input.dart';
import 'package:otcpay/component/ListItem.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/component/Wrapper.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/model/AdRequest.dart';
import 'package:otcpay/style/Color.dart';

class RequestOrderPage extends StatefulWidget {
  final String id;
  final bool needInverse;

  final RATE_MODE mode;
  final String rate;
  final String fromCurrency;
  final String toCurrency;
  final String upperLimit;
  final String lowerLimit;

  const RequestOrderPage(
      {@required this.id,
      @required this.mode,
      @required this.rate,
      @required this.toCurrency,
      @required this.fromCurrency,
      this.needInverse = false,
      this.upperLimit,
      this.lowerLimit});

  @override
  _RequestOrderPageState createState() => _RequestOrderPageState();
}

class _RequestOrderPageState extends State<RequestOrderPage> {
  final GlobalKey<ConfirmButtonState> buttonKey = GlobalKey();
  final TextEditingController quantityController = TextEditingController();

  bool isTotal = false;

  double _getEqCryptoAmount() {
    if (isTotal) {
      return double.parse(quantityController.text) / double.parse(widget.rate);
    }

    return double.parse(quantityController.text) * double.parse(widget.rate);
  }

  bool _validateInput() {
    if (isTotal) {
      if (widget.upperLimit != null &&
          widget.upperLimit.isNotEmpty &&
          widget.lowerLimit != null &&
          widget.lowerLimit.isNotEmpty &&
          quantityController.text.isNotEmpty) {
        double quantity = double.parse(quantityController.text);
        return quantity <= double.parse(widget.upperLimit) &&
            quantity >= double.parse(widget.lowerLimit);
      }

      if (widget.needInverse) {
        return quantityController.text.isNotEmpty;
      }

      return false;
    }

    return quantityController.text.isNotEmpty;
  }

  String getValue() {
    if (isTotal) {
      double result = _getEqCryptoAmount();
      if (widget.mode == RATE_MODE.BUY) {
        return result.toStringAsFixed(5) + ' ' + widget.toCurrency;
      } else {
        return result.toStringAsFixed(5) + ' ' + widget.fromCurrency;
      }
    }

    if (widget.mode == RATE_MODE.BUY) {
      double result =
          double.parse(quantityController.text) * double.parse(widget.rate);
      return widget.fromCurrency + ' ' + result.toStringAsFixed(3);
    }

    double result =
        double.parse(quantityController.text) * double.parse(widget.rate);
    return widget.toCurrency + ' ' + result.toStringAsFixed(3);
  }

  String _getDisplayCurrency() {
    if (isTotal) {
      return widget.mode == RATE_MODE.BUY
          ? widget.fromCurrency
          : widget.toCurrency;
    }

    return widget.mode == RATE_MODE.BUY
        ? widget.toCurrency
        : widget.fromCurrency;
  }

  String _getLimitDisplay() {
    if (widget.upperLimit != null &&
        widget.upperLimit.isNotEmpty &&
        widget.lowerLimit != null &&
        widget.lowerLimit.isNotEmpty) {
      return widget.lowerLimit + ' - ' + widget.upperLimit;
    }

    return '100 - 20000';
  }

  void _submit() {
    BlocProvider.of<GeneralBloc>(context).add(SubmitOrder(
        request: AdRequest(
            amount: isTotal
                ? _getEqCryptoAmount().toString()
                : quantityController.text,
            totalCost: isTotal
                ? quantityController.text
                : _getEqCryptoAmount().toString(),
            offerRate: widget.rate,
            userId: BlocProvider.of<AuthenticateBloc>(context).repo.userId,
            name: BlocProvider.of<ProofBloc>(context).repo.username,
            contact: BlocProvider.of<ProofBloc>(context).repo.whatsappId,
            crytocurrency: widget.mode == RATE_MODE.BUY
                ? widget.toCurrency
                : widget.fromCurrency,
            settleCurrency: widget.mode == RATE_MODE.BUY
                ? widget.fromCurrency
                : widget.toCurrency,
            type: EnumToString.convertToString(widget.mode),
            adId: widget.id)));
  }

  void _toggle() {
    setState(() {
      isTotal = !isTotal;
    });
  }

  void _confirm() {
    Navigator.pushNamedAndRemoveUntil(context, '/home', (route) => false,
        arguments: 'otc_created');
  }

  void _showAlert(String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) => Theme(
        data: ThemeData.dark(),
        child: CupertinoAlertDialog(
          title: NormalTitle(title: message),
          actions: [
            CupertinoDialogAction(
                isDefaultAction: true,
                child: NormalTitle(title: tr(LocaleKeys.common_ok)),
                onPressed: _dismissAlert)
          ],
        ),
      ),
    );
  }

  void _dismissAlert() {
    Navigator.pop(context);
  }

  void _showConfirm() {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        backgroundColor: globalBackgroundColor,
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return BlocBuilder<GeneralBloc, GeneralState>(
              builder: (context, state) {
            return Container(
              height: 380,
              child: LoadingWrapper(
                generalState: state,
                isLoadingState: SubmittingOrder,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(right: 20, top: 10, bottom: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          InkWell(
                            child: NormalTitle(
                              title: tr(LocaleKeys.common_close),
                              color: subTitleColor,
                            ),
                            onTap: _dismissAlert,
                          )
                        ],
                      ),
                    ),
                    InputTitle(
                        title: tr(LocaleKeys.requestOrderPage_confirmOrder),
                        fontSize: 16),
                    Container(
                        margin: EdgeInsets.only(bottom: 10, top: 10),
                        child: Divider(color: dividerColor, thickness: 1)),
                    Container(
                      margin: EdgeInsets.only(left: 20, right: 20, bottom: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          NormalTitle(title: tr(LocaleKeys.common_price)),
                          NormalTitle(
                              title: widget.rate +
                                  ' ' +
                                  widget.fromCurrency +
                                  '/' +
                                  widget.toCurrency),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 20, right: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          NormalTitle(
                              title: isTotal
                                  ? tr(LocaleKeys.common_totalCost)
                                  : tr(LocaleKeys.common_quantity)),
                          NormalTitle(
                              title: isTotal
                                  ? _getDisplayCurrency() +
                                      ' ' +
                                      quantityController.text
                                  : quantityController.text +
                                      ' ' +
                                      _getDisplayCurrency()),
                        ],
                      ),
                    ),
                    Container(
                        margin: EdgeInsets.only(bottom: 40, top: 10),
                        child: Divider(color: dividerColor, thickness: 1)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        BoldTitle(
                            title: getValue(),
                            fontSize: 22,
                            color: globalSecondaryColor)
                      ],
                    ),
                    ConfirmButton(
                        title: tr(LocaleKeys.common_confirmCap),
                        height: 50,
                        margin: EdgeInsets.only(
                            left: 20, right: 20, top: 40, bottom: 20),
                        callback: _submit),
                  ],
                ),
              ),
            );
          });
        });
  }

  @override
  void initState() {
    super.initState();

    quantityController.addListener(() {
      buttonKey.currentState.setState(() {});
    });
  }

  @override
  void dispose() {
    quantityController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<GeneralBloc, GeneralState>(
        listener: (BuildContext context, GeneralState state) {
      if (state is OrderSuccess) _confirm();
      if (state is Error) _showAlert(state.message);
    }, builder: (BuildContext context, GeneralState state) {
      return Scaffold(
          appBar: AppBar(
            title: Text(tr(LocaleKeys.requestOrderPage_requestOrderTitle)),
            centerTitle: true,
            elevation: 1.0,
          ),
          body: ListView(
            children: [
              Container(
                margin: EdgeInsets.only(top: 30, bottom: 5, left: 20),
                child: BoldTitle(
                    title: tr(LocaleKeys.requestOrderPage_placeRequestTitle),
                    fontSize: 24),
              ),
              Container(
                margin: EdgeInsets.only(left: 20),
                child: SubTitle(
                  title: tr(LocaleKeys.requestOrderPage_placeRequestSubTitle),
                  color: subTitleColor,
                  fontSize: 12,
                ),
              ),
              Container(
                height: 40,
                child: Row(
                  children: [
                    BuySellIndicator(mode: widget.mode),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      child: BoldTitle(
                          title: widget.mode == RATE_MODE.SELL
                              ? widget.fromCurrency
                              : widget.toCurrency,
                          fontSize: 16),
                    )
                  ],
                ),
              ),
              InputTitle(
                  title: isTotal
                      ? tr(LocaleKeys.common_totalCost)
                      : tr(LocaleKeys.common_quantity)),
              InputBox(
                  height: 50.0,
                  controller: quantityController,
                  hintText: isTotal
                      ? _getLimitDisplay()
                      : widget.mode == RATE_MODE.BUY
                          ? tr(LocaleKeys.common_numOf) + widget.toCurrency
                          : tr(LocaleKeys.common_numOf) + widget.fromCurrency,
                  showSuffixIcon: !isTotal,
                  leadingIcon: isTotal
                      ? Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                                margin: EdgeInsets.only(left: 20),
                                child: NormalTitle(
                                    title: widget.mode == RATE_MODE.BUY
                                        ? widget.fromCurrency + ' '
                                        : widget.toCurrency + ' ')),
                          ],
                        )
                      : null,
                  overrideSuffixIcon: !isTotal,
                  keyboardType: TextInputType.numberWithOptions(decimal: true),
                  suffixIcon: isTotal
                      ? null
                      : Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                                margin: EdgeInsets.only(right: 20),
                                child: NormalTitle(
                                    title: widget.mode == RATE_MODE.SELL
                                        ? widget.fromCurrency
                                        : widget.toCurrency)),
                          ],
                        ),
                  margin: EdgeInsets.only(left: 20, right: 20, bottom: 10)),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 20),
                    child: RichText(
                        text:
                            TextSpan(style: TextStyle(fontSize: 12), children: [
                      TextSpan(
                          text: '*',
                          style: TextStyle(color: globalSecondaryColor)),
                      TextSpan(
                          text: tr(LocaleKeys.requestOrderPage_offerPrice),
                          style: TextStyle(color: subTitleColor)),
                      TextSpan(
                          text: widget.rate,
                          style: TextStyle(
                              color: globalSecondaryColor,
                              fontWeight: FontWeight.bold)),
                      TextSpan(
                          text: ' ' +
                              widget.fromCurrency +
                              '/' +
                              widget.toCurrency,
                          style: TextStyle(color: subTitleColor))
                    ])),
                  ),
                  Container(
                    margin: EdgeInsets.only(right: 15),
                    child: InkWell(
                      onTap: _toggle,
                      child: Container(
                        padding: EdgeInsets.all(5),
                        width: 90,
                        child: Row(
                          children: [
                            Image.asset('icons/exchange.png'),
                            Text(
                                isTotal
                                    ? tr(LocaleKeys.requestOrderPage_byQuantity)
                                    : tr(LocaleKeys.requestOrderPage_byTotal),
                                style: TextStyle(
                                    color: globalSecondaryColor,
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold))
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
              ConfirmButton(
                  key: buttonKey,
                  margin: EdgeInsets.only(
                      top: 50.0, left: 20.0, right: 20.0, bottom: 20.0),
                  validator: _validateInput,
                  title: widget.mode == RATE_MODE.BUY
                      ? tr(LocaleKeys.requestOrderPage_purchase)
                      : tr(LocaleKeys.requestOrderPage_sale),
                  height: 50,
                  callback: _showConfirm),
            ],
          ));
    });
  }
}
