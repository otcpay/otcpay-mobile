import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/auth/authenticate.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/component/Container.dart';
import 'package:otcpay/component/ListItem.dart';
import 'package:otcpay/component/Shimmer.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/model/ComparisonResult.dart';
import 'package:otcpay/model/rate/OtcRate.dart';
import 'package:otcpay/screen/home/payment/RequestOrderPage.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/style/Spec.dart';

import '../../LoginPage.dart';

class Board extends StatefulWidget {
  @override
  _BoardState createState() => _BoardState();
}

class _BoardState extends State<Board> {
  int index = 0;

  void setIndex(selectedIndex) {
    setState(() {
      index = selectedIndex;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ShadowContainer(
        height: 385.0,
        margin: EdgeInsets.only(top: 10.0),
        child: DefaultTabController(
            length: 2,
            child: Column(
              children: [
                Material(
                    elevation: globalElevation,
                    color: inputBoxBackgroundColor,
                    child: Container(
                      padding: EdgeInsets.only(bottom: 5.0),
                      child: TabBar(
                        onTap: setIndex,
                        indicatorSize: TabBarIndicatorSize.label,
                        indicatorColor: globalPrimaryColor,
                        tabs: [
                          Tab(
                              child: BoldTitle(
                                  title: 'USDT',
                                  color: index == 0
                                      ? Colors.white
                                      : subTitleColor)),
                          Tab(
                              child: BoldTitle(
                                  title: 'FIAT-FIAT',
                                  color: index == 1
                                      ? Colors.white
                                      : subTitleColor)),
                        ],
                      ),
                    )),
                BlocBuilder<GeneralBloc, GeneralState>(
                    builder: (BuildContext context, GeneralState state) {
                  return Expanded(
                      child: TabBarView(
                    children: [_USDTBoard(), _FIATBoard()],
                  ));
                })
              ],
            )));
  }
}

class _USDTBoard extends StatelessWidget {
  void _navigate(BuildContext context, OtcRate e) {
    AuthenticateState state = BlocProvider.of<AuthenticateBloc>(context).state;

    if (state is Authenticated) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (innerContext) => RequestOrderPage(
                    id: '',
                    needInverse: true,
                    mode: EnumToString.fromString(RATE_MODE.values, e.type),
                    rate: e.otcBoardRate.toStringAsFixed(3),
                    fromCurrency: e.sourceCurrency,
                    toCurrency: e.targetCurrency,
                  )));
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (_) => LoginPage(isFirstPage: true)),
          (route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    List<OtcRate> otcRates = BlocProvider.of<GeneralBloc>(context).otcRates;
    if (otcRates.length == 0) {
      return BoardShimmer();
    }

    return BlocBuilder<GeneralBloc, GeneralState>(
        builder: (BuildContext context, GeneralState state) {
      return Column(
          children: otcRates
              .where((element) => element.defaultRate)
              .toList()
              .asMap()
              .map((i, e) => MapEntry(
                  i,
                  DailyRateItem(
                      fromCurrency: e.sourceCurrency,
                      toCurrency: e.targetCurrency,
                      rate: e.otcBoardRate.toStringAsFixed(3),
                      isGradient: i % 2 == 0,
                      callback: () => _navigate(context, e),
                      mode: EnumToString.fromString(RATE_MODE.values, e.type))))
              .values
              .toList());
    });
  }
}

class _FIATBoard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<ComparisonResult> defaultFiatRates =
        BlocProvider.of<GeneralBloc>(context).defaultFiatRates;
    if (defaultFiatRates.length == 0) {
      return BoardShimmer();
    }

    return BlocBuilder<GeneralBloc, GeneralState>(
        builder: (BuildContext context, GeneralState state) {
      return Column(
          children: defaultFiatRates
              .where((element) => element.defaultRate)
              .toList()
              .asMap()
              .map((i, e) => MapEntry(
                  i,
                  DailyRateItem(
                      fromCurrency: e.fromCurrency,
                      toCurrency: e.toCurrency,
                      rate: e.otcBoardRate.toString(),
                      isGradient: i % 2 == 0,
                      callback: () => BlocProvider.of<GeneralBloc>(context)
                              .add(NavigateTab(index: 1, payload: {
                            'fromCurrency': e.fromCurrency,
                            'toCurrency': e.toCurrency,
                            'rate': e.otcBoardRate.toString(),
                          })),
                      mode: RATE_MODE.REMIT)))
              .values
              .toList());
    });
  }
}
