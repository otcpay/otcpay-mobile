import 'package:carousel_slider/carousel_slider.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/component/Shimmer.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/model/Banner.dart';
import 'package:otcpay/style/Spec.dart';
import 'package:url_launcher/url_launcher.dart';

class BannerWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GeneralBloc, GeneralState>(
        builder: (BuildContext context, GeneralState state) {
      return _TopBanner();
    });
  }
}

class _TopBanner extends StatefulWidget {
  @override
  _TopBannerState createState() => _TopBannerState();
}

class _TopBannerState extends State<_TopBanner> {
  int _current = 0;

  void _showSnackbar(String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  Future<void> _openUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      _showSnackbar(tr(LocaleKeys.hotPage_cannotLaunchBrowser));
    }
  }

  List<Widget> _renderBanner(List<BannerObject> imgList) {
    imgList.sort((a, b) => a.order.compareTo(b.order));
    return imgList.where((element) => element.order < 6).map((i) {
      return Builder(
        builder: (BuildContext context) {
          return GestureDetector(
            onTap: () => _openUrl(i.hyperlink),
            child: Container(
                width: MediaQuery.of(context).size.width,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: Image.network(i.url, fit: BoxFit.fill),
                )),
          );
        },
      );
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    List<dynamic> bannerList = new List<dynamic>.from(
        BlocProvider.of<GeneralBloc>(context).mapping['hotpage'] ?? []);
    List<BannerObject> imgList = bannerList
        .map((e) => BannerObject.fromJson(Map<String, dynamic>.from(e)))
        .toList();

    if (imgList.length == 0) {
      return BannerShimmer();
    }

    return Container(
      height: bannerHeight,
      child: Stack(
        children: [
          CarouselSlider(
            options: CarouselOptions(
                onPageChanged: (index, reason) {
                  setState(() {
                    _current = index;
                  });
                },
                height: bannerHeight,
                autoPlay: true,
                viewportFraction: 1),
            items: _renderBanner(imgList),
          ),
          Positioned(
            bottom: 5,
            left: 0,
            right: 0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: imgList
                  .where((element) => element.order < 6)
                  .toList()
                  .asMap()
                  .entries
                  .map((entry) {
                return Container(
                  width: _current == entry.key ? 12 : 6,
                  height: 6,
                  margin: EdgeInsets.only(top: 6, left: 4, right: 4),
                  decoration: BoxDecoration(
                      borderRadius: _current == entry.key
                          ? BorderRadius.circular(6)
                          : null,
                      shape: _current == entry.key
                          ? BoxShape.rectangle
                          : BoxShape.circle,
                      color: (Theme.of(context).brightness == Brightness.dark
                              ? Colors.white
                              : Colors.black)
                          .withOpacity(_current == entry.key ? 0.9 : 0.4)),
                );
              }).toList(),
            ),
          )
        ],
      ),
    );
  }
}
