import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/component/Container.dart';
import 'package:otcpay/component/Indicator.dart';
import 'package:otcpay/model/rate/CryptoRate.dart';

class IndexRegion extends StatelessWidget {
  static const List<String> _indexes = ['BTC', 'ETH', 'XRP', 'DOGE'];

  double _getChange(double open, double close) {
    return (close - open) / open * 100;
  }

  String _getPercentage(double open, double close) {
    double change = _getChange(open, close);
    if (change > 0) {
      return '+' + change.toStringAsFixed(2) + '%';
    }

    return change.toStringAsFixed(2) + '%';
  }

  List<Widget> _renderCurrencyIndicator(List<CryptoRate> cryptoRates) {
    return _indexes
        .asMap()
        .map((i, e) => MapEntry(
            i,
            CurrencyIndicator(
                currency: _indexes[i],
                rate: cryptoRates[i].close.toString(),
                percentage:
                    _getPercentage(cryptoRates[i].open, cryptoRates[i].close),
                isUp:
                    _getChange(cryptoRates[i].open, cryptoRates[i].close) > 0)))
        .values
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<GeneralBloc, GeneralState>(
        builder: (BuildContext context, GeneralState state) {
      List<CryptoRate> cryptoRates =
          BlocProvider.of<GeneralBloc>(context).cryptoRates;
      return ShadowContainer(
          height: 90.0,
          margin: EdgeInsets.only(top: 10.0),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: _renderCurrencyIndicator(cryptoRates)));
    });
  }
}
