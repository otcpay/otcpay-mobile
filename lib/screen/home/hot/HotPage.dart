import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/bloc/hot/hot.dart';
import 'package:otcpay/component/Indicator.dart';
import 'package:otcpay/component/ListItem.dart';
import 'package:otcpay/component/Notification.dart';
import 'package:otcpay/component/Shimmer.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/component/Wrapper.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/style/Spec.dart';
import 'package:otcpay/util/TimeUtil.dart';

import 'Banner.dart';
import 'Board.dart';
import 'IndexRegion.dart';

class HotPage extends StatefulWidget {
  @override
  _HotPageState createState() => _HotPageState();
}

class _HotPageState extends State<HotPage> {
  Completer<void> _refreshCompleter;

  HotBloc bloc;

  Future<void> _refreshData() async {
    bloc.add(InitialQuoteFetch(refresh: true));

    return _refreshCompleter.future;
  }

  List<Widget> _renderQuoteList() {
    return bloc.displayList
        .map((e) => Container(
              margin: quoteItemMargin,
              child: QuoteListItem(
                fromCurrency: e.sendCurrency,
                toCurrency: e.receivedCurrency,
                person: e.name,
                time: getRelativeTime(e.updatedDate),
                amount: e.receivedAmount,
                rate: e.referenceRate,
                saved: e.amountSaved,
                status: e.status,
              ),
            ))
        .toList();
  }

  @override
  void initState() {
    super.initState();

    _refreshCompleter = Completer<void>();

    bloc = BlocProvider.of<HotBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(45),
          child: AppBar(
            title: Image.asset('icons/Group.png', height: 45),
            elevation: 1,
            titleSpacing: 5,
            actions: [
              NotificationButton(),
            ],
          ),
        ),
        body: ListView(
          padding: EdgeInsets.only(left: 10.0, right: 10.0),
          children: [
            BannerWrapper(),
            IndexRegion(),
            BlocBuilder<GeneralBloc, GeneralState>(
                builder: (BuildContext context, GeneralState state) {
              return Container(
                height: 20,
                margin: EdgeInsets.only(top: 20.0, left: 15.0),
                child: Row(
                  children: [
                    BoldTitle(title: tr(LocaleKeys.hotPage_otcDailyRate)),
                    Container(
                      margin: EdgeInsets.only(left: 14.0),
                      child: HighlightIndicator(
                          label: tr(LocaleKeys.hotPage_updated) +
                              getRelativeTime(
                                  BlocProvider.of<GeneralBloc>(context)
                                      .refTime)),
                    )
                  ],
                ),
              );
            }),
            Board(),
            Container(
                height: 20,
                margin: EdgeInsets.only(top: 20.0, left: 15.0),
                child: Row(
                  children: [
                    BoldTitle(title: tr(LocaleKeys.hotPage_otcQuotationRecord)),
                  ],
                )),
            BlocConsumer<HotBloc, HotState>(
                listener: (BuildContext context, HotState state) {
              if (state is QuoteLoaded) {
                _refreshCompleter?.complete();
                _refreshCompleter = Completer();
              }
            }, builder: (BuildContext context, HotState state) {
              return Container(
                height: 380.0,
                margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                child: bloc.displayList.length == 0
                    ? QuoteItemListShimmer()
                    : LoadingWrapper(
                        hotState: state,
                        isLoadingState: QuoteLoading,
                        child: RefreshIndicator(
                            onRefresh: _refreshData,
                            child: NotificationListener<
                                    OverscrollIndicatorNotification>(
                                onNotification: (OverscrollIndicatorNotification
                                    overscroll) {
                                  overscroll.disallowGlow();
                                  return false;
                                },
                                child: ListView(children: _renderQuoteList()))),
                      ),
              );
            })
          ],
        ));
  }
}
