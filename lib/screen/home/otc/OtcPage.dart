import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/otc/otc.dart';
import 'package:otcpay/component/Indicator.dart';
import 'package:otcpay/component/ListItem.dart';
import 'package:otcpay/component/Shimmer.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/screen/home/otc/header/DropdownHeader.dart';
import 'package:otcpay/util/TimeUtil.dart';

import 'header/MainFilter.dart';

class OtcPage extends StatefulWidget {
  @override
  _OtcPageState createState() => _OtcPageState();
}

class _OtcPageState extends State<OtcPage> {
  Completer<void> _refreshCompleter;

  ScrollController _scrollController;

  OtcBloc bloc;

  List<Widget> _otcList() {
    return bloc.displayList
        .map((e) => OTCListItem(
              id: e.id,
              name: e.name,
              quantity: e.quantity,
              upperLimit: e.upperLimit,
              lowerLimit: e.lowerLimit,
              fromCurrency: e.settleCurrency,
              toCurrency: e.crytocurrency,
              rate: e.offer,
              profilePic: e.profilePic,
              time: getRelativeTime(e.updatedDate),
              verified: e.verified,
              status: e.status,
              mode: EnumToString.fromString(RATE_MODE.values, e.type),
            ))
        .toList();
  }

  Future<void> _refreshData() async {
    bloc.add(InitialFetch(true));

    return _refreshCompleter.future;
  }

  void _showPublished() {
    final snackBar =
        SnackBar(content: Text(tr(LocaleKeys.otcPage_advertisementPublished)));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  void initState() {
    super.initState();

    _refreshCompleter = Completer<void>();

    bloc = BlocProvider.of<OtcBloc>(context);

    _scrollController = ScrollController();
    _scrollController.addListener(() {
      if (_scrollController.position.atEdge &&
          _scrollController.position.pixels != 0 &&
          bloc.lastFetchNum > 0) {
        bloc.add(FetchMore());
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: BlocConsumer<OtcBloc, OtcState>(
            listener: (BuildContext context, OtcState state) {
          if (state is Loaded) {
            _refreshCompleter?.complete();
            _refreshCompleter = Completer();

            if (state.isSuccess != null && state.isSuccess) _showPublished();
          }
        }, builder: (BuildContext context, OtcState state) {
          return Stack(
            children: [
              Container(
                margin: EdgeInsets.only(top: 120),
                child: bloc.fullList.length == 0
                    ? OTCItemListShimmer()
                    : RefreshIndicator(
                        onRefresh: _refreshData,
                        child: ListView(
                          controller: _scrollController,
                          padding: EdgeInsets.zero,
                          children: _otcList(),
                        ),
                      ),
              ),
              Positioned(left: 15, right: 10, top: 85, child: DropdownHeader()),
              Positioned(left: 0, right: 0, top: 0, child: MainFilter()),
              state is Loading
                  ? Align(
                      alignment: Alignment.center,
                      child: LoadingIndicator(),
                    )
                  : Container()
            ],
          );
        }),
      ),
    );
  }
}
