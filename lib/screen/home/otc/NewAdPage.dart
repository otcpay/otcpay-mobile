import 'package:clippy_flutter/diagonal.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/auth/authenticate.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/bloc/otc/otc.dart';
import 'package:otcpay/bloc/proof/proof.dart';
import 'package:otcpay/component/Dropdown.dart';
import 'package:otcpay/component/Indicator.dart';
import 'package:otcpay/component/Input.dart';
import 'package:otcpay/component/ListItem.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/component/Wrapper.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/model/Advertisement.dart';
import 'package:otcpay/screen/home/otc/header/MainFilter.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/util/StringUtil.dart';

class NewAdPage extends StatefulWidget {
  final RATE_MODE mode;
  final int currency;
  final bool isEdit;
  final String id;
  final String settleCurrency;
  final String max;
  final String min;
  final String quantity;
  final String rate;

  const NewAdPage(
      {@required this.mode,
      @required this.currency,
      this.isEdit = false,
      this.id,
      this.settleCurrency,
      this.max,
      this.min,
      this.quantity,
      this.rate});

  @override
  _NewAdPageState createState() => _NewAdPageState();
}

class _NewAdPageState extends State<NewAdPage> {
  final GlobalKey<ConfigurableExpansionTileState> dropDownKey = GlobalKey();
  final GlobalKey<ConfirmButtonState> key = GlobalKey();

  final TextEditingController priceController = TextEditingController();
  final TextEditingController quantityController = TextEditingController();
  final TextEditingController maxController = TextEditingController();
  final TextEditingController minController = TextEditingController();

  List<String> dollars = ['HKD', 'USD', 'MOP', 'CNY', 'TWD'];

  GeneralBloc bloc;
  OtcBloc otcBloc;

  RATE_MODE mode;

  int currency;
  int dollar = 0;

  void _buy() {
    setState(() {
      mode = RATE_MODE.BUY;
    });
  }

  void _sell() {
    setState(() {
      mode = RATE_MODE.SELL;
    });
  }

  void _setUSDT() {
    setState(() {
      currency = 0;
    });
  }

  void _setUSDC() {
    setState(() {
      currency = 1;
    });
  }

  void _setBUSD() {
    setState(() {
      currency = 2;
    });
  }

  void _setBTC() {
    setState(() {
      currency = 3;
    });
  }

  void _setETH() {
    setState(() {
      currency = 4;
    });
  }

  void _setCurrency(int key) {
    setState(() => {dollar = key});

    dropDownKey.currentState.handleTap();
  }

  void _updateButtonState() {
    if (key.currentState != null) {
      key.currentState.setState(() {});
    }
  }

  void _postAd() {
    Advertisement ad = Advertisement(
        quantity: quantityController.text,
        contact: BlocProvider.of<ProofBloc>(context).repo.account,
        crytocurrency: options[currency],
        lowerLimit: minController.text,
        upperLimit: maxController.text,
        name: BlocProvider.of<ProofBloc>(context).repo.username,
        offer: priceController.text,
        settleCurrency: dollars[dollar],
        type: EnumToString.convertToString(mode),
        profilePic: BlocProvider.of<ProofBloc>(context).repo.profilePic,
        verified: BlocProvider.of<ProofBloc>(context).repo.verified,
        userId: BlocProvider.of<AuthenticateBloc>(context).repo.userId);

    otcBloc.add(PostAd(ad));
  }

  void _updateAd() {
    bloc.add(UpdateAd(id: widget.id, data: {
      'crytocurrency': options[currency],
      'lowerLimit': minController.text,
      'upperLimit': maxController.text,
      'settleCurrency': dollars[dollar],
      'quantity': quantityController.text,
      'offer': priceController.text,
      'status': 'Listed',
    }));
    Navigator.of(context).pop();
  }

  bool _validate() {
    return priceController.text.length > 0 &&
        quantityController.text.length > 0 &&
        maxController.text.length > 0 &&
        minController.text.length > 0;
  }

  Widget _currencyOption(
      String title, int currencyIndex, void Function() callback) {
    return Container(
      margin: EdgeInsets.only(top: 10, left: 10, right: 15),
      child: InkWell(
        onTap: callback,
        child: currency == currencyIndex
            ? BoldTitle(title: title, fontSize: 16)
            : SubTitle(title: title, fontSize: 12, color: subTitleColor),
      ),
    );
  }

  Widget _buySellOption(
      RATE_MODE rateMode, double width, void Function() callback) {
    return GestureDetector(
        onTap: callback,
        child: Container(
            color: mode == rateMode
                ? mode == RATE_MODE.BUY
                    ? upColor
                    : downColor
                : labelColor,
            width: width / 2,
            height: 60.0,
            child: Center(
                child: BoldTitle(
                    title: getBuySellRemitButtonTitle(rateMode),
                    fontSize: 16))));
  }

  List<Widget> _dollarFilter() {
    return dollars
        .asMap()
        .map((key, value) => MapEntry(
            key,
            GestureDetector(
                onTap: () => _setCurrency(key),
                child: Container(
                    color: dollar == key ? labelColor : inputBoxBackgroundColor,
                    height: 28,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[NormalTitle(title: value)],
                    )))))
        .values
        .toList();
  }

  @override
  void initState() {
    super.initState();

    bloc = BlocProvider.of<GeneralBloc>(context);
    otcBloc = BlocProvider.of<OtcBloc>(context);

    mode = widget.mode;
    currency = widget.currency;

    priceController.addListener(_updateButtonState);
    quantityController.addListener(_updateButtonState);
    maxController.addListener(_updateButtonState);
    minController.addListener(_updateButtonState);

    if (widget.quantity != null) quantityController.text = widget.quantity;
    if (widget.rate != null) priceController.text = widget.rate;
    if (widget.max != null) maxController.text = widget.max;
    if (widget.min != null) minController.text = widget.min;
  }

  @override
  void dispose() {
    priceController.dispose();
    quantityController.dispose();
    maxController.dispose();
    minController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    dollars = [bloc.mapping['mapping'][otcBloc.region]];
    if (otcBloc.region != 'USA') dollars.add('USD');

    return Scaffold(
        appBar: AppBar(
          title: Text(widget.isEdit
              ? tr(LocaleKeys.otcPage_updatePost)
              : tr(LocaleKeys.otcPage_newPost)),
          centerTitle: true,
          elevation: 1.0,
        ),
        body: BlocConsumer<OtcBloc, OtcState>(
            listener: (BuildContext context, OtcState state) {
          if (state is Loaded) Navigator.of(context).pop();
        }, builder: (BuildContext context, OtcState state) {
          return LoadingWrapper(
            otcState: state,
            isLoadingState: Loading,
            child: ListView(
              children: [
                widget.isEdit
                    ? Container()
                    : Container(
                        margin: EdgeInsets.only(left: 20, bottom: 10, top: 40),
                        child: BoldTitle(
                            title: tr(LocaleKeys.otcPage_postAdTitle),
                            fontSize: 24),
                      ),
                widget.isEdit
                    ? Container()
                    : Container(
                        margin: EdgeInsets.only(left: 20, bottom: 10),
                        child: SubTitle(
                            color: subTitleColor,
                            fontSize: 12,
                            title: tr(LocaleKeys.otcPage_postAdSubTitle))),
                Container(
                  height: 60,
                  margin: EdgeInsets.only(
                      top: widget.isEdit ? 20 : 0, left: 20, right: 20),
                  child: Row(
                    children: [
                      Expanded(
                          child: Stack(
                        children: [
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Diagonal(
                                clipHeight: 50.0,
                                axis: Axis.vertical,
                                position: DiagonalPosition.TOP_RIGHT,
                                child:
                                    _buySellOption(RATE_MODE.BUY, width, _buy),
                              )),
                          Align(
                            alignment: Alignment.centerRight,
                            child: Diagonal(
                              clipHeight: 50.0,
                              axis: Axis.vertical,
                              position: DiagonalPosition.BOTTOM_LEFT,
                              child:
                                  _buySellOption(RATE_MODE.SELL, width, _sell),
                            ),
                          )
                        ],
                      ))
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 10, top: 10),
                  child: Row(
                    children: [
                      BuySellIndicator(mode: mode),
                      _currencyOption('USDT', 0, _setUSDT),
                      _currencyOption('USDC', 1, _setUSDC),
                      _currencyOption('BUSD', 2, _setBUSD),
                      _currencyOption('BTC', 3, _setBTC),
                      _currencyOption('ETH', 4, _setETH),
                    ],
                  ),
                ),
                InputTitle(title: tr(LocaleKeys.common_offer)),
                Container(
                    margin: EdgeInsets.only(left: 15, right: 15),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                            flex: 1,
                            child: Card(
                              color: inputBoxBackgroundColor,
                              margin: EdgeInsets.only(top: 5),
                              child: Container(
                                padding: EdgeInsets.only(top: 10, bottom: 10),
                                child: ConfigurableExpansionTile(
                                    key: dropDownKey,
                                    animatedWidgetFollowingHeader: const Icon(
                                      Icons.arrow_drop_down,
                                      color: Colors.white,
                                    ),
                                    header: Flexible(
                                        child: NormalTitle(
                                            title: dollars[dollar],
                                            color: Colors.white)),
                                    headerExpanded: NormalTitle(
                                        title: dollars[dollar],
                                        color: Colors.white),
                                    headerBackgroundColorStart:
                                        Colors.transparent,
                                    expandedBackgroundColor: Colors.transparent,
                                    headerBackgroundColorEnd:
                                        Colors.transparent,
                                    children: _dollarFilter()),
                              ),
                            )),
                        Expanded(
                          flex: 4,
                          child: InputBox(
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true),
                              controller: priceController,
                              hintText: 'Price'),
                        ),
                      ],
                    )),
                InputTitle(title: tr(LocaleKeys.common_quantity)),
                InputBox(
                    margin: EdgeInsets.only(left: 15, right: 15),
                    keyboardType:
                        TextInputType.numberWithOptions(decimal: true),
                    controller: quantityController,
                    hintText: tr(LocaleKeys.common_numOf) + options[currency]),
                InputTitle(title: tr(LocaleKeys.common_limit)),
                InputBox(
                    margin: EdgeInsets.only(left: 15, right: 15, bottom: 7),
                    keyboardType:
                        TextInputType.numberWithOptions(decimal: true),
                    controller: minController,
                    hintText: tr(LocaleKeys.otcPage_minAmount)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [BoldTitle(title: tr(LocaleKeys.otcPage_to))],
                ),
                InputBox(
                    margin: EdgeInsets.only(left: 15, right: 15, top: 7),
                    keyboardType:
                        TextInputType.numberWithOptions(decimal: true),
                    controller: maxController,
                    hintText: tr(LocaleKeys.otcPage_maxAmount)),
                ConfirmButton(
                    key: key,
                    validator: _validate,
                    color: mode == RATE_MODE.SELL ? downColor : upColor,
                    disabledColor: mode == RATE_MODE.SELL
                        ? downBackgroundColor
                        : upBackgroundColor,
                    margin: EdgeInsets.only(
                        top: 50.0, left: 20.0, right: 20.0, bottom: 20.0),
                    title: widget.isEdit
                        ? tr(LocaleKeys.common_update)
                        : tr(LocaleKeys.common_publish),
                    height: 50,
                    callback: widget.isEdit ? _updateAd : _postAd),
              ],
            ),
          );
        }));
  }
}
