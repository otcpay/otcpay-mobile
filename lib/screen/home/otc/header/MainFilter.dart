import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/auth/authenticate.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/bloc/otc/otc.dart';
import 'package:otcpay/component/Dropdown.dart';
import 'package:otcpay/component/ListItem.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/util/StringUtil.dart';

import '../../../LoginPage.dart';
import '../NewAdPage.dart';

final List<String> options = const ['USDT', 'USDC', 'BUSD', 'BTC', 'ETH'];

class MainFilter extends StatefulWidget {
  @override
  _MainFilterState createState() => _MainFilterState();
}

class _MainFilterState extends State<MainFilter> {
  final GlobalKey<ConfigurableExpansionTileState> key = GlobalKey();

  RATE_MODE buySellStage = RATE_MODE.BUY;
  int currency = 0;
  String selectedRegion = 'Hong Kong';

  OtcBloc bloc;

  Map<String, dynamic> cachedMapping;

  void _setBuy() {
    buySellStage = RATE_MODE.BUY;
    bloc.add(ApplyFilter(
        buySellMode: buySellStage,
        currency: currency,
        region: selectedRegion,
        regionCurrency: cachedMapping['mapping'][selectedRegion]));
  }

  void _setSell() {
    buySellStage = RATE_MODE.SELL;
    bloc.add(ApplyFilter(
        buySellMode: buySellStage,
        currency: currency,
        region: selectedRegion,
        regionCurrency: cachedMapping['mapping'][selectedRegion]));
  }

  void _setCurrency(int i) {
    currency = i;
    bloc.add(ApplyFilter(
        buySellMode: buySellStage,
        currency: currency,
        region: selectedRegion,
        regionCurrency: cachedMapping['mapping'][selectedRegion]));
  }

  void _setRegion(String region) {
    selectedRegion = region;
    bloc.add(ApplyFilter(
      buySellMode: buySellStage,
      currency: currency,
      region: selectedRegion,
      regionCurrency: cachedMapping['mapping'][selectedRegion],
    ));

    key.currentState.handleTap();
  }

  void _postAd() {
    AuthenticateState state = BlocProvider.of<AuthenticateBloc>(context).state;

    if (state is Authenticated) {
      Navigator.of(context).push(MaterialPageRoute(
          builder: (_) => NewAdPage(currency: currency, mode: buySellStage)));
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (_) => LoginPage(isFirstPage: true)),
          (route) => false);
    }
  }

  Widget _modeFilter(RATE_MODE mode, void Function() callback) {
    return GestureDetector(
      onTap: callback,
      child: AnimatedDefaultTextStyle(
          child: Text(mode == RATE_MODE.BUY
              ? tr(LocaleKeys.common_buy)
              : tr(LocaleKeys.common_sell)),
          style: TextStyle(
              color: bloc.buySellStage == mode ? Colors.white : subTitleColor,
              fontSize: bloc.buySellStage == mode ? 24 : 14,
              fontWeight: bloc.buySellStage == mode
                  ? FontWeight.bold
                  : FontWeight.normal),
          duration: const Duration(milliseconds: 200)),
    );
  }

  Widget _getIcon(String value) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5),
      child: Image.asset('icons/' + getIconName(value) + '.png',
          width: 15, height: 15),
    );
  }

  List<Widget> _currencyOptions() {
    return options
        .asMap()
        .map((i, e) => MapEntry(
            i,
            Container(
              margin: EdgeInsets.only(right: i < 2 ? 5 : 0),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: bloc.currency == i
                          ? BorderSide(color: globalSecondaryColor)
                          : BorderSide.none)),
              child: TextButton(
                  style: ButtonStyle(
                      tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      minimumSize: MaterialStateProperty.all(Size(40, 20)),
                      padding: MaterialStateProperty.all(EdgeInsets.zero)),
                  onPressed: () => _setCurrency(i),
                  child: NormalTitle(
                      title: e,
                      color: bloc.currency == i
                          ? globalSecondaryColor
                          : subTitleColor)),
            )))
        .values
        .toList();
  }

  List<Widget> _regionFilter() {
    return ['Hong Kong', 'USA', 'Macau', 'China', 'Taiwan']
        .map((region) => GestureDetector(
            onTap: () => _setRegion(region),
            child: Container(
                color: selectedRegion == region
                    ? labelColor
                    : globalBackgroundColor,
                height: 28,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[_getIcon(region), SubTitle(title: region)],
                ))))
        .toList();
  }

  @override
  void initState() {
    super.initState();

    bloc = BlocProvider.of<OtcBloc>(context);
    cachedMapping = BlocProvider.of<GeneralBloc>(context).mapping;

    String regionCurrency = 'HKD';
    if (cachedMapping['mapping'] != null) {
      regionCurrency = cachedMapping['mapping'][selectedRegion];
    }

    bloc.add(ApplyFilter(
        buySellMode: buySellStage,
        currency: currency,
        region: selectedRegion,
        regionCurrency: regionCurrency));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OtcBloc, OtcState>(builder: (context, snapshot) {
      return Column(
        children: [
          Container(
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  margin: EdgeInsets.only(left: 15.0),
                  width: 110,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      _modeFilter(RATE_MODE.BUY, _setBuy),
                      _modeFilter(RATE_MODE.SELL, _setSell),
                    ],
                  ),
                ),
                Material(
                  child: IconButton(
                      iconSize: 20,
                      icon: Icon(Icons.add_box, color: Colors.white),
                      onPressed: _postAd),
                )
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(left: 15),
                width: 220,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: _currencyOptions(),
                ),
              ),
              Container(
                  width: 100,
                  color: dropdownColor,
                  child: ConfigurableExpansionTile(
                      key: key,
                      animatedWidgetFollowingHeader: const Icon(
                        Icons.arrow_drop_down,
                        color: globalPrimaryColor,
                      ),
                      header: Flexible(
                          child: Row(
                        children: [
                          _getIcon(selectedRegion),
                          SubTitle(
                              title: selectedRegion, color: globalPrimaryColor),
                        ],
                      )),
                      headerExpanded: Container(
                          color: Colors.transparent,
                          child: Row(
                            children: [
                              _getIcon(selectedRegion),
                              SubTitle(
                                  title: selectedRegion,
                                  color: globalPrimaryColor),
                            ],
                          )),
                      headerBackgroundColorStart: dropdownColor,
                      expandedBackgroundColor: dropdownColor,
                      headerBackgroundColorEnd: dropdownColor,
                      children: _regionFilter()))
            ],
          ),
        ],
      );
    });
  }
}
