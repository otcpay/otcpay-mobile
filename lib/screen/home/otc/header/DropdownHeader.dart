import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/general/general_bloc.dart';
import 'package:otcpay/bloc/otc/otc.dart';
import 'package:otcpay/component/Dropdown.dart';
import 'package:otcpay/component/ListItem.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/util/StringUtil.dart';

class DropdownHeader extends StatefulWidget {
  @override
  _DropdownHeaderState createState() => _DropdownHeaderState();
}

class _DropdownHeaderState extends State<DropdownHeader> {
  final GlobalKey<ConfigurableExpansionTileState> dropDownKey = GlobalKey();

  int sortOption = 0;
  int dollar = 0;

  List<String> dollars = ['HKD'];

  OtcBloc bloc;

  void _sortByPrice() {
    setState(() {
      sortOption = 0;
      bloc.add(ApplySorting(sorting: 0));
    });
  }

  void _sortByDate() {
    setState(() {
      sortOption = 1;
      bloc.add(ApplySorting(sorting: 1));
    });
  }

  void _filterByCurrency(int key) {
    setState(() {
      dollar = key;
      BlocProvider.of<OtcBloc>(context)
          .add(ApplyFilter(dollar: dollars[dollar]));
    });

    dropDownKey.currentState.handleTap();
  }

  Widget _getIcon(String value) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5),
      child: Image.asset('icons/' + getIconName(value) + '.png',
          width: 15, height: 15),
    );
  }

  List<Widget> _dollarFilter() {
    return dollars
        .asMap()
        .map((key, value) => MapEntry(
            key,
            GestureDetector(
                onTap: () => _filterByCurrency(key),
                child: Container(
                    color: dollar == key ? labelColor : globalBackgroundColor,
                    height: 28,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        _getIcon(value),
                        SubTitle(title: value)
                      ],
                    )))))
        .values
        .toList();
  }

  @override
  void initState() {
    super.initState();

    bloc = BlocProvider.of<OtcBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OtcBloc, OtcState>(builder: (context, state) {
      if (BlocProvider.of<GeneralBloc>(context).mapping['mapping'] != null) {
        dollars = [
          BlocProvider.of<GeneralBloc>(context).mapping['mapping'][bloc.region]
        ];
      }
      if (bloc.region != 'USA') dollars.add('USD');

      return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
              child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(right: 20),
                child: Material(
                  child: InkWell(
                    borderRadius: BorderRadius.circular(5),
                    onTap: _sortByPrice,
                    child: Container(
                        padding: EdgeInsets.only(top: 3, bottom: 3),
                        decoration: BoxDecoration(
                            color: Colors.transparent,
                            border: Border.all(
                                color: sortOption == 0
                                    ? globalPrimaryColor
                                    : Colors.white),
                            borderRadius: BorderRadius.circular(5)),
                        width: 100,
                        child: Center(
                            child: SubTitle(
                                title: bloc.buySellStage == RATE_MODE.BUY
                                    ? tr(LocaleKeys.otcPage_priceLowToHigh)
                                    : tr(LocaleKeys.otcPage_priceHighToLow),
                                color: sortOption == 0
                                    ? globalPrimaryColor
                                    : Colors.white))),
                  ),
                ),
              ),
              Material(
                child: InkWell(
                  borderRadius: BorderRadius.circular(5),
                  onTap: _sortByDate,
                  child: Container(
                      padding: EdgeInsets.only(top: 3, bottom: 3),
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: sortOption == 1
                                  ? globalPrimaryColor
                                  : Colors.white),
                          borderRadius: BorderRadius.circular(5)),
                      width: 100,
                      child: Center(
                          child: SubTitle(
                              title: tr(LocaleKeys.otcPage_dateNewToOld),
                              color: sortOption == 1
                                  ? globalPrimaryColor
                                  : Colors.white))),
                ),
              )
            ],
          )),
          Container(
              width: 75,
              color: dropdownColor,
              child: ConfigurableExpansionTile(
                  key: dropDownKey,
                  animatedWidgetFollowingHeader: const Icon(
                    Icons.arrow_drop_down,
                    color: globalPrimaryColor,
                  ),
                  header: Flexible(
                      child: Row(
                    children: [
                      _getIcon(dollars[dollar]),
                      SubTitle(
                          title: dollars[dollar], color: globalPrimaryColor),
                    ],
                  )),
                  headerExpanded: Container(
                      color: Colors.transparent,
                      child: Row(
                        children: [
                          _getIcon(dollars[dollar]),
                          SubTitle(
                              title: dollars[dollar],
                              color: globalPrimaryColor),
                        ],
                      )),
                  headerBackgroundColorStart: globalBackgroundColor,
                  expandedBackgroundColor: globalBackgroundColor,
                  headerBackgroundColorEnd: globalBackgroundColor,
                  children: _dollarFilter()))
        ],
      );
    });
  }
}
