import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:otcpay/component/Input.dart';
import 'package:otcpay/component/Password.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/screen/signup/ProfilePage.dart';
import 'package:otcpay/style/Spec.dart';
import 'package:otcpay/util/ParamUtil.dart';

class PasswordPage extends StatefulWidget {
  final String username;

  const PasswordPage({@required this.username});

  @override
  _PasswordPageState createState() => _PasswordPageState();
}

class _PasswordPageState extends State<PasswordPage> {
  final GlobalKey<ConfirmButtonState> key = GlobalKey();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController confirmController = TextEditingController();
  final validCharacters = RegExp(r'^[a-zA-Z0-9_\-=@,\.;!\?]+$');

  String _checkReason(String input) {
    if (input.length > 0 && input.length < 6)
      return tr(LocaleKeys.resetPasswordPage_tooShort);
    if (input.length > 16) return tr(LocaleKeys.resetPasswordPage_tooLong);
    if (input.length > 0 && !isAsciiChar(validCharacters, input))
      return tr(LocaleKeys.resetPasswordPage_invalidChar);

    return '';
  }

  bool _verifyPw() {
    return passwordController.text.length > 5 &&
        passwordController.text.length < 17 &&
        isAsciiChar(validCharacters, passwordController.text);
  }

  bool _verifyConfirmPw() {
    return passwordController.text == confirmController.text;
  }

  bool _validateInput() {
    return confirmController.text.length > 0 &&
        passwordController.text.length > 0 &&
        _verifyConfirmPw();
  }

  void _clickNext() {
    if (_verifyConfirmPw()) {
      Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (_) => ProfilePage(
              username: widget.username, password: passwordController.text)));
    }
  }

  void _updateButtonState() {
    setState(() {});
  }

  @override
  void initState() {
    super.initState();

    confirmController.addListener(_updateButtonState);
    passwordController.addListener(_updateButtonState);
  }

  @override
  void dispose() {
    confirmController.dispose();
    passwordController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(tr(LocaleKeys.signupPage_signup)),
          centerTitle: true,
          elevation: 1.0,
        ),
        body: ListView(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(preAuthMargin, 30.0, 0.0, 0.0),
              child: Text(tr(LocaleKeys.signupPage_createPassword),
                  style: TextStyle(
                      fontSize: 24.0,
                      color: Colors.white,
                      fontWeight: FontWeight.w900)),
            ),
            ResetPasswordContent(
                buttonKey: key,
                pwUniqueController: passwordController,
                resetPwUniqueController: confirmController,
                buttonTitle: tr(LocaleKeys.common_confirmCap),
                buttonCallback: _clickNext,
                checkReason: _checkReason(passwordController.text),
                verifyPw: _verifyPw,
                verifyConfirmPw: _verifyConfirmPw,
                validatePassword: _validateInput,
                policyMargin: EdgeInsets.fromLTRB(
                    preAuthMargin, 8.0, preAuthMargin, 30.0)),
          ],
        ));
  }
}
