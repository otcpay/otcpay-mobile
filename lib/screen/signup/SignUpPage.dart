import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/auth/authenticate.dart';
import 'package:otcpay/component/Dropdown.dart';
import 'package:otcpay/component/Input.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/component/Wrapper.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/screen/signup/VerificationPage.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/style/Spec.dart';
import 'package:otcpay/util/ParamUtil.dart';

class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final GlobalKey<ConfigurableExpansionTileState> dropDownKey = GlobalKey();
  final GlobalKey<ConfirmButtonState> key = GlobalKey();
  final TextEditingController usernameController = TextEditingController();

  List<String> codes = ['852', '86', '1'];
  int code = 0;

  bool dirty = false;
  bool isPhone = true;
  bool checkedToC = true;

  AuthenticateBloc bloc;

  void _checkUser() {
    if (isValidAccount(usernameController.text)) {
      bloc.add(CheckAccount(usernameController.text));
    } else {
      _showAlert();
    }
  }

  void _getUsername() {
    if (!dirty) {
      usernameController.text = ModalRoute.of(context).settings.arguments;

      dirty = true;
    }
  }

  void toggleToC() {
    setState(() {
      checkedToC = !checkedToC;
    });
  }

  void toggleMode() {
    setState(() {
      isPhone = !isPhone;
    });
  }

  void _gotToTerms() {
    Navigator.pushNamed(context, '/terms');
  }

  void _dismissAlert() {
    Navigator.pop(context);
  }

  void _dismissAndNavigate() {
    _dismissAlert();

    ScaffoldMessenger.of(context).clearSnackBars();

    Navigator.pushNamed(context, '/login');
  }

  void _next() {
    try {
      // clear snackbar in best effort
      ScaffoldMessenger.of(context).clearSnackBars();
    } catch (e) {
      // do nothing
    }

    FocusScope.of(context).unfocus();

    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (_) => VerificationPage(
                username: usernameController.text,
                code: codes[code],
                isPhone: isPhone)));
  }

  void _showSnackbar(String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void _showAlert() {
    showDialog(
      context: context,
      builder: (BuildContext context) => Theme(
        data: ThemeData.dark(),
        child: CupertinoAlertDialog(
          title: NormalTitle(title: tr(LocaleKeys.signupPage_invalidMessage)),
          actions: [
            CupertinoDialogAction(
                isDefaultAction: true,
                child: NormalTitle(title: tr(LocaleKeys.common_ok)),
                onPressed: _dismissAlert)
          ],
        ),
      ),
    );
  }

  void _showLoginAlert() {
    showDialog(
      context: context,
      builder: (BuildContext context) => Theme(
        data: ThemeData.dark(),
        child: CupertinoAlertDialog(
          title: NormalTitle(title: tr(LocaleKeys.signupPage_registered)),
          actions: [
            CupertinoDialogAction(
                isDefaultAction: false,
                child: NormalTitle(title: tr(LocaleKeys.common_cancel)),
                onPressed: _dismissAlert),
            CupertinoDialogAction(
                isDefaultAction: false,
                child: NormalTitle(
                    title: tr(LocaleKeys.signupPage_login),
                    color: globalPrimaryColor),
                onPressed: _dismissAndNavigate)
          ],
        ),
      ),
    );
  }

  void _updateButtonState() {
    if (key.currentState != null) {
      key.currentState.setState(() {});
    }
  }

  void _setCode(int key) {
    setState(() {
      code = key;
    });

    dropDownKey.currentState.handleTap();
  }

  bool _validateInput() {
    return usernameController.text.length > 0 && checkedToC;
  }

  List<Widget> _codeFilter() {
    return codes
        .asMap()
        .map((key, value) => MapEntry(
            key,
            GestureDetector(
                onTap: () => _setCode(key),
                child: Container(
                    color: code == key ? labelColor : inputBoxBackgroundColor,
                    height: 28,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[NormalTitle(title: '+' + value)],
                    )))))
        .values
        .toList();
  }

  @override
  void initState() {
    super.initState();

    bloc = BlocProvider.of<AuthenticateBloc>(context);
    bloc.add(DismissLoading());

    usernameController.addListener(_updateButtonState);
  }

  @override
  void dispose() {
    usernameController.dispose();

    try {
      // clear snackbar in best effort
      ScaffoldMessenger.of(context).clearSnackBars();
    } catch (e) {
      // do nothing
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _getUsername();
    return BlocConsumer<AuthenticateBloc, AuthenticateState>(
        listener: (BuildContext context, AuthenticateState state) {
      if (state is AuthenticateError) _showSnackbar(state.message);
      if (state is CheckError) _showLoginAlert();
      if (state is Validated) _next();
    }, builder: (BuildContext context, AuthenticateState state) {
      return Scaffold(
          appBar: AppBar(
            title: Text(tr(LocaleKeys.signupPage_signup)),
            centerTitle: true,
            elevation: 1.0,
          ),
          body: LoadingWrapper(
            state: state,
            child: ListView(
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(
                      preAuthMargin, 30.0, preAuthMargin, 40.0),
                  child: Text(tr(LocaleKeys.signupPage_registration),
                      style: TextStyle(
                          fontSize: 24.0,
                          color: Colors.white,
                          fontWeight: FontWeight.w900)),
                ),
                InputTitle(
                    title: isPhone
                        ? tr(LocaleKeys.common_mobile)
                        : tr(LocaleKeys.common_email)),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    isPhone
                        ? Container(
                            width: 90,
                            child: Card(
                              color: inputBoxBackgroundColor,
                              margin: EdgeInsets.only(left: preAuthMargin),
                              child: Container(
                                padding: EdgeInsets.only(top: 10, bottom: 10),
                                child: ConfigurableExpansionTile(
                                    key: dropDownKey,
                                    animatedWidgetFollowingHeader: const Icon(
                                      Icons.arrow_drop_down,
                                      color: Colors.white,
                                    ),
                                    header: Flexible(
                                        child: NormalTitle(
                                            title: '+' + codes[code],
                                            color: Colors.white)),
                                    headerExpanded: NormalTitle(
                                        title: '+' + codes[code],
                                        color: Colors.white),
                                    headerBackgroundColorStart:
                                        Colors.transparent,
                                    expandedBackgroundColor: Colors.transparent,
                                    headerBackgroundColorEnd:
                                        Colors.transparent,
                                    children: _codeFilter()),
                              ),
                            ),
                          )
                        : Container(),
                    Expanded(
                        flex: 1,
                        child: InputBox(
                            controller: usernameController,
                            hintText: isPhone
                                ? tr(LocaleKeys.common_mobileInputHint)
                                : tr(LocaleKeys.common_emailInputHint),
                            margin: EdgeInsets.only(
                                left: isPhone ? 9 : preAuthMargin,
                                right: preAuthMargin)))
                  ],
                ),
                Container(
                  height: 30.0,
                  margin: EdgeInsets.only(
                      left: 10, top: 30, right: preAuthMargin, bottom: 5),
                  child: Row(
                    children: [
                      IconButton(
                          padding: EdgeInsets.zero,
                          iconSize: 25.0,
                          splashRadius: 15.0,
                          icon: Icon(Icons.check_circle_outline_outlined,
                              color: checkedToC ? Colors.green : Colors.white),
                          onPressed: toggleToC),
                      Container(
                        child: RichText(
                          text: TextSpan(children: [
                            TextSpan(
                              text: tr(LocaleKeys.loginPage_agree),
                              style: TextStyle(
                                  color: Colors.white, fontSize: 12.0),
                            ),
                            TextSpan(
                              text: tr(LocaleKeys.loginPage_toc),
                              recognizer: TapGestureRecognizer()
                                ..onTap = _gotToTerms,
                              style: TextStyle(
                                  color: Colors.blueAccent,
                                  fontSize: 12.0,
                                  decoration: TextDecoration.underline),
                            )
                          ]),
                        ),
                      )
                    ],
                  ),
                ),
                ConfirmButton(
                    key: key,
                    validator: _validateInput,
                    title: tr(LocaleKeys.common_sendVerification),
                    height: 50,
                    margin: EdgeInsets.only(
                        left: preAuthMargin, right: preAuthMargin),
                    callback: _checkUser),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 40, left: preAuthMargin),
                      child: TextButton(
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          onPressed: toggleMode,
                          child: NormalTitle(
                              title: isPhone
                                  ? tr(LocaleKeys.signupPage_switchToEmail)
                                  : tr(LocaleKeys.signupPage_switchToPhone),
                              color: globalPrimaryColor)),
                    )
                  ],
                ),
              ],
            ),
          ));
    });
  }
}
