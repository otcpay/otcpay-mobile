import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:otcpay/bloc/auth/authenticate.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/component/Dropdown.dart';
import 'package:otcpay/component/Input.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/component/Wrapper.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/model/Profile.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/style/Spec.dart';

class ProfilePage extends StatefulWidget {
  final String username;
  final String password;

  const ProfilePage({this.username = '', this.password = ''});

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  void _showSnackbar(String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void _next() {
    try {
      // clear snackbar in best effort
      ScaffoldMessenger.of(context).clearSnackBars();
    } catch (e) {
      // do nothing
    }

    Navigator.pushNamedAndRemoveUntil(context, '/home', (route) => false,
        arguments: 'success');
  }

  @override
  void initState() {
    super.initState();

    BlocProvider.of<AuthenticateBloc>(context).add(DismissLoading());
  }

  @override
  void dispose() {
    try {
      // clear snackbar in best effort
      ScaffoldMessenger.of(context).clearSnackBars();
    } catch (e) {
      // do nothing
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthenticateBloc, AuthenticateState>(
        listener: (BuildContext context, AuthenticateState state) {
      if (state is Authenticated) _next();
      if (state is AuthenticateError) _showSnackbar(state.message);
    }, builder: (BuildContext context, AuthenticateState state) {
      return Scaffold(
        appBar: AppBar(
          title: Text(tr(LocaleKeys.profilePage_profile)),
          centerTitle: true,
          elevation: 1.0,
        ),
        body: LoadingWrapper(
            state: state,
            child: ProfilePageContent(
                account: widget.username,
                username: widget.username,
                password: widget.password,
                buttonTitle: tr(LocaleKeys.profilePage_signupCap))),
      );
    });
  }
}

class ProfilePageContent extends StatefulWidget {
  final String account;
  final String username;
  final String region;
  final String whatsappId;
  final String wechatId;
  final String telegramId;
  final String password;
  final String buttonTitle;
  final String profilePic;
  final bool isRegister;

  const ProfilePageContent(
      {this.account = '',
      this.username = '',
      this.region = '',
      this.whatsappId = '',
      this.wechatId = '',
      this.telegramId = '',
      this.password = '',
      this.profilePic = '',
      @required this.buttonTitle,
      this.isRegister = true});

  @override
  _ProfilePageContentState createState() => _ProfilePageContentState();
}

class _ProfilePageContentState extends State<ProfilePageContent> {
  final GlobalKey<ConfirmButtonState> key = GlobalKey();
  final TextEditingController accountController = TextEditingController();
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController whatsappController = TextEditingController();
  final TextEditingController wechatController = TextEditingController();
  final TextEditingController telegramController = TextEditingController();
  final picker = ImagePicker();
  String _image;

  AuthenticateBloc bloc;

  bool dirty = false;

  String tempUsername = '';
  String tempWhatsapp = '';
  String tempWechat = '';
  String tempTelegram = '';

  String username = '';
  String password = '';
  String profilePic = '';
  String country = '';
  List<String> countries = ['Hong Kong', 'China', 'Taiwan', 'Macau'];

  Future _getImage(ImageSource source) async {
    Navigator.of(context).pop();
    final pickedFile = await picker.getImage(source: source);
    if (pickedFile != null) {
      final f = File(pickedFile.path);
      int sizeInBytes = f.lengthSync();
      double sizeInMb = sizeInBytes / (1024 * 1024);
      if (sizeInMb > 5) {
        _showSnackbar(tr(LocaleKeys.common_fileTooLarge));
        return;
      }

      setState(() {
        _image = pickedFile.path;
        dirty = true;
        profilePic = 'empty';
      });
    } else {
      print('No image selected.');
    }
  }

  void _onValueChange(dynamic newValue) {
    setState(() {
      dirty = true;
      country = newValue;
    });
  }

  void _register() {
    if (usernameController.text.length <= 18) {
      if (_validateUsername()) {
        bloc.add(Register(
            account: accountController.text,
            password: password,
            imagePath: _image,
            username: usernameController.text,
            region: country,
            whatsappId: whatsappController.text,
            wechatId: wechatController.text,
            telegramId: telegramController.text));
      } else {
        _showSnackbar(tr(LocaleKeys.profilePage_invalidUsername));
      }
    } else {
      _showUsernameAlert();
    }
  }

  void _showUsernameAlert() {
    showDialog(
      context: context,
      builder: (BuildContext context) => Theme(
        data: ThemeData.dark(),
        child: CupertinoAlertDialog(
          title: NormalTitle(title: tr(LocaleKeys.profilePage_usernameTooLong)),
          actions: [
            CupertinoDialogAction(
                isDefaultAction: false,
                child: NormalTitle(title: tr(LocaleKeys.common_ok)),
                onPressed: _dismiss),
          ],
        ),
      ),
    );
  }

  void _updateProfile() {
    BlocProvider.of<GeneralBloc>(context).add(UpdateProfile(
        userId: BlocProvider.of<AuthenticateBloc>(context).repo.userId,
        profile: Profile(
            profilePic: profilePic == 'empty' ? _image : profilePic,
            username: usernameController.text,
            region: country,
            whatsappId: whatsappController.text,
            wechatId: wechatController.text,
            telegramId: telegramController.text)));
  }

  void _dismiss() {
    Navigator.of(context).pop();
  }

  void _choosePhotoSource() {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        backgroundColor: inputBoxBackgroundColor,
        context: context,
        builder: (context) {
          return Container(
            height: 175,
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: labelColor,
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(8),
                          topLeft: Radius.circular(8))),
                  child: ListTile(
                    onTap: () => _getImage(ImageSource.camera),
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        BoldTitle(
                            title: tr(LocaleKeys.common_camera), fontSize: 16),
                      ],
                    ),
                  ),
                ),
                Container(
                  color: labelColor,
                  margin: EdgeInsets.only(bottom: 5),
                  child: ListTile(
                    onTap: () => _getImage(ImageSource.gallery),
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        BoldTitle(
                            title: tr(LocaleKeys.common_gallery), fontSize: 16),
                      ],
                    ),
                  ),
                ),
                Container(
                  color: labelColor,
                  child: ListTile(
                    onTap: _dismiss,
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        BoldTitle(
                            title: tr(LocaleKeys.common_cancel),
                            fontSize: 16,
                            color: cancelColor),
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  void _usernameUpdateButtonState() {
    if (tempUsername != usernameController.text) {
      tempUsername = usernameController.text;

      setState(() {
        dirty = true;
      });
    }
  }

  void _whatsappUpdateButtonState() {
    if (tempWhatsapp != whatsappController.text) {
      tempWhatsapp = whatsappController.text;

      setState(() {
        dirty = true;
      });
    }
  }

  void _wechatUpdateButtonState() {
    if (tempWechat != wechatController.text) {
      tempWechat = wechatController.text;

      setState(() {
        dirty = true;
      });
    }
  }

  void _telegramUpdateButtonState() {
    if (tempTelegram != telegramController.text) {
      tempTelegram = telegramController.text;

      setState(() {
        dirty = true;
      });
    }
  }

  void _showSnackbar(String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  bool _validateInput() {
    return usernameController.text.length > 0 &&
        accountController.text.length > 0 &&
        country.length > 0 &&
        dirty;
  }

  bool _validateUsername() {
    return usernameController.text != accountController.text;
  }

  Widget _requiredTitle(String title) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        InputTitle(title: title),
        Container(
            margin: EdgeInsets.only(right: preAuthMargin, bottom: 10),
            child: SubTitle(
                title: tr(LocaleKeys.common_required), color: labelColor))
      ],
    );
  }

  @override
  void initState() {
    super.initState();

    bloc = BlocProvider.of<AuthenticateBloc>(context);

    countries = new Map<String, dynamic>.from(
            BlocProvider.of<GeneralBloc>(context).mapping['mapping'])
        .keys
        .toList();

    accountController.text = widget.account;
    usernameController.text = widget.username;
    country = widget.region;
    password = widget.password;
    profilePic = widget.profilePic;
    whatsappController.text = widget.whatsappId;
    wechatController.text = widget.wechatId;
    telegramController.text = widget.telegramId;

    tempUsername = widget.username;
    tempWhatsapp = widget.whatsappId;
    tempWechat = widget.wechatId;
    tempTelegram = widget.telegramId;

    usernameController.addListener(_usernameUpdateButtonState);
    whatsappController.addListener(_whatsappUpdateButtonState);
    wechatController.addListener(_wechatUpdateButtonState);
    telegramController.addListener(_telegramUpdateButtonState);
  }

  @override
  void dispose() {
    accountController.dispose();
    usernameController.dispose();
    whatsappController.dispose();
    wechatController.dispose();
    telegramController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FixedBottomButtonWrapper(
      children: [
        Container(
          height: 150,
          margin: EdgeInsets.only(bottom: 20, left: 20, right: 20),
          child: Stack(
            fit: StackFit.expand,
            clipBehavior: Clip.none,
            children: [
              Positioned(
                  left: 0,
                  right: 0,
                  top: 40,
                  child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0)),
                    color: inputBoxBackgroundColor,
                    elevation: 4.0,
                    child: Container(height: 110),
                  )),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    margin: EdgeInsets.only(bottom: 15.0),
                    child: TextButton(
                      style: ButtonStyle(
                          padding: MaterialStateProperty.all(EdgeInsets.only(
                              left: preAuthMargin, right: preAuthMargin)),
                          shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0))),
                          backgroundColor: MaterialStateColor.resolveWith(
                              (states) => labelColor)),
                      onPressed: _choosePhotoSource,
                      child: SubTitle(
                          title: tr(LocaleKeys.profilePage_changePhoto),
                          fontSize: 12),
                    ),
                  )),
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                      margin: EdgeInsets.only(top: 10),
                      child: (_image == null || _image == '') &&
                              (profilePic == '' || profilePic == null)
                          ? Image.asset('icons/account_photo.png', height: 70)
                          : ClipRRect(
                              borderRadius: BorderRadius.circular(8),
                              child: profilePic != '' && profilePic != 'empty'
                                  ? Image.network(
                                      profilePic + '?alt=media',
                                      height: 70,
                                    )
                                  : Image.file(
                                      File(_image),
                                      height: 70,
                                    ))))
            ],
          ),
        ),
        _requiredTitle(tr(LocaleKeys.profilePage_account)),
        InputBox(
            margin: EdgeInsets.only(
                left: preAuthMargin, right: preAuthMargin, bottom: 10),
            controller: accountController,
            readOnly: true,
            hintText: ''),
        _requiredTitle(tr(LocaleKeys.profilePage_username)),
        InputBox(
            margin: EdgeInsets.only(
                left: preAuthMargin, right: preAuthMargin, bottom: 10),
            controller: usernameController,
            hintText: tr(LocaleKeys.profilePage_usernameHint)),
        _requiredTitle(tr(LocaleKeys.profilePage_region)),
        Container(
          margin: EdgeInsets.only(
              left: preAuthMargin, right: preAuthMargin, bottom: 20),
          child: Material(
            elevation: 2,
            child: DropDownField(
                showIcon: true,
                value: country,
                textStyle: TextStyle(color: Colors.white, fontSize: 14),
                required: false,
                hintText: tr(LocaleKeys.profilePage_regionHint),
                hintStyle: TextStyle(color: subTitleColor, fontSize: 14.0),
                items: countries,
                onValueChanged: _onValueChange),
          ),
        ),
        Container(
            margin: EdgeInsets.only(
                left: preAuthMargin, right: preAuthMargin, bottom: 5),
            child: Divider(color: dividerColor, thickness: 2)),
        InputTitle(title: tr(LocaleKeys.profilePage_whatsapp)),
        InputBox(
            margin: EdgeInsets.only(
                left: preAuthMargin, right: preAuthMargin, bottom: 10),
            controller: whatsappController,
            hintText: tr(LocaleKeys.profilePage_whatsappHint)),
        InputTitle(title: tr(LocaleKeys.profilePage_wechat)),
        InputBox(
            margin: EdgeInsets.only(
                left: preAuthMargin, right: preAuthMargin, bottom: 10),
            controller: wechatController,
            hintText: tr(LocaleKeys.profilePage_wechatHint)),
        InputTitle(title: tr(LocaleKeys.profilePage_telegram)),
        InputBox(
            margin: EdgeInsets.only(
                left: preAuthMargin, right: preAuthMargin, bottom: 10),
            controller: telegramController,
            hintText: tr(LocaleKeys.profilePage_telegramHint))
      ],
      button: ConfirmButton(
          key: key,
          validator: _validateInput,
          title: widget.buttonTitle,
          height: 50,
          margin: EdgeInsets.only(
              left: preAuthMargin, right: preAuthMargin, bottom: 20, top: 30),
          callback: widget.isRegister ? _register : _updateProfile),
    );
  }
}
