import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:otcpay/api/service.dart';
import 'package:otcpay/component/VerificationCode.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/screen/signup/PasswordPage.dart';

class VerificationPage extends StatefulWidget {
  final bool isPhone;
  final String username;
  final String code;

  const VerificationPage(
      {@required this.username, @required this.isPhone, this.code});

  @override
  _VerificationPageState createState() => _VerificationPageState();
}

class _VerificationPageState extends State<VerificationPage> {
  bool dirty = false;

  void _clickNext() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (_) => PasswordPage(username: widget.username)));
  }

  String _getUsername() {
    if (widget.isPhone) {
      return widget.code + widget.username;
    }

    return widget.username;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(tr(LocaleKeys.signupPage_signup)),
          centerTitle: true,
          elevation: 1.0,
        ),
        body: VerificationCode(
          callback: _clickNext,
          account: _getUsername(),
          showClose: false,
          needFullWidth: true,
          type: EmailType.VERIFICATION,
        ));
  }
}
