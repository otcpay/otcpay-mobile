import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:otcpay/bloc/auth/authenticate_bloc.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/bloc/proof/proof.dart';
import 'package:otcpay/component/Container.dart';
import 'package:otcpay/component/Indicator.dart';
import 'package:otcpay/component/Input.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/component/Wrapper.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:otcpay/model/InvoicePayment.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/style/Spec.dart';

import 'UploadPage.dart';

enum ProofChoice { idCard, passport }

class VerifyPage extends StatefulWidget {
  @override
  _VerifyPageState createState() => _VerifyPageState();
}

class _VerifyPageState extends State<VerifyPage> {
  void _previous() {
    Navigator.pushNamedAndRemoveUntil(context, '/home', (route) => false);
  }

  List<Widget> _topDescription() {
    return [
      Container(
        margin: EdgeInsets.only(top: 30, bottom: 5, left: preAuthMargin),
        child: BoldTitle(
            title: tr(LocaleKeys.verifyPage_purposeCheck), fontSize: 24),
      ),
      Container(
        margin: EdgeInsets.only(left: preAuthMargin),
        child: SubTitle(
          title: tr(LocaleKeys.verifyPage_idVerificationDescription1),
          color: subTitleColor,
          fontSize: 12,
        ),
      ),
      Container(
        margin: EdgeInsets.only(left: preAuthMargin),
        child: SubTitle(
          title: tr(LocaleKeys.verifyPage_idVerificationDescription2),
          color: subTitleColor,
          fontSize: 12,
        ),
      ),
      Container(
        margin: EdgeInsets.only(top: 15, left: preAuthMargin),
        child: BoldTitle(
            title: tr(LocaleKeys.verifyPage_chooseIdType), fontSize: 14),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: _previous,
            icon: Icon(Icons.arrow_back, color: Colors.white),
          ),
          title: Text(tr(LocaleKeys.verifyPage_idVerificationTitle)),
          centerTitle: true,
          elevation: 1.0,
        ),
        body: VerifyPageContent(
            topDescription: _topDescription(),
            buttonTitle: tr(LocaleKeys.common_save)));
  }
}

class VerifyPageContent extends StatefulWidget {
  final List<Widget> topDescription;
  final void Function() callback;
  final String buttonTitle;
  final bool overrideState;
  final bool showOptions;

  const VerifyPageContent(
      {@required this.topDescription,
      @required this.buttonTitle,
      this.callback,
      this.overrideState = false,
      this.showOptions = true});

  @override
  _VerifyPageContentState createState() => _VerifyPageContentState();
}

class _VerifyPageContentState extends State<VerifyPageContent> {
  final picker = ImagePicker();

  ProofChoice _choice = ProofChoice.idCard;

  String path;

  ProofBloc bloc;

  Future _getImage(ProofType type) async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      final f = File(pickedFile.path);
      int sizeInBytes = f.lengthSync();
      double sizeInMb = sizeInBytes / (1024 * 1024);
      if (sizeInMb > 5) {
        _showSnackbar(tr(LocaleKeys.common_fileTooLarge));
        return;
      }

      bloc.add(Proving(path: pickedFile.path, proofType: type));
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  UploadPage(title: _getTitle(type), subType: _choice)));
    } else {
      print('No image selected.');
    }
  }

  Future _getInvoice() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      setState(() {
        path = pickedFile.path;
      });
    } else {
      print('No image selected.');
    }
  }

  String _getTitle(ProofType type) {
    if (type == ProofType.identity) {
      return _choice == ProofChoice.passport
          ? tr(LocaleKeys.verifyPage_passport)
          : tr(LocaleKeys.verifyPage_idCard);
    } else if (type == ProofType.address) {
      return tr(LocaleKeys.verifyPage_proofAddress);
    }

    return tr(LocaleKeys.verifyPage_bankDetail);
  }

  void _showSnackbar(String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void _toggle(ProofChoice newProofChoice) {
    setState(() {
      bloc.add(ClearProof());
      _choice = newProofChoice;
    });
  }

  void _identityProofPage() {
    if (bloc.repo.idProof.image != null && bloc.repo.idProof.image.isNotEmpty) {
      bloc.add(Proving(
          path: bloc.repo.idProof.image, proofType: ProofType.identity));
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => UploadPage(
                  title: _getTitle(ProofType.identity), subType: _choice)));
    } else {
      _getImage(ProofType.identity);
    }
  }

  void _addressProofPage() {
    if (bloc.repo.addressProof.image != null &&
        bloc.repo.addressProof.image.isNotEmpty) {
      bloc.add(Proving(
          path: bloc.repo.addressProof.image, proofType: ProofType.address));
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => UploadPage(
                  title: _getTitle(ProofType.address), subType: _choice)));
    } else {
      _getImage(ProofType.address);
    }
  }

  void _updateProof() {
    if (bloc.state is AllProved || bloc.state is ProvingInfo) {
      bloc.add(UpdateProof(
          userId: BlocProvider.of<AuthenticateBloc>(context).repo.userId,
          callback: _uploadCallback));
    }
  }

  void _uploadCallback() {
    _showSnackbar(tr(LocaleKeys.verifyPage_uploadSuccess));

    Future.delayed(const Duration(milliseconds: 1500), () {
      try {
        // clear snackbar in best effort
        ScaffoldMessenger.of(context).clearSnackBars();
      } catch (e) {
        // do nothing
      }

      Navigator.of(context).pushNamedAndRemoveUntil('/home', (route) => false);
    });
  }

  void _confirm() {
    if (widget.overrideState) {
      BlocProvider.of<GeneralBloc>(context).add(
          ProceedToNext(step: 3, payment: InvoicePayment(invoiceImage: path)));
      widget.callback();
    } else {
      _updateProof();
    }
  }

  bool _isIdentityProvedState(ProofState state) {
    return bloc.repo.idProof != null &&
        bloc.repo.idProof.detail != null &&
        bloc.repo.idProof.detail.length > 0;
  }

  bool _isAddressProvedState(ProofState state) {
    return bloc.repo.addressProof != null &&
        bloc.repo.addressProof.detail != null &&
        bloc.repo.addressProof.detail.length > 0;
  }

  bool _canSave() {
    if (widget.overrideState) return true;

    return _isIdentityProvedState(bloc.state) &&
        _isAddressProvedState(bloc.state);
  }

  Widget _radioButton(ProofChoice choice) {
    return ProofRadioButton(
      proofValue: choice,
      proofGroupValue: _choice,
      proofCallback: _toggle,
    );
  }

  List<Widget> _bottomWidget() {
    List<Widget> widgets = [];

    if (!widget.overrideState) {
      widgets.add(Container(
        margin: EdgeInsets.only(
            left: preAuthMargin, right: preAuthMargin, bottom: 20),
        child: SubTitle(
            color: subTitleColor, title: tr(LocaleKeys.verifyPage_protectData)),
      ));
    } else {
      widgets.addAll([
        Container(
          margin: EdgeInsets.only(bottom: 20, left: preAuthMargin),
          child: Row(
            children: [
              BoldTitle(
                  title: tr(LocaleKeys.verifyPage_purposeCheck), fontSize: 14),
              OptionalIndicator()
            ],
          ),
        ),
        DotLineContainer(
          showMask: path != null && path.isNotEmpty,
          callback: _getInvoice,
          margin: EdgeInsets.only(
              left: preAuthMargin, right: preAuthMargin, bottom: 30),
          height: 90,
          image: Image.asset('icons/invoice_detail.png'),
          title: tr(LocaleKeys.verifyPage_uploadInvoiceTitle),
          subTitle: tr(LocaleKeys.verifyPage_uploadInvoiceSubTitle),
        ),
      ]);
    }

    return widgets;
  }

  @override
  void initState() {
    super.initState();

    bloc = BlocProvider.of<ProofBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ProofBloc, ProofState>(
        listener: (BuildContext context, ProofState state) {
      if (state is UpdateError) _showSnackbar(state.message);
    }, builder: (BuildContext context, ProofState state) {
      return LoadingWrapper(
        proofState: state,
        isLoadingState: Updating,
        child: FixedBottomButtonWrapper(
          children: [
            ...widget.topDescription,
            widget.showOptions
                ? Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        width: 110,
                        child: ListTile(
                          dense: true,
                          contentPadding: EdgeInsets.zero,
                          leading: _radioButton(ProofChoice.idCard),
                          title: Align(
                            child: NormalTitle(
                                title: tr(LocaleKeys.verifyPage_idCard),
                                fontSize: 14,
                                color: _choice == ProofChoice.idCard
                                    ? globalPrimaryColor
                                    : Colors.white),
                            alignment: Alignment(-25, 0),
                          ),
                        ),
                      ),
                      Container(
                        width: 130,
                        child: ListTile(
                          dense: true,
                          contentPadding: EdgeInsets.zero,
                          leading: _radioButton(ProofChoice.passport),
                          title: Align(
                            child: NormalTitle(
                                title: tr(LocaleKeys.verifyPage_passport),
                                fontSize: 14,
                                color: _choice == ProofChoice.passport
                                    ? globalPrimaryColor
                                    : Colors.white),
                            alignment: Alignment(-5, 0),
                          ),
                        ),
                      )
                    ],
                  )
                : Container(),
            DotLineContainer(
              showMask: _isIdentityProvedState(state),
              callback: _identityProofPage,
              margin: EdgeInsets.only(
                  left: preAuthMargin, right: preAuthMargin, bottom: 30),
              height: 100,
              image: Image.asset('icons/identity_proof.png'),
              title: tr(LocaleKeys.verifyPage_uploadIdTitle),
              subTitle: tr(LocaleKeys.verifyPage_uploadIdSubTitle),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 20, left: preAuthMargin),
              child: BoldTitle(
                  title: tr(LocaleKeys.verifyPage_proofAddress), fontSize: 14),
            ),
            DotLineContainer(
              showMask: _isAddressProvedState(state),
              callback: _addressProofPage,
              margin: EdgeInsets.only(
                  left: preAuthMargin, right: preAuthMargin, bottom: 30),
              height: 100,
              image: Image.asset('icons/address_proof.png'),
              title: tr(LocaleKeys.verifyPage_uploadAddressTitle),
              subTitle: tr(LocaleKeys.verifyPage_uploadAddressSubTitle),
            ),
            ..._bottomWidget(),
          ],
          button: ConfirmButton(
              validator: _canSave,
              title: widget.buttonTitle,
              height: 50,
              margin: EdgeInsets.only(
                  left: preAuthMargin,
                  right: preAuthMargin,
                  bottom: 30,
                  top: 30),
              callback: _confirm),
        ),
      );
    });
  }
}
