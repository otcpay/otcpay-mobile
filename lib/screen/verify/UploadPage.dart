import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:otcpay/bloc/proof/proof.dart';
import 'package:otcpay/component/Input.dart';
import 'package:otcpay/component/Title.dart';
import 'package:otcpay/component/Wrapper.dart';
import 'package:otcpay/style/Color.dart';
import 'package:otcpay/style/Spec.dart';

import 'VerifyPage.dart';

class UploadPage extends StatefulWidget {
  final String title;
  final ProofChoice subType;

  const UploadPage({@required this.title, @required this.subType});

  @override
  _UploadPageState createState() => _UploadPageState();
}

class _UploadPageState extends State<UploadPage> {
  final GlobalKey<ConfirmButtonState> key = GlobalKey();
  final TextEditingController numberController = TextEditingController();
  final TextEditingController houseController = TextEditingController();
  final TextEditingController streetController = TextEditingController();
  final TextEditingController cityController = TextEditingController();
  final TextEditingController postalCodeController = TextEditingController();
  final picker = ImagePicker();

  ProofBloc bloc;

  ProofType type;

  void _updateButtonState() {
    if (key.currentState != null) {
      key.currentState.setState(() {});
    }
  }

  bool _validateInput() {
    if (type == ProofType.identity) {
      return numberController.text.length > 0;
    }

    if (type == ProofType.address) {
      return houseController.text.length > 0 &&
          streetController.text.length > 0 &&
          cityController.text.length > 0;
    }

    return false;
  }

  String _getInputTitle() {
    return widget.subType == ProofChoice.passport
        ? 'Passport Number'
        : 'ID Card Number';
  }

  Widget _renderContainer(ProofState state) {
    if (state is ProvingInfo) {
      String path = state.path;
      if (path != null && path.contains('http')) {
        return Image.network(path + '?alt=media',
            fit: type == ProofType.identity
                ? BoxFit.fitHeight
                : BoxFit.fitWidth);
      }

      return Image.file(File(path ?? ''),
          fit: type == ProofType.identity ? BoxFit.fitHeight : BoxFit.fitWidth);
    }

    if ((state is ProvedIdentity) && type == ProofType.identity) {
      return Image.file(File(state.idImage ?? ''), fit: BoxFit.fitHeight);
    }

    if ((state is AllProved) && type == ProofType.identity) {
      String path = bloc.repo.idProof.image;
      if (path != null && path.contains('http')) {
        return Image.network(path + '?alt=media', fit: BoxFit.fitHeight);
      }

      return Image.file(File(path ?? ''), fit: BoxFit.fitHeight);
    }

    if ((state is ProvedAddress) && type == ProofType.address) {
      return Image.file(File(state.addressImage ?? ''), fit: BoxFit.fitWidth);
    }

    if ((state is AllProved) && type == ProofType.address) {
      String path = bloc.repo.addressProof.image;
      if (path != null && path.contains('http')) {
        return Image.network(path + '?alt=media', fit: BoxFit.fitWidth);
      }
      return Image.file(File(path ?? ''), fit: BoxFit.fitWidth);
    }

    return null;
  }

  String _getAddress() {
    if (houseController.text != null &&
        houseController.text.length > 0 &&
        streetController.text != null &&
        streetController.text.length > 0 &&
        cityController.text != null &&
        cityController.text.length > 0) {
      return houseController.text +
          ' ' +
          streetController.text +
          ' ' +
          cityController.text;
    }

    return '';
  }

  Future _getImage(ImageSource source) async {
    Navigator.of(context).pop();
    final pickedFile = await picker.getImage(source: source);
    if (pickedFile != null) {
      final f = File(pickedFile.path);
      int sizeInBytes = f.lengthSync();
      double sizeInMb = sizeInBytes / (1024 * 1024);
      if (sizeInMb > 5) {
        _showSnackbar('File must be smaller than 5MB');
        return;
      }

      bloc.add(Proving(path: pickedFile.path, proofType: type));
    } else {
      print('No image selected.');
    }
  }

  void _showSnackbar(String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  void _dismiss() {
    Navigator.of(context).pop();
  }

  void _save() {
    type = (bloc.state as ProvingInfo).proofType;
    String path = (bloc.state as ProvingInfo).path;
    Proved event;
    if (type == ProofType.identity) {
      event = Proved(
          idImage: path, idNumber: numberController.text, proofType: type);
    } else if (type == ProofType.address) {
      event =
          Proved(addressImage: path, address: _getAddress(), proofType: type);
    }

    bloc.add(event);

    _dismiss();
  }

  void _choosePhotoSource() {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        backgroundColor: inputBoxBackgroundColor,
        context: context,
        builder: (context) {
          return Container(
            height: 175,
            child: Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                      color: labelColor,
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(8),
                          topLeft: Radius.circular(8))),
                  child: ListTile(
                    onTap: () => _getImage(ImageSource.camera),
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        BoldTitle(title: 'Camera', fontSize: 16),
                      ],
                    ),
                  ),
                ),
                Container(
                  color: labelColor,
                  margin: EdgeInsets.only(bottom: 5),
                  child: ListTile(
                    onTap: () => _getImage(ImageSource.gallery),
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        BoldTitle(title: 'Select Photos', fontSize: 16),
                      ],
                    ),
                  ),
                ),
                Container(
                  color: labelColor,
                  child: ListTile(
                    onTap: _dismiss,
                    title: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        BoldTitle(
                            title: 'Cancel', fontSize: 16, color: cancelColor),
                      ],
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  Widget _changePhotoButton() {
    return Positioned(
        bottom: 5,
        right: 10,
        child: Container(
          child: TextButton(
            style: ButtonStyle(
                padding: MaterialStateProperty.all(
                    EdgeInsets.only(left: 10, right: 10)),
                shape: MaterialStateProperty.all(RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4.0))),
                backgroundColor:
                    MaterialStateColor.resolveWith((states) => labelColor)),
            onPressed: _choosePhotoSource,
            child: SubTitle(title: 'Replace Photo', fontSize: 12),
          ),
        ));
  }

  List<Widget> _children(ProofState state) {
    List<Widget> children = [];

    if (type == ProofType.identity) {
      children.addAll([
        Container(
            height: 215,
            margin: EdgeInsets.all(preAuthMargin),
            child: Stack(
              children: [
                Positioned.fill(child: _renderContainer(state)),
                _changePhotoButton(),
              ],
            )),
        InputTitle(title: _getInputTitle()),
        InputBox(
            margin: EdgeInsets.only(
                left: preAuthMargin, right: preAuthMargin, bottom: 10),
            controller: numberController,
            hintText: 'K1234567890')
      ]);
    }

    if (type == ProofType.address) {
      children.addAll([
        Container(
            height: 415,
            margin: EdgeInsets.all(preAuthMargin),
            child: Stack(
              children: [
                Positioned.fill(child: _renderContainer(state)),
                _changePhotoButton(),
              ],
            )),
        InputTitle(title: 'Address'),
        InputBox(
            margin: EdgeInsets.only(
                left: preAuthMargin, right: preAuthMargin, bottom: 5),
            controller: houseController,
            hintText: 'Apt / House No'),
        InputBox(
            margin: EdgeInsets.only(
                left: preAuthMargin, right: preAuthMargin, bottom: 5),
            controller: streetController,
            hintText: 'Street Address'),
        InputBox(
            margin: EdgeInsets.only(
                left: preAuthMargin, right: preAuthMargin, bottom: 5),
            controller: cityController,
            hintText: 'City'),
        InputBox(
            margin: EdgeInsets.only(
                left: preAuthMargin, right: preAuthMargin, bottom: 10),
            controller: postalCodeController,
            hintText: 'Postal Code (optional)')
      ]);
    }

    return children;
  }

  @override
  void initState() {
    super.initState();

    bloc = BlocProvider.of<ProofBloc>(context);
    if (bloc.state is ProvingInfo) {
      type = (bloc.state as ProvingInfo).proofType;
    }

    if (bloc.repo.idProof.detail != null) {
      numberController.text = bloc.repo.idProof.detail;
    }

    numberController.addListener(_updateButtonState);
    houseController.addListener(_updateButtonState);
    streetController.addListener(_updateButtonState);
    cityController.addListener(_updateButtonState);
    postalCodeController.addListener(_updateButtonState);
  }

  @override
  void dispose() {
    numberController.dispose();
    houseController.dispose();
    streetController.dispose();
    cityController.dispose();
    postalCodeController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<ProofBloc, ProofState>(
        listener: (BuildContext context, ProofState state) {
      if (state is ProvingInfo) {
        type = (bloc.state as ProvingInfo).proofType;
      }
    }, builder: (BuildContext context, ProofState state) {
      return Scaffold(
          appBar: AppBar(
            leading: IconButton(
              onPressed: _dismiss,
              icon: Icon(Icons.arrow_back, color: Colors.white),
            ),
            title: Text(widget.title),
            centerTitle: true,
            elevation: 1.0,
          ),
          body: FixedBottomButtonWrapper(
            children: _children(state),
            button: ConfirmButton(
                key: key,
                validator: _validateInput,
                title: 'SAVE',
                height: 50,
                margin: EdgeInsets.only(
                    left: preAuthMargin,
                    right: preAuthMargin,
                    bottom: 30,
                    top: 30),
                callback: _save),
          ));
    });
  }
}
