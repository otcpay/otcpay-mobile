import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:otcpay/generated/locale_keys.g.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:introduction_screen/introduction_screen.dart';

import 'package:otcpay/style/Color.dart';

class OnBoardingPage extends StatefulWidget {
  @override
  _OnBoardingPageState createState() => _OnBoardingPageState();
}

class _OnBoardingPageState extends State<OnBoardingPage> {
  void _onIntroEnd() {
    Navigator.pushNamed(context, '/login', arguments: 'onFirstPage');

    SharedPreferences.getInstance().then((prefs) {
      prefs.setInt('stage', 1);
    });
  }

  Widget _buildImage(String assetName) {
    return Padding(
        padding: EdgeInsets.only(top: 100),
        child: Image.asset('icons/$assetName.png'));
  }

  @override
  Widget build(BuildContext context) {
    const bodyStyle = TextStyle(fontSize: 12.0, color: Colors.white);
    const pageDecoration = const PageDecoration(
      titleTextStyle: TextStyle(
          fontSize: 22.0,
          fontWeight: FontWeight.w800,
          color: globalPrimaryColor),
      bodyTextStyle: bodyStyle,
      descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 0.0),
      pageColor: globalBackgroundColor,
      imageFlex: 7,
      bodyFlex: 4,
      imagePadding: EdgeInsets.zero,
    );

    return IntroductionScreen(
      globalBackgroundColor: globalBackgroundColor,
      pages: [
        PageViewModel(
          title: "Cost Saving",
          body: "save 2.68% per global remittance",
          image: _buildImage('Image1'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Fastest Settlement",
          body: "Delivery international payments within the same day.",
          image: _buildImage('Image2'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Secured Daily Turnover",
          body: "Reached daily international transaction of HKD 25mio+",
          image: _buildImage('Image3'),
          decoration: pageDecoration,
        ),
      ],
      onDone: _onIntroEnd,
      skipFlex: 0,
      nextFlex: 0,
      done: Text(tr(LocaleKeys.common_done),
          style: TextStyle(
              fontWeight: FontWeight.w600, color: globalPrimaryColor)),
      dotsDecorator: const DotsDecorator(
        size: Size(22.0, 3.0),
        color: Colors.black38,
        activeColor: globalPrimaryColor,
        activeSize: Size(22.0, 3.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(22.0)),
        ),
        activeShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(22.0)),
        ),
      ),
    );
  }
}
