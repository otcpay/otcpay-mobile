import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/bloc/auth/authenticate_bloc.dart';
import 'package:otcpay/bloc/general/general.dart';
import 'package:otcpay/component/ListItem.dart';
import 'package:otcpay/component/Wrapper.dart';
import 'package:otcpay/generated/locale_keys.g.dart';

class NotificationPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  GeneralBloc bloc;

  void _allRead() {
    bloc.add(ReadAllNotification(
        BlocProvider.of<AuthenticateBloc>(context).repo.userId));
  }

  void _showSnackbar(String message) {
    final snackBar = SnackBar(content: Text(message));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  void initState() {
    super.initState();

    bloc = BlocProvider.of<GeneralBloc>(context);
  }

  @override
  void dispose() {
    try {
      // clear snackbar in best effort
      ScaffoldMessenger.of(context).clearSnackBars();
    } catch (e) {
      // do nothing
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<GeneralBloc, GeneralState>(
        listener: (BuildContext context, GeneralState state) {
      if (state is Error) _showSnackbar(state.message);
    }, builder: (BuildContext context, GeneralState state) {
      return Scaffold(
          appBar: AppBar(
            title: Text(tr(LocaleKeys.notificationPage_notification)),
            centerTitle: true,
            elevation: 1.0,
            actions: [
              TextButton(
                  onPressed: _allRead,
                  child: Text(
                    tr(LocaleKeys.notificationPage_allRead),
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12.0,
                    ),
                  )),
            ],
          ),
          body: LoadingWrapper(
              generalState: state,
              isLoadingState: LoadingNotification,
              child: ListView(
                  children: bloc.notifications
                      .map((e) => NotificationItem(
                          title: e.title,
                          body: e.body,
                          time: e.createdAt,
                          read: e.read))
                      .toList())));
    });
  }
}
