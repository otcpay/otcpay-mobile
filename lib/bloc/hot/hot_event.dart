import 'package:equatable/equatable.dart';

abstract class HotStateEvent extends Equatable {
  const HotStateEvent();

  @override
  List<Object> get props => [];
}

class InitialQuoteFetch extends HotStateEvent {
  final bool refresh;

  const InitialQuoteFetch({this.refresh});
}
