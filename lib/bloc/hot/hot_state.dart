import 'package:equatable/equatable.dart';

abstract class HotState extends Equatable {
  const HotState();

  @override
  List<Object> get props => [];
}

class QuoteLoading extends HotState {}

class FakeLoading extends HotState {}

class QuoteLoaded extends HotState {}
