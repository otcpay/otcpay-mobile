import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/model/InvoicePayment.dart';

import 'hot.dart';
import 'package:otcpay/api/settlement.dart';

class HotBloc extends Bloc<HotStateEvent, HotState> {
  HotBloc() : super(QuoteLoading());

  List<InvoicePayment> displayList = [];

  @override
  Stream<HotState> mapEventToState(HotStateEvent event) async* {
    if (event is InitialQuoteFetch) {
      yield* _fetch(event);
    }
  }

  Stream<HotState> _fetch(InitialQuoteFetch event) async* {
    if (displayList.isEmpty || event.refresh) {
      if (event.refresh) {
        yield FakeLoading();
      } else {
        yield QuoteLoading();
      }

      List<InvoicePayment> invoicePaymentList = await getInvoicePayment();
      displayList = invoicePaymentList;

      yield QuoteLoaded();
    }
  }
}
