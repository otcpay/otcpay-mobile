import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:otcpay/component/ListItem.dart';
import 'package:otcpay/model/Advertisement.dart';
import 'package:otcpay/screen/home/otc/header/MainFilter.dart';

import 'otc.dart';
import 'package:otcpay/api/advertisement.dart';

class OtcBloc extends Bloc<OtcStateEvent, OtcState> {
  OtcBloc() : super(Loading());

  List<Advertisement> fullList = [];
  List<Advertisement> displayList = [];

  RATE_MODE buySellStage = RATE_MODE.BUY;
  int currency = -1;
  int sorting = 0;
  String region = 'Hong Kong';
  String dollar = 'HKD';

  int lastFetchNum = 0;

  @override
  Stream<OtcState> mapEventToState(OtcStateEvent event) async* {
    if (event is InitialFetch) {
      yield* _fetch(event);
    } else if (event is ApplyFilter) {
      yield* _applyFilter(event);
    } else if (event is PostAd) {
      yield* _postAd(event);
    } else if (event is ApplySorting) {
      yield* _applySorting(event);
    } else if (event is FetchMore) {
      yield* _fetchMore(event);
    }
  }

  Stream<OtcState> _fetch(InitialFetch event) async* {
    if (event.refresh) {
      yield FakeLoading();
    } else {
      yield Loading();
    }

    List<Advertisement> data = await getAdvertisement();
    lastFetchNum = data.length;

    if (!event.refresh) {
      fullList = data;
      displayList.addAll(fullList);
    } else {
      fullList = data;
      _updateDisplayList();
    }

    yield Loaded();
  }

  Stream<OtcState> _fetchMore(FetchMore event) async* {
    yield Loading();

    List<Advertisement> data =
        await getPaginatedAdvertisement(fullList.last.updatedDate);
    lastFetchNum = data.length;
    fullList.addAll(data);
    _updateDisplayList();

    yield Loaded();
  }

  Stream<OtcState> _applyFilter(ApplyFilter event) async* {
    if (event.currency != currency ||
        event.buySellMode != buySellStage ||
        event.region != region ||
        (event.dollar != null &&
            event.dollar.isNotEmpty &&
            event.dollar != dollar)) {
      yield Loading();

      if (event.dollar == null || event.dollar.isEmpty) {
        currency = event.currency;
        buySellStage = event.buySellMode;
        region = event.region;
        dollar = event.regionCurrency;
      } else {
        dollar = event.dollar;
      }

      displayList = fullList
          .where((element) =>
              element.crytocurrency == options[currency] &&
              EnumToString.convertToString(buySellStage) == element.type &&
              element.region == region &&
              element.settleCurrency == dollar)
          .toList();

      _sortList();

      yield Loaded();
    }
  }

  Stream<OtcState> _applySorting(ApplySorting event) async* {
    if (sorting != event.sorting) {
      yield Loading();

      sorting = event.sorting;
      _sortList();

      yield Loaded();
    }
  }

  Stream<OtcState> _postAd(PostAd event) async* {
    yield Loading();

    event.advertisement.region = region;
    Advertisement newAd = await postAd(event.advertisement);

    currency =
        options.indexWhere((e) => e == event.advertisement.crytocurrency);
    buySellStage =
        EnumToString.fromString(RATE_MODE.values, event.advertisement.type);

    fullList.insert(0, newAd);
    displayList = fullList
        .where((element) =>
            element.crytocurrency == options[currency] &&
            EnumToString.convertToString(buySellStage) == element.type &&
            element.settleCurrency == dollar)
        .toList();

    _sortList();

    yield Loaded(isSuccess: true);
  }

  void _sortList() {
    if (sorting == 0) {
      List<Advertisement> activeList =
          displayList.where((e) => e.status != 'Expired').toList();
      List<Advertisement> expiredList =
          displayList.where((e) => e.status == 'Expired').toList();
      if (buySellStage == RATE_MODE.BUY) {
        activeList.sort(
            (a, b) => double.parse(a.offer).compareTo(double.parse(b.offer)));
        expiredList.sort(
            (a, b) => double.parse(a.offer).compareTo(double.parse(b.offer)));
      } else {
        activeList.sort(
            (a, b) => double.parse(b.offer).compareTo(double.parse(a.offer)));
        expiredList.sort(
            (a, b) => double.parse(b.offer).compareTo(double.parse(a.offer)));
      }
      displayList = [...activeList, ...expiredList];
    } else {
      displayList.sort((a, b) =>
          double.parse(b.updatedDate).compareTo(double.parse(a.updatedDate)));
    }
  }

  void _updateDisplayList() {
    displayList = fullList
        .where((element) =>
            element.crytocurrency == options[currency] &&
            EnumToString.convertToString(buySellStage) == element.type &&
            element.settleCurrency == dollar)
        .toList();

    _sortList();
  }
}
