import 'package:equatable/equatable.dart';

abstract class OtcState extends Equatable {
  const OtcState();

  @override
  List<Object> get props => [];
}

class Loading extends OtcState {}

class FakeLoading extends OtcState {}

class Loaded extends OtcState {
  final bool isSuccess;

  const Loaded({this.isSuccess});
}
