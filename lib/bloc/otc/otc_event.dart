import 'package:equatable/equatable.dart';
import 'package:otcpay/component/ListItem.dart';
import 'package:otcpay/model/Advertisement.dart';

abstract class OtcStateEvent extends Equatable {
  const OtcStateEvent();

  @override
  List<Object> get props => [];
}

class InitialFetch extends OtcStateEvent {
  final bool refresh;

  const InitialFetch(this.refresh);
}

class FetchMore extends OtcStateEvent {}

class ApplyFilter extends OtcStateEvent {
  final RATE_MODE buySellMode;
  final int currency;
  final String region;
  final String dollar;
  final String regionCurrency;

  const ApplyFilter(
      {this.buySellMode,
      this.currency,
      this.region,
      this.dollar,
      this.regionCurrency});
}

class ApplySorting extends OtcStateEvent {
  final int sorting;

  const ApplySorting({this.sorting});
}

class PostAd extends OtcStateEvent {
  final Advertisement advertisement;

  const PostAd(this.advertisement);
}
