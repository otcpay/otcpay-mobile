import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:otcpay/api/service.dart';

abstract class AuthenticateStateEvent extends Equatable {
  const AuthenticateStateEvent();

  @override
  List<Object> get props => [];
}

class Authenticate extends AuthenticateStateEvent {
  final String username;
  final String password;

  const Authenticate(this.username, this.password);

  @override
  String toString() => 'Authenticate { username: $username }';
}

class Validate extends AuthenticateStateEvent {}

class Logout extends AuthenticateStateEvent {}

class Register extends AuthenticateStateEvent {
  final String imagePath;
  final String account;
  final String password;
  final String username;
  final String region;
  final String whatsappId;
  final String wechatId;
  final String telegramId;

  const Register(
      {@required this.username,
      @required this.account,
      @required this.password,
      @required this.region,
      this.imagePath,
      this.wechatId,
      this.whatsappId,
      this.telegramId});

  @override
  String toString() => 'Register { username: $account }';
}

class CheckAccount extends AuthenticateStateEvent {
  final String username;

  const CheckAccount(this.username);

  @override
  String toString() => 'CheckAccount { username: $username }';
}

class ResetCheckAccount extends AuthenticateStateEvent {
  final String username;

  const ResetCheckAccount(this.username);

  @override
  String toString() => 'ResetCheckAccount { username: $username }';
}

class VerifyCode extends AuthenticateStateEvent {
  final String account;

  final String code;

  const VerifyCode({@required this.code, @required this.account});

  @override
  String toString() => 'VerifyCode { account: $account, code: $code }';
}

class SendCode extends AuthenticateStateEvent {
  final String account;
  final EmailType type;

  const SendCode({@required this.account, @required this.type});

  @override
  String toString() => 'SendCode { account: $account, type: $type }';
}

class ResetPw extends AuthenticateStateEvent {
  final String username;
  final String password;

  const ResetPw({@required this.username, @required this.password});

  @override
  String toString() => 'ResetPw { username: $username }';
}

class DismissLoading extends AuthenticateStateEvent {}
