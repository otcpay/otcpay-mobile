import 'package:equatable/equatable.dart';

abstract class AuthenticateState extends Equatable {
  const AuthenticateState();

  @override
  List<Object> get props => [];
}

class Authenticated extends AuthenticateState {
  final String detail;

  const Authenticated(this.detail);
}

class Authenticating extends AuthenticateState {}

class NotAuthenticated extends AuthenticateState {}

class AuthenticateError extends AuthenticateState {
  final String message;

  const AuthenticateError(this.message);
}

class Validating extends AuthenticateState {}

class Validated extends AuthenticateState {}

class CheckError extends AuthenticateState {}

class ValidateError extends AuthenticateState {
  final String message;

  const ValidateError(this.message);
}

class VerifyError extends AuthenticateState {
  final String message;

  const VerifyError(this.message);
}

class Verifying extends AuthenticateState {}

class Verified extends AuthenticateState {}

class Resetting extends AuthenticateState {}

class Reset extends AuthenticateState {}
