import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:otcpay/api/service.dart';
import 'package:otcpay/api/user.dart';
import 'package:otcpay/api/file.dart';
import 'package:otcpay/bloc/auth/authenticate.dart';
import 'package:otcpay/model/AuthenticationDetail.dart';
import 'package:otcpay/model/User.dart';
import 'package:otcpay/util/ParamUtil.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:random_string/random_string.dart';

class AuthenticateBloc extends Bloc<AuthenticateStateEvent, AuthenticateState> {
  AuthenticationDetail repo = AuthenticationDetail();

  AuthenticateBloc() : super(NotAuthenticated());

  final TwilioPhoneVerify twilioService = TwilioPhoneVerify();
  final EmailService emailService = EmailService();

  String tempCode = '';

  @override
  Stream<AuthenticateState> mapEventToState(
      AuthenticateStateEvent event) async* {
    if (event is Authenticate) {
      yield* _authenticate(event);
    } else if (event is Validate) {
      yield* _validate(event);
    } else if (event is Register) {
      yield* _register(event);
    } else if (event is CheckAccount) {
      yield* _checkUser(event);
    } else if (event is SendCode) {
      yield* _sendCode(event);
    } else if (event is VerifyCode) {
      yield* _verifyCode(event);
    } else if (event is DismissLoading) {
      yield* _dismissLoading(event);
    } else if (event is ResetCheckAccount) {
      yield* _checkAccount(event);
    } else if (event is ResetPw) {
      yield* _resetPw(event);
    } else if (event is Logout) {
      yield* _logout(event);
    }
  }

  Stream<AuthenticateState> _authenticate(Authenticate event) async* {
    yield Authenticating();
    try {
      AuthenticationDetail detail =
          await authenticate(event.username, event.password);

      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('token', detail.jwttoken);
      prefs.setString('username', event.username);
      prefs.setString('password', event.password);
      prefs.setString('userId', detail.userId);

      repo = detail;

      yield Authenticated(detail.jwttoken);
    } catch (e) {
      repo = AuthenticationDetail();

      print(e);
      if (e is FormatException) {
        yield AuthenticateError(e.message);
      } else if (e is String) {
        yield AuthenticateError(e);
      } else {
        yield AuthenticateError('Unknown error, please try again later');
      }
    }
  }

  Stream<AuthenticateState> _logout(Logout event) async* {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('token', 'empty');

    repo = AuthenticationDetail();

    yield NotAuthenticated();
  }

  Stream<AuthenticateState> _validate(Validate event) async* {
    yield Authenticating();
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token') ?? 'empty';
      print('token: ' + token);
      if (token != 'empty') {
        ValidateDetail detail = await validateToken(token);
        repo.jwttoken = token;
        repo.role = detail.role;
        repo.userId = detail.userId;

        yield Authenticated(token);
      } else {
        repo = AuthenticationDetail();

        yield NotAuthenticated();
      }
    } catch (e) {
      repo = AuthenticationDetail();

      print(e);
      if (e is FormatException) {
        yield ValidateError(e.message);
      } else if (e is String) {
        yield ValidateError(e);
      } else {
        yield ValidateError('Unknown error, please try again later');
      }
    }
  }

  Stream<AuthenticateState> _register(Register event) async* {
    yield Authenticating();
    try {
      String response = await uploadFile(event.imagePath, 'profile');
      String profilePic = response;

      User user = User(
          username: event.username,
          account: event.account,
          password: event.password,
          region: event.region,
          profilePic: profilePic,
          wechatId: event.wechatId,
          whatsappId: event.whatsappId,
          telegramId: event.telegramId);
      AuthenticationDetail detail = await register(user);

      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('token', detail.jwttoken);
      prefs.setString('username', event.username);
      prefs.setString('password', event.password);
      prefs.setString('userId', detail.userId);

      repo = detail;

      yield Authenticated(detail.jwttoken);
    } catch (e) {
      print(e);
      if (e is FormatException) {
        yield AuthenticateError(e.message);
      } else if (e is String) {
        yield AuthenticateError(e);
      } else {
        yield AuthenticateError('Unknown error, please try again later');
      }
    }
  }

  Stream<AuthenticateState> _checkUser(CheckAccount event) async* {
    yield Authenticating();
    try {
      bool result = await checkAccount(event.username);
      if (result) {
        // if true means user found
        yield CheckError();
      } else {
        yield Validated();
      }
    } catch (e) {
      print(e);
      if (e is FormatException) {
        yield AuthenticateError(e.message);
      } else if (e is String) {
        yield AuthenticateError(e);
      } else {
        yield AuthenticateError('Unknown error, please try again later');
      }
    }
  }

  Stream<AuthenticateState> _checkAccount(ResetCheckAccount event) async* {
    yield Validating();
    try {
      bool result = await checkAccount(event.username);
      if (result) {
        // if true means user found
        yield Validated();
      } else {
        yield AuthenticateError('Please enter a valid mobile or email');
      }
    } catch (e) {
      print(e);
      if (e is FormatException) {
        yield AuthenticateError(e.message);
      } else if (e is String) {
        yield AuthenticateError(e);
      } else {
        yield AuthenticateError('Unknown error, please try again later');
      }
    }
  }

  Stream<AuthenticateState> _verifyCode(VerifyCode event) async* {
    yield Verifying();
    try {
      if (isValidPhoneNum(event.account)) {
        var result =
            await twilioService.verifySmsCode(event.account, event.code);
        if (result['message'] == 'approved') {
          yield Verified();
        } else {
          if (result['message'].contains('Invalid parameter')) {
            yield VerifyError('Wrong code, please enter again');
          } else {
            yield VerifyError(result['message']);
          }
        }
      } else {
        print('tempCode: ' + tempCode);
        if (tempCode == event.code) {
          yield Verified();
          tempCode = '';
        } else {
          yield VerifyError('Code doesn\'t match');
        }
      }
    } catch (e, s) {
      print(e);
      print(s);
      if (e is FormatException) {
        yield VerifyError(e.message);
      } else if (e is String) {
        yield VerifyError(e);
      } else {
        yield VerifyError('Unknown error, please try again later');
      }
    }
  }

  Stream<AuthenticateState> _sendCode(SendCode event) async* {
    tempCode = '';

    try {
      if (isValidPhoneNum(event.account)) {
        var result = await twilioService.sendSmsCode(event.account);
        if (result['message'] != 'success') {
          yield VerifyError(
              'Failed to send verification code, please try again later');
        }
      } else {
        String code = randomNumeric(4);
        await emailService.sendCode(event.account, code, event.type);
        tempCode = code;
      }
    } catch (e, s) {
      print(e);
      print(s);
      if (e is FormatException) {
        yield VerifyError(e.message);
      } else if (e is String) {
        yield VerifyError(e);
      } else {
        yield VerifyError('Unknown error, please try again later');
      }
    }
  }

  Stream<AuthenticateState> _resetPw(ResetPw event) async* {
    yield Resetting();
    try {
      bool result = await resetPassword(event.username, event.password);
      if (result) {
        // if true means success
        yield Reset();
      } else {
        yield AuthenticateError(
            'Failed to reset password, please try again later');
      }
    } catch (e) {
      print(e);
      if (e is FormatException) {
        yield AuthenticateError(e.message);
      } else if (e is String) {
        yield AuthenticateError(e);
      } else {
        yield AuthenticateError('Unknown error, please try again later');
      }
    }
  }

  Stream<AuthenticateState> _dismissLoading(DismissLoading event) async* {
    yield NotAuthenticated();
  }
}
