import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:otcpay/bloc/proof/proof_event.dart';

abstract class ProofState extends Equatable {
  const ProofState();

  @override
  List<Object> get props => [];
}

class ProvingInfo extends ProofState {
  final String path;
  final ProofType proofType;

  const ProvingInfo({@required this.path, @required this.proofType});
}

class ProvedIdentity extends ProofState {
  final String idNumber;
  final String idImage;

  const ProvedIdentity({@required this.idImage, @required this.idNumber});
}

class ProvedAddress extends ProofState {
  final String address;
  final String addressImage;

  const ProvedAddress({@required this.address, @required this.addressImage});
}

class ProvedBank extends ProofState {
  final String bankNumber;
  final String bankImage;
  final String bankHolder;
  final String bankName;
  final String bankCode;

  const ProvedBank(
      {@required this.bankCode,
      @required this.bankHolder,
      @required this.bankImage,
      @required this.bankName,
      @required this.bankNumber});
}

class AllProved extends ProofState {
  final bool provedIdentity;
  final bool provedAddress;

  const AllProved(
      {this.provedAddress = false,
      this.provedIdentity = false});
}

class Updating extends ProofState {
  final bool provedIdentity;
  final bool provedAddress;

  const Updating(
      {this.provedAddress = false,
      this.provedIdentity = false});
}

class UpdateError extends ProofState {
  final String message;

  const UpdateError(this.message);
}
