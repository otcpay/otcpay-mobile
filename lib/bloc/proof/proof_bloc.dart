import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:otcpay/api/file.dart';
import 'package:otcpay/api/profile.dart';
import 'package:otcpay/bloc/proof/proof.dart';
import 'package:otcpay/model/Profile.dart';

class ProofBloc extends Bloc<ProofStateEvent, ProofState> {
  Profile repo =
      Profile(idProof: Proof(), addressProof: Proof(), wallets: [], banks: []);

  ProofBloc() : super(AllProved());

  @override
  Stream<ProofState> mapEventToState(ProofStateEvent event) async* {
    if (event is Proving) {
      yield* _proving(event);
    } else if (event is Proved) {
      yield* _proved(event);
    } else if (event is UpdateProof) {
      yield* _updateProof(event);
    } else if (event is GetProfile) {
      yield* _getProfile(event);
    } else if (event is ClearProfile) {
      yield* _clear(event);
    } else if (event is UpdateWallets) {
      yield* _updateWallets(event);
    } else if (event is UpdateBanks) {
      yield* _updateBanks(event);
    } else if (event is AddBank) {
      yield* _addBank(event);
    } else if (event is AddWallet) {
      yield* _addWallet(event);
    } else if (event is DeleteBank) {
      yield* _deleteBank(event);
    } else if (event is DeleteWallet) {
      yield* _deleteWallet(event);
    } else if (event is UpdateDefaultBank) {
      yield* _updateDefaultBank(event);
    } else if (event is UpdateDefaultWallet) {
      yield* _updateDefaultWallet(event);
    } else if (event is ClearProof) {
      yield* _clearProof(event);
    }
  }

  Stream<ProofState> _clearProof(ClearProof event) async* {
    yield ProvingInfo(path: '', proofType: null);
    repo.idProof = Proof();
    bool provedAddress = repo.addressProof != null &&
        repo.addressProof.detail != null &&
        repo.addressProof.detail.length > 0;
    yield AllProved(provedAddress: provedAddress, provedIdentity: false);
  }

  Stream<ProofState> _proving(Proving event) async* {
    bool provedIdentity = repo.idProof != null &&
        repo.idProof.detail != null &&
        repo.idProof.detail.length > 0;
    bool provedAddress = repo.addressProof != null &&
        repo.addressProof.detail != null &&
        repo.addressProof.detail.length > 0;

    yield AllProved(
        provedIdentity: provedIdentity, provedAddress: provedAddress);
    yield ProvingInfo(path: event.path, proofType: event.proofType);
  }

  Stream<ProofState> _proved(Proved event) async* {
    if (event.proofType == ProofType.identity) {
      repo.idProof = Proof(detail: event.idNumber, image: event.idImage);
    } else if (event.proofType == ProofType.address) {
      repo.addressProof =
          Proof(detail: event.address, image: event.addressImage);
    }

    bool provedIdentity = repo.idProof != null &&
        repo.idProof.detail != null &&
        repo.idProof.detail.length > 0;
    bool provedAddress = repo.addressProof != null &&
        repo.addressProof.detail != null &&
        repo.addressProof.detail.length > 0;

    yield AllProved(
        provedAddress: provedAddress, provedIdentity: provedIdentity);
  }

  Stream<ProofState> _updateProof(UpdateProof event) async* {
    yield Updating(
        provedIdentity: repo.idProof.image != null && repo.idProof.image != '',
        provedAddress:
            repo.addressProof.image != null && repo.addressProof.image != '');
    try {
      List responses = await Future.wait([
        uploadFile(repo.idProof.image, 'idProof'),
        uploadFile(repo.addressProof.image, 'addressProof'),
      ]);

      Proof idProof = Proof(detail: repo.idProof.detail, image: responses[0]);
      Proof addressProof =
          Proof(detail: repo.addressProof.detail, image: responses[1]);
      Profile profile = Profile(
        idProof: idProof,
        addressProof: addressProof,
      );

      bool result = await updateProfile(event.userId, profile);
      if (result) {
        // if true means success
        yield AllProved(
            provedIdentity: responses[0] != '',
            provedAddress: responses[1] != '');

        event.callback();
      } else {
        yield UpdateError('Failed to update profile, please try again later');
      }
    } catch (e, s) {
      print(e);
      print(s);
      if (e is FormatException) {
        yield UpdateError(e.message);
      } else if (e is String) {
        yield UpdateError(e);
      } else {
        yield UpdateError('Unknown error, please try again later');
      }
    }
  }

  Stream<ProofState> _getProfile(GetProfile event) async* {
    yield Updating();

    if (event.userId != null && event.userId != '')
      repo = await getProfile(event.userId);

    yield AllProved(
        provedAddress: repo.addressProof != null &&
            repo.addressProof.image != null &&
            repo.addressProof.image != '',
        provedIdentity: repo.idProof != null &&
            repo.idProof.image != null &&
            repo.idProof.image != '');
  }

  Stream<ProofState> _clear(ClearProfile event) async* {
    yield Updating();

    repo = Profile(
        idProof: Proof(), addressProof: Proof(), wallets: [], banks: []);

    yield AllProved();
  }

  Stream<ProofState> _updateBanks(UpdateBanks event) async* {
    yield Updating();

    try {
      repo.banks[event.index] = event.data;
      bool result = await updateBanks(event.userId, repo.banks);
      if (result) {
        // if true means success
        yield AllProved(
            provedAddress: repo.addressProof != null &&
                repo.addressProof.image != null &&
                repo.addressProof.image != '',
            provedIdentity: repo.idProof != null &&
                repo.idProof.image != null &&
                repo.idProof.image != '');

        event.callback();
      } else {
        yield UpdateError(
            'Failed to update bank detail, please try again later');
      }
    } catch (e, s) {
      print(e);
      print(s);
      if (e is FormatException) {
        yield UpdateError(e.message);
      } else if (e is String) {
        yield UpdateError(e);
      } else {
        yield UpdateError('Unknown error, please try again later');
      }
    }
  }

  Stream<ProofState> _updateWallets(UpdateWallets event) async* {
    yield Updating();

    try {
      repo.wallets[event.index] = event.data;
      bool result = await updateWallets(event.userId, repo.wallets);
      if (result) {
        // if true means success
        yield AllProved(
            provedAddress: repo.addressProof != null &&
                repo.addressProof.image != null &&
                repo.addressProof.image != '',
            provedIdentity: repo.idProof != null &&
                repo.idProof.image != null &&
                repo.idProof.image != '');

        event.callback();
      } else {
        yield UpdateError(
            'Failed to update wallet detail, please try again later');
      }
    } catch (e, s) {
      print(e);
      print(s);
      if (e is FormatException) {
        yield UpdateError(e.message);
      } else if (e is String) {
        yield UpdateError(e);
      } else {
        yield UpdateError('Unknown error, please try again later');
      }
    }
  }

  Stream<ProofState> _addBank(AddBank event) async* {
    yield Updating();

    try {
      String response = await uploadFile(event.data.image, 'bankProof');
      event.data.image = response;

      repo.banks.add(event.data);
      bool result = await updateBanks(event.userId, repo.banks);
      if (result) {
        // if true means success
        yield AllProved(
            provedAddress: repo.addressProof != null &&
                repo.addressProof.image != null &&
                repo.addressProof.image != '',
            provedIdentity: repo.idProof != null &&
                repo.idProof.image != null &&
                repo.idProof.image != '');

        event.callback();
      } else {
        yield UpdateError(
            'Failed to update bank detail, please try again later');
      }
    } catch (e, s) {
      print(e);
      print(s);
      if (e is FormatException) {
        yield UpdateError(e.message);
      } else if (e is String) {
        yield UpdateError(e);
      } else {
        yield UpdateError('Unknown error, please try again later');
      }
    }
  }

  Stream<ProofState> _addWallet(AddWallet event) async* {
    yield Updating();

    try {
      repo.wallets.add(event.data);
      bool result = await updateWallets(event.userId, repo.wallets);
      if (result) {
        // if true means success
        yield AllProved(
            provedAddress: repo.addressProof != null &&
                repo.addressProof.image != null &&
                repo.addressProof.image != '',
            provedIdentity: repo.idProof != null &&
                repo.idProof.image != null &&
                repo.idProof.image != '');

        event.callback();
      } else {
        yield UpdateError(
            'Failed to update wallet detail, please try again later');
      }
    } catch (e, s) {
      print(e);
      print(s);
      if (e is FormatException) {
        yield UpdateError(e.message);
      } else if (e is String) {
        yield UpdateError(e);
      } else {
        yield UpdateError('Unknown error, please try again later');
      }
    }
  }

  Stream<ProofState> _deleteBank(DeleteBank event) async* {
    yield Updating();

    try {
      repo.banks.removeAt(event.index);
      bool result = await updateBanks(event.userId, repo.banks);
      if (result) {
        // if true means success
        yield AllProved(
            provedAddress: repo.addressProof != null &&
                repo.addressProof.image != null &&
                repo.addressProof.image != '',
            provedIdentity: repo.idProof != null &&
                repo.idProof.image != null &&
                repo.idProof.image != '');

        event.callback();
      } else {
        yield UpdateError(
            'Failed to update bank detail, please try again later');
      }
    } catch (e, s) {
      print(e);
      print(s);
      if (e is FormatException) {
        yield UpdateError(e.message);
      } else if (e is String) {
        yield UpdateError(e);
      } else {
        yield UpdateError('Unknown error, please try again later');
      }
    }
  }

  Stream<ProofState> _deleteWallet(DeleteWallet event) async* {
    yield Updating();

    try {
      repo.wallets.removeAt(event.index);
      bool result = await updateWallets(event.userId, repo.wallets);
      if (result) {
        // if true means success
        yield AllProved(
            provedAddress: repo.addressProof != null &&
                repo.addressProof.image != null &&
                repo.addressProof.image != '',
            provedIdentity: repo.idProof != null &&
                repo.idProof.image != null &&
                repo.idProof.image != '');

        event.callback();
      } else {
        yield UpdateError(
            'Failed to update wallet detail, please try again later');
      }
    } catch (e, s) {
      print(e);
      print(s);
      if (e is FormatException) {
        yield UpdateError(e.message);
      } else if (e is String) {
        yield UpdateError(e);
      } else {
        yield UpdateError('Unknown error, please try again later');
      }
    }
  }

  Stream<ProofState> _updateDefaultBank(UpdateDefaultBank event) async* {
    yield Updating();

    try {
      repo.defaultBanks = event.index;
      bool result = await updateDefaultBanks(event.userId, event.index);
      if (result) {
        // if true means success
        yield AllProved(
            provedAddress: repo.addressProof != null &&
                repo.addressProof.image != null &&
                repo.addressProof.image != '',
            provedIdentity: repo.idProof != null &&
                repo.idProof.image != null &&
                repo.idProof.image != '');

        event.callback();
      } else {
        yield UpdateError(
            'Failed to update default bank recipient, please try again later');
      }
    } catch (e, s) {
      print(e);
      print(s);
      if (e is FormatException) {
        yield UpdateError(e.message);
      } else if (e is String) {
        yield UpdateError(e);
      } else {
        yield UpdateError('Unknown error, please try again later');
      }
    }
  }

  Stream<ProofState> _updateDefaultWallet(UpdateDefaultWallet event) async* {
    yield Updating();

    try {
      repo.defaultWallets = event.index;
      bool result = await updateDefaultWallets(event.userId, event.index);
      if (result) {
        // if true means success
        yield AllProved(
            provedAddress: repo.addressProof != null &&
                repo.addressProof.image != null &&
                repo.addressProof.image != '',
            provedIdentity: repo.idProof != null &&
                repo.idProof.image != null &&
                repo.idProof.image != '');

        event.callback();
      } else {
        yield UpdateError(
            'Failed to update default wallet, please try again later');
      }
    } catch (e, s) {
      print(e);
      print(s);
      if (e is FormatException) {
        yield UpdateError(e.message);
      } else if (e is String) {
        yield UpdateError(e);
      } else {
        yield UpdateError('Unknown error, please try again later');
      }
    }
  }
}
