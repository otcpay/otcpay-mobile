import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:otcpay/model/Profile.dart';

enum ProofType { identity, address, bank }

abstract class ProofStateEvent extends Equatable {
  const ProofStateEvent();

  @override
  List<Object> get props => [];
}

class ClearProof extends ProofStateEvent {}

class Proved extends ProofStateEvent {
  final ProofType proofType;

  final String idNumber;
  final String idImage;
  final String address;
  final String addressImage;
  final String bankNumber;
  final String bankImage;
  final String bankHolder;
  final String bankName;
  final String bankCode;

  const Proved({
    @required this.proofType,
    this.idNumber,
    this.address,
    this.idImage,
    this.addressImage,
    this.bankCode,
    this.bankHolder,
    this.bankImage,
    this.bankName,
    this.bankNumber,
  });
}

class Proving extends ProofStateEvent {
  final String path;
  final ProofType proofType;

  const Proving({@required this.path, @required this.proofType});
}

class UpdateProof extends ProofStateEvent {
  final String userId;
  final void Function() callback;

  const UpdateProof({@required this.userId, @required this.callback});
}

class UpdateWallets extends ProofStateEvent {
  final String userId;
  final int index;
  final Wallet data;
  final void Function() callback;

  const UpdateWallets(
      {@required this.userId,
      @required this.index,
      @required this.data,
      @required this.callback});
}

class UpdateBanks extends ProofStateEvent {
  final String userId;
  final int index;
  final BankProof data;
  final void Function() callback;

  const UpdateBanks(
      {@required this.userId,
      @required this.index,
      @required this.data,
      @required this.callback});
}

class AddWallet extends ProofStateEvent {
  final String userId;
  final Wallet data;
  final void Function() callback;

  const AddWallet(
      {@required this.userId, @required this.data, @required this.callback});
}

class AddBank extends ProofStateEvent {
  final String userId;
  final BankProof data;
  final void Function() callback;

  const AddBank(
      {@required this.userId, @required this.data, @required this.callback});
}

class DeleteWallet extends ProofStateEvent {
  final String userId;
  final int index;
  final void Function() callback;

  const DeleteWallet(
      {@required this.userId, @required this.index, @required this.callback});
}

class DeleteBank extends ProofStateEvent {
  final String userId;
  final int index;
  final void Function() callback;

  const DeleteBank(
      {@required this.userId, @required this.index, @required this.callback});
}

class GetProfile extends ProofStateEvent {
  final String userId;

  const GetProfile(this.userId);
}

class ClearProfile extends ProofStateEvent {}

class UpdateDefaultBank extends ProofStateEvent {
  final String userId;
  final int index;
  final void Function() callback;

  const UpdateDefaultBank(
      {@required this.userId, @required this.index, @required this.callback});
}

class UpdateDefaultWallet extends ProofStateEvent {
  final String userId;
  final int index;
  final void Function() callback;

  const UpdateDefaultWallet(
      {@required this.userId, @required this.index, @required this.callback});
}
