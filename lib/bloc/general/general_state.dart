import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:otcpay/model/InvoicePayment.dart';

abstract class GeneralState extends Equatable {
  const GeneralState();

  @override
  List<Object> get props => [];
}

class Navigating extends GeneralState {}

class Proceeding extends GeneralState {}

class Proceeded extends GeneralState {}

class SubmittingPayment extends GeneralState {}

class PaymentSuccess extends GeneralState {
  final InvoicePayment newPayment;

  const PaymentSuccess(this.newPayment);
}

class OrderSuccess extends GeneralState {}

class SubmittingOrder extends GeneralState {}

class NavigatedTabBar extends GeneralState {
  final int index;
  final Map payload;

  const NavigatedTabBar({@required this.index, this.payload});
}

class ProfileUpdating extends GeneralState {}

class ProfileUpdated extends GeneralState {}

class Error extends GeneralState {
  final String message;

  const Error(this.message);
}

class FetchingPayment extends GeneralState {}

class FetchedPayment extends GeneralState {
  final bool hasMore;

  const FetchedPayment({this.hasMore});
}

class Comparing extends GeneralState {}

class Compared extends GeneralState {}

class LoadingNotification extends GeneralState {}

class NotificationLoaded extends GeneralState {}

class FakeLoading extends GeneralState {}
