import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:otcpay/model/AdRequest.dart';
import 'package:otcpay/model/InvoicePayment.dart';
import 'package:otcpay/model/Profile.dart';

abstract class GeneralStateEvent extends Equatable {
  const GeneralStateEvent();

  @override
  List<Object> get props => [];
}

class NavigateTab extends GeneralStateEvent {
  final int index;
  final Map payload;

  const NavigateTab({@required this.index, this.payload});

  @override
  String toString() => 'NavigateTab { index: $index }';
}

class SubmitOrder extends GeneralStateEvent {
  final AdRequest request;

  const SubmitOrder({@required this.request});
}

class UpdateProfile extends GeneralStateEvent {
  final String userId;
  final Profile profile;

  const UpdateProfile({this.userId, this.profile});
}

class GetMapping extends GeneralStateEvent {}

class GetComparisonResult extends GeneralStateEvent {
  final String fromCurrency;
  final String toCurrency;

  const GetComparisonResult(
      {@required this.fromCurrency, @required this.toCurrency});
}

class ProceedToNext extends GeneralStateEvent {
  final int step;
  final InvoicePayment payment;

  const ProceedToNext({@required this.step, @required this.payment});
}

class SubmitPayment extends GeneralStateEvent {
  final String userId;
  final String name;
  final String settlementMethod;
  final String referenceRate;
  final String amountNeeded;

  const SubmitPayment(
      {@required this.userId,
      @required this.name,
      @required this.settlementMethod,
      this.referenceRate,
      this.amountNeeded});
}

class FetchPaymentOrder extends GeneralStateEvent {
  final String userId;
  final int page;
  final bool refresh;
  final bool isLoadMore;

  const FetchPaymentOrder(
      {@required this.userId,
      this.refresh = false,
      this.page = 1,
      this.isLoadMore = false});
}

class FetchOTCOrder extends GeneralStateEvent {
  final String userId;
  final bool refresh;

  const FetchOTCOrder({@required this.userId, this.refresh});
}

class FetchAdOrder extends GeneralStateEvent {
  final String userId;
  final int page;
  final bool refresh;
  final bool isLoadMore;

  const FetchAdOrder(
      {@required this.userId,
      this.refresh,
      this.page = 1,
      this.isLoadMore = false});
}

class FetchAd extends GeneralStateEvent {
  final String userId;
  final int page;
  final bool refresh;
  final bool isLoadMore;

  const FetchAd(
      {@required this.userId,
      this.refresh,
      this.page = 1,
      this.isLoadMore = false});
}

class UpdateAd extends GeneralStateEvent {
  final String id;
  final Map<String, dynamic> data;

  const UpdateAd({@required this.id, @required this.data});
}

class DeleteAd extends GeneralStateEvent {
  final String id;

  const DeleteAd(this.id);
}

class FetchNotification extends GeneralStateEvent {
  final String id;

  const FetchNotification(this.id);
}

class ReadAllNotification extends GeneralStateEvent {
  final String id;

  const ReadAllNotification(this.id);
}

class ClearOrderRecord extends GeneralStateEvent {}
