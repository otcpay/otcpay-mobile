import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:otcpay/api/advertisement.dart';
import 'package:otcpay/api/buysell.dart';
import 'package:otcpay/api/file.dart';
import 'package:otcpay/api/rate.dart';
import 'package:otcpay/api/settlement.dart';
import 'package:otcpay/api/system.dart';
import 'package:otcpay/api/user.dart';
import 'package:otcpay/model/AdRequest.dart';
import 'package:otcpay/model/Advertisement.dart';
import 'package:otcpay/model/ComparisonResult.dart';
import 'package:otcpay/model/InvoicePayment.dart';
import 'package:otcpay/model/Notification.dart';
import 'package:otcpay/model/rate/CryptoRate.dart';
import 'package:otcpay/model/rate/OtcRate.dart';
import 'package:otcpay/util/ParamUtil.dart';
import 'package:otcpay/util/TimeUtil.dart';

import 'general.dart';

class GeneralBloc extends Bloc<GeneralStateEvent, GeneralState> {
  int tabIndex = 0;
  int paymentStep = 0;

  String refTime = getCrawlTime();

  Map<String, dynamic> mapping = {};
  List<InvoicePayment> payments = [];
  List<AdRequest> requests = [];
  List<Advertisement> ads = [];
  List<Notification> notifications = [];

  List<ComparisonResult> defaultFiatRates = [];
  List<OtcRate> otcRates = [];
  List<CryptoRate> cryptoRates = [
    new CryptoRate(open: 1, close: 1),
    new CryptoRate(open: 1, close: 1),
    new CryptoRate(open: 1, close: 1),
    new CryptoRate(open: 1, close: 1)
  ];

  ComparisonResult result = ComparisonResult(rates: []);

  InvoicePayment paymentDetail = InvoicePayment();

  GeneralBloc() : super(NavigatedTabBar(index: 0));

  @override
  Stream<GeneralState> mapEventToState(GeneralStateEvent event) async* {
    if (event is NavigateTab) {
      yield* _navigateTab(event);
    } else if (event is SubmitOrder) {
      yield* _submitOrder(event);
    } else if (event is UpdateProfile) {
      yield* _updateProfile(event);
    } else if (event is GetMapping) {
      yield* _getMapping(event);
    } else if (event is ProceedToNext) {
      yield* _proceed(event);
    } else if (event is SubmitPayment) {
      yield* _submitPayment(event);
    } else if (event is FetchPaymentOrder) {
      yield* _getUserPaymentOrder(event);
    } else if (event is FetchAdOrder) {
      yield* _getUserAdOrder(event);
    } else if (event is FetchAd) {
      yield* _getUserAd(event);
    } else if (event is GetComparisonResult) {
      yield* _getComparisonResult(event);
    } else if (event is UpdateAd) {
      yield* _updateAd(event);
    } else if (event is DeleteAd) {
      yield* _deleteAd(event);
    } else if (event is FetchNotification) {
      yield* _fetchNotifications(event);
    } else if (event is ReadAllNotification) {
      yield* _readAllNotifications(event);
    } else if (event is ClearOrderRecord) {
      yield* _clearOrderRecord(event);
    }
  }

  Stream<GeneralState> _navigateTab(NavigateTab event) async* {
    yield Navigating();

    tabIndex = event.index;
    if (tabIndex == 1) {
      paymentStep = 0;
      result = ComparisonResult(rates: []);
    }

    yield NavigatedTabBar(index: event.index, payload: event.payload);
  }

  Stream<GeneralState> _proceed(ProceedToNext event) async* {
    yield Proceeding();

    InvoicePayment newPayment = InvoicePayment.fromJson(paymentDetail.toJson());

    paymentStep = event.step;
    if (paymentStep == 1) {
      newPayment.sendCurrency = event.payment.sendCurrency;
      newPayment.sendFrom = event.payment.sendFrom;
      newPayment.sendTo = event.payment.sendTo;
      newPayment.receivedCurrency = event.payment.receivedCurrency;
      newPayment.receivedAmount = event.payment.receivedAmount;
      newPayment.referenceRate = event.payment.referenceRate;
      newPayment.type = event.payment.type;
    } else if (paymentStep == 2) {
      newPayment.amountNeeded = event.payment.amountNeeded;
      newPayment.amountSaved = event.payment.amountSaved;
    } else if (paymentStep == 3) {
      newPayment.invoiceImage = event.payment.invoiceImage;
    } else if (paymentStep == 4) {
      newPayment.receiveBank = event.payment.receiveBank;
      newPayment.receiveName = event.payment.receiveName;
      newPayment.receiveBankNumber = event.payment.receiveBankNumber;
      newPayment.receiveBankAddress = event.payment.receiveBankAddress;
      newPayment.bankImage = event.payment.bankImage;
      newPayment.contact = event.payment.contact;
      newPayment.contactMethod = event.payment.contactMethod;
    }

    paymentDetail = newPayment;

    yield Proceeded();
  }

  Stream<GeneralState> _submitPayment(SubmitPayment event) async* {
    yield SubmittingPayment();

    paymentDetail.name = event.name;
    paymentDetail.userId = event.userId;
    paymentDetail.settlementMethod = event.settlementMethod;

    if (paymentDetail.settlementMethod == 'USDT') {
      paymentDetail.amountNeeded = event.amountNeeded;
      paymentDetail.amountSaved = '0';
      paymentDetail.referenceRate = event.referenceRate;
    }

    try {
      List responses = await Future.wait([
        uploadFile(paymentDetail.invoiceImage, 'invoice'),
        uploadFile(paymentDetail.bankImage, 'bank'),
      ]);
      paymentDetail.invoiceImage = responses[0];
      paymentDetail.bankImage = responses[1];

      InvoicePayment newPayment = await createInvoicePayment(paymentDetail);

      paymentStep = 0;
      paymentDetail = InvoicePayment();

      yield PaymentSuccess(newPayment);
    } catch (e) {
      print(e);
      if (e is FormatException) {
        yield Error(e.message);
      } else if (e is String) {
        yield Error(e);
      } else {
        yield Error('Unknown error, please try again later');
      }
    }
  }

  Stream<GeneralState> _submitOrder(SubmitOrder event) async* {
    yield SubmittingOrder();

    try {
      bool result = await createRequest(event.request);
      if (result) {
        yield OrderSuccess();
      } else {
        yield Error('Failed to update profile, please try again later');
      }
    } catch (e) {
      print(e);
      if (e is FormatException) {
        yield Error(e.message);
      } else if (e is String) {
        yield Error(e);
      } else {
        yield Error('Unknown error, please try again later');
      }
    }
  }

  Stream<GeneralState> _updateProfile(UpdateProfile event) async* {
    yield ProfileUpdating();

    try {
      bool result = await updateUser(event.userId, event.profile);
      if (result) {
        yield ProfileUpdated();
      } else {
        yield Error('Failed to update profile, please try again later');
      }
    } catch (e) {
      print(e);
      if (e is FormatException) {
        yield Error(e.message);
      } else if (e is String) {
        yield Error(e);
      } else {
        yield Error('Unknown error, please try again later');
      }
    }
  }

  Stream<GeneralState> _getMapping(GetMapping event) async* {
    List<dynamic> futures = await Future.wait([
      getMapping(),
      getDefaultDailyRate(),
      getDefaultCryptoRate(),
      getOtcRate(),
      getStatisticTime()
    ]);

    mapping = (futures[0] as Map<String, dynamic>);
    defaultFiatRates = (futures[1] as List<ComparisonResult>);
    cryptoRates = (futures[2] as List<CryptoRate>);
    otcRates = (futures[3] as List<OtcRate>);
    String lastStatisticUpdateTime = futures[4];
    if (lastStatisticUpdateTime.compareTo(refTime) == 1) {
      refTime = lastStatisticUpdateTime;
    }

    yield NavigatedTabBar(index: 0);
  }

  Stream<GeneralState> _getUserPaymentOrder(FetchPaymentOrder event) async* {
    if (payments.isEmpty || event.refresh || event.isLoadMore) {
      if (event.refresh) {
        yield FakeLoading();
      } else {
        yield FetchingPayment();
      }

      List<InvoicePayment> invoicePaymentList =
          await getUserInvoicePayment(event.userId, event.page);
      if (event.isLoadMore) {
        payments.addAll(invoicePaymentList);
      } else {
        payments = invoicePaymentList;
      }

      try {
        payments.sort((a, b) => b.updatedDate.compareTo(a.updatedDate));
      } catch (e) {}
      yield FetchedPayment(hasMore: invoicePaymentList.length > 0);
    }
  }

  Stream<GeneralState> _getUserAdOrder(FetchAdOrder event) async* {
    if (requests.isEmpty || event.refresh || event.isLoadMore) {
      if (event.refresh) {
        yield FakeLoading();
      } else {
        yield FetchingPayment();
      }

      List<AdRequest> requestList =
          await getUserRequest(event.userId, event.page);
      if (event.isLoadMore) {
        requests.addAll(requestList);
      } else {
        requests = requestList;
      }

      try {
        requestList.sort((a, b) => b.updatedDate.compareTo(a.updatedDate));
      } catch (e) {}
      yield FetchedPayment(hasMore: requestList.length > 0);
    }
  }

  Stream<GeneralState> _getUserAd(FetchAd event) async* {
    if (ads.isEmpty || event.refresh || event.isLoadMore) {
      if (event.refresh) {
        yield FakeLoading();
      } else {
        yield FetchingPayment();
      }

      List<Advertisement> adList = await getUserAd(event.userId, event.page);
      if (event.isLoadMore) {
        ads.addAll(adList);
      } else {
        ads = adList;
      }

      try {
        ads.sort((a, b) => b.updatedDate.compareTo(a.updatedDate));
      } catch (e) {}
      yield FetchedPayment(hasMore: adList.length > 0);
    }
  }

  Stream<GeneralState> _getComparisonResult(GetComparisonResult event) async* {
    yield Comparing();

    result = await getDailyRate(
        fromCurrency: event.fromCurrency, toCurrency: event.toCurrency);

    yield Compared();
  }

  Stream<GeneralState> _updateAd(UpdateAd event) async* {
    yield FetchingPayment();

    try {
      bool result = await updateAd(event.id, event.data);
      if (result) {
        int index = ads.indexWhere((e) => e.id == event.id);
        Map<String, dynamic> data = ads[index].toJson();
        Map<String, dynamic> merge =
            mergeMap(data, event.data, acceptNull: true);
        ads[index] = Advertisement.fromJson(merge);
        yield FetchedPayment(hasMore: true);
      } else {
        yield Error('Failed to Update Ad, please try again later');
      }
    } catch (e, s) {
      print(e);
      print(s);
      if (e is FormatException) {
        yield Error(e.message);
      } else if (e is String) {
        yield Error(e);
      } else {
        yield Error('Unknown error, please try again later');
      }
    }
  }

  Stream<GeneralState> _deleteAd(DeleteAd event) async* {
    yield FetchingPayment();

    try {
      bool result = await deleteAd(event.id);
      if (result) {
        ads.removeWhere((e) => e.id == event.id);
        yield FetchedPayment();
      } else {
        yield Error('Failed to delete Ad, please try again later');
      }
    } catch (e) {
      print(e);
      if (e is FormatException) {
        yield Error(e.message);
      } else if (e is String) {
        yield Error(e);
      } else {
        yield Error('Unknown error, please try again later');
      }
    }
  }

  Stream<GeneralState> _fetchNotifications(FetchNotification event) async* {
    yield LoadingNotification();

    try {
      notifications = await fetchUserNotification(event.id);
      yield NotificationLoaded();
    } catch (e) {
      print(e);
      if (e is FormatException) {
        yield Error(e.message);
      } else if (e is String) {
        yield Error(e);
      } else {
        yield Error('Unknown error, please try again later');
      }
    }
  }

  Stream<GeneralState> _readAllNotifications(ReadAllNotification event) async* {
    yield LoadingNotification();

    try {
      notifications.forEach((n) => n.read = true);
      bool result = await readAllNotification(event.id);
      if (result) {
        yield NotificationLoaded();
      } else {
        yield Error(
            'Failed to update notification status, please try again later');
      }
    } catch (e) {
      print(e);
      if (e is FormatException) {
        yield Error(e.message);
      } else if (e is String) {
        yield Error(e);
      } else {
        yield Error('Unknown error, please try again later');
      }
    }
  }

  Stream<GeneralState> _clearOrderRecord(ClearOrderRecord event) async* {
    yield FakeLoading();

    payments = [];
    requests = [];
    ads = [];
    notifications = [];

    yield NavigatedTabBar(index: 0);
  }
}
