import 'package:date_time_format/date_time_format.dart';

String getRelativeTime(String time) {
  String relativeTime = DateTimeFormat.relative(
      DateTime.fromMillisecondsSinceEpoch(int.parse(time)),
      formatAfter: Duration(days: 1),
      format: r'G:i, j M Y');

  if (relativeTime == null || relativeTime.isEmpty) {
    return 'Just now';
  }

  return relativeTime;
}

String getCrawlTime() {
  DateTime time = DateTime.now();
  int newDay = time.day;
  if (time.hour <= 17) {
    newDay -= 1;
  }
  DateTime newTime = new DateTime(time.year, time.month, newDay, 17, 52,
      time.second, time.millisecond, time.millisecond);
  return newTime.millisecondsSinceEpoch.toString();
}
