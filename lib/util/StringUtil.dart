import 'package:easy_localization/easy_localization.dart';
import 'package:otcpay/component/ListItem.dart';
import 'package:otcpay/generated/locale_keys.g.dart';

String trimHypen(String value) {
  if (value != null) {
    return value.replaceAll('-', '');
  }

  return '';
}

String getIconName(String value) {
  if (value != null) {
    return value.replaceAll(' ', '_');
  }

  return '';
}

String getBuySellRemitButtonTitle(RATE_MODE mode) {
  if (mode == RATE_MODE.BUY) return tr(LocaleKeys.common_buy);
  if (mode == RATE_MODE.SELL) return tr(LocaleKeys.common_sell);

  return tr(LocaleKeys.hotPage_remit);
}
