bool isValidEmail(String input) {
  return RegExp(
          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
      .hasMatch(input);
}

bool isValidPhoneNum(String input) {
  return RegExp(r'^[0-9]*$').hasMatch(input);
}

bool isValidAccount(String input) {
  return isValidEmail(input) || isValidPhoneNum(input);
}

_copyValues<K, V>(
    Map<K, V> from, Map<K, V> to, bool recursive, bool acceptNull) {
  for (var key in from.keys) {
    if (from[key] is Map<K, V> && recursive) {
      if (!(to[key] is Map<K, V>)) {
        to[key] = <K, V>{} as V;
      }
      _copyValues(from[key] as Map, to[key] as Map, recursive, acceptNull);
    } else {
      if (from[key] != null || acceptNull) to[key] = from[key];
    }
  }
}

bool isAsciiChar(final validCharacters, String input) {
  return validCharacters.hasMatch(input);
}

Map<K, V> mergeMap<K, V>(Map<K, V> map1, Map<K, V> map2,
    {bool recursive: true, bool acceptNull: false}) {
  List maps = [map1, map2];
  Map<K, V> result = <K, V>{};
  maps.forEach((map) {
    if (map != null) _copyValues(map, result, recursive, acceptNull);
  });
  return result;
}
